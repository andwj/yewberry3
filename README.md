
Yewberry Programming Language
=============================

by Andrew Apted, 2020.


About
-----

Yewberry is a statically-typed programming language which is
currently under development.  It has a syntax like Scheme or
LISP, but the semantics are very different.


Legalese
--------

See the [LICENSE.md](LICENSE.md) file for the terms.

Yewberry comes with NO WARRANTY of any kind, express or implied.
Please read the license for full details.

