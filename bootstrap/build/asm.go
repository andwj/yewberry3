// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
import "strings"
import "os"
import "os/exec"

func Assemble() error {
	Stage.input = Stage.output
	Stage.output = ""
	Stage.failures = 0

	var err error

	/*
	f, err := os.Open(Stage.input)
	if err != nil {
		return err
	}
	defer f.Close()
	*/

	lex := NewLexer(strings.NewReader(Stage.input))

	for {
		t := lex.Scan()
		if t.Kind == TOK_EOF {
			break
		}

		if t.Kind == TOK_ERROR {
			return fmt.Errorf("syntax error in LST file: %s", t.Str)
		} else if t.Kind != TOK_Expr || len(t.Children) < 1 {
			return fmt.Errorf("bad syntax in LST file")
		} else {
			head := t.Children[0]

			if head.Match("assemble") {
				err = AssembleFile(t)
				if err != nil {
					return err
				}
			} else {
				// simply copy input token to output
				OutputToken(t)
				Output("\n")
			}
		}
	}

	if Stage.failures > 0 {
		return fmt.Errorf("%d files failed to assemble", Stage.failures)
	}

	_, err = DirWriteFile("STAGE5_asm", "", "", Stage.output)
	return err
}

func AssembleFile(t *Token) error {
	if len(t.Children) < 2 ||
		t.Children[1].Kind != TOK_String {

		return fmt.Errorf("bad syntax in assemble statement")
	}

	in_name := t.Children[1].Str
	out_name := ""

	if len(t.Children) >= 3 {
		out_name = t.Children[2].Str
	} else {
		out_name = FileRemoveExtension(in_name) + ".o"
	}

	if in_name == "" || out_name == "" {
		return fmt.Errorf("bad filename in assemble statement")
	}

	// WISH: ability to select a different assembler
	tool := "as"

	// TODO want to be able to provide options to the assembler
	//      [ but those options may depend on which one it is.... ]

	cmd := exec.Command(tool, in_name, "-o", out_name)

	// drop normal output on stdout, prevent clobbering our messages
	cmd.Stdout = nil

	// collect any error messages from the assembler
	err_box := new(strings.Builder)
	cmd.Stderr = err_box

	err := cmd.Start()
	if err != nil {
		return fmt.Errorf("could not run %s: %s", tool, err.Error())
	}

	err = cmd.Wait()
	if err != nil {
		// TODO do something better with errors
		fmt.Fprintf(os.Stderr, "ERROR FROM %s: %s\n", tool, err.Error())
		fmt.Fprintf(os.Stderr, "%s\n", err_box.String())

		// increase error count, don't fail yet
		Stage.failures += 1
		return Ok
	}

	// assembly was successful, add object file to output list
	OutputObjectFile(out_name)

	return Ok
}

func OutputObjectFile(obj_name string) {
	Output("(link %q)\n", obj_name)
}
