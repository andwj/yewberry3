// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
import "strings"
import "os"
import "os/exec"
import "path/filepath"

func CompileC() error {
	Stage.input = Stage.output
	Stage.output = ""
	Stage.failures = 0

	var err error

	/*
	f, err := os.Open(Stage.input)
	if err != nil {
		return err
	}
	defer f.Close() */

	lex := NewLexer(strings.NewReader(Stage.input))

	for {
		t := lex.Scan()
		if t.Kind == TOK_EOF {
			break
		}

		if t.Kind == TOK_ERROR {
			return fmt.Errorf("syntax error in LST file: %s", t.Str)
		} else if t.Kind != TOK_Expr || len(t.Children) < 1 {
			return fmt.Errorf("bad syntax in LST file")
		} else {
			head := t.Children[0]

			if head.Match("cc") {
				err = CompileCFile(t)
				if err != nil {
					return err
				}
			} else {
				// simply copy input token to output
				OutputToken(t)
				Output("\n")
			}
		}
	}

	if Stage.failures > 0 {
		return fmt.Errorf("%d C files failed to compile", Stage.failures)
	}

	_, err = DirWriteFile("STAGE4_cc", "", "", Stage.output)
	return err
}

func CompileCFile(t *Token) error {
	if len(t.Children) < 2 ||
		t.Children[1].Kind != TOK_String {

		return fmt.Errorf("bad syntax in cc statement")
	}

	in_name  := t.Children[1].Str
	out_name := ""

	if len(t.Children) >= 3 {
		out_name = t.Children[2].Str
	} else {
		out_name = FileRemoveExtension(in_name) + ".o"
	}

	if in_name == "" || out_name == "" {
		return fmt.Errorf("bad filename in cc statement")
	}

	// TODO ability to select compiler (may need different arrangement of args)

	tool := "gcc"
	in_arg := "-c"
	out_arg := "-o"

	// TODO also want to be able to provide options to compiler
	//      [ and those options may depend on which compiler it is.... ]

	// TODO allow using glibc, also support non-Linux OS
	spec_arg := "-specs"
	spec_file := "/usr/local/musl/lib/musl-gcc.specs"

	c_standard := "-std=c99"

	rt_inc := "-I" + filepath.Join(YewberryRoot, "runtime", "c")

	cmd := exec.Command(tool, c_standard, rt_inc, spec_arg, spec_file, in_arg, in_name, out_arg, out_name)

	// drop normal output on stdout, prevent clobbering our messages
	cmd.Stdout = nil

	// collect any error messages from the C compiler
	err_box := new(strings.Builder)
	cmd.Stderr = err_box

	err := cmd.Start()
	if err != nil {
		// this is a fatal error (instead of a soft error) since
		// if the C compiler cannot be found then no other files
		// will be able to be processed.
		return fmt.Errorf("could not run %s: %s", tool, err.Error())
	}

	err = cmd.Wait()
	if err != nil {
		// TODO do something better with errors
		fmt.Fprintf(os.Stderr, "ERROR FROM %s: %s\n", tool, err.Error())
		fmt.Fprintf(os.Stderr, "%s\n", err_box.String())

		// increase error count, don't fail yet
		Stage.failures += 1
		return Ok
	}

	// compilation was successful, add object file to output
	OutputObjectFile(out_name)
	return Ok
}

func OutputAssemblyFile(asm_name string) error {
	// TODO want some module name here, for clearer filenames
	mod := ""

	// determine name of object file
	f, err := DirTempFile(asm_name, mod, "o")

	if err != nil {
		return err
	}

	obj_name := f.Name()
	f.Close()

	Output("(assemble %q %q)\n", asm_name, obj_name)
	return Ok
}
