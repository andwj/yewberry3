// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

// import "fmt"
import "strings"
import "os"
import "io/ioutil"
import "path/filepath"

var temp_dir string

func DirCreate(source_name string) error {
	// remove directory component
	source_name = filepath.Base(source_name)

	// remove existing extension
	source_name = FileRemoveExtension(source_name)

	// create a prefix for ioutil.TempDir
	prefix := source_name + "-"

	// determine where to create our temporary dir
	dir := os.Getenv("YEWBERRY_TMPDIR")
	if dir == "" {
		dir = os.TempDir()
	}

	got_dir, err := ioutil.TempDir(dir, prefix)
	if err != nil {
		return err
	}

	temp_dir = got_dir
	return Ok
}

func DirRemove() {
	if temp_dir != "" && !Options.keep {
		os.RemoveAll(temp_dir)
	}
}

func DirTempFile(path, mod, ext string) (f *os.File, err error) {
	// remove directory component
	path = filepath.Base(path)

	// remove existing extension
	old_ext := filepath.Ext(path)
	path = strings.TrimSuffix(path, old_ext)

	// create a pattern for ioutil.TempFile, the '*' becomes a
	// random string.
	pattern := path + "_*"

	if ext != "" {
		pattern = pattern + "." + ext
	}

	// the module name is optional
	if mod != "" {
		pattern = mod + "__" + pattern
	}

	return ioutil.TempFile(temp_dir, pattern)
}

func DirWriteFile(path, mod, ext string, data string) (filename string, err error) {
	var f *os.File

	f, err = DirTempFile(path, mod, ext)
	if err != nil {
		return "", err
	}

	filename = f.Name()

	_, err = f.WriteString(data)

	f.Close()

	return filename, err
}
