// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
import "strings"
import "os"
import "os/exec"

var Linker struct {
	obj_names []string

	response_name string
}

func Link() error {
	Stage.input = Stage.output
	Stage.output = ""

	Linker.obj_names = make([]string, 0)

	err := LinkCollectObjects(Stage.input)
	if err != nil {
		return err
	}

	if len(Linker.obj_names) == 0 {
		return fmt.Errorf("no object files to link")
	}

	err = LinkCreateResponseFile()
	if err != nil {
		return err
	}

	// WISH: ability to select a different linker
	tool := "gold"

	// TODO want to be able to provide options to the linker
	//      [ but those options may depend on which one it is.... ]

	cmd := exec.Command(tool, "@" + Linker.response_name, "-o", Options.executable)

	// drop normal output on stdout, prevent clobbering our messages
	cmd.Stdout = nil

	// collect any error messages from the linker
	err_box := new(strings.Builder)
	cmd.Stderr = err_box

	err = cmd.Start()
	if err != nil {
		return fmt.Errorf("could not run %s: %s", tool, err.Error())
	}

	err = cmd.Wait()
	if err != nil {
		// TODO do something better with errors
		fmt.Fprintf(os.Stderr, "ERROR FROM %s: %s\n", tool, err.Error())
		fmt.Fprintf(os.Stderr, "%s\n", err_box.String())

		return fmt.Errorf("%s failed to link program", tool)
	}

	return Ok
}

func LinkCollectObjects(list string) error {
	lex := NewLexer(strings.NewReader(list))

	for {
		t := lex.Scan()
		if t.Kind == TOK_EOF {
			break
		}

		if t.Kind == TOK_ERROR {
			return fmt.Errorf("syntax error in LST file: %s", t.Str)
		} else if t.Kind != TOK_Expr || len(t.Children) < 1 {
			return fmt.Errorf("bad syntax in LST file")
		} else {
			head := t.Children[0]

			if head.Match("link") {
				err := LinkAddObject(t)
				if err != nil {
					return err
				}
			}
		}
	}

	return Ok
}

func LinkAddObject(t *Token) error {
	if len(t.Children) < 2 ||
		t.Children[1].Kind != TOK_String {

		return fmt.Errorf("bad syntax in link statement: %s", t.Children[1].String())
	}

	name := t.Children[1].Str
	if name == "" {
		return fmt.Errorf("bad filename in link statement")
	}

	Linker.obj_names = append(Linker.obj_names, name)
	return Ok
}

func LinkCreateResponseFile() error {
	// a response file is a file containing arguments for a program.
	// we use one for the linking stage since there may hundreds of
	// object files, each one potentially having a long pathname,
	// so it can readily surpass the operating system's limit.

	f, err := DirTempFile("LINK", "", "")
	if err != nil {
		return err
	}

	// the crt1.o has startup code and must be first
	fmt.Fprintf(f, "/usr/local/musl/lib/crt1.o\n")

	for _, obj_name := range Linker.obj_names {
		// TODO escape spaces (etc) here?
		fmt.Fprintf(f, "%s\n", obj_name)
	}

	// add the musl C library
	fmt.Fprintf(f, "/usr/local/musl/lib/libc.a\n")

	Linker.response_name = f.Name()

	f.Close()
	return Ok
}
