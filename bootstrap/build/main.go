// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "fmt"
import "runtime"
import "path/filepath"

import "gitlab.com/andwj/argv"

var Options struct {
	source     string
	executable string

	keep bool

	help    bool
	version bool
}

var YewberryRoot string

var Stage struct {
	// the input to the current stage
	// [ either a filename, or a set of S expressions ]
	input string

	// the output of the current stage
	// [ either a filename, or a set of S expressions ]
	output string

	// for CC and ASM stages: number of failed files
	failures int
}

func main() {
	var err error

	argv.Generic("o", "output", &Options.executable, "file", "name of output executable")
	argv.Gap()
	argv.Enabler("h", "help", &Options.help, "display this help text")
	argv.Enabler("", "version", &Options.version, "display the version")

	// TEMPORARY while we debug the beast....
	Options.keep = true

	err = argv.Parse()
	if err != nil {
		FatalError("%s", err.Error())
	}

	names := argv.Unparsed()

	if Options.version {
		ShowVersion()
		os.Exit(0)
	}
	if Options.help || len(names) == 0 {
		ShowUsage()
		os.Exit(0)
	}

	if len(names) > 1 {
		FatalError("too many filenames")
	}

	Options.source = names[0]

	// determine executable name if unset
	DetermineExecutable()

	// this can fatal error
	YewberryRoot = FindRootDir()

	err = DirCreate(Options.source)
	if err == nil {
		// FIXME temporary
		fmt.Fprintf(os.Stderr, "temp_dir: %s\n", temp_dir)

		err = BuildStuff()
		DirRemove()
	}

	if err != nil {
		FatalError("%s", err.Error())
	}

	os.Exit(0)
}

func DetermineExecutable() {
	var err error

	if Options.executable == "" {
		Options.executable = FileRemoveExtension(filepath.Base(Options.source))
		if runtime.GOOS == "windows" {
			Options.executable += ".exe"
		}
	}

	// we need the executable name to be absolute
	Options.executable, err = filepath.Abs(Options.executable)

	if err != nil {
		FatalError("cannot getwd: %s", err.Error())
	}
}

func BuildStuff() error {
	// firstly, run the module tool and generate initial list
	err := ModAnalyse(Options.source)

	// secondly, run the yewberry compiler and get C/asm files
	if err == nil {
		err = CompileYewberry()
	}

	// thirdly, copy the needed runtime files
	if err == nil {
		err = CopyRuntimeFiles()
	}

	// change current directory to the temp dir
	if err == nil {
		err = os.Chdir(temp_dir)
	}

	// next run the C compiler
	if err == nil {
		err = CompileC()
	}

	// now run the assembler
	if err == nil {
		err = Assemble()
	}

	// finally, run the linker
	if err == nil {
		err = Link()
	}

	return err
}

func ShowUsage() {
	Message("Usage: yb-build FILE.yb   [OPTIONS...]")
	Message("   or: yb-build FILE.ymod [OPTIONS...]")
	Message("")
	Message("Available options:")
	argv.Display(os.Stdout)
}

func ShowVersion() {
	// FIXME read YB_VERSION file in YewberryRoot
	Message("Could not determine version")
}

func FatalError(format string, a ...interface{}) {
	format = "yb-build: " + format + "\n"
	fmt.Fprintf(os.Stderr, format, a...)
	os.Exit(2)
}

func Message(format string, a ...interface{}) {
	fmt.Printf(format, a...)
	fmt.Printf("\n")
}
