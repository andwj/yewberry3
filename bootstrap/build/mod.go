// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
import "strings"
import "os"
import "os/exec"

func ModAnalyse(filename string) error {
	tool := "yb-mod"

	cmd := exec.Command(tool, "analyse", filename)

	// capture stdout as the module list
	mod_list := new(strings.Builder)
	cmd.Stdout = mod_list

	// collect any error messages from the yb-mod tool
	err_box := new(strings.Builder)
	cmd.Stderr = err_box

	err := cmd.Start()
	if err != nil {
		return fmt.Errorf("could not run %s: %s", tool, err.Error())
	}

	err = cmd.Wait()
	if err != nil {
		// TODO do something better with errors
		fmt.Fprintf(os.Stderr, "ERROR FROM %s: %s\n", tool, err.Error())
		fmt.Fprintf(os.Stderr, "%s\n", err_box.String())

		return fmt.Errorf("%s failed to resolve modules", tool)
	}

	Stage.output, err = DirWriteFile("STAGE1_mod", "", "", mod_list.String())
	return err
}
