// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "fmt"
import "strings"
import "io/ioutil"
import "path/filepath"
import "runtime"

func CopyRuntimeFiles() error {
	Stage.input = Stage.output
	Stage.output = ""
	Stage.failures = 0

	var err error

	/*
	f, err := os.Open(Stage.input)
	if err != nil {
		return err
	}
	defer f.Close()
	*/

	lex := NewLexer(strings.NewReader(Stage.input))

	for {
		t := lex.Scan()
		if t.Kind == TOK_EOF {
			break
		}

		if t.Kind == TOK_ERROR {
			return fmt.Errorf("syntax error in LST file: %s", t.Str)
		} else if t.Kind != TOK_Expr || len(t.Children) < 1 {
			return fmt.Errorf("bad syntax in LST file")
		} else {
			head := t.Children[0]

			if head.Match("runtime") {
				err = CopyRuntime(t)
				if err != nil {
					return err
				}
			} else {
				// simply copy input token to output
				OutputToken(t)
				Output("\n")
			}
		}
	}

	if Stage.failures > 0 {
		return fmt.Errorf("%d runtimes files failed to transfer", Stage.failures)
	}

	_, err = DirWriteFile("STAGE3_rt", "", "", Stage.output)
	return err
}

func CopyRuntime(t *Token) error {
	if len(t.Children) < 2 ||
		t.Children[1].Kind != TOK_String {

		return fmt.Errorf("bad syntax in runtime statement")
	}

	rt_file := t.Children[1].Str

	if rt_file == "" {
		return fmt.Errorf("bad filename in runtime statement")
	}

	in_name  := filepath.Join(YewberryRoot, "runtime", rt_file)
	out_name := filepath.Join(temp_dir, filepath.Base(rt_file))

	var err error
	var data []byte

	// on Unix-like systems we just make a symbolic link, but on
	// Windows we copy the file.

	if runtime.GOOS == "windows" {
		data, err = ioutil.ReadFile(in_name)
		if err == nil {
			err = ioutil.WriteFile(out_name, data, 0666)
		}
	} else {
		err = os.Symlink(in_name, out_name)
	}

	if err != nil {
		// TODO do something better with errors
		fmt.Fprintf(os.Stderr, "ERROR COPYING FILE: %s\n", err.Error())

		// increase error count, don't fail yet
		Stage.failures += 1
		return Ok
	}

	return Ok
}
