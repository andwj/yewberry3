// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
import "strings"
import "os"
import "os/exec"

func CompileYewberry() error {
	Stage.input = Stage.output
	Stage.output = ""

	tool := "yb-compile"

	// TODO provide options to the compiler

	cmd := exec.Command(tool, Stage.input, "--outdir", temp_dir)

	// capture stdout as the cc_list
	cc_list := new(strings.Builder)
	cmd.Stdout = cc_list

	// collect any error messages from the compiler
	err_box := new(strings.Builder)
	cmd.Stderr = err_box

	err := cmd.Start()
	if err != nil {
		return fmt.Errorf("could not run %s: %s", tool, err.Error())
	}

	err = cmd.Wait()
	if err != nil {
		// TODO do something better with errors
		fmt.Fprintf(os.Stderr, "ERROR FROM %s: %s\n", tool, err.Error())
		fmt.Fprintf(os.Stderr, "%s\n", err_box.String())

		return fmt.Errorf("%s failed to compile program", tool)
	}

	Stage.output = cc_list.String()

	_, err = DirWriteFile("STAGE2_yb", "", "", Stage.output)
	return err
}
