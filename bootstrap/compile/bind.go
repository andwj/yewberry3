// Copyright 2019 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"

type LocalVar struct {
	name     string
	ty       *Type
	mutable  bool
	external bool
	out_name string
}

type Upvar struct {
	name     string
	out_name string
	index    int     // into YFunction.upvars[]
	far_up   *Upvar  // only for up-up-vars, this is the "transfer" upvar
	orig_var *LocalVar
}

type JunctionInfo struct {
	name string

	// FIXME this is currently not set anywhere
	tail bool   // true if that ND_Block is in tail position

	// only used by the bind code
	parent *JunctionInfo
	used   bool

	// these are only used by the deduce code
	uptype       *Type
	skip_types []*Type

	// the following fields are only used by the back-end
	c_label string    // the C label for `goto`, created at first skip
	c_dest_var string
}

type LocalGroup struct {
	vars map[string]*LocalVar

	parent *LocalGroup
}

func (group *LocalGroup) Size() int {
	return len(group.vars)
}

type BindState struct {
	// the local variables which are currently "in scope", including
	// the parameters of the function/method being compiled.  It is a
	// chain via "parent" field (from innermost block out).  Only used
	// during BindClosure(), vars are not looked up after that.
	locals *LocalGroup

	// the current closure, or NIL for global vars
	cl *Closure

	// the parent (outer) environment, NIL for top-level ones
	parent *BindState

	// the active junctions
	junctions *JunctionInfo

	// use to generate unique names for lambda functions
	num_lambda int
}

//----------------------------------------------------------------------

// BindClosure analyses a code tree and creates LocalVars for each
// local binding which is found.  It also converts bare TOK_Name
// nodes to ND_Local and ND_Global nodes, adorning them with a
// pointer to the corresponding LocalVar or GlobalDef.
func BindClosure(cl *Closure, parent * BindState) cmError {
	// generic funcs and methods don't get bind/deduce/compile on
	// them, only on a monomorphized copy (when used).
	if cl.is_generic {
		return OKAY
	}

	bs := new(BindState)
	bs.locals = nil
	bs.cl = cl
	bs.parent = parent

	bs.PushLocalGroup()
	defer bs.PopLocalGroup()

	fun_type := cl.ty
	if fun_type.base == TYP_Generic { panic("weird func") }

	// create a LocalVar for each parameter
	for _, par := range fun_type.param {
		lvar := bs.AddLocal(par.name, false /* mutable */)
		lvar.ty = par.ty

		cl.parameters = append(cl.parameters, lvar)
	}

	if bs.BindNode(cl.t_body) != OKAY {
		cl.failed = true
		return FAILED
	}

	return OKAY
}

func BindVar(gdef *GlobalDef) cmError {
	bs := new(BindState)

	return bs.BindNode(gdef.expr)
}

//----------------------------------------------------------------------

func (bs *BindState) BindNode(t *Token) cmError {
	ErrorSetToken(t)

	switch t.Kind {
	case TOK_Name:
		return bs.BindName(t)

	case ND_Void, ND_Integer, ND_Float, ND_String, ND_Enum, ND_Default:
		return OKAY

	case ND_Let:
		return bs.BindLet(t)

	case ND_Block:
		return bs.BindBlock(t)

	case ND_Skip:
		return bs.BindSkip(t)

	case ND_IsTag:
		// skip the raw tags
		return bs.BindNode(t.Children[0])

	case ND_Match:
		return bs.BindMatch(t)

	case ND_SetVar:
		// NOTE: we need a local variable to be found and become a
		//       ND_Local node, hence recurse into both nodes.
		return bs.BindChildren(t)

	case ND_Lambda:
		return bs.BindLambda(t)

	case ND_DefVar:
		PostError("cannot use 'let' in that context")

	case ND_DefFunc, ND_DefMethod:
		PostError("cannot define globals within code")

	default:
		// everything else uses a basic pass
		return bs.BindChildren(t)
	}

	return FAILED
}

func (bs *BindState) BindChildren(t *Token) cmError {
	for _, sub := range t.Children {
		if bs.BindNode(sub) != OKAY {
			return FAILED
		}
	}

	return OKAY
}

func (bs *BindState) BindName(t *Token) cmError {
	// ignore field/method names, they are resolved in deduce code
	if t.IsField() {
		return OKAY
	}

	if t.Match("_") {
		PostError("cannot use '%s' in that context", t.Str)
		return FAILED
	}

	// handle the three special float constants
	if t.Module == "" {
		switch {
		case t.Match("+INF"), t.Match("-INF"), t.Match("NAN"):
			t.Kind = ND_Float
			t.Info.ty = float_def.ty
			return OKAY
		}
	}

	name := t.Str

	var lvar *LocalVar
	var lwho *BindState

	// find scope of the name.
	// some identifiers from macros need to ignore local scopes.
	if t.Cat != IDC_GlobalDef {
		lvar, lwho = bs.LookupLocal(name)
	}

	// a global?
	if lvar == nil {
		gdef := N_GetDef(name, t.Module)
		if gdef == nil {
			if t.Cat == IDC_GlobalDef {
				PostError("unknown global: %s", name)
			} else {
				PostError("unknown identifier: %s", name)
			}
			return FAILED
		}

		t.Kind = ND_Global
		t.Info.gdef = gdef

		return OKAY
	}

	// disallow using self inside a `.new` method
	if bs.cl != nil && bs.cl.is_new && t.Match("self") {
		PostError("cannot refer to self inside a '.new' method")
		return FAILED
	}

	if lwho == bs {
		// a local
		t.Kind = ND_Local
		t.Info.lvar = lvar

	} else {
		// an upvar
		t.Kind = ND_Upvar
		t.Info.upvar = bs.CaptureVar(lwho, lvar)

		// handle up-up-vars, ensure intermediate parent(s) also
		// gets an upvar which captures the variable.
		upv := t.Info.upvar

		for p := bs.parent ; p != lwho ; p = p.parent {
			upv.far_up = p.CaptureVar(lwho, lvar)
			upv = upv.far_up
		}
	}

	return OKAY
}

//----------------------------------------------------------------------

func (bs *BindState) BindLambda(t *Token) cmError {
	t_pars := t.Children[0]
	t_body := t.Children[1]

	bs.num_lambda += 1
	clos_name := fmt.Sprintf("lam%d:%s", bs.num_lambda, bs.cl.clos_name)

	template := CreateClosureStuff(t, t_pars, t_body, clos_name)
	t.Info.lambda = template

	if BindClosure(template, bs) != OKAY {
		template.failed = true
		return FAILED
	}

	return OKAY
}

func (bs *BindState) BindLet(t *Token) cmError {
	bs.PushLocalGroup()
	defer bs.PopLocalGroup()

	// create a local variable binding and slot
	t_var := t.Children[0]
	name := t_var.Str

	// must do expression BEFORE creating the local,
	// so that forms like (let verb verb) work properly.
	if bs.BindNode(t.Children[1]) != OKAY {
		return FAILED
	}

	lvar := bs.AddLocal(name, t.Info.mutable)

	t_var.Kind = ND_Local
	t_var.Info.lvar = lvar

	// do body
	if bs.BindNode(t.Children[2]) != OKAY {
		return FAILED
	}

	return OKAY
}

func (bs *BindState) BindBlock(t *Token) cmError {
	// if block has a junction, push it onto the active list.
	// when we visit the ND_Skip, we should find the junction here.
	junc := t.Info.junction
	if junc != nil {
		junc.parent = bs.junctions
		bs.junctions = junc
	}

	res := bs.BindChildren(t)

	if junc != nil {
		// pop it off
		bs.junctions = bs.junctions.parent

		// remove the junction if nothing used it
		if !junc.used {
			t.Info.junction = nil
		}
	}

	return res
}

func (bs *BindState) BindSkip(t *Token) cmError {
	// find the junction
	for junc := bs.junctions; junc != nil; junc = junc.parent {
		if t.Str == junc.name {
			t.Info.junction = junc
			junc.used = true
			break
		}
	}

	if t.Info.junction == nil {
		PostError("junction %s not found", t.Str)
		return FAILED
	}

	return bs.BindChildren(t)
}

//----------------------------------------------------------------------

func (bs *BindState) BindMatch(t *Token) cmError {
	t_expr := t.Children[0]
	if bs.BindNode(t_expr) != OKAY {
		return FAILED
	}

	for i := 1; i < len(t.Children); i++ {
		rule := t.Children[i]
		if bs.BindMatchRule(rule) != OKAY {
			return FAILED
		}
	}

	return OKAY
}

func (bs *BindState) BindMatchRule(t *Token) cmError {
	// save this group in the ND_MatchRule token, since
	// CompileMatchRule needs to know all the bindings.
	t.Info.group = bs.PushLocalGroup()
	defer bs.PopLocalGroup()

	if bs.BindMatchPattern(t.Children[0]) != OKAY {
		return FAILED
	}

	for i := 1; i < len(t.Children); i++ {
		if bs.BindNode(t.Children[i]) != OKAY {
			return FAILED
		}
	}
	return OKAY
}

func (bs *BindState) BindMatchPattern(pat *Token) cmError {
	switch pat.Kind {
	case TOK_Name:
		return bs.BindPattern_Name(pat)

	case ND_MatchAll, ND_MatchTags:
		// nothing needed
		return OKAY

	case ND_MatchConsts:
		return bs.BindPattern_Consts(pat)

	case ND_MatchStruct:
		return bs.BindPattern_common(pat)

	default:
		PostError("cannot bind match pattern: %s", pat.String())
		return FAILED
	}
}

func (bs *BindState) BindPattern_Name(pat *Token) cmError {
	name := pat.Str

	// is the name a global constant?
	def := N_GetDef(name, pat.Module)
	if def != nil {
		if def.mutable {
			PostError("match must be constant, but %s is mutable", name)
			return FAILED
		}

		// create a new node for ND_MatchConsts
		t_glob := NewNode(ND_Global, pat.Pos)
		t_glob.Str = pat.Str
		t_glob.Info.gdef = def

		pat.Kind = ND_MatchConsts
		pat.Str  = ""
		pat.Children = make([]*Token, 0)
		pat.Add(t_glob)

		return OKAY
	}

	/* name is a new binding */

	if bs.locals.vars[name] != nil {
		PostError("duplicate binding name '%s'", name)
		return FAILED
	}

	lvar := bs.AddLocal(name, false) // not mutable

	pat.Kind = ND_MatchVar
	pat.Str  = name
	pat.Info.lvar = lvar

	return OKAY
}

func (bs *BindState) BindPattern_Consts(pat *Token) cmError {
	for _, child := range pat.Children {
		if child.Kind == TOK_Name {
			name := child.Str

			// is the name a global constant?
			def := N_GetDef(name, child.Module)
			if def == nil {
				PostError("unknown global in match pattern: %s", name)
				return FAILED
			}

			if def.mutable {
				PostError("match must be constant, but %s is mutable", name)
				return FAILED
			}

			child.Kind = ND_Global
			child.Info.gdef = def
		}
	}

	return OKAY
}

func (bs *BindState) BindPattern_common(pat *Token) cmError {
	var err2 cmError

	for _, t_sub := range pat.Children {
		if t_sub.IsTag() {
			continue
		}

		if t_sub.Kind == ND_Field {
			err2 = bs.BindMatchPattern(t_sub.Children[0])
		} else {
			err2 = bs.BindMatchPattern(t_sub)
		}

		if err2 != OKAY {
			return FAILED
		}
	}
	return OKAY
}

//----------------------------------------------------------------------

func (bs *BindState) PushLocalGroup() *LocalGroup {
	group := new(LocalGroup)
	group.vars = make(map[string]*LocalVar)
	group.parent = bs.locals

	bs.locals = group

	return group
}

func (bs *BindState) PopLocalGroup() {
	if bs.locals == nil {
		panic("PopLocalGroup without push")
	}

	bs.locals = bs.locals.parent
}

func (bs *BindState) AddLocal(name string, mutable bool) *LocalVar {
	if bs.locals == nil {
		panic("AddLocal when closure has no LocalGroup")
	}

	lvar := new(LocalVar)
	lvar.name = name
	lvar.mutable = mutable

	// the special '_' match-all name is never stored in the group,
	// though its LocalVar will get used in various places.
	if name != "_" {
		bs.locals.vars[name] = lvar
	}

	return lvar
}

func (bs *BindState) LookupLocal(name string) (*LocalVar, *BindState) {
	for p := bs; p != nil; p = p.parent {
		for group := p.locals; group != nil; group = group.parent {
			lvar := group.vars[name]

			if lvar != nil {
				// found it.
				return lvar, p
			}
		}
	}

	// not found
	return nil, nil
}

func (bs *BindState) CaptureVar(lwho *BindState, lvar *LocalVar) *Upvar {
	// already captured?
	for _, upv := range bs.cl.upvars {
		if upv.orig_var == lvar {
			return upv
		}
	}

	lvar.external = true

	upv := new(Upvar)
	upv.name = lvar.name
	upv.orig_var = lvar

	upv.index = len(bs.cl.upvars)
	upv.out_name = fmt.Sprintf("_self->upvars[%d]", upv.index)

	bs.cl.upvars = append(bs.cl.upvars, upv)
	return upv
}

//----------------------------------------------------------------------

func MarkTailCalls(t *Token) {
	switch t.Kind {
	case ND_FunCall, ND_MethodCall:
		t.Info.tail = true

	case ND_Cast, ND_BaseCast:
		// type casts don't perform any real action, they just tell
		// the compiler that something has a new type, hence we can
		// allow their sub-expression to be a tail call.
		MarkTailCalls(t.Children[0])

	case ND_DefFunc, ND_DefMethod:
		// body of function definition
		MarkTailCalls(t.Children[2])

	case ND_Block:
		// only last element of a sequence can be a tail call
		// FIXME: review this check, happens in global var stuff
		if len(t.Children) > 0 {
			MarkTailCalls(t.Children[len(t.Children)-1])
		}

		// this is needed in order to support ND_Skip nodes, which
		// are a tail call when the corresponding junction is.
		// ND_Skip nodes get marked as tail calls elsewhere.
		if t.Info.junction != nil {
			t.Info.junction.tail = true
		}

	case ND_Let:
		// body of a let expression
		MarkTailCalls(t.Children[2])

	case ND_If:
		// both the then and else bodies can be tail calls
		MarkTailCalls(t.Children[1])
		MarkTailCalls(t.Children[2])

	case ND_Match:
		// bodies of match rules can be tail calls
		for i := 1; i < len(t.Children); i++ {
			rule := t.Children[i]
			body := rule.Children[1]
			MarkTailCalls(body)
		}

	default:
		// everything else: no tail calls are possible.
		//
		// for example, an ND_Array element cannot be a tail call
		// since there will be code AFTER the code computing the
		// element which stores it in the array.
	}
}
