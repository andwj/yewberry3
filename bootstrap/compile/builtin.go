// Copyright 2018 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
import "strconv"

type Builtin struct {
	name string

	c_runtime   string
	asm_runtime string

	out_name string   // if "", use the handler
	handler  func(*Closure, *Token, string)
}

var builtins map[string]*Builtin

func SetupBuiltins() {
	builtins = make(map[string]*Builtin)

	/* ---- functions ---- */

	// string manipulation / conversion
	RegisterFunction("parse-int", "int")
	RegisterFunction("parse-float", "float")

	// random numbers
	RegisterFunction("rand-seed", "random")
	RegisterFunction("rand-int", "random")
	RegisterFunction("rand-float", "random")

	// very basic I/O and OS library
	RegisterFunction("print", "os")
	RegisterFunction("print-err", "os")
	RegisterFunction("input-line", "os")
	RegisterFunction("exit-program", "os")
	RegisterFunction("abort-program", "os")

	// garbage collection
	RegisterFunction("gc-enable", "")
	RegisterFunction("gc-step", "")
	RegisterFunction("gc-collect-all", "")
	RegisterFunction("gc-used-memory", "")

	// support code
	RegisterFunction("rt-read-next-arg", "")
	RegisterFunction("rt-read-next-env", "")


	/* ---- methods ---- */

	// integer math
	RegisterMethod("int.mod", "int", "_rt_int_mod")
	RegisterMethod("int.pow", "int", "_rt_int_pow")
	RegisterMethod("int.abs", "int", "_rt_int_abs")
	RegisterMethod("int.str", "int", "_rt_int_to_str")

	RegisterMethod("int.cmp",  "int", "_rt_int_cmp")
	RegisterMethod("int.hash", "int", "_rt_int_hash")

	// floating point
	RegisterMethod("float.rem", "float", "_rt_float_rem")
	RegisterMethod("float.mod", "float", "_rt_float_mod")
	RegisterMethod("float.pow", "float", "_rt_float_pow")
	RegisterMethod("float.abs", "float", "_rt_float_abs")

	RegisterMethod("float.int", "float", "_rt_float_to_int")
	RegisterMethod("float.str", "float", "_rt_float_to_str")
	RegisterMethod("float.from-int", "float", "_rt_float_from_int")

	RegisterMethod("float.cmp",  "float", "_rt_float_cmp")
	RegisterMethod("float.hash", "float", "_rt_float_hash")

	RegisterMethod("float.nan?", "float", "_rt_float_nan_3F")
	RegisterMethod("float.inf?", "float", "_rt_float_inf_3F")

	RegisterMethod("float.floor", "float", "_rt_float_floor")
	RegisterMethod("float.ceil",  "float", "_rt_float_ceil")
	RegisterMethod("float.round", "float", "_rt_float_round")
	RegisterMethod("float.trunc", "float", "_rt_float_trunc")

	RegisterMethod("float.log",   "float", "_rt_float_log")
	RegisterMethod("float.log10", "float", "_rt_float_log10")
	RegisterMethod("float.sqrt",  "float", "_rt_float_sqrt")
	RegisterMethod("float.frexp", "float", "_rt_float_frexp")
	RegisterMethod("float.ldexp", "float", "_rt_float_ldexp")

	RegisterMethod("float.sin",  "float", "_rt_float_sin")
	RegisterMethod("float.cos",  "float", "_rt_float_cos")
	RegisterMethod("float.tan",  "float", "_rt_float_tan")
	RegisterMethod("float.asin", "float", "_rt_float_asin")
	RegisterMethod("float.acos", "float", "_rt_float_acos")
	RegisterMethod("float.atan", "float", "_rt_float_atan")

	RegisterMethod("float.hypot", "float", "_rt_float_hypot")
	RegisterMethod("float.atan2", "float", "_rt_float_atan2")

	// chars
	RegisterMethod("char.lower", "char_case", "_rt_char_lower")
	RegisterMethod("char.upper", "char_case", "_rt_char_upper")

	RegisterMethod("char.str", "string", "_rt_char_to_str")

	RegisterMethod("char.letter?",  "char_type", "_rt_char_is_letter")
	RegisterMethod("char.digit?",   "char_type", "_rt_char_is_digit")
	RegisterMethod("char.symbol?",  "char_type", "_rt_char_is_symbol")
	RegisterMethod("char.mark?",    "char_type", "_rt_char_is_mark")
	RegisterMethod("char.space?",   "char_type", "_rt_char_is_space")
	RegisterMethod("char.control?", "char_type", "_rt_char_is_control")

	// strings
	RegisterMethod("str.add", "string", "_rt_string_add")
	RegisterMethod("str.subseq", "string", "_rt_string_subseq")

	RegisterMethod("str.cmp",  "string", "_rt_string_cmp")
	RegisterMethod("str.hash", "string", "_rt_string_hash")

	RegisterMethod("str.lower", "char_case", "_rt_string_lower")
	RegisterMethod("str.upper", "char_case", "_rt_string_upper")

	// arrays
	RegisterMethod("array.subseq", "array", "_rt_array_subseq")


	/* ---- optimized methods ---- */

	// boolean logic
	RegisterHandler("bool.not", "", NAT_bool_not)

	// comparisons
	RegisterHandler("int.eq?", "int", NAT_int_eq)
	RegisterHandler("int.lt?", "int", NAT_int_lt)
	RegisterHandler("int.gt?", "int", NAT_int_gt)
	RegisterHandler("int.ne?", "int", NAT_int_ne)
	RegisterHandler("int.le?", "int", NAT_int_le)
	RegisterHandler("int.ge?", "int", NAT_int_ge)

	RegisterHandler("float.eq?", "float", NAT_float_eq)
	RegisterHandler("float.lt?", "float", NAT_float_lt)
	RegisterHandler("float.gt?", "float", NAT_float_gt)
	RegisterHandler("float.ne?", "float", NAT_float_ne)
	RegisterHandler("float.le?", "float", NAT_float_le)
	RegisterHandler("float.ge?", "float", NAT_float_ge)

	RegisterHandler("str.eq?", "string", NAT_str_eq)
	RegisterHandler("str.lt?", "string", NAT_str_lt)
	RegisterHandler("str.gt?", "string", NAT_str_gt)
	RegisterHandler("str.ne?", "string", NAT_str_ne)
	RegisterHandler("str.le?", "string", NAT_str_le)
	RegisterHandler("str.ge?", "string", NAT_str_ge)

	RegisterHandler("bool.eq?", "", NAT_bool_eq)
	RegisterHandler("bool.ne?", "", NAT_bool_ne)

	// integer arithmetic
	RegisterHandler("int.add", "int", NAT_int_add)
	RegisterHandler("int.sub", "int", NAT_int_sub)
	RegisterHandler("int.mul", "int", NAT_int_mul)
	RegisterHandler("int.div", "int", NAT_int_div)
	RegisterHandler("int.rem", "int", NAT_int_rem)
	RegisterHandler("int.neg", "int", NAT_int_neg)

	// floating point arithmetic
	RegisterHandler("float.add", "float", NAT_float_add)
	RegisterHandler("float.sub", "float", NAT_float_sub)
	RegisterHandler("float.mul", "float", NAT_float_mul)
	RegisterHandler("float.div", "float", NAT_float_div)
	RegisterHandler("float.neg", "float", NAT_float_neg)

	// bitwise operations
	RegisterHandler("int.or",   "int", NAT_int_or)
	RegisterHandler("int.and",  "int", NAT_int_and)
	RegisterHandler("int.xor",  "int", NAT_int_xor)
	RegisterHandler("int.flip", "int", NAT_int_flip)

	RegisterHandler("int.left", "int",  NAT_shift_left)
	RegisterHandler("int.right", "int", NAT_shift_right)

	// strings
	RegisterHandler("str.len", "string", NAT_str_len)
	RegisterHandler("str.get-elem", "string", NAT_str_get_elem)

	// arrays
	RegisterHandler("array.new", "array", NAT_array_new)
	RegisterHandler("array.len", "array", NAT_array_len)

	RegisterHandler("array.get-elem", "array", NAT_array_get_elem)
	RegisterHandler("array.set-elem", "array", NAT_array_set_elem)
	RegisterHandler("array.append", "array", NAT_array_append_elem)
	RegisterHandler("array.insert", "array", NAT_array_insert_elem)
	RegisterHandler("array.remove", "array", NAT_array_remove_elem)
	RegisterHandler("array.remove-seq", "array", NAT_array_remove_seq)
	RegisterHandler("array.clear-all", "array", NAT_array_clear_all)

	// byte vectors
	RegisterHandler("byte-vec.len", "bytevec", NAT_dummy)
	RegisterHandler("byte-vec.copy", "bytevec", NAT_dummy)
	RegisterHandler("byte-vec.subseq", "bytevec", NAT_dummy)
	RegisterHandler("byte-vec.add", "bytevec", NAT_dummy)

	RegisterHandler("byte-vec.get-elem", "bytevec", NAT_dummy)
	RegisterHandler("byte-vec.set-elem", "bytevec", NAT_dummy)
	RegisterHandler("byte-vec.append", "bytevec", NAT_dummy)
	RegisterHandler("byte-vec.insert", "bytevec", NAT_dummy)
	RegisterHandler("byte-vec.remove", "bytevec", NAT_dummy)
	RegisterHandler("byte-vec.remove-seq", "bytevec", NAT_dummy)
	RegisterHandler("byte-vec.clear-all", "bytevec", NAT_dummy)

	// miscellaneous
}

func RegisterFunction(name, c_runtime string) *Builtin {
	b := new(Builtin)

	b.name = name

	if c_runtime != "" {
		b.c_runtime = "c/rt_" + c_runtime + ".c"
	}

	builtins[name] = b
	return b
}

func RegisterMethod(name, runtime, out_name string) {
	b := RegisterFunction(name, runtime)
	b.out_name = out_name
}

func RegisterHandler(name, runtime string, handler func(*Closure, *Token, string)) {
	b := RegisterFunction(name, runtime)
	b.handler = handler
}

func Builtin_Resolve(cl *Closure) cmError {
	cl.builtin = builtins[cl.t_body.Str]

	if cl.builtin == nil {
		cl.failed = true
		PostError("No such #native function: %s", cl.t_body.Str)
		return FAILED
	}

	cl.clos_name = cl.builtin.name

	return OKAY
}

//----------------------------------------------------------------------

func NAT_int_add(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, "+")
}

func NAT_int_sub(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, "-")
}

func NAT_int_mul(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, "*")
}

func NAT_int_div(cl *Closure, t *Token, dest_var string) {
	// C99 guarantees that `/` does truncated division
	NAT_IntDivisionOp(cl, t, dest_var, "/")
}

func NAT_int_rem(cl *Closure, t *Token, dest_var string) {
	// C99 guarantees that `%` uses truncated division
	NAT_IntDivisionOp(cl, t, dest_var, "%")
}

func NAT_int_neg(cl *Closure, t *Token, dest_var string) {
	NAT_UnaryOp(cl, t, dest_var, "-")
}

func NAT_int_eq(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, "==")
}

func NAT_int_lt(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, "<")
}

func NAT_int_gt(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, ">")
}

func NAT_int_ne(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, "!=")
}

func NAT_int_le(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, "<=")
}

func NAT_int_ge(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, ">=")
}

func NAT_int_or(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, "|")
}

func NAT_int_and(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, "&")
}

func NAT_int_xor(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, "^")
}

func NAT_int_flip(cl *Closure, t *Token, dest_var string) {
	NAT_UnaryOp(cl, t, dest_var, "~")
}

func NAT_shift_left(cl *Closure, t *Token, dest_var string) {
	NAT_IntShift(cl, t, dest_var, "<<", ">>")
}

func NAT_shift_right(cl *Closure, t *Token, dest_var string) {
	NAT_IntShift(cl, t, dest_var, ">>", "<<")
}

//----------------------------------------------------------------------

func NAT_float_add(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, "+")
}

func NAT_float_sub(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, "-")
}

func NAT_float_mul(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, "*")
}

func NAT_float_div(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, "/")
}

func NAT_float_neg(cl *Closure, t *Token, dest_var string) {
	NAT_UnaryOp(cl, t, dest_var, "-")
}

func NAT_float_eq(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, "==")
}

func NAT_float_lt(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, "<")
}

func NAT_float_gt(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, ">")
}

func NAT_float_ne(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, "!=")
}

func NAT_float_le(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, "<=")
}

func NAT_float_ge(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, ">=")
}

//----------------------------------------------------------------------

func NAT_bool_not(cl *Closure, t *Token, dest_var string) {
	NAT_UnaryOp(cl, t, dest_var, "!")
}

func NAT_bool_eq(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, "==")
}

func NAT_bool_ne(cl *Closure, t *Token, dest_var string) {
	NAT_BinaryOp(cl, t, dest_var, "!=")
}

//----------------------------------------------------------------------

func NAT_str_len(cl *Closure, t *Token, dest_var string) {
	exp_str := cl.CompileAsValue(t.Children[1])

	if dest_var == "" {
		// merely getting the length has no side-effects
		return
	}

	cl.c_code.Printf("%s = (YInt) (%s)->mem.total;", dest_var, exp_str)
}

func NAT_str_get_elem(cl *Closure, t *Token, dest_var string) {
	arr_str := cl.CompileAsValue(t.Children[1])
	idx_str := cl.CompileAsValue(t.Children[2])

	if dest_var == "" {
		// merely reading an element has no side-effects
		return
	}

	// add code to check for out-of-bounds
	cl.c_code.Printf("if (%s < 0 || (uint32_t)%s >= (%s)->mem.total)",
		idx_str, idx_str, arr_str)
	cl.c_code.Printf("  %s = (YChar)0;", dest_var)
	cl.c_code.Printf("else")
	cl.c_code.Printf("  %s = (%s)->chars[%s];", dest_var, arr_str, idx_str)
}

func NAT_str_eq(cl *Closure, t *Token, dest_var string) {
	NAT_StringCompare(cl, t, dest_var, "==")
}

func NAT_str_lt(cl *Closure, t *Token, dest_var string) {
	NAT_StringCompare(cl, t, dest_var, "<")
}

func NAT_str_gt(cl *Closure, t *Token, dest_var string) {
	NAT_StringCompare(cl, t, dest_var, ">")
}

func NAT_str_ne(cl *Closure, t *Token, dest_var string) {
	NAT_StringCompare(cl, t, dest_var, "!=")
}

func NAT_str_le(cl *Closure, t *Token, dest_var string) {
	NAT_StringCompare(cl, t, dest_var, "<=")
}

func NAT_str_ge(cl *Closure, t *Token, dest_var string) {
	NAT_StringCompare(cl, t, dest_var, ">=")
}

//----------------------------------------------------------------------

func NAT_array_new(cl *Closure, t *Token, dest_var string) {
	len_str := "0"
	cap_str := cl.CompileAsValue(t.Children[2])

	if dest_var == "" {
		// no point creating something which is not used
		return
	}

	arr_type := t.Children[1].Info.ty.Real()

	cl.c_code.Printf("%s = _gc_alloc_array(%s, %s, %d);", dest_var,
		len_str, cap_str, arr_type.ElemType().IsAllocated_int())
}

func NAT_array_len(cl *Closure, t *Token, dest_var string) {
	arr_str := cl.CompileAsValue(t.Children[1])

	if dest_var == "" {
		// merely getting the length has no side-effects
		return
	}

	cl.c_code.Printf("%s = (YInt) (%s)->len;", dest_var, arr_str)
}

func NAT_array_get_elem(cl *Closure, t *Token, dest_var string) {
	arr_str := cl.CompileAsValue(t.Children[1])
	idx_str := cl.CompileAsValue(t.Children[2])

	if dest_var == "" {
		// merely reading an element has no side-effects
		return
	}

	arr_type := t.Children[1].Info.ty.Real()

	access_str := fmt.Sprintf("(%s)->elems[%s]", arr_str, idx_str)

	marsh := cl.Unmarshal(access_str, arr_type.ElemType())

	// add code to check for out-of-bounds
	cl.c_code.Printf("if (%s < 0 || (uint32_t)%s >= (%s)->len)",
		idx_str, idx_str, arr_str)
	cl.c_code.Printf("  _rt_fatal_error(\"out-of-bounds array access\");")
	cl.c_code.Printf("%s = %s;", dest_var, marsh)
}

func NAT_array_set_elem(cl *Closure, t *Token, dest_var string) {
	arr_str := cl.CompileAsValue(t.Children[1])
	idx_str := cl.CompileAsValue(t.Children[2])
	val_str := cl.CompileAsValue(t.Children[3])

	arr_type := t.Children[1].Info.ty.Real()

	elem_dest := fmt.Sprintf("(%s)->elems[%s]", arr_str, idx_str)
	elem_type := arr_type.ElemType()

	// add code to check for out-of-bounds
	cl.c_code.Printf("if (%s < 0 || (uint32_t)%s >= (%s)->len)",
		idx_str, idx_str, arr_str)
	cl.c_code.Printf("  _rt_fatal_error(\"out-of-bounds array access\");")

	cl.Marshal(elem_dest, val_str, elem_type)
}

func NAT_array_append_elem(cl *Closure, t *Token, dest_var string) {
	arr_str := cl.CompileAsValue(t.Children[1])
	val_str := cl.CompileAsValue(t.Children[2])

	arr_type := t.Children[1].Info.ty.Real()
	marsh := cl.MarshalVal(val_str, arr_type.ElemType())

	cl.c_code.Printf("_rt_array_append(%s, %s);", arr_str, marsh)
}

func NAT_array_insert_elem(cl *Closure, t *Token, dest_var string) {
	arr_str := cl.CompileAsValue(t.Children[1])
	pos_str := cl.CompileAsValue(t.Children[2])
	val_str := cl.CompileAsValue(t.Children[3])

	arr_type := t.Children[1].Info.ty.Real()
	marsh := cl.MarshalVal(val_str, arr_type.ElemType())

	cl.c_code.Printf("_rt_array_insert(%s, %s, %s);", arr_str, pos_str, marsh)
}

func NAT_array_remove_elem(cl *Closure, t *Token, dest_var string) {
	arr_str := cl.CompileAsValue(t.Children[1])
	pos_str := cl.CompileAsValue(t.Children[2])

	cl.c_code.Printf("_rt_array_delete(%s, %s, (%s) + 1);", arr_str, pos_str, pos_str)
}

func NAT_array_remove_seq(cl *Closure, t *Token, dest_var string) {
	arr_str   := cl.CompileAsValue(t.Children[1])
	start_str := cl.CompileAsValue(t.Children[2])
	end_str   := cl.CompileAsValue(t.Children[3])

	cl.c_code.Printf("_rt_array_delete(%s, %s, %s);", arr_str, start_str, end_str)
}

func NAT_array_clear_all(cl *Closure, t *Token, dest_var string) {
	arr_str := cl.CompileAsValue(t.Children[1])

	cl.c_code.Printf("_rt_array_clear(%s);", arr_str)
}

//----------------------------------------------------------------------

func NAT_CFunction(cl *Closure, t *Token, dest_var, name string) {
	line := ""
	if dest_var != "" {
		line = dest_var + " = "
	} else if !t.Info.ty.IsVoid() {
		line = "(void) "
	}

	line += name + "("

	params := t.Children[1:]
	for i, t_par := range params {
		if i > 0 { line += ", " }

		line += cl.CompileAsValue(t_par)
	}

	line += ");"

	cl.c_code.Add(line)
}

func NAT_UnaryOp(cl *Closure, t *Token, dest_var, op string) {
	val_str := cl.CompileAsValue(t.Children[1])

	if dest_var == "" {
		// math operators do not have side-effects
		return
	}

	cl.c_code.Printf("%s = %s %s;", dest_var, op, val_str)
}

func NAT_BinaryOp(cl *Closure, t *Token, dest_var, op string) {
	L := cl.CompileAsValue(t.Children[1])
	R := cl.CompileAsValue(t.Children[2])

	if dest_var == "" {
		// math operators do not have side-effects
		return
	}

	cl.c_code.Printf("%s = %s %s %s;", dest_var, L, op, R)
}

func NAT_IntDivisionOp(cl *Closure, t *Token, dest_var, op string) {
	num := cl.CompileAsValue(t.Children[1])
	div := cl.CompileAsValue(t.Children[2])

	if dest_var == "" {
		// math operators do not have side-effects
		return
	}

	cl.c_code.Printf("if (%s == 0)", div)
	cl.c_code.Printf("  %s = Y_INAN;", dest_var)
	cl.c_code.Printf("else")
	cl.c_code.Printf("  %s = %s %s %s;", dest_var, num, op, div)
}

func NAT_IntShift(cl *Closure, t *Token, dest_var, op1, op2 string) {
	t_val   := t.Children[1]
	t_count := t.Children[2]

	val   := cl.CompileAsValue(t_val)
	count := cl.CompileAsValue(t_count)

	if dest_var == "" {
		// math operators do not have side-effects
		return
	}

	// when we know the count is a POSITIVE integer literal, then we
	// avoid the `if` statement and use simpler C code.
	if t_count.Kind == ND_Integer {
		i_val, err := strconv.Atoi(t_count.Str)
		if err == nil && i_val >= 0 {
			cl.c_code.Printf("%s = %s %s %s;", dest_var, val, op1, count)
			return
		}
	}

	cl.c_code.Printf("if (%s < 0)", count)
	cl.c_code.Printf("  %s = %s %s (- %s);", dest_var, val, op2, count)
	cl.c_code.Printf("else")
	cl.c_code.Printf("  %s = %s %s %s;", dest_var, val, op1, count)
}

func NAT_StringCompare(cl *Closure, t *Token, dest_var, op string) {
	L := cl.CompileAsValue(t.Children[1])
	R := cl.CompileAsValue(t.Children[2])

	if dest_var == "" {
		// comparing strings does not have side-effects
		return
	}

	cl.c_code.Printf("%s = _rt_string_cmp(%s, %s) %s YCMP_EQUAL;", dest_var, L, R, op)
}

func NAT_dummy(cl *Closure, t *Token, dest_var string) {
	PostError("native method NYI: %s", t.Children[0].Str)
}
