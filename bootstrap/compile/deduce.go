// Copyright 2018 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
import "strconv"
import "unicode"

type DeduceState struct {
	// for functions, this is the current closure.
	// it is NIL for global vars.
	cl *Closure

	// for global variables, this is the current def
	gdef *GlobalDef

	// for NewTempVar
	temps int

	// this used by DeduceMatch code
	rec *MatchSeenRecord
}

func DeduceFunction(gdef *GlobalDef) {
	cl := gdef.fun_cl

	// generic functions don't get deduced
	if cl.is_generic {
		return
	}

	if DeduceClosure(cl) != OKAY {
		gdef.failed = true
	}
}

func DeduceMethod(cl *Closure) {
	// generic methods don't get deduced
	if cl.is_generic {
		return
	}

	if DeduceClosure(cl) != OKAY {
		return
	}
}

func DeduceClosure(cl *Closure) cmError {
	// if a previous step (e.g. BindClosure) failed, don't bother
	if cl.failed {
		return FAILED
	}

//println("DeduceClosure:", cl.clos_name)

	// check parameters / result for map key types
	if DeduceCheckParams(cl) != OKAY {
		return FAILED
	}

	ds := new(DeduceState)
	ds.cl = cl

	ret_type := cl.ty.ret

	new_body := ds.DeduceNode(cl.t_body, ret_type)
	if new_body == nil {
		cl.failed = true
		return FAILED
	}

	cl.t_body = new_body

	// check the body matches the return type
	if cl.builtin == nil {
		if !cl.t_body.Info.ty.AssignableTo(cl.ty.ret) {
			PostError("type mismatch on function result: wanted %s, body is %s",
				cl.ty.ret.String(), cl.t_body.Info.ty.String())
			return FAILED
		}
	}

	// optimize const-exprs in the body
	return EvaluateClosure(cl)
}

func DeduceVar(gdef *GlobalDef) {
	if gdef.failed {
		return
	}

	ds := new(DeduceState)
	ds.gdef = gdef

	// if expression has a known type, give it to variable now,
	// since that helps with cyclic usage of global vars.
	gdef.ty = gdef.expr.Info.ty

	// a top-most allocation needs special treatment
	gdef.expr.Info.is_top = true

	new_expr := ds.DeduceNode(gdef.expr, nil)
	if new_expr == nil {
		gdef.failed = true
		return
	}

	gdef.expr = new_expr
	gdef.ty = gdef.expr.Info.ty
}

func DeduceCheckParams(cl *Closure) cmError {
	// check for valid map keys
	bad_type := cl.ty.UnusableMapKey(10)
	if bad_type != nil {
		PostError("type not usable as map key: %s", bad_type.String())
		return FAILED
	}
	return OKAY
}

//----------------------------------------------------------------------

// DeduceNode visits everything in a code tree or data structure
// tree, and assigns a type to all the nodes which need one.
// Things that have a type already usually keep it, though if it
// turns out to be erroneous then an error is raised.
//
// For each type of node in a code tree there is a corresponding
// DeduceXXX() method to handle the type deduction.  Many of these
// take an 'uptype' parameter, which is the type which a parent node
// believes the child could or should be, though the child is free
// to check it and/or ignore it (the parent will catch any type
// mismatch, sooner or later).
//
// Each node is visited once, and after visited will have a valid
// type in the Info.ty field (unless a compile error occurred).
//
func (ds *DeduceState) DeduceNode(t *Token, uptype *Type) *Token {
	ErrorSetToken(t)

	// a node is never deduced more than once
	if t.Info.deduced {
		return t
	}
	t.Info.deduced = true

	switch t.Kind {
	case ND_Local:
		return ds.DeduceLocal(t, uptype)

	case ND_Upvar:
		return ds.DeduceUpvar(t, uptype)

	case ND_Global:
		return ds.DeduceGlobal(t, uptype)

	case TOK_Name:
		if t.IsField() {
			PostError("unexpected field name: %s", t.Str)
		} else {
			PostError("unexpected identifier: %s", t.Str)
		}
		return nil

	case ND_Void, ND_DummySelf:
		// already has a type
		return t

	case ND_Integer, ND_Float, ND_String:
		return ds.DeduceBasicLiteral(t, uptype)

	case ND_Struct:
		return ds.DeduceDataStruct(t, uptype)

	case ND_Class:
		return ds.DeduceClassLiteral(t, uptype)

	case ND_Enum:
		return ds.DeduceEnumLiteral(t, uptype)

	case ND_Union:
		return ds.DeduceUnionLiteral(t, uptype)

	case ND_Opt:
		return ds.DeduceOptLiteral(t, uptype)

	case ND_Default:
		return ds.DeduceDefaultLiteral(t)

	case ND_Field:
		child2 := ds.DeduceNode(t.Children[0], uptype)
		if child2 != nil {
			t.Children[0] = child2
		}
		return child2

	case ND_StrBuild:
		return ds.DeduceStringBuilder(t)

	case ND_FunCall:
		return ds.DeduceFunCall(t)

	case ND_MethodCall:
		return ds.DeduceMethodCall(t)

	case ND_Block:
		return ds.DeduceBlock(t, uptype)

	case ND_If:
		return ds.DeduceIf(t, uptype)

	case ND_Loop:
		return ds.DeduceLoop(t)

	case ND_Skip:
		return ds.DeduceSkip(t)

	case ND_Cast:
		return ds.DeduceCast(t)

	case ND_BaseCast:
		return ds.DeduceBaseCast(t)

	case ND_Unwrap, ND_Flatten:
		return ds.DeduceUnwrap(t)

	case ND_IsTag:
		return ds.DeduceIsTag(t, uptype)

	case ND_RefEqual:
		return ds.DeduceRefEqual(t)

	case ND_Fmt:
		return ds.DeduceFmt(t)

	case ND_Match:
		return ds.DeduceMatch(t, uptype)

	case ND_Let:
		return ds.DeduceLet(t, uptype)

	case ND_Construct:
		// this is only created here, no need to deduce it
		return t

	case ND_SetVar:
		// result is always void, no need to pass uptype
		return ds.DeduceSetVar(t)

	case ND_SetField:
		// result is always void, no need to pass uptype
		return ds.DeduceSetField(t)

	case ND_GetField:
		return ds.DeduceGetField(t)

	case ND_Lambda:
		// type is known, no need to pass uptype
		return ds.DeduceLambda(t)

	case ND_Native:
		// only used to declare a function as native code
		return t

	default:
		panic("DeduceNode: unexpected node: " + t.String())
	}
}

func (ds *DeduceState) Deduct(t *Token, new_type *Type) {
	t.Info.ty = new_type
}

//----------------------------------------------------------------------

func (ds *DeduceState) DeduceBlock(t *Token, uptype *Type) *Token {
	// empty blocks can only happen within ND_Construct
	if len(t.Children) == 0 {
		ds.Deduct(t, void_def.ty)
		return t
	}

	// collect skip nodes as we visit the children.
	junc := t.Info.junction
	if junc != nil {
		junc.uptype = uptype
		junc.skip_types = make([]*Type, 0)
	}

	last_node := t.Children[len(t.Children)-1]

	failed := false

	for i, sub := range t.Children {
		var downtype *Type

		if sub == last_node {
			downtype = uptype
		}

		sub2 := ds.DeduceNode(sub, downtype)
		if sub2 == nil {
			// try to deduce the other statements
			failed = true
		} else {
			t.Children[i] = sub2
		}
	}

	if failed {
		return nil
	}

	ds.Deduct(t, last_node.Info.ty)

	// type checking between skip nodes and the junction
	if junc != nil {
		if ds.VerifyJunction(t) != OKAY {
			return nil
		}
	}

	return t
}

func (ds *DeduceState) DeduceGlobal(t *Token, uptype *Type) *Token {
	// we can ignore uptype here.
	// [ global vars never get deduced from function bodies ]

	gdef := t.Info.gdef

	if gdef.is_fun && gdef.fun_cl.is_generic {
		PostError("invalid use of generic function: %s", gdef.name)
		return nil
	}

	// ignore failed globals and failed functions
	if gdef.failed {
		return nil
	}
	if gdef.fun_cl != nil && gdef.fun_cl.failed {
		return nil
	}

	if gdef.ty == nil {
		// we are probably doing the deduction of another var, since
		// in normal circumstances all global vars are done before any
		// functions or methods are done (hence their type is set).
		//
		// if this var (in gdef) has been seen already, then there must
		// be cyclic references which cannot be resolved.

		if ds.cl != nil {
			panic("GLOBAL WITH NO TYPE: " + gdef.name)
		}
		if gdef.expr == nil {
			panic("gdef with nil expr")
		}

		// FIXME ensure module name is included!
		nn := gdef.name

		if globals_vars_seen[nn] {
			PostError("cannot deduce type of global: %s (cyclic refs)", gdef.name)
			return nil
		}

		globals_vars_seen[gdef.name] = true

		DeduceVar(gdef)

		if gdef.ty == nil || gdef.failed {
			return nil
		}
	}

	// node type always mirrors the variable
	ds.Deduct(t, gdef.ty)
	return t
}

func (ds *DeduceState) DeduceLocal(t *Token, uptype *Type) *Token {
	lvar := t.Info.lvar

	if lvar.ty == nil {
		PostError("could not determine type of local '%s'", lvar.name)
		return nil
	}

	// node type always mirrors the variable
	ds.Deduct(t, lvar.ty)
	return t
}

func (ds *DeduceState) DeduceUpvar(t *Token, uptype *Type) *Token {
	upv := t.Info.upvar

	if upv.orig_var.ty == nil {
		PostError("could not determine type of local '%s'", upv.name)
		return nil
	}

	// node type always mirrors the variable
	ds.Deduct(t, upv.orig_var.ty)
	return t
}

func (ds *DeduceState) DeduceBasicLiteral(t *Token, uptype *Type) *Token {
	lit_type := t.Info.ty

	// verify the literal is a valid integer/float/etc
	switch t.Kind {
	case ND_Integer:
		_, err2 := DecodeInteger(t.Str)
		if err2 != OKAY {
			return nil
		}

	case ND_Float:
		switch t.Str {
		case "+INF", "-INF", "NAN":
			// these three special cases are ok

		default:
			_, err := strconv.ParseFloat(t.Str, 64)
			if err != nil {
				PostError("invalid float constant: %s", t.Str)
				return nil
			}
		}
	}

	// auto-cast plain literals to a custom type
	if uptype != nil && uptype.IsCustom() &&
		!lit_type.IsCustom() && lit_type.CastableTo(uptype) {

		ds.Deduct(t, uptype)
	}

	return t
}

func (ds *DeduceState) DeduceDataStruct(t *Token, uptype *Type) *Token {
	// this is used for stuff in `{}`, either with a type name at
	// the beginning or without.  it might have field names too.
	// the goal here is to transform to a proper node kind like
	// ND_Array, ND_Map, (etc)...  if we cannot determine the type,
	// it becomes a class.

	// handle a bare generic type like `array` or `map`.
	// for these we infer overall type from their elements.
	if t.Info.data_def != nil {
		data_def := t.Info.data_def

		cs := data_def.GetConstructors()

		if cs.Count() == 0 {
			PostError("generic type %s is lacking constructor methods",
				data_def.name)
			return nil
		}
		if cs.Count() > 1 {
			PostError("multiple constructor methods in type %s",
				data_def.name)
			return nil
		}

		if uptype != nil {
			real_up := uptype
			if real_up.base == TYP_Custom {
				real_up = real_up.impl
			}

			if real_up.base == TYP_Usage && real_up.using == data_def {
				// the uptype agrees with the generic type, and it
				// has known type parameters, so upgrade to it.
				t.Info.ty = uptype
				t.Info.data_def = nil
				goto known_type

			} else {
				// cannot use uptype, it is different than data_def
			}
		}

		return ds.VagueConstructor(t, cs)

known_type:
	}

	data_type := t.Info.ty

	if data_type == nil {
		if uptype == nil {
			t.Kind = ND_Class
			return ds.DeduceClassLiteral(t, nil)
		}

		data_type = uptype
		ds.Deduct(t, data_type)
	}

	real_type := data_type
	if real_type.base == TYP_Custom {
		real_type = real_type.impl
	}

	/* look for constructor methods.... */

	if data_type.base == TYP_Usage {
		cs := data_type.using.GetConstructors()
		if cs.Count() > 0 {
			return ds.DeduceConstructor(t, cs)
		}
	}

	if data_type.base == TYP_Custom {
		// we disallow custom non-generic types to use a `.new` method.
		//
		// Rationale: constructor methods like `.new` when used on a global
		// variable run in environment where some/most global vars are not
		// initialized (or even exist) yet, so any usage of them, including
		// via function or method calls, will probably crash the program.
		// That makes them a massive foot-gun, and hence best avoided.

		cs := data_type.def.GetConstructors()
		if cs.Count() > 0 {
			PostError("custom type %s cannot have constructors",
				data_type.def.name)
			return nil
		}

		if real_type.base == TYP_Usage {
			cs := real_type.using.GetConstructors()
			if cs.Count() > 0 {
				return ds.DeduceConstructor(t, cs)
			}
		}
	}

	if real_type.base == TYP_Usage {
		PostError("generic type %s is lacking constructor methods",
			data_type.using.name)
		return nil
	}

	if real_type.base == TYP_Class {
		t.Kind = ND_Class
		return ds.DeduceClassLiteral(t, nil)
	}

	// disallow field names or `...` for most types
	if len(t.Children) > 0 && t.Children[0].Kind == ND_Field {
		PostError("unexpected field name in constructor for type %s",
			data_type.String())
		return nil
	}
	if t.Info.open != 0 {
		PostError("cannot use '...' in constructor for type %s",
			data_type.String())
		return nil
	}

	// check that enum/union has a tag name
	switch real_type.base {
	case TYP_Enum, TYP_Union:
		if len(t.Children) < 1 || t.Children[0].Kind != ND_Enum {
			PostError("missing tag for enum/union constructor")
			return nil
		}
	}

	// use the real (fundamental) type to determine how to
	// interpret the data structure.

	switch real_type.base {
	case TYP_Int, TYP_Float, TYP_String:
		return ds.DeduceCustomLiteral(t, data_type)

	case TYP_Enum:
		if len(t.Children) > 1 {
			PostError("enum literal has extra rubbish at end")
			return nil
		}
		t_enum := NewNode(ND_Enum, t.Pos)
		t_enum.Str = t.Children[0].Str  // tag name
		t_enum.Info.ty = data_type

		return ds.DeduceEnumLiteral(t_enum, nil)

	case TYP_Union:
		if len(t.Children) > 2 {
			PostError("enum literal has extra rubbish at end")
			return nil
		}
		t_union := NewNode(ND_Union, t.Pos)
		t_union.Str = t.Children[0].Str  // tag name
		t_union.Children = t.Children[1:]
		t_union.Info.ty = data_type
		t_union.Info.is_top = t.Info.is_top

		return ds.DeduceUnionLiteral(t_union, nil)

	case TYP_ByteVec:
		// FIXME
		PostError("byte-vec constructors NYI")
		return nil

	default:
		PostError("invalid type for constructor: %s", data_type.String())
		return nil
	}
}

func (ds *DeduceState) DeduceConstructor(t *Token, cs ConstructorSet) *Token {
	if cs.Count() > 1 {
		PostError("multiple constructor methods in type %s", t.Info.ty.String())
		return nil
	}

	if len(t.Children) > 0 && t.Children[0].Kind == ND_Field {
		PostError("unexpected field name in constructor for type %s",
			t.Info.ty.String())
		return nil
	}
	if t.Info.open != 0 {
		PostError("cannot use '...' in constructor for type %s",
			t.Info.ty.String())
		return nil
	}

	if cs.arrays != nil {
		return ds.ConstructArray(t)
	}
	if cs.maps != nil {
		return ds.ConstructMap(t)
	}
	return ds.ConstructNormal(t)
}

func (ds *DeduceState) VagueConstructor(t *Token, cs ConstructorSet) *Token {
	// the exact type of the data struct is not known here, only
	// that it is a specific generic type like `array` or `map`.

	if len(t.Children) == 0 {
		PostError("missing elements for %s constructor",
			t.Info.data_def.name)
		return nil
	}
	if len(t.Children) > 0 && t.Children[0].Kind == ND_Field {
		PostError("unexpected field name in %s constructor",
			t.Info.data_def.name)
		return nil
	}
	if t.Info.open != 0 {
		PostError("cannot use '...' in %s constructor",
			t.Info.data_def.name)
		return nil
	}

	if cs.arrays != nil {
		return ds.VagueArray(t)
	}
	if cs.maps != nil {
		return ds.VagueMap(t)
	}
	return ds.VagueNormal(t)
}

func (ds *DeduceState) VagueNormal(t *Token) *Token {
	data_def := t.Info.data_def

	meth := data_def.GetMethod(".new")
	if meth == nil {
		panic(".new method is awol")
	}

	// we will try to infer the type parameters based on the
	// signature of the `.new` method.  this will fail if any of
	// the method parameters are more complex than a TYP_TypeParam.

	m_type := meth.ty
	if m_type.base == TYP_Generic {
		m_type = m_type.impl
	}
	if m_type.base != TYP_Function {
		panic("weird .new method")
	}

	if len(t.Children) != len(m_type.param)-1 {
		PostError("wrong number of values for %s: wanted %d, got %d",
			data_def.name, len(m_type.param)-1, len(t.Children))
		return nil
	}

	cons2 := NewNode(ND_Struct, t.Pos)

	tgroup := make(map[string]*Type)

	for i, t_elem := range t.Children {
		elem2 := ds.DeduceNode(t_elem, nil)
		if elem2 == nil {
			return nil
		}

		par := m_type.param[1+i]
		if par.ty.base == TYP_TypeParam {
			tgroup[par.ty.tp_name] = elem2.Info.ty
		}

		cons2.Add(elem2)
	}

	// construct the new Type, but check that all parameters are known
	new_type := NewType(TYP_Usage)
	new_type.using = data_def

	for _, g_par := range data_def.param {
		g_type := tgroup[g_par]
		if g_type == nil {
			PostError("could not infer types for %s constructor", data_def.name)
			return nil
		}
		new_type.AddParam(g_par, g_type)
	}

	if Gen_Expand(new_type, nil) != OKAY {
		return nil
	}
	cons2.Info.ty = new_type

	return ds.ConstructNormal(cons2)
}

func (ds *DeduceState) VagueArray(t *Token) *Token {
	data_def := t.Info.data_def

	// if the generic type has multiple parameters, there is no
	// way to infer them.
	if len(data_def.param) != 1 {
		PostError("could not infer types for %s constructor", data_def.name)
		return nil
	}

	x_name := data_def.name + " element"

	// perform initial pass to find any definite types.
	// we don't deduce the children in this pass....
	elem_type := ds.ComputeDownForGroup(t.Children, nil)

	// second pass actually deduces each element
	for i, t_elem := range t.Children {
		elem2 := ds.DeduceNode(t_elem, elem_type)
		if elem2 == nil {
			return nil
		}
		t.Children[i] = elem2

		if elem2.Info.ty.IsVoid() {
			PostError("%ss cannot be void", x_name)
			return nil
		}
	}

	elem_type = ds.FinalTypeForGroup(t.Children, x_name)
	if elem_type == nil {
		return nil
	}

	// construct a complete type spec
	new_type := NewType(TYP_Usage)
	new_type.using = data_def
	new_type.AddParam(data_def.param[0], elem_type)

	if Gen_Expand(new_type, nil) != OKAY {
		return nil
	}

	ds.Deduct(t, new_type)

	return ds.ConstructArray(t)
}

func (ds *DeduceState) VagueMap(t *Token) *Token {
	data_def := t.Info.data_def

	// we need the generic type to have TWO parameters, and we assume
	// the first is the key type, the second is the value type.
	if len(data_def.param) != 2 {
		PostError("could not infer types for %s constructor", data_def.name)
		return nil
	}

	// the elements might not be paired up yet...
	if ds.CollectMapPairs(t) != OKAY {
		return nil
	}

	k_name := data_def.name + " keys"
	e_name := data_def.name + " elements"

	// collect keys and elements
	key_group  := make([]*Token, 0)
	elem_group := make([]*Token, 0)

	for _, pair := range t.Children {
		key_group  = append(key_group,  pair.Children[0])
		elem_group = append(elem_group, pair.Children[1])
	}

	// perform initial pass to find any definite types.
	// we don't deduce the children in this pass....
	key_type  := ds.ComputeDownForGroup(key_group,  nil)
	elem_type := ds.ComputeDownForGroup(elem_group, nil)

	// second pass actually deduces each element
	for _, pair := range t.Children {
		t_key  := pair.Children[0]
		t_elem := pair.Children[1]

		t_key  = ds.DeduceNode(t_key,  key_type)
		t_elem = ds.DeduceNode(t_elem, elem_type)

		if t_key == nil || t_elem == nil {
			return nil
		}

		pair.Children[0] = t_key
		pair.Children[1] = t_elem

		if t_key.Info.ty.IsVoid() {
			PostError("%ss cannot be void", k_name)
			return nil
		}
		if t_elem.Info.ty.IsVoid() {
			PostError("%ss cannot be void", e_name)
			return nil
		}
	}

	key_type  = ds.FinalTypeForGroup(key_group,  k_name)
	elem_type = ds.FinalTypeForGroup(elem_group, e_name)

	if key_type == nil || elem_type == nil {
		return nil
	}

	// construct a complete type spec
	new_type := NewType(TYP_Usage)
	new_type.using = data_def
	new_type.AddParam(data_def.param[0],  key_type)
	new_type.AddParam(data_def.param[1], elem_type)

	if Gen_Expand(new_type, nil) != OKAY {
		return nil
	}

	ds.Deduct(t, new_type)

	return ds.ConstructMap(t)
}

func (ds *DeduceState) ConstructNormal(t *Token) *Token {
	// need to convert {TYPE a b c} --> .new DUMMY a b c

	data_type := t.Info.ty

	t_cons := NewNode(ND_Construct, t.Pos)
	t_cons.Info.ty = data_type

	var t_var *Token

	if ds.cl == nil && t.Info.is_top {
		t_var = NewNode(ND_Global, t.Pos)
		t_var.Str = ds.gdef.name
		t_var.Info.gdef = ds.gdef
		t_var.Info.ty = data_type

	} else {
		t_var = ds.NewTempVar(data_type, t.Pos)
	}

	// must not try and deduce this var!
	t_var.Info.deduced = true

	// create a method call
	m_name := NewNode(TOK_Name, t.Pos)
	m_name.Str = ".new"

	m_dummy := NewNode(ND_DummySelf, t.Pos)
	m_dummy.Info.ty = data_type

	mcall := NewNode(ND_MethodCall, t.Pos)
	mcall.Add(m_name)
	mcall.Add(m_dummy)

	for _, child := range t.Children {
		mcall.Add(child)
	}

	// process the newly constructed method call
	mcall = ds.DeduceNode(mcall, nil)
	if mcall == nil {
		return nil
	}

	block := NewNode(ND_Block, t.Pos)

	t_cons.Add(t_var)
	t_cons.Add(mcall)
	t_cons.Add(block)

	return t_cons
}

func (ds *DeduceState) ConstructArray(t *Token) *Token {
	// an array literal like:
	//    {TYPE a b c}
	//
	// becomes a tree like:
	//    (ND_Construct "_temp"
	//       (.new-array DUMMY CAPACITY)
	//       (ND_Block
	//          (.append _temp a)
	//          (.append _temp b)
	//          (.append _temp c)
	//       )
	//    )

	// TODO probably should deduce the elements (if not already)
	//      and check their type here, for a better error message.

	data_type := t.Info.ty

	t_cons := NewNode(ND_Construct, t.Pos)
	t_cons.Info.ty = data_type

	var t_var *Token

	if ds.cl == nil && t.Info.is_top {
		t_var = NewNode(ND_Global, t.Pos)
		t_var.Str = ds.gdef.name
		t_var.Info.gdef = ds.gdef
		t_var.Info.ty = data_type

	} else {
		t_var = ds.NewTempVar(data_type, t.Pos)
	}

	// must not try and deduce this var!
	t_var.Info.deduced = true

	// create a method call
	m_name := NewNode(TOK_Name, t.Pos)
	m_name.Str = ".new-array"

	m_cap := NewNode(ND_Integer, t.Pos)
	m_cap.Str = strconv.Itoa(len(t.Children))
	m_cap.Info.ty = int_def.ty

	m_dummy := NewNode(ND_DummySelf, t.Pos)
	m_dummy.Info.ty = data_type

	mcall := NewNode(ND_MethodCall, t.Pos)
	mcall.Add(m_name)
	mcall.Add(m_dummy)
	mcall.Add(m_cap)

	block := NewNode(ND_Block, t.Pos)

	for _, t_elem := range t.Children {
		m_name := NewNode(TOK_Name, t_elem.Pos)
		m_name.Str = ".append"

		t_append := NewNode(ND_MethodCall, t_elem.Pos)
		t_append.Add(m_name)
		t_append.Add(t_var)
		t_append.Add(t_elem)

		block.Add(t_append)
	}

	// deduce the new nodes
	mcall = ds.DeduceNode(mcall, nil)
	block = ds.DeduceNode(block, nil)

	if mcall == nil || block == nil {
		return nil
	}

	t_cons.Add(t_var)
	t_cons.Add(mcall)
	t_cons.Add(block)

	return t_cons
}

func (ds *DeduceState) ConstructMap(t *Token) *Token {
	if ds.CollectMapPairs(t) != OKAY {
		return nil
	}

	// TODO probably should deduce the elements (if not already)
	//      and check their type here, for a better error message.

	data_type := t.Info.ty

	t_cons := NewNode(ND_Construct, t.Pos)
	t_cons.Info.ty = data_type

	var t_var *Token

	if ds.cl == nil && t.Info.is_top {
		t_var = NewNode(ND_Global, t.Pos)
		t_var.Str = ds.gdef.name
		t_var.Info.gdef = ds.gdef
		t_var.Info.ty = data_type

	} else {
		t_var = ds.NewTempVar(data_type, t.Pos)
	}

	// must not try and deduce this var!
	t_var.Info.deduced = true

	// create a method call
	m_name := NewNode(TOK_Name, t.Pos)
	m_name.Str = ".new-map"

	m_cap := NewNode(ND_Integer, t.Pos)
	m_cap.Str = strconv.Itoa(len(t.Children))
	m_cap.Info.ty = int_def.ty

	m_dummy := NewNode(ND_DummySelf, t.Pos)
	m_dummy.Info.ty = data_type

	mcall := NewNode(ND_MethodCall, t.Pos)
	mcall.Add(m_name)
	mcall.Add(m_dummy)
	mcall.Add(m_cap)

	block := NewNode(ND_Block, t.Pos)

	for _, pair := range t.Children {
		t_key  := pair.Children[0]
		t_elem := pair.Children[1]

		m_name := NewNode(TOK_Name, t_key.Pos)
		m_name.Str = ".set-elem"

		t_setter := NewNode(ND_MethodCall, t_key.Pos)
		t_setter.Add(m_name)
		t_setter.Add(t_var)
		t_setter.Add(t_key)
		t_setter.Add(t_elem)

		block.Add(t_setter)
	}

	// deduce the new nodes
	mcall = ds.DeduceNode(mcall, nil)
	block = ds.DeduceNode(block, nil)

	if mcall == nil || block == nil {
		return nil
	}

	t_cons.Add(t_var)
	t_cons.Add(mcall)
	t_cons.Add(block)

	return t_cons
}

func (ds *DeduceState) DeduceCustomLiteral(t *Token, data_type *Type) *Token {
	if len(t.Children) < 1 {
		PostError("custom literal is missing value")
		return nil
	}
	if len(t.Children) > 1 {
		PostError("custom literal has extra rubbish at end")
		return nil
	}

	// this is quite simple, it always acts like a cast
	t.Kind = ND_Cast
	return ds.DeduceCast(t)
}

func (ds *DeduceState) DeduceClassLiteral(t *Token, uptype *Type) *Token {
	class_type := t.Info.ty

	if class_type == nil {
		if uptype == nil {
			return ds.DeduceClassFromElems(t)
		}

		class_type = uptype
		ds.Deduct(t, class_type)
	}

	if class_type.Real().base != TYP_Class {
		PostError("class literal with non-class type: %s", class_type.String())
		return nil
	}

	if t.Info.open > 0 {
		if len(t.Children) > len(class_type.Real().param) {
			PostError("too many class fields: wanted %d or less, got %d",
				len(class_type.Real().param), len(t.Children))
			return nil
		}
	} else {
		if len(t.Children) != len(class_type.Real().param) {
			PostError("wrong number of class fields: wanted %d, got %d",
				len(class_type.Real().param), len(t.Children))
			return nil
		}
	}

	// Note: duplicate fields are detected by the parser.

	seen_fields := make(map[int]bool)

	for i, child := range t.Children {
		field_name := class_type.Real().param[i].name
		field_idx  := i

		t_elem := child

		if child.Kind == ND_Field {
			t_elem = child.Children[0]

			field_name = child.Str
			field_idx = class_type.Real().FindParam(field_name)

			if field_idx < 0 {
				PostError("unknown class field %s in %s", field_name,
					class_type.String())
				return nil
			}
		}

		field_type := class_type.Real().param[field_idx].ty

		elem2 := ds.DeduceNode(t_elem, field_type)
		if elem2 == nil {
			return nil
		}

		t_field := NewNode(ND_Field, elem2.Pos)
		t_field.Str = field_name
		t_field.Info.ty = field_type
		t_field.Info.field = field_idx
		t_field.Add(elem2)

		t.Children[i] = t_field

		seen_fields[field_idx] = true
	}

	// if open, add default values for any missing fields
	total := len(class_type.Real().param)

	for i := 0; i < total; i++ {
		if !seen_fields[i] {
			field_name := class_type.Real().param[i].name
			field_type := class_type.Real().param[i].ty

			if !field_type.Defaultable() {
				PostError("cannot omit field %s in %s (no default value)",
					field_name, class_type.String())
				return nil
			}

			deflt := NewNode(ND_Default, t.Pos)
			deflt.Info.ty = field_type

			deflt = ds.DeduceDefaultLiteral(deflt)
			if deflt == nil {
				return nil
			}

			t_field := NewNode(ND_Field, t.Pos)
			t_field.Str = field_name
			t_field.Info.ty = field_type
			t_field.Info.field = i
			t_field.Add(deflt)

			t.Add(t_field)
		}
	}

	if ds.VerifyClass(t) != OKAY {
		return nil
	}

	// convert from ND_Class --> ND_AllocObj + ND_SetField
	return ds.ConstructClass(t)
}

func (ds *DeduceState) DeduceClassFromElems(t *Token) *Token {
	// silently allow a '...', even though it is meaningless
	t.Info.open = 0

	// WISH: produce a single Type for each combination of fields
	//       [ using the `instantiations` table or similar ]
	class_type := NewType(TYP_Class)

	// Note that duplicate fields are detected by the parser

	for i, child := range t.Children {
		t_elem := child
		field_name := ""

		if child.Kind == ND_Field {
			t_elem = child.Children[0]
			field_name = child.Str

			// if a field name is numeric, check that it matches
			// the expected order.
			if len(field_name) >= 2 {
				r := []rune(field_name)

				if unicode.IsDigit(r[1]) {
					want_name := fmt.Sprintf(".%d", i)
					if field_name != want_name {
						PostError("wrong field name: expected %s, got %s",
							want_name, field_name)
						return nil
					}
				}
			}
		}

		if field_name == "" {
			field_name = fmt.Sprintf(".%d", i)
		}

		elem2 := ds.DeduceNode(t_elem, nil)
		if elem2 == nil {
			return nil
		}

		field_type := t_elem.Info.ty

		if field_type.IsVoid() {
			PostError("class elements cannot be void")
			return nil
		}

		t_field := NewNode(ND_Field, child.Pos)
		t_field.Str = field_name
		t_field.Info.ty    = field_type
		t_field.Info.field = i
		t_field.Add(elem2)

		t.Children[i] = t_field

		class_type.AddParam(field_name, field_type)
	}

	ds.Deduct(t, class_type)

	// convert from ND_Class --> ND_AllocObj + ND_SetField
	return ds.ConstructClass(t)
}

func (ds *DeduceState) VerifyClass(t *Token) cmError {
	class_type := t.Info.ty
	if class_type == nil {
		panic("class_type is nil")
	}

	for _, child := range t.Children {
		field_idx  := child.Info.field
		field_name := class_type.Real().param[field_idx].name
		field_type := class_type.Real().param[field_idx].ty

		t_elem := child
		if child.Kind == ND_Field {
			t_elem = child.Children[0]
		}

		if !t_elem.Info.ty.AssignableTo(field_type) {
			PostError("type mismatch on field %s: wanted %s, got %s",
				field_name, field_type.String(), t_elem.Info.ty.String())
			return FAILED
		}
	}

	return OKAY
}

func (ds *DeduceState) ConstructClass(t *Token) *Token {
	class_type := t.Info.ty

	t_cons := NewNode(ND_Construct, t.Pos)
	t_cons.Info.ty = class_type

	var t_var *Token

	if ds.cl == nil && t.Info.is_top {
		t_var = NewNode(ND_Global, t.Pos)
		t_var.Str = ds.gdef.name
		t_var.Info.gdef = ds.gdef
		t_var.Info.ty = class_type

	} else {
		t_var = ds.NewTempVar(class_type, t.Pos)
	}

	t_var.Info.deduced = true

	t_alloc := NewNode(ND_AllocObj, t.Pos)
	t_alloc.Info.ty = class_type
	t_alloc.Info.length = len(t.Children)
	t_alloc.Info.deduced = true

	t_pop := NewNode(ND_Block, t.Pos)
	t_pop.Info.ty = void_def.ty

	for _, child := range t.Children {
		// each child is a ND_Field by now
		t_value := child.Children[0]

		field_idx := child.Info.field
		if field_idx < 0 { panic("field_idx not set") }

		field_name := class_type.Real().param[field_idx].name

		t_setter := NewNode(ND_SetField, t.Pos)
		t_setter.Str = field_name
		t_setter.Info.field = field_idx

		t_setter.Add(t_var)
		t_setter.Add(t_value)

		t_pop.Add(t_setter)
	}

	t_cons.Add(t_var)
	t_cons.Add(t_alloc)
	t_cons.Add(t_pop)

	return t_cons
}

func (ds *DeduceState) CollectMapPairs(t *Token) cmError {
	children := t.Children

	if len(children) == 0 || children[0].Kind == ND_MapElem {
		return OKAY
	}

	// recreate the children as ND_MapElem tokens
	t.Children = make([]*Token, 0)

	if len(children) % 2 != 0 {
		PostError("map literal: odd number of elements")
		return FAILED
	}

	for i := 0; i < len(children); i += 2 {
		t_key := children[i]
		t_val := children[i+1]

		pair := NewNode(ND_MapElem, t_key.Pos)
		pair.Add(t_key)
		pair.Add(t_val)

		t.Add(pair)
	}

	return OKAY
}

func (ds *DeduceState) DeduceEnumLiteral(t *Token, uptype *Type) *Token {
	enum_type := t.Info.ty

	if enum_type == nil {
		if uptype == nil {
			PostError("unknown type for enum tag: %s", t.Str)
			return nil
		}

		enum_type = uptype
		ds.Deduct(t, enum_type)
	}

	if enum_type.Real().base != TYP_Enum {
		PostError("enum literal with non-enum type: %s",
			enum_type.String())
		return nil
	}

	tag_id := enum_type.Real().FindParam(t.Str)
	if tag_id < 0 {
		PostError("enum literal: unknown tag %s in %s", t.Str,
			enum_type.String())
		return nil
	}
	t.Info.tag_id = tag_id

	return t
}

func (ds *DeduceState) DeduceUnionLiteral(t *Token, uptype *Type) *Token {
	union_type := t.Info.ty

	if union_type == nil {
		if uptype == nil {
			PostError("unknown type for union literal")
			return nil
		}

		union_type = uptype
		ds.Deduct(t, union_type)
	}

	if union_type.Real().base != TYP_Union {
		PostError("union literal with non-union type: %s", union_type.String())
		return nil
	}

	tag_id := union_type.Real().FindParam(t.Str)
	if tag_id < 0 {
		PostError("union literal: unknown tag %s in %s", t.Str, union_type.String())
		return nil
	}
	t.Info.tag_id = tag_id

	// a missing datum should be void
	if len(t.Children) < 2 {
		t_void := NewNode(ND_Void, t.Pos)
		t_void.Info.ty = void_def.ty
		t.Add(t_void)
	}

	// handle the datum component
	t_datum := t.Children[0]

	datum_type := union_type.Real().param[tag_id].ty

	t_datum = ds.DeduceNode(t_datum, datum_type)
	if t_datum == nil {
		return nil
	}
	t.Children[0] = t_datum

	if !t_datum.Info.ty.AssignableTo(datum_type) {
		PostError("type mismatch on union datum: wanted %s, got %s",
			datum_type.String(), t_datum.Info.ty.String())
		return nil
	}

	// check for valid map keys
	bad_type := union_type.UnusableMapKey(10)
	if bad_type != nil {
		PostError("type not usable as map key: %s", bad_type.String())
		return nil
	}

	// convert from ND_Union --> ND_AllocObj + ND_SetUnion
	return ds.ConstructUnion(t)
}

func (ds *DeduceState) DeduceOptLiteral(t *Token, uptype *Type) *Token {
	t_child := t.Children[0]

	// when uptype is (opt XX), we can assume value will be XX.
	var downtype *Type

	if uptype != nil && uptype.UnwrappedType() != nil {
		downtype = uptype.UnwrappedType()
	}

	t_child = ds.DeduceNode(t_child, downtype)
	if t_child == nil {
		return nil
	}
	t.Children[0] = t_child

	child_type := t_child.Info.ty

	if child_type.IsVoid() {
		PostError("cannot create an optional with void")
		return nil
	}

	union_type := NewOptionalType(child_type)
	if Gen_Expand(union_type, nil) != OKAY {
		return nil
	}

	// create a ND_Union node for ConstructUnion
	t_union := NewNode(ND_Union, t_child.Pos)
	t_union.Str = "`VAL"
	t_union.Info.tag_id = 1
	t_union.Info.is_top = t.Info.is_top
	t_union.Add(t_child)

	ds.Deduct(t_union, union_type)

	return ds.ConstructUnion(t_union)
}

func (ds *DeduceState) ConstructUnion(t *Token) *Token {
	union_type := t.Info.ty
	tag_id := t.Info.tag_id
	datum_type := union_type.Real().param[tag_id].ty
	t_datum := t.Children[0]

	t_cons := NewNode(ND_Construct, t.Pos)
	t_cons.Info.ty = union_type

	var t_var *Token

	if ds.cl == nil && t.Info.is_top {
		t_var = NewNode(ND_Global, t.Pos)
		t_var.Str = ds.gdef.name
		t_var.Info.gdef = ds.gdef
		t_var.Info.ty = union_type

	} else {
		t_var = ds.NewTempVar(union_type, t.Pos)
	}

	t_var.Info.deduced = true

	t_alloc := NewNode(ND_AllocObj, t.Pos)
	t_alloc.Info.ty = union_type
	t_alloc.Info.tag_id = tag_id
	t_alloc.Info.deduced = true

	t_pop := NewNode(ND_Block, t.Pos)
	t_pop.Info.ty = void_def.ty

	if !datum_type.IsVoid() {
		t_setter := NewNode(ND_SetUnion, t.Pos)
		t_setter.Info.ty = void_def.ty
		t_setter.Info.tag_id = tag_id

		t_setter.Add(t_var)
		t_setter.Add(t_datum)

		t_pop.Add(t_setter)
	}

	t_cons.Add(t_var)
	t_cons.Add(t_alloc)
	t_cons.Add(t_pop)

	return t_cons
}

func (ds *DeduceState) DeduceDefaultLiteral(t *Token) *Token {
	// node type is always known (specified by user)
	def_type := t.Info.ty

	// for generic containers, transform to an empty constructor
	if (def_type.base == TYP_Usage && def_type.using != opt_def) ||
		(def_type.base == TYP_Custom && def_type.impl.base == TYP_Usage) {

		t.Kind = ND_Struct
		return ds.DeduceDataStruct(t, nil)
	}

	if !def_type.Defaultable() {
		PostError("expected a type with a default, got %s", def_type.String())
		return nil
	}

	// transform node to a more primitive form

	switch def_type.Real().base {
	case TYP_Void:
		t.Kind = ND_Void

	case TYP_Int:
		t.Kind = ND_Integer
		t.Str  = "0"

	case TYP_Float:
		t.Kind = ND_Float
		t.Str  = "0.0"

	case TYP_String:
		t.Kind = ND_String
		t.Str  = ""

	case TYP_Enum:
		t.Kind = ND_Enum
		t.Str  = "`NONE"
		t.Info.tag_id = 0

	case TYP_ByteVec:
		// FIXME do it like we had: {byte-vec ...}
		t.Kind = ND_AllocObj

	case TYP_Union:
		// create a ND_Union node for ConstructUnion
		t_void := NewNode(ND_Void, t.Pos)
		t_void.Info.ty = void_def.ty

		t_union := NewNode(ND_Union, t.Pos)
		t_union.Str = "`NONE"
		t_union.Info.tag_id = 0
		t_union.Info.ty = def_type
		t_union.Info.is_top = t.Info.is_top
		t_union.Add(t_void)

		return ds.ConstructUnion(t_union)

	case TYP_Class:
		// construct an open class literal (all fields get default)
		t.Kind = ND_Class
		t.Info.open = 1

		// this does the actual work...
		return ds.DeduceClassLiteral(t, nil)

	default:
		panic("weird thing for ND_Default")
	}

	return t
}

func (ds *DeduceState) DeduceStringBuilder(t *Token) *Token {
	// node type is already known (string)

	// deduce the elements
	for i, t_elem := range t.Children {
		// Note: cannot pass str_type here
		elem2 := ds.DeduceNode(t_elem, nil)
		if elem2 == nil {
			return nil
		}
		t.Children[i] = elem2

		if elem2.Info.ty.IsVoid() {
			PostError("expected a value for string builder, got void")
			return nil
		}

		if elem2.Info.ty.Real().base != TYP_String {
			// create a call to the `.str` method
			m_name := NewNode(TOK_Name, elem2.Pos)
			m_name.Str = ".str"

			mcall := NewNode(ND_MethodCall, elem2.Pos)
			mcall.Add(m_name)
			mcall.Add(elem2)

			// visit the newly constructed node
			mcall = ds.DeduceNode(mcall, str_def.ty)
			if mcall == nil {
				return nil
			}

			t.Children[i] = mcall

			if mcall.Info.ty.Real().base != TYP_String {
				PostError("strange .str method, wanted str, got %s",
					mcall.Info.ty.String())
				return nil
			}
		}
	}

	return t
}

func (ds *DeduceState) NewTempVar(ty *Type, pos Position) *Token {
	// note the space in the name, prevents a clash with a user var
	ds.temps += 1
	name := fmt.Sprintf("CONS v%d", ds.temps)

	lvar := new(LocalVar)
	lvar.name = name
	lvar.ty = ty
	lvar.mutable = true

	t_var := NewNode(ND_Local, pos)
	t_var.Str = name
	t_var.Info.ty = ty
	t_var.Info.lvar = lvar

	return t_var
}

//----------------------------------------------------------------------

func (ds *DeduceState) DeduceFunCall(t *Token) *Token {
	// no uptype here, there's not much we could do with it

	t_func := t.Children[0]
	params := t.Children[1:]

	if t_func.Kind == ND_Global &&
		t_func.Info.gdef.is_fun &&
		t_func.Info.gdef.fun_cl.is_generic {

		return ds.DeduceGenericCall(t, t_func.Info.gdef.fun_cl)
	}

	t_func = ds.DeduceNode(t_func, nil)
	if t_func == nil {
		return nil
	}
	t.Children[0] = t_func

	fun_type := t_func.Info.ty

	if fun_type.Real().base != TYP_Function {
		PostError("cannot call non-function, got %s", fun_type.String())
		return nil
	}

	if ds.DeduceParamsAndResult(t, params, fun_type, false) != OKAY {
		return nil
	}
	return t
}

func (ds *DeduceState) DeduceMethodCall(t *Token) *Token {
	t_method := t.Children[0]
	t_recv   := t.Children[1]
	params   := t.Children[1:] // include object itself

	// we *need* to know what the receiver of the method is
	t_recv = ds.DeduceNode(t_recv, nil)
	if t_recv == nil {
		return nil
	}
	t.Children[1] = t_recv

	recv_type := t_recv.Info.ty

	IntFace := (recv_type.base == TYP_Interface)

	var fun_type *Type

	if IntFace {
		idx := recv_type.FindParam(t_method.Str)
		if idx < 0 {
			PostError("unknown method %s in interface %s", t_method.Str, recv_type.String())
			return nil
		}

		fun_type = recv_type.param[idx].ty

		t.Info.op = nil

	} else {
		meth := recv_type.LookupMethod(t_method.Str)

		if meth == nil {
			PostError("unknown method %s in type: %s", t_method.Str,
				recv_type.String())
			return nil
		}

		// handle methods on a generic receiver
		if meth.is_generic {
			meth = ds.InstantiateMethod(meth, recv_type)
			if meth == nil || meth.failed {
				return nil
			}
		}

		fun_type = meth.ty

		// save it for CompileMethodCall
		t.Info.lambda = meth

		// this is for methods on a plain receiver but with generic parameters.
		// [ a method on a generic receiver has already been monomorphized
		//   to a concrete type ]
		if meth.is_generic {
			return ds.DeduceGenericCall(t, meth)
		}

		if meth.cruddy_crud != "" {  // UGH FIXME
			t.Info.monomorph = meth.cruddy_crud
		}
	}

	if fun_type == nil {
		panic("method without type")
	}

	if ds.DeduceParamsAndResult(t, params, fun_type, IntFace) != OKAY {
		return nil
	}

	// a little bit of magic: disallow operators with two different
	// custom types.  also upgrade result to a custom type if either
	// side is a custom type (when operator allows it).

	if t.Info.op != nil {
		if len(t.Children) == 3 {
			L := t.Children[1]
			R := t.Children[2]

			L_type := L.Info.ty
			R_type := R.Info.ty
			O_type := t.Info.ty

			if L_type.IsCustom() && R_type.IsCustom() {
				if L_type.def != R_type.def {
					PostError("type mismatch on binary operator, got %s and %s",
						L_type.def.name, R_type.def.name)
					return nil
				}
			}

			if  R_type.IsCustom() && !O_type.IsCustom() &&
				R_type.Real().base == O_type.Real().base {

				ds.Deduct(t, R_type)
			}
		}

		if len(t.Children) >= 2 {
			L := t.Children[1]

			L_type := L.Info.ty
			O_type := t.Info.ty

			if  L_type.IsCustom() && !O_type.IsCustom() &&
				L_type.Real().base == O_type.Real().base {

				ds.Deduct(t, L_type)
			}
		}
	}

	return t
}

func (ds *DeduceState) DeduceParamsAndResult(t *Token, params []*Token,
	fun_type *Type, IntFace bool) cmError {

	// hopefully cannot happen... [ REVIEW CASES ]
	if fun_type.Real().base == TYP_Generic {
		panic("generic func doesn't blend")
	}

	if len(params) != len(fun_type.Real().param) {
		PostError("wrong number of parameters: wanted %d, got %d",
			len(fun_type.Real().param), len(params))
		return FAILED
	}

	for i, t_par := range params {
		par_info := fun_type.Real().param[i]
		downtype := par_info.ty

		// for interfaces we do not know the exact type of receiver
		if IntFace && i == 0 {
			downtype = nil
		}

		t_par2 := ds.DeduceNode(t_par, downtype)
		if t_par2 == nil {
			return FAILED
		}
		// TODO: it is not clear that this updates original list
		params[i] = t_par2

		if IntFace && i == 0 {
			// for interfaces we cannot check the receiver
		} else {
			if !t_par2.Info.ty.AssignableTo(par_info.ty) {
				if par_info.name == "" {
					PostError("type mismatch on parameter #%d: wanted %s, got %s",
						i+1, par_info.ty.String(), t_par2.Info.ty.String())
				} else {
					PostError("type mismatch on parameter '%s': wanted %s, got %s",
						par_info.name, par_info.ty.String(), t_par2.Info.ty.String())
				}
				return FAILED
			}
		}
	}

	// node type is the function/method return type
	ds.Deduct(t, fun_type.Real().ret)
	return OKAY
}

func (ds *DeduceState) DeduceGenericCall(t *Token, gen_cl *Closure) *Token {
	params := t.Children[1:]

	fun_type := gen_cl.ty

	if fun_type.base != TYP_Generic {
		panic("non-generic in DeduceGenericCall")
	}

	if len(params) != len(fun_type.impl.param) {
		PostError("wrong number of parameters: wanted %d, got %d",
			len(fun_type.param), len(params))
		return nil
	}

	// we can only instantiate (monomorphize) the function once we
	// know all the types involved.

	// create a mapping which defines each `@XX` place-holder
	tgroup := make(map[string]*Type)

	for i, t_par := range params {
		dest_par  := fun_type.impl.param[i]
		par_msg   := fmt.Sprintf("parameter '%s'", dest_par.name)

		var downtype *Type
		if dest_par.ty.base != TYP_Generic {
			downtype = dest_par.ty
		}

		t_par2 := ds.DeduceNode(t_par, downtype)
		if t_par2 == nil {
			return nil
		}
		// TODO: it is not clear that this updates original param list
		params[i] = t_par2

		if dest_par.ty.base == TYP_Generic || dest_par.ty.base == TYP_TypeParam {
			err := Gen_AssignableTo(t_par2.Info.ty, dest_par.ty, par_msg, tgroup)
			if err != nil {
				PostError("%s", err.Error())
				return nil
			}
		} else {
			if !t_par2.Info.ty.AssignableTo(dest_par.ty) {
				PostError("type mismatch on parameter '%s': wanted %s, got %s",
					dest_par.name, dest_par.ty.String(), t_par2.Info.ty.String())
				return nil
			}
		}
	}

	mmf, err2 := Gen_Monomorphize(gen_cl, tgroup)
	if err2 != OKAY {
		return nil
	}

	// FIXME !!! this don't belong here
	var c_name string
	if gen_cl.is_method {
		c_name = EncodeName("_me_", "", mmf.clos_name)
	} else {
		c_name = EncodeName("_fu_", "", mmf.clos_name)
	}

	t.Info.monomorph = c_name

	// node type is return type of function/method
	ds.Deduct(t, mmf.ty.ret)

	return t
}

func (ds *DeduceState) InstantiateMethod(meth *Closure, recv_type *Type) *Closure {
	if recv_type.base == TYP_Custom {
		recv_type = recv_type.impl
	}
	if recv_type.base != TYP_Usage {
		panic("strange generic receiver for method")
	}

	// construct a mapping for Gen_Monomorphize
	tgroup := make(map[string]*Type)

	for _, par := range recv_type.param {
		tgroup[par.name] = par.ty
	}

	mmf, err2 := Gen_Monomorphize(meth, tgroup)
	if err2 != OKAY {
		return nil
	}

	// FIXME belongs in compile code, not here
	c_name := EncodeName("_me_", "", mmf.clos_name)

	mmf.cruddy_crud = c_name  // FIXME

	return mmf
}

func (ds *DeduceState) DeduceCast(t *Token) *Token {
	// target type is always known (specified by user).
	dest_type := t.Info.ty

	// these is no downtype here, as the purpose of `cast` is to go
	// from one actual type to another, NOT to assign a type.

	t_child := ds.DeduceNode(t.Children[0], nil)
	if t_child == nil {
		return nil
	}
	t.Children[0] = t_child

	src_type := t_child.Info.ty

	if !src_type.CastableTo(dest_type) {
		PostError("cannot cast from %s to %s",
			src_type.String(), dest_type.String())
		return nil
	}

	return t
}

func (ds *DeduceState) DeduceBaseCast(t *Token) *Token {
	t_child := ds.DeduceNode(t.Children[0], nil)
	if t_child == nil {
		return nil
	}
	t.Children[0] = t_child

	child_type := t_child.Info.ty

	if child_type.IsCustom() ||
		(child_type.base == TYP_Usage && !child_type.using.builtin) {
		// ok
	} else {
		PostError("expected a custom type for base-cast, got %s",
			child_type.String())
		return nil
	}

	new_type := child_type.impl
	if new_type == nil { panic("custom_impl is nil") }

	// simplify node for compiler
	t.Kind = ND_Cast

	ds.Deduct(t, new_type)
	return t
}

func (ds *DeduceState) DeduceUnwrap(t *Token) *Token {
	t_child := ds.DeduceNode(t.Children[0], nil)
	if t_child == nil {
		return nil
	}
	t.Children[0] = t_child

	src_type  := t_child.Info.ty
	dest_type := src_type.UnwrappedType()

	if dest_type == nil {
		PostError("wanted Opt/Result type for unwrap/flatten, got %s", src_type.String())
		return nil
	}

	if t.Kind == ND_Flatten {
		// add a default node for compiler to use
		deflt := NewNode(ND_Default, t.Pos)
		deflt.Info.ty = dest_type

		// this will check for valid defaultable types
		deflt = ds.DeduceDefaultLiteral(deflt)
		if deflt == nil {
			return nil
		}

		t.Add(deflt)
	}

	ds.Deduct(t, dest_type)
	return t
}

func (ds *DeduceState) DeduceIsTag(t *Token, uptype *Type) *Token {
	t_enum := ds.DeduceNode(t.Children[0], nil)
	if t_enum == nil {
		return nil
	}
	t.Children[0] = t_enum

	enum_type := t_enum.Info.ty

	switch enum_type.Real().base {
	case TYP_Enum, TYP_Union:
		// ok
	default:
		PostError("is? can only be used with unions or enums, got %s",
			enum_type.String())
		return nil
	}

	for i := 1; i < len(t.Children); i++ {
		t_tag := t.Children[i]
		t_tag.Info.tag_id = enum_type.Real().FindParam(t_tag.Str)

		if t_tag.Info.tag_id < 0 {
			PostError("unknown tag %s in type: %s", t_tag.Str,
				enum_type.String())
			return nil
		}
	}

	return t
}

func (ds *DeduceState) DeduceRefEqual(t *Token) *Token {
	t_left  := t.Children[0]
	t_right := t.Children[1]

	t_left  = ds.DeduceNode(t_left,  t_right.Info.ty)
	t_right = ds.DeduceNode(t_right, t_left .Info.ty)

	if t_left == nil || t_right == nil {
		return nil
	}

	t.Children[0] = t_left
	t.Children[1] = t_right

	L_type := t_left .Info.ty
	R_type := t_right.Info.ty

	switch L_type.Real().base {
	case TYP_Array, TYP_ByteVec, TYP_Class, TYP_Function, TYP_Interface:
		// ok
	default:
		PostError("ref-eq? with non-reference type: %s", L_type.String())
		return nil
	}

	if L_type.AssignableTo(R_type) || R_type.AssignableTo(L_type) {
		// ok
	} else {
		PostError("ref-eq? with incompatible types: %s and %s",
			L_type.String(), R_type.String())
		return nil
	}

	return t
}

func (ds *DeduceState) DeduceLet(t *Token, uptype *Type) *Token {
	t_var  := t.Children[0]
	t_exp  := t.Children[1]
	t_body := t.Children[2]

	lvar := t_var.Info.lvar
	if lvar == nil {
		panic("let form Failed to find variable: " + t_var.Str)
	}

	// visit the expression, it provides the variable's type
	t_exp = ds.DeduceNode(t_exp, nil)
	if t_exp == nil {
		return nil
	}
	t.Children[1] = t_exp

	lvar.ty = t_exp.Info.ty

	// directly update the variable's node
	ds.Deduct(t_var, lvar.ty)

	if lvar.ty.IsVoid() {
		PostError("local variables cannot be void")
		return nil
	}

	// deduce the body once variable is known
	t_body = ds.DeduceNode(t_body, uptype)
	if t_body == nil {
		return nil
	}
	t.Children[2] = t_body

	ds.Deduct(t, t_body.Info.ty)
	return t
}

func (ds *DeduceState) DeduceSetVar(t *Token) *Token {
	t_var := t.Children[0]
	t_exp := t.Children[1]

	var var_type *Type
	var var_mut  bool

	switch t_var.Kind {
	case ND_Local:
		lvar := t_var.Info.lvar
		var_type = lvar.ty
		var_mut  = lvar.mutable

	case ND_Upvar:
		upvar := t_var.Info.upvar
		var_type = upvar.orig_var.ty
		var_mut  = upvar.orig_var.mutable

	case ND_Global:
		def := t_var.Info.gdef
		var_type = def.ty
		var_mut  = def.mutable

	default:
		PostError("weird set! variable: %s", t_var.String())
		return nil
	}

	if var_type == nil {
		panic("set var with nil type")
	}

	if !var_mut {
		PostError("variable '%s' is read-only", t_var.Str)
		return nil
	}

	// visit expression, it should have same/compatible type as var
	t_exp = ds.DeduceNode(t_exp, var_type)
	if t_exp == nil {
		return nil
	}
	t.Children[1] = t_exp

	// verify value really is compatible
	if !t_exp.Info.ty.AssignableTo(var_type) {
		PostError("type mismatch in assignment: wanted %s, got %s",
			var_type.String(), t_exp.Info.ty.String())
		return nil
	}

	// node itself is always void_type, so nothing needed

	return t
}

func (ds *DeduceState) DeduceSetField(t *Token) *Token {
	t_arr := t.Children[0]
	t_exp := t.Children[1]

	// there is no way of inferring the container type
	t_arr = ds.DeduceNode(t_arr, nil)
	if t_arr == nil {
		return nil
	}
	t.Children[0] = t_arr

	arr_type := t_arr.Info.ty

	// handle the indexor
	field_idx := ds.DeduceFieldIndex(arr_type, t.Str)
	if field_idx < 0 {
		return nil
	}

	// remember index for the compiler
	t.Info.field = field_idx

	// deduce the value node
	field_type := arr_type.Real().param[field_idx].ty

	t_exp = ds.DeduceNode(t_exp, field_type)
	if t_exp == nil {
		return nil
	}
	t.Children[1] = t_exp

	if !t_exp.Info.ty.AssignableTo(field_type) {
		PostError("type mismatch in assignment: wanted %s, got %s",
			field_type.String(), t_exp.Info.ty.String())
		return nil
	}

	// node itself is always void_type, so nothing needed

	return t
}

func (ds *DeduceState) DeduceGetField(t *Token) *Token {
	// there is no way of inferring the container type
	t_arr := ds.DeduceNode(t.Children[0], nil)
	if t_arr == nil {
		return nil
	}
	t.Children[0] = t_arr

	arr_type := t_arr.Info.ty

	// handle the indexor
	field_idx := ds.DeduceFieldIndex(arr_type, t.Str)
	if field_idx < 0 {
		return nil
	}

	// remember index for the compiler
	t.Info.field = field_idx

	field_type := arr_type.Real().param[field_idx].ty

	ds.Deduct(t, field_type)
	return t
}

func (ds *DeduceState) DeduceFieldIndex(arr_type *Type, name string) int {
	var field_idx int

	switch arr_type.Real().base {
	case TYP_Class:
		field_idx = arr_type.Real().FindParam(name)
		if field_idx < 0 {
			PostError("unknown class field %s in %s", name,
				arr_type.String())
			return -1
		}

//--	case TYP_Tuple:
//--		// see if part after dot is a number or a name
//--		r := []rune(name)
//--
//--		if len(r) >= 2 && '0' <= r[1] && r[1] <= '9' {
//--			n, _ := fmt.Sscanf(name[1:], "%d", &field_idx)
//--			if n != 1 || field_idx < 0 {
//--				PostError("bad or unknown tuple field: %s", name)
//--				return -1
//--			}
//--			if field_idx >= len(arr_type.Real().param) {
//--				PostError("tuple field is out-of-range (.%d > %d)",
//--					field_idx, len(arr_type.Real().param)-1)
//--				return -1
//--			}
//--		} else {
//--			field_idx = arr_type.Real().FindParam(name)
//--			if field_idx < 0 {
//--				PostError("unknown tuple field %s in %s", name,
//--					arr_type.String())
//--				return -1
//--			}
//--		}

	default:
		PostError("cannot use [] access with type: %s", arr_type.String())
		return -1
	}

	return field_idx
}

func (ds *DeduceState) DeduceFmt(t *Token) *Token {
	// node type is already known (str)

	t_child := ds.DeduceNode(t.Children[0], nil)
	if t_child == nil {
		return nil
	}
	t.Children[0] = t_child

	spec := t.Info.fmt_spec

	// check compatibility with the spec string
	if spec.CheckType(t_child.Info.ty) != OKAY {
		return nil
	}

	return t
}

func (ds *DeduceState) DeduceLambda(t *Token) *Token {
	// NOTE: this template was created in BindLambda()
	template := t.Info.lambda

	// function type is already known
	if template.ty == nil {
		panic("lambda with no type")
	}

	ds.Deduct(t, template.ty)

	// Note 1: bind code has already done BindClosure() on the template.
	// Note 2: the lambda function gets compiled in CompileLambda().

	if DeduceClosure(template) != OKAY {
		return nil
	}

	return t
}

func (ds *DeduceState) DeduceIf(t *Token, uptype *Type) *Token {
	t_cond := t.Children[0]
	t_then := t.Children[1]
	t_else := t.Children[2]

	// handle condition
	t_cond = ds.DeduceNode(t_cond, bool_def.ty)
	if t_cond == nil {
		return nil
	}
	t.Children[0] = t_cond

	if !t_cond.Info.ty.IsBool() {
		PostError("if condition must be a bool, got %s", t_cond.Info.ty.String())
		return nil
	}

	// determine the down types for the children.
	// prefer the uptype when available, which mimics the same
	// pass-through behavior of ND_Block and ND_Let nodes.
	downtype := ds.ComputeDownForGroup(t.Children[1:], uptype)

	t_then = ds.DeduceNode(t_then, downtype)
	t_else = ds.DeduceNode(t_else, downtype)

	if t_then == nil || t_else == nil {
		return nil
	}

	t.Children[1] = t_then
	t.Children[2] = t_else

	// verify that all the clauses have compatible types:
	//   - if one is no-return, use the other one
	//   - if one is void, final type is always void
	//   - if can assign one side to other, use most specific type
	//   - otherwise types are not compatible

	final_type := ds.FinalTypeForGroup(t.Children[1:], "'if' result")
	if final_type == nil {
		return nil
	}

	ds.Deduct(t, final_type)
	return t
}

func (ds *DeduceState) ComputeDownForGroup(bodies []*Token, uptype *Type) *Type {
	// always prefer the uptype if available
	if uptype != nil {
		return uptype
	}

	var downtype *Type

	for _, body := range bodies {
		body_type := body.Info.ty

		// ignore void
		if body_type == nil || body_type.IsVoid() {
			continue
		}

		// FIXME we want the most *specific* type, this is not adequate
		if downtype == nil ||
			(body_type.IsCustom() && downtype.AssignableTo(body_type)) {

			downtype = body_type
		}
	}

	return downtype
}

func (ds *DeduceState) FinalTypeForGroup(bodies []*Token, what string) *Type {
	// returns NIL if the types are incompatible, or group is empty

	// TODO handle no-return

	var res_type *Type

	for _, t_body := range bodies {
		body_type := t_body.Info.ty

		if body_type.IsVoid() {
			return void_def.ty
		}

		// FIXME we want the most *specific* type, this is not adequate
		// FIXME REVIEW THIS
		if res_type == nil {
			res_type = body_type
		} else if res_type.AssignableTo(body_type) {
			res_type = body_type
		} else if body_type.AssignableTo(res_type) {
			// ok
		} else {
			PostError("type mismatch with %s: %s and %s",
				what, body_type.String(), res_type.String())
			return nil
		}
	}

	return res_type
}

func (ds *DeduceState) DeduceLoop(t *Token) *Token {
	t_body := ds.DeduceNode(t.Children[0], nil)

	if t_body == nil {
		return nil
	}

	t.Children[0] = t_body

	// ignore type of body (it does not matter)

	// node itself is always void_type, so nothing needed

	return t
}

func (ds *DeduceState) DeduceSkip(t *Token) *Token {
	junc := t.Info.junction

	// the last expression of the junction's block has not been
	// deduced yet, so cannot use that as the downtype, but we CAN
	// pass that block's uptype.

	t_exp := ds.DeduceNode(t.Children[0], junc.uptype)
	if t_exp == nil {
		return nil
	}
	t.Children[0] = t_exp

	// add the expressions type to the list
	junc.skip_types = append(junc.skip_types, t_exp.Info.ty)

	// node itself is already set as void
	return t
}

func (ds *DeduceState) VerifyJunction(t_block *Token) cmError {
	// here we check if all the skip node expressions have a
	// type compatible with the junction expression.

	junc := t_block.Info.junction

	junc_type := t_block.Info.ty

	for _, skip_type := range junc.skip_types {
		if skip_type.AssignableTo(junc_type) {
			// ok
		} else {
			PostError("type mismatch in skip: wanted %s, got %s",
				junc_type.String(), skip_type.String())
			return FAILED
		}
	}

	return OKAY
}

//----------------------------------------------------------------------

// MatchSeenRecord keeps track of which enum tags have been
// seen (handled) by match rules.  It also knows when a rule
// has been exhaustive (e.g. with the `_` catch-all pattern).
type MatchSeenRecord struct {
	tags      map[string]bool
	catch_all bool

	parent *MatchSeenRecord
}

func (ds *DeduceState) DeduceMatch(t *Token, uptype *Type) *Token {
	// we cannot infer the expression type since patterns are too
	// "loose" -- their meaning depends heavily on the type of
	// the expression...
	t_exp := ds.DeduceNode(t.Children[0], nil)
	if t_exp == nil {
		return nil
	}
	t.Children[0] = t_exp

	expr_type := t_exp.Info.ty

	ds.PushSeenRecord()
	defer ds.PopSeenRecord()

	// deduce all rules, except we don't do their bodies yet
	bodies := make([]*Token, 0)

	for i := 1; i < len(t.Children); i++ {
		t_rule := ds.DeduceMatchRule(t.Children[i], expr_type)
		if t_rule == nil {
			return nil
		}
		t.Children[i] = t_rule

		bodies = append(bodies, t_rule.Children[1])
	}

	// compute expected type for the bodies
	downtype := ds.ComputeDownForGroup(bodies, uptype)

	// deduce all the bodies now
	bodies = make([]*Token, 0)

	failed := false

	for i := 1; i < len(t.Children); i++ {
		t_rule := t.Children[i]

		t_body := ds.DeduceNode(t_rule.Children[1], downtype)
		if t_body == nil {
			failed = true
		} else {
			t_rule.Children[1] = t_body
			bodies = append(bodies, t_body)
		}
	}

	if failed {
		return nil
	}

	// check all bodies have compatible types
	final_type := ds.FinalTypeForGroup(bodies, "'match' result")
	if final_type == nil {
		return nil
	}

	// for enums and unions, check that all tags were handled.
	// also check for cases where the bodies produce a result but
	// the rules were not exhaustive (so the match may fail).

	if !ds.rec.catch_all {
		switch expr_type.Real().base {
		case TYP_Enum, TYP_Union:
			PostError("enum/union match is not exhaustive, %s not handled",
				ds.TrackUnusedTag(expr_type))
			return nil
		}

		if !final_type.IsVoid() {
			PostError("match produces a result, but rules are not exhaustive")
			return nil
		}
	}

	ds.Deduct(t, final_type)
	return t
}

func (ds *DeduceState) PushSeenRecord() {
	rec := new(MatchSeenRecord)
	rec.tags = make(map[string]bool)
	rec.parent = ds.rec

	ds.rec = rec
}

func (ds *DeduceState) PopSeenRecord() {
	ds.rec = ds.rec.parent
}

func (ds *DeduceState) DeduceMatchRule(t_rule *Token, expr_type *Type) *Token {
	ErrorSetToken(t_rule)

	t_pat := t_rule.Children[0]

	t_pat = ds.DeduceMatchPattern(t_pat, expr_type)
	if t_pat == nil {
		return nil
	}
	t_rule.Children[0] = t_pat

	// where clause?
	if len(t_rule.Children) >= 3 {
		t_where := t_rule.Children[2]

		t_where = ds.DeduceNode(t_where, bool_def.ty)
		if t_where == nil {
			return nil
		}
		t_rule.Children[2] = t_where

		if !t_where.Info.ty.IsBool() {
			PostError("where condition must be a bool, got %s", t_where.Info.ty.String())
			return nil
		}
	}

	/* track usage of tags / exhaustive rules */

	if ds.rec.catch_all {
		// this is debatable, but I think this error can be helpful
		// when you have some complex rules.
		PostError("unneeded match rule (previous were exhaustive)")
		return nil
	}

	// ignore rules with a where clause
	has_where := len(t_rule.Children) >= 3

	if !has_where {
		if ds.MatchesAnything(t_pat, expr_type) {
			ds.rec.catch_all = true
			return t_rule
		}

		// keep track of enum/union tags
		if expr_type.Real().base == TYP_Union || expr_type.Real().base == TYP_Enum {
			if ds.TrackTags(t_rule, expr_type) != OKAY {
				return nil
			}
		}
	}

	return t_rule
}

func (ds *DeduceState) TrackTags(t_rule *Token, expr_type *Type) cmError {
	t_pat := t_rule.Children[0]

	switch t_pat.Kind {
	case ND_MatchTags:
		for _, t_tag := range t_pat.Children {
			if ds.rec.tags[t_tag.Str] {
				PostError("bad match: tag %s already handled", t_tag.Str)
				return FAILED
			}
			ds.rec.tags[t_tag.Str] = true
		}

	case ND_MatchConsts:
		for _, t_const := range t_pat.Children {
			// literal numbers/strings are ignored here

			if t_const.Kind == ND_Global {
				gdef := t_const.Info.gdef

				if ds.TrackGlobalDef(gdef, expr_type) != OKAY {
					return FAILED
				}
			}
		}

	case ND_Union:
		if expr_type.Real().base != TYP_Union {
			panic("non union type in a ND_Union pattern")
		}

		tag_id := t_pat.Info.tag_id
		datum_type := expr_type.Real().param[tag_id].ty
		t_datum := t_pat.Children[0]

		if ds.MatchesAnything(t_datum, datum_type) {
			if ds.rec.tags[t_pat.Str] {
				PostError("bad match: tag %s already handled", t_pat.Str)
				return FAILED
			}
			ds.rec.tags[t_pat.Str] = true
		}
	}

	// check whether all tags have been seen yet
	if ds.TrackUnusedTag(expr_type) == "" {
		ds.rec.catch_all = true
	}

	return OKAY
}

func (ds *DeduceState) TrackGlobalDef(gdef *GlobalDef, expr_type *Type) cmError {
	// ignore failed globals or type mismatches here
	if gdef.failed || gdef.ty != nil {
		return OKAY
	}
	if gdef.ty.Real().base != expr_type.Real().base {
		return OKAY
	}

	var_type := gdef.ty

	switch var_type.Real().base {
	case TYP_Enum:
		// FIXME

	case TYP_Union:
		// FIXME for unions with no datum
		// consider the tag handled if it has no datum
		/*
		if var_type.Real().param[tag_id].ty.IsVoid() {
			tag_name := def.ty.param[tag_id].name

			if ds.rec.tags[tag_name] {
				PostError("bad match: tag %s already handled", tag_name)
				return FAILED
			}
			ds.rec.tags[tag_name] = true
		}
		*/
	}

	return OKAY
}

func (ds *DeduceState) TrackUnusedTag(enum_type *Type) string {
	// finds a enum/union tag that has not been definitely used
	// in a match pattern (ones without a where clause).
	// returns "" if none is found.

	for _, par := range enum_type.param {
		if !ds.rec.tags[par.name] {
			return par.name
		}
	}

	return ""
}

func (ds *DeduceState) DeduceMatchPattern(pat *Token, expr_type *Type) *Token {
	// a goal here is to turn all TOK_MatchStruct nodes into more
	// specialized ones, such as ND_Array, ND_Class, etc...

	switch pat.Kind {
	case ND_MatchAll:
		return pat

	case ND_MatchConsts:
		return ds.DeducePattern_Consts(pat, expr_type)

	case ND_MatchTags:
		return ds.DeducePattern_Tags(pat, expr_type)

	case ND_MatchStruct:
		return ds.DeducePattern_Struct(pat, expr_type)

	case ND_MatchVar:
		return ds.DeducePattern_Var(pat, expr_type)

	case ND_Array, ND_Map, ND_Set, ND_Class:
		// since those forms are *only* created here, they don't need to
		// be processed again.
		return pat

	default:
		PostError("cannot deduce match pattern: %s", pat.String())
		return nil
	}
}

func (ds *DeduceState) DeducePattern_Consts(pat *Token, expr_type *Type) *Token {
	// children are either a literal (int/str/float) or an ND_Global.
	// FIXME: review this...
	for i, child := range pat.Children {
		var child2 *Token

		switch child.Kind {
		case ND_Integer, ND_Float, ND_String, ND_Enum:
			child2 = ds.DeduceBasicLiteral(child, expr_type)
			if child2 == nil {
				return nil
			}
			pat.Children[i] = child2

		case ND_Global:
			child2 = ds.DeduceGlobal(child, expr_type)
			if child2 == nil {
				return nil
			}
			pat.Children[i] = child2

		default:
			panic("weird node in ND_MatchConsts")
		}

		child_type := child2.Info.ty

		switch child_type.Real().base {
		case TYP_Int, TYP_Float, TYP_String, TYP_Enum:
			// ok
		default:
			PostError("match constant must be a simple type, got %s",
				child_type.String())
			return nil
		}

		if !child_type.AssignableTo(expr_type) {
			PostError("type mismatch on match constant: wanted %s, got %s",
				expr_type.String(), child_type.String())
			return nil
		}
	}

	return pat
}

func (ds *DeduceState) DeducePattern_Tags(pat *Token, expr_type *Type) *Token {
	switch expr_type.Real().base {
	case TYP_Union, TYP_Enum:
		// ok
	default:
		PostError("expected union/enum type for tag match, got %s", expr_type.String())
		return nil
	}

	for _, t_tag := range pat.Children {
		t_tag.Info.tag_id = expr_type.Real().FindParam(t_tag.Str)
		if t_tag.Info.tag_id < 0 {
			PostError("unknown tag %s in type: %s", t_tag.Str, expr_type.String())
			return nil
		}
	}

	return pat
}

func (ds *DeduceState) DeducePattern_Struct(pat *Token, expr_type *Type) *Token {
	// how we interpret stuff in {} is based on the expression type
	switch expr_type.Real().base {
	case TYP_Array, TYP_ByteVec:
		return ds.DeducePattern_Array(pat, expr_type)

//??	case TYP_Map:
//??		return ds.DeducePattern_Map(pat, expr_type)
//??
//??	case TYP_Set:
//??		return ds.DeducePattern_Set(pat, expr_type)

	case TYP_Class:
		return ds.DeducePattern_Class(pat, "class", expr_type)

	case TYP_Union:
		return ds.DeducePattern_Union(pat, expr_type)

	default:
		PostError("bad match against data in {}, value is %s",
			expr_type.String())
		return nil
	}
}

func (ds *DeduceState) DeducePattern_Array(pat *Token, arr_type *Type) *Token {
	pat.Kind = ND_Array

	elem_type := arr_type.Real().ElemType()

	// WISH: for byte-vec, check if literals are in range 0..255

	for i, child := range pat.Children {
		child2 := ds.DeduceMatchPattern(child, elem_type)
		if child2 == nil {
			return nil
		}
		pat.Children[i] = child2
	}

	return pat
}

func (ds *DeduceState) DeducePattern_Set(pat *Token, set_type *Type) *Token {
	pat.Kind = ND_Set

	key_type := set_type.Real().KeyType()

	for i, child := range pat.Children {
		if child.Kind == ND_MatchVar {
			PostError("bad pattern: cannot bind a set key to a variable")
			return nil
		}

		child2 := ds.DeduceMatchPattern(child, key_type)
		if child2 == nil {
			return nil
		}
		pat.Children[i] = child2
	}

	return pat
}

func (ds *DeduceState) DeducePattern_Map(pat *Token, map_type *Type) *Token {
	pat.Kind = ND_Map

	if ds.CollectMapPairs(pat) != OKAY {
		return nil
	}

	key_type  := map_type.Real().KeyType()
	elem_type := map_type.Real().ElemType()

	for _, pair := range pat.Children {
		t_key  := pair.Children[0]
		t_elem := pair.Children[1]

		if t_key.Kind == ND_MatchVar {
			PostError("bad pattern: cannot bind a map key to a variable")
			return nil
		}
		if t_key.Kind == ND_MatchAll && t_elem.Kind != ND_MatchAll {
			PostError("bad map pattern: key is '_' but value isn't")
			return nil
		}

		t_key  = ds.DeduceMatchPattern(t_key,  key_type)
		t_elem = ds.DeduceMatchPattern(t_elem, elem_type)

		pair.Children[0] = t_key
		pair.Children[1] = t_elem
	}

	return pat
}

func (ds *DeduceState) DeducePattern_Class(pat *Token, what string, class_type *Type) *Token {
	pat.Kind = ND_Class

	// an empty `{}` without `...` can never match anything
	if len(pat.Children) == 0 && pat.Info.open == 0 {
		PostError("empty pattern in {} for %s match", what)
		return nil
	}

	children := pat.Children
	pat.Children = make([]*Token, 0)

	// size check
	total := len(children)

	if pat.Info.open == 0 {
		if total != len(class_type.Real().param) {
			PostError("wrong size for %s pattern: wanted %d, got %d",
				what, len(class_type.Real().param), total)
			return nil
		}
	} else {
		if total > len(class_type.Real().param) {
			PostError("wrong size for %s pattern: wanted %d (or less), got %d",
				what, len(class_type.Real().param), total)
			return nil
		}
	}

	// handle each field
	for i, sub := range children {
		field_idx  := i
		t_value    := sub

		if pat.Info.open < 0 {
			field_idx = len(class_type.Real().param) - total + i
		}

		if sub.Kind == ND_Field {
			t_value = sub.Children[0]

			field_idx = class_type.Real().FindParam(sub.Str)
			if field_idx < 0 {
				PostError("no such field %s in %s", sub.Str, class_type.String())
				return nil
			}
		}

		field_name := class_type.Real().param[field_idx].name
		field_type := class_type.Real().param[field_idx].ty

		t_val2 := ds.DeduceMatchPattern(t_value, field_type)
		if t_val2 == nil {
			return nil
		}

		// we need each field to be an ND_Field from here on, and
		// the compiling code relies on Info.field being valid.

		sub2 := NewNode(ND_Field, sub.Pos)
		sub2.Str = field_name
		sub2.Info.ty    = field_type
		sub2.Info.field = field_idx
		sub2.Add(t_val2)

		pat.Add(sub2)
	}

	return pat
}

func (ds *DeduceState) DeducePattern_Union(pat *Token, union_type *Type) *Token {
	pat.Kind = ND_Union

	if pat.Info.open != 0 {
		PostError("bad match: cannot use '...' in union pattern")
		return nil
	}

	children := pat.Children

	if len(children) == 0 {
		PostError("empty pattern in {} for union match")
		return nil
	}

	t_tag := children[0]
	if t_tag.Kind == ND_MatchAll {
		PostError("bad union pattern: cannot use '_' for tag")
		return nil

	} else if t_tag.Kind != ND_MatchTags || len(t_tag.Children) != 1 {
		PostError("bad union pattern: expected tag as first element")
		return nil
	}

	// find tag in union type
	tag_name := t_tag.Children[0].Str

	tag_id := union_type.Real().FindParam(tag_name)
	if tag_id < 0 {
		PostError("unknown union tag %s in type: %s", tag_name, union_type.String())
		return nil
	}

	// compiler needs tag name/number in this ND_Union node
	pat.Str = tag_name
	pat.Info.tag_id = tag_id
	pat.Children = make([]*Token, 0)

	// check presence/absence of the datum in the pattern
	datum_type := union_type.Real().param[tag_id].ty

	if len(children) < 2 && !datum_type.IsVoid() {
		PostError("bad union pattern: missing datum after tag")
		return nil
	}
	if len(children) > 2 {
		PostError("extra rubbish at end of union pattern")
		return nil
	}

	// if datum is void, it should be absent (or the match-all '_')
	var t_datum *Token

	if datum_type.IsVoid() {
		if len(children) > 1 && children[1].Kind != ND_MatchAll {
			PostError("bad union pattern: %s is void, but got datum pattern",
				tag_name)
			return nil
		}
		t_datum = NewNode(ND_MatchAll, pat.Pos)

	} else {
		t_datum = children[1]

		t_datum = ds.DeduceMatchPattern(t_datum, datum_type)
		if t_datum == nil {
			return nil
		}

		// TODO: it is not clear that this updates the original node
		children[1] = t_datum
	}

	pat.Add(t_datum)
	return pat
}

func (ds *DeduceState) DeducePattern_Var(pat *Token, expr_type *Type) *Token {
	pat.Info.lvar.ty = expr_type
	return pat
}

func (ds *DeduceState) MatchesAnything(pat *Token, expr_type *Type) bool {
	switch pat.Kind {
	case ND_MatchAll, ND_MatchVar:
		return true

	case ND_Array, ND_Map, ND_Set:
		// if pattern has no '...' then it only matches a fixed sized.
		if pat.Info.open == 0 {
			return false
		}

		elem_type := expr_type.Real().ElemType()

		for _, child := range pat.Children {
			if !ds.MatchesAnything(child, elem_type) {
				return false
			}
		}
		return true

	case ND_Class:
		// without '...', the pattern must be same size as class
		if pat.Info.open == 0 {
			if len(expr_type.Real().param) != len(pat.Children) {
				return false
			}
		}
		for _, child := range pat.Children {
			t_field    := child.Children[0]
			field_type := child.Info.ty

			if !ds.MatchesAnything(t_field, field_type) {
				return false
			}
		}
		return true

	case ND_MapElem:
		t_key  := pat.Children[0]
		t_elem := pat.Children[1]

		return (t_key.Kind == ND_MatchAll) && (t_elem.Kind == ND_MatchAll)

	default:
		return false
	}
}
