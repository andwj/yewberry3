// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

// import "fmt"
import "math"
import "strconv"

type EvalState struct {
	cl   *Closure
	gdef *GlobalDef

	vars []*LocalVar
}

func EvaluateClosure(cl *Closure) cmError {
	es := new(EvalState)
	es.cl = cl
	es.vars = make([]*LocalVar, 0)

	new_body := es.EvalNode(cl.t_body)
	if new_body == nil {
		cl.failed = true
		return FAILED
	}

	cl.t_body = new_body
	return OKAY
}

func EvaluateVar(gdef *GlobalDef) cmError {
	es := new(EvalState)
	es.gdef = gdef
	es.vars = make([]*LocalVar, 0)

	// mark it as evaluated.
	// done _before_ actually doing it, just to safeguard against
	// infinite looping (which should never happen since the deduce
	// logic will catch cyclic usage of simply-typed vars).
	gdef.eval = true

	new_expr := es.EvalNode(gdef.expr)
	if new_expr == nil {
		gdef.failed = true
		return FAILED
	}

	gdef.expr = new_expr
	return OKAY
}

//----------------------------------------------------------------------

// EvalNode will traverse the node tree looking for const-exprs,
// and it evaluates those constant expressions and replaces their
// nodes with new ones.
//
// when evaluating a global variable (i.e. not a function), it also
// checks that everything is a valid "const expr", i.e. nothing
// requires code to compute its value
//
// returns NIL if an error was raised.
//
func (es *EvalState) EvalNode(t *Token) *Token {
	ErrorSetToken(t)

	switch t.Kind {
	case ND_Integer, ND_Float, ND_Enum, ND_String:
		// basic literal is always ok
		return t

	case ND_Cast:
		return es.EvalCast(t)

	case ND_Global:
		return es.EvalGlobal(t)

	case ND_AllocObj:
		// an empty union literal (etc) is ok
		return t

	case ND_Construct:
		return es.EvalConstruct(t)

	case ND_StrBuild:
		return es.EvalStrBuild(t)

	case ND_Block:
		if len(t.Children) == 1 {
			return es.EvalBlock(t)
		}

	case ND_If:
		return es.EvalIf(t)

	case ND_FunCall:
		return es.EvalFunCall(t)

	case ND_MethodCall:
		return es.EvalMethod(t)
	}

	// all other node kinds will be code, not data-ish.

	if es.EvalChildren(t) == nil {
		return nil
	}

	return es.Code(t)
}

func (es *EvalState) EvalChildren(t *Token) *Token {
	// handle the node in a general way, just do the children

	for i, child := range t.Children {
		child2 := es.EvalNode(child)
		if child2 == nil {
			return nil
		}
		t.Children[i] = child2
	}
	return t
}

func (es *EvalState) Code(t *Token) *Token {
	if es.gdef != nil {
		PostError("expected a constant expr, got: %s", t.String())
		return nil
	}
	return t
}

func (es *EvalState) IsConstLit(t *Token) bool {
	switch t.Kind {
	case ND_Integer, ND_Float, ND_Enum, ND_String:
		return true

	default:
		return false
	}
}

func (es *EvalState) EvalCast(t *Token) *Token {
	child := es.EvalNode(t.Children[0])
	if child == nil {
		return nil
	}

	if es.IsConstLit(child) {
		// just update the type information
		child.Info.ty = t.Info.ty
		return child
	}

	// if child is also a cast operation, simplify the tree
	if child.Kind == ND_Cast {
		child = child.Children[0]
	}

	t.Children[0] = child
	return t
}

func (es *EvalState) EvalGlobal(t *Token) *Token {
	gdef := t.Info.gdef

	// TODO review if `gdef.is_fun` needs special treatment

	if gdef.failed {
		return t
	}

	// we cannot inline a mutable variable
	if gdef.mutable {
		if es.gdef != nil {
			PostError("const expression uses mutable variable %s", gdef.name)
			return nil
		}
		return t
	}

	if gdef.ty == nil {
		panic("GLOBAL HAS NO TYPE: " + gdef.name)
	}

	// for a basic type, inline the value when possible
	if !gdef.is_fun &&
		(gdef.ty.IsBasic() || gdef.ty.Real().base == TYP_Function) &&
		(es.cl == nil || Options.optim_level > 0) {

		if !gdef.eval {
			if EvaluateVar(gdef) != OKAY {
				return nil
			}
		}
		return gdef.expr
	}

	return t
}

func (es *EvalState) EvalConstruct(t *Token) *Token {
	// the body of the ND_Construct is a block with:
	//    (a) ND_MethodCall with `.append` or `.set-elem`
	//    (b) ND_SetField
	//    (c) ND_SetUnion
	block := t.Children[2]

	if block.Kind != ND_Block { panic("weird ND_Construct") }

	for _, child := range block.Children {
		switch child.Kind {
		case ND_SetField:
			val := es.EvalNode(child.Children[1])
			if val == nil {
				return nil
			}
			child.Children[1] = val

		case ND_SetUnion:
			val := es.EvalNode(child.Children[1])
			if val == nil {
				return nil
			}
			child.Children[1] = val
			return t

		case ND_MethodCall:
			// skip method name and the global var
			for i := 2; i < len(child.Children); i++ {
				val := es.EvalNode(child.Children[i])
				if val == nil {
					return nil
				}
				child.Children[i] = val
			}

		default:
			panic("weird compound literal")
		}
	}

	return t
}

func (es *EvalState) EvalStrBuild(t *Token) *Token {
	final_str := ""
	all_const := true

	for i, child := range t.Children {
		child2 := es.EvalNode(child)
		if child2 == nil {
			return nil
		}
		t.Children[i] = child2

		if child2.Kind == ND_String {
			final_str = final_str + child2.Str
		} else {
			all_const = false
		}
	}

	if all_const {
		t_str := NewNode(ND_String, t.Pos)
		t_str.Info.ty = t.Info.ty
		t_str.Str = final_str
		return t_str
	}

	return es.Code(t)
}

func (es *EvalState) EvalBlock(t *Token) *Token {
	child := es.EvalNode(t.Children[0])
	if child == nil {
		return nil
	}

	if es.IsConstLit(child) {
		return child
	}

	return es.Code(t)
}

func (es *EvalState) EvalIf(t *Token) *Token {
	t_cond := es.EvalNode(t.Children[0])
	t_then := es.EvalNode(t.Children[1])
	t_else := es.EvalNode(t.Children[2])

	if t_cond == nil || t_then == nil || t_else == nil {
		return nil
	}

	// is the condition a constant?
	if t_cond.Kind == ND_Enum {
		if t_cond.Info.tag_id >= 1 {
			return t_then
		} else {
			return t_else
		}
	}

	t.Children[0] = t_cond
	t.Children[1] = t_then
	t.Children[2] = t_else

	return es.Code(t)
}

func (es *EvalState) EvalFunCall(t *Token) *Token {
	if es.gdef != nil {
		t_func := t.Children[0]
		if t_func.Kind == ND_Global && t_func.Info.gdef.is_fun {
			PostError("unsupported function in const expr: %s",
				t_func.Info.gdef.name)
			return nil
		}
	}
	return es.EvalChildren(t)
}

func (es *EvalState) EvalMethod(t *Token) *Token {
	t_method := t.Children[0]

	if len(t.Children) == 2 {
		return es.EvalUnaryOperator(t)
	}
	if len(t.Children) == 3 {
		return es.EvalBinaryOperator(t)
	}

	if es.gdef != nil {
		PostError("unsupported method in const expr: %s", t_method.Str)
		return nil
	}

	return es.EvalChildren(t)
}

func (es *EvalState) EvalUnaryOperator(t *Token) *Token {
	name := t.Children[0].Str
	val  := t.Children[1]

	val = es.EvalNode(val)
	if val == nil {
		return nil
	}
	t.Children[1] = val

	if val.Info.ty.IsBool() {
		// check for known name
		switch name {
		case ".not", ".str":
		default: goto unknown
		}

		if val.Kind != ND_Enum {
			return es.Code(t)
		}

		V := (val.Info.tag_id >= 1)

		switch name {
		case ".not":
			return es.Result_Bool(t, ! V)

		case ".str":
			if V {
				return es.Result_Str(t, "TRUE")
			} else {
				return es.Result_Str(t, "FALSE")
			}

		default:
			panic("missed a method")
		}
	}

	if val.Info.ty.IsChar() {
		if name != ".str" {
			goto unknown
		}
		if val.Kind != ND_Integer {
			return es.Code(t)
		}

		I, _ := DecodeInteger(val.Str)
		r  := make([]rune, 1)
		r[0] = rune(I)

		return es.Result_Str(t, string(r))
	}

	switch val.Info.ty.Real().base {
	case TYP_Int:
		// check for known name
		switch name {
		case ".neg", ".abs", ".flip",
			".int", ".float", ".str":
		default: goto unknown
		}

		if val.Kind != ND_Integer {
			return es.Code(t)
		}

		// Note: deduce code has validated the integer
		V, _ := DecodeInteger(val.Str)

		switch name {
		case ".neg":
			return es.Result_Int(t, 0 - V)

		case ".abs":
			if V < 0 { V = -V }
			return es.Result_Int(t, V)

		case ".flip":
			return es.Result_Int(t, ^ V)

		case ".int":
			return es.Result_Int(t, V)

		case ".float":
			if V == math.MinInt64 /* INAN */ {
				PostError("bad integer to convert to float: INAN")
				return nil
			}
			return es.Result_Float(t, float64(V))

		case ".str":
			str := strconv.FormatInt(V, 10)
			return es.Result_Str(t, str)

		default:
			panic("missed a method")
		}

	case TYP_Float:
		// NOTE: the trigonometric functions like `sin`, `atan` (etc)
		//       and a few others are not supported, the line needs to
		//       be drawn somewhere are I draw it there.

		// check for known name
		switch name {
		case ".neg", ".abs", ".sqrt",
			".floor", ".ceil", ".round", ".trunc",
			".float", ".int", ".str":
		default: goto unknown
		}

		// Note: deduce code has validated the float
		// TODO handle +INF, -INF, NAN
		V, _ := strconv.ParseFloat(val.Str, 64)

		switch name {
		case ".neg":
			return es.Result_Float(t, 0.0 - V)

		case ".abs":
			if V < 0 { V = -V }
			return es.Result_Float(t, V)

		case ".sqrt":
			if V < 0 {
				PostError("negative number for .sqrt")
				return nil
			}
			return es.Result_Float(t, math.Sqrt(V))

		case ".floor":
			return es.Result_Float(t, math.Floor(V))

		case ".ceil":
			return es.Result_Float(t, math.Ceil(V))

		case ".round":
			return es.Result_Float(t, math.Round(V))

		case ".trunc":
			return es.Result_Float(t, math.Trunc(V))

		case ".float":
			return es.Result_Float(t, V)

		case ".int":
			const limit = 9.223372036854775e+18
			if math.IsNaN(V) {
				PostError("bad float to convert to integer: NAN")
				return nil
			}
			if math.IsInf(V, 0) || V > limit || V < -limit {
				PostError("float constant too large for an integer")
				return nil
			}
			return es.Result_Int(t, int64(V))

		case ".str":
			str := strconv.FormatFloat(V, 'g', -1, 64)
			return es.Result_Str(t, str)

		default:
			panic("missed a method")
		}

	case TYP_String:
		if name != ".str" {
			goto unknown
		}
		if val.Kind != ND_String {
			return es.Code(t)
		}
		return es.Result_Str(t, val.Str)
	}

unknown:
	if es.gdef != nil {
		PostError("unsupported method in const expr: %s", name)
		return nil
	}
	return t
}

func (es *EvalState) EvalBinaryOperator(t *Token) *Token {
	name := t.Children[0].Str

	L := es.EvalNode(t.Children[1])
	R := es.EvalNode(t.Children[2])

	if L == nil || R == nil {
		return nil
	}

	t.Children[1] = L
	t.Children[2] = R

	if L.Info.ty.IsBool() {
		// check for known name
		switch name {
		case ".eq?", ".ne?":
		default: goto unknown
		}

		if L.Kind != ND_Enum || R.Kind != ND_Enum {
			return es.Code(t)
		}

		LV := (L.Info.tag_id >= 1)
		RV := (R.Info.tag_id >= 1)

		switch name {
		case ".eq?":
			return es.Result_Bool(t, LV == RV)
		case ".ne?":
			return es.Result_Bool(t, LV != RV)
		default:
			panic("missed a method")
		}
	}

	switch L.Info.ty.Real().base {
	case TYP_Int:
		// check for known name
		switch name {
		case ".add", ".sub", ".mul", ".div", ".rem", ".mod", ".pow",
			".or", ".and", ".xor", ".left", ".right",
			".eq?", ".lt?", ".gt?", ".ne?", ".le?", ".ge?":
		default: goto unknown
		}

		// WISH: strength reduce when R is ND_Integer and we
		//       have `.mul` with a power of two (etc...), or
		//       even `.add` or `.sub` with a zero.

		if L.Kind != ND_Integer || R.Kind != ND_Integer {
			return es.Code(t)
		}

		// Note: deduce code has validated the integers
		LV, _ := DecodeInteger(L.Str)
		RV, _ := DecodeInteger(R.Str)

		switch name {
		case ".add":
			return es.Result_Int(t, LV + RV)

		case ".sub":
			return es.Result_Int(t, LV - RV)

		case ".mul":
			return es.Result_Int(t, LV * RV)

		case ".div":
			if RV == 0 {
				PostError("division by zero in const expression")
				return nil
			}
			return es.Result_Int(t, LV / RV)

		case ".rem":
			if RV == 0 {
				PostError("division by zero in const expression")
				return nil
			}
			return es.Result_Int(t, LV % RV)

		case ".mod":
			if RV == 0 {
				PostError("division by zero in const expression")
				return nil
			}
			return es.Result_Int(t, IntegerModulo(LV, RV))

		case ".pow":
			res, err2 := IntegerPower(LV, RV)
			if err2 != OKAY {
				return nil
			}
			return es.Result_Int(t, res)

		case ".or":
			return es.Result_Int(t, LV | RV)

		case ".and":
			return es.Result_Int(t, LV & RV)

		case ".xor":
			return es.Result_Int(t, LV ^ RV)

		case ".left":
			if RV > 0 { LV = LV << uint( RV) }
			if RV < 0 { LV = LV >> uint(-RV) }
			return es.Result_Int(t, LV)

		case ".right":
			if RV > 0 { LV = LV >> uint( RV) }
			if RV < 0 { LV = LV << uint(-RV) }
			return es.Result_Int(t, LV)

		case ".eq?":
			return es.Result_Bool(t, LV == RV)

		case ".lt?":
			return es.Result_Bool(t, LV < RV)

		case ".gt?":
			return es.Result_Bool(t, LV > RV)

		case ".ne?":
			return es.Result_Bool(t, LV != RV)

		case ".le?":
			return es.Result_Bool(t, LV <= RV)

		case ".ge?":
			return es.Result_Bool(t, LV >= RV)

		default:
			panic("missed a method")
		}

	case TYP_Float:
		// check for known name
		switch name {
		case ".add", ".sub", ".mul", ".div", ".rem", ".mod", ".pow",
			".eq?", ".lt?", ".gt?", ".ne?", ".le?", ".ge?":
		default: goto unknown
		}

		if L.Kind != ND_Float || R.Kind != ND_Float {
			return es.Code(t)
		}

		// Note: deduce code has validated the floats
		// TODO handle +INF, -INF, NAN
		LV, _ := strconv.ParseFloat(L.Str, 64)
		RV, _ := strconv.ParseFloat(R.Str, 64)

		switch name {
		case ".add":
			return es.Result_Float(t, LV + RV)

		case ".sub":
			return es.Result_Float(t, LV - RV)

		case ".mul":
			return es.Result_Float(t, LV * RV)

		case ".div":
			if RV == 0 {
				PostError("division by zero in const expression")
				return nil
			}
			return es.Result_Float(t, LV / RV)

		case ".rem":
			if RV == 0 {
				PostError("division by zero in const expression")
				return nil
			}
			return es.Result_Float(t, math.Mod(LV, RV))

		case ".mod":
			if RV == 0 {
				PostError("division by zero in const expression")
				return nil
			}
			LV = LV - math.Floor(LV / RV) * RV
			return es.Result_Float(t, LV)

		case ".pow":
			if RV < 0 {
				PostError("negative exponent to ** operator")
				return nil
			}
			return es.Result_Float(t, math.Pow(LV, RV))

		case ".eq?":
			return es.Result_Bool(t, LV == RV)

		case ".lt?":
			return es.Result_Bool(t, LV < RV)

		case ".gt?":
			return es.Result_Bool(t, LV > RV)

		case ".ne?":
			return es.Result_Bool(t, LV != RV)

		case ".le?":
			return es.Result_Bool(t, LV <= RV)

		case ".ge?":
			return es.Result_Bool(t, LV >= RV)

		default:
			panic("missed a method")
		}

	case TYP_String:
		if name != ".add" {
			goto unknown
		}
		if L.Kind != ND_String || R.Kind != ND_String {
			return es.Code(t)
		}
		return es.Result_Str(t, L.Str + R.Str)
	}

unknown:
	if es.gdef != nil {
		PostError("unsupported method in const expr: %s", name)
		return nil
	}
	return t
}

func (es *EvalState) Result_Int(meth *Token, val int64) *Token {
	t := NewNode(ND_Integer, meth.Pos)
	t.Info.ty = meth.Info.ty
	t.Str  = strconv.FormatInt(val, 10)
	return t
}

func (es *EvalState) Result_Float(meth *Token, val float64) *Token {
	t := NewNode(ND_Float, meth.Pos)
	t.Info.ty = meth.Info.ty
	t.Str  = strconv.FormatFloat(val, 'g', -1, 64)
	return t
}

func (es *EvalState) Result_Str(meth *Token, s string) *Token {
	t := NewNode(ND_String, meth.Pos)
	t.Info.ty = meth.Info.ty
	t.Str = s
	return t
}

func (es *EvalState) Result_Bool(meth *Token, val bool) *Token {
	t := NewNode(ND_Enum, meth.Pos)
	t.Info.ty = meth.Info.ty

	if val {
		t.Str = "`TRUE"
		t.Info.tag_id = 1
	} else {
		t.Str = "`FALSE"
		t.Info.tag_id = 0
	}
	return t
}

//----------------------------------------------------------------------

func DecodeInteger(s string) (int64, cmError) {
	// we allow integers that technically overflow a signed 64-bit
	// integer but fit into an unsigned 64-bit integer.  they are
	// written as positive but wrap around to a negative value.

	i, err := strconv.ParseInt(s, 0, 64)
	if err == nil {
		return i, OKAY
	}

	// try unsigned version
	u, err := strconv.ParseUint(s, 0, 64)
	if err == nil {
		return int64(u), OKAY
	}

	PostError("invalid integer constant: %s", s)
	return 0, FAILED
}

func IntegerModulo(num, div int64) int64 {
	// this implements a modulo which uses the *floor* of the
	// integer division i.e. as if the division was done with
	// infinite precision, then rounded down toward -infinity.
	//
	// examples:  5 mod  3 =  2
	//           -5 mod  3 =  1
	//            5 mod -3 = -1
	//           -5 mod -3 = -2

	// parent should check for division by zero
	if div == 0 {
		return 0
	}

	// WISH: a purely integer way to calculate this
	n := float64(num)
	d := float64(div)

	tmp := n - math.Floor(n/d)*d

	return int64(tmp)
}

func IntegerPower(a, b int64) (int64, cmError) {
	// handle some special cases...
	switch {
	case b < 0:
		PostError("negative exponent to ** operator")
		return 0, FAILED

	case b == 0:
		return 1, OKAY

	case b == 1:
		return a, OKAY

	case a == 0 || a == 1:
		return a, OKAY

	case a == -1:
		if (b % 2) == 0 {
			return 1, OKAY
		} else {
			return -1, OKAY
		}
	}

	// here we have abs(a) >= 2 && b >= 2.
	// hence any b >= 64 will definitely overflow an int64.

	if b >= 64 {
		PostError("overflow with ** operator")
		return 0, FAILED
	}

	res := int64(a)

	for i := b; i > 1; i-- {
		newval := res * int64(a)

		// check for overflow
		if newval / int64(a) != res {
			PostError("overflow with ** operator")
			return 0, FAILED
		}

		res = newval
	}

	return res, OKAY
}
