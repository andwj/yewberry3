// Copyright 2018 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
import "math"

func FloatToInt(v float64) int64 {
	const limit = 9.223372036854775e+18

	if v > limit {
		return math.MaxInt64
	}
	if v < -limit {
		return math.MinInt64
	}

	return int64(v)
}

//----------------------------------------------------------------------

type Formatter struct {
	before string // NOTE: only used by parser
	after  string //

	verb  rune   // letter, or 0 if string had no format directive
	field string // field width, "" if not given
	prec  string // precision, "" if not given

	plus  bool // '+' flag in YB, Go and C
	minus bool // '-' flag in YB, ' ' in Go/C
	quote bool // ''' flag in YB

	left  bool // '<' flag in YB, '-' in Go/C
	right bool // '>' flag in YB
	zeros bool // '0' at start of field width
}

func FormatDecode(s string) (*Formatter, error) {
	r := []rune(s)
	f := new(Formatter)

	pos := 0

	for pos < len(r) {
		ch := r[pos]
		pos++

		if ch != '%' {
			if f.verb == 0 {
				f.before += string(ch)
			} else {
				f.after += string(ch)
			}
			continue
		}

		if pos >= len(r) {
			return nil, fmt.Errorf("bad format str: directive has no verb")
		}

		// handle percent escape
		if r[pos] == '%' {
			if f.verb == 0 {
				f.before += string(ch)
			} else {
				f.after += string(ch)
			}
			pos++ // skip extra percent
			continue
		}

		if f.verb != 0 {
			return nil, fmt.Errorf("bad format str: multiple directives found")
		}

		/* parse the directive */

		do_field := false
		do_prec := false

		for pos < len(r) {
			ch := r[pos]
			pos++

			digit := ('0' <= ch && ch <= '9')

			if digit {
				if do_field {
					f.field += string(ch)
					continue
				}
				if do_prec {
					f.prec += string(ch)
					continue
				}
			}

			// stop parsing field width or precision
			if do_field && f.field == "" {
				f.field = "0"
			}
			if do_prec && f.prec == "" {
				f.prec = "0"
			}
			do_field = false
			do_prec = false

			// found the verb?
			if ('a' <= ch && ch <= 'z') || ('A' <= ch && ch <= 'Z') {
				switch ch {
				case 'b', 'c', 'd', 'e', 'f', 'g' /* REVIEW */:
					// ok
				case 'o', 'p', 's', 't', 'x', 'X':
					// ok
				default:
					return nil, fmt.Errorf("bad format str: unknown verb %%%c", ch)
				}

				f.verb = ch
				break
			}

			// begin field width?
			if digit {
				if f.field != "" {
					return nil, fmt.Errorf("bad format str: more than one field width")
				}
				do_field = true

				if ch == '0' {
					f.zeros = true
				} else {
					f.field += string(ch)
				}
				continue
			}

			// begin precision?
			if ch == '.' {
				if f.prec != "" {
					return nil, fmt.Errorf("bad format str: more than one precision")
				}
				do_prec = true
				continue
			}

			// everything else is a flag
			switch ch {
			case '+':
				f.plus = true
			case '-':
				f.minus = true
			case '\'':
				f.quote = true
			case '<':
				f.left = true
			case '>':
				f.right = true
			default:
				return nil, fmt.Errorf("bad format str: unknown flag %q", ch)
			}
		}

		if f.verb == 0 {
			return nil, fmt.Errorf("bad format str: directive has no verb")
		}
	}

	return f, Ok
}

func (f *Formatter) CheckType(ty *Type) cmError {
	real_base := ty.Real().base

	switch f.verb {
	case 'b', 'c', 'd', 'o', 'x', 'X':
		if real_base != TYP_Int {
			PostError("incompatible types for %%%c: wanted int, got %s",
				f.verb, real_base.String())
			return FAILED
		}

	case 'e', 'f', 'g':
		if real_base != TYP_Float {
			PostError("incompatible types for %%%c: wanted float, got %s",
				f.verb, real_base.String())
			return FAILED
		}

	case 's':
		if real_base != TYP_String {
			PostError("incompatible types for %%%c: wanted str, got %s",
				f.verb, real_base.String())
			return FAILED
		}

	case 't':
		if !(real_base == TYP_Union || real_base == TYP_Enum) {
			PostError("incompatible types for %%%c: wanted union, got %s",
				f.verb, real_base.String())
			return FAILED
		}

	case 'p':
		if real_base < TYP_Array {
			PostError("incompatible types for %%%c: wanted a ref type, got %s",
				f.verb, real_base.String())
			return FAILED
		}
	}

	return OKAY
}

func (f *Formatter) Encode() string {
	// NOTE: the following is only suitable for Go's fmt package

	verb := string(f.verb)

	if f.quote {
		switch f.verb {
		case 'b': verb = "#b"
		case 'o': verb = "#o"
		case 't': // TODO
		case 'x': verb = "#x"
		case 'X': verb = "#X"
		case 's', 'c': verb = "q"
		}
	}

	res := "%"

	if f.plus {
		res += "+"
	} else if f.minus {
		res += " "
	}
	if f.left {
		res += "-"
	}

	if f.field != "" {
		if f.zeros {
			res += "0"
		}
		res += f.field
	}
	if f.prec != "" {
		res += "." + f.prec
	}

	res += verb

	return res
}
