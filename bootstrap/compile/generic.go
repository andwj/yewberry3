// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
import "strings"
import "sort"

var instantiations map[string]*Type
var expansions     map[string]*Type

// Gen_Instantiate takes a generic(-ish) type, like (array @T) or a
// used-defined type, and constructs a concrete type with all the
// generic place-holders replaced by their entry in `tgroup`.
// returns NIL if an error was produced.
func Gen_Instantiate(gen_type *Type, tgroup map[string]*Type) *Type {
	// a pure `@XX` name
	if gen_type.base == TYP_TypeParam {
		name := gen_type.tp_name
		if tgroup[name] == nil {
			PostError("cannot instantiate generic type, '%s' is unknown", name)
			return nil
		}
		return tgroup[name]
	}

	// anything which is not wrapped in TYP_Generic is concrete
	// and can be used as-is.  that includes TYP_Usage.
	if gen_type.base != TYP_Generic {
		return gen_type
	}

	template := gen_type.impl

	// find an existing instantiation...
	morph_str := gen_type.MorphString() + " [ "
	morph_str += Gen_MorphString(tgroup) + " ]"

// DEBUG
//	println("morph_str:", morph_str)

	exist := instantiations[morph_str]
	if exist != nil {
		return exist
	}

	new_type := NewType(template.base)

	if template.base == TYP_Usage {
		new_type.using = template.using
	}

	// need to add it NOW to the instantiations table
	// [ BEFORE we recurse into the components... ]
	instantiations[morph_str] = new_type

//??	// remember the type parameters
//??	// [ needed to monomorphize methods on a generic receiver ]
//??	new_type.tgroup = tgroup

	for _, par := range template.param {
		par_type := Gen_Instantiate(par.ty, tgroup)
		if par_type == nil {
			return nil
		}

		// disallow `void` except where it makes sense
		// FIXME : this seems like the wrong place, REVIEW
		switch new_type.base {
		case TYP_Array, TYP_Class, TYP_Function:
			if par_type.IsVoid() {
				PostError("cannot use void in %s type", new_type.base.String())
				return nil
			}
		}

		new_type.AddParam(par.name, par_type)
	}
	if template.ret != nil {
		ret_type := Gen_Instantiate(template.ret, tgroup)
		if ret_type == nil {
			return nil
		}
		new_type.ret = ret_type
	}

	// expand a TYP_Usage now, once its parameters have been done.
	if new_type.base == TYP_Usage {
		if Gen_Expand(new_type, nil) != OKAY {
			return nil
		}
	}

//--	// construct a nicer name for error messages
//--	Gen_NiceName(new_type, gen_type)

	return new_type
}

// Gen_Expand visits a type tree (possibly cyclic) and instantiates
// each TYP_Usage it finds which has not been instantiated yet.
// this process is similar to macro expansion, though here we can
// re-use any previous expansion of the same type + params
// (which *normally* prevents an infinite recursion).
func Gen_Expand(ty *Type, seen map[*Type]bool) cmError {
	if seen == nil {
		seen = make(map[*Type]bool)
	}
	if seen[ty] {
		return OKAY
	}
	seen[ty] = true

	switch ty.base {
	case TYP_Usage:
		def := ty.using

		if !def.generic {
			panic("TYP_Usage with a non-generic type")
		}
		if def.ty == nil {
			panic("unfinished generic type in Gen_Expand")
		}

		// already done?
		if ty.impl != nil {
			return OKAY
		}

		// grab the parameters given to the TYP_Usage and convert
		// to a form usable by Gen_Instantiate.  we also expand
		// any of the parameters as we go.
		egroup := make(map[string]*Type)

		for _, par := range ty.param {
			// FIXME not 100% sure this is valid
			if Gen_Expand(par.ty, seen) != OKAY {
				return FAILED
			}
			egroup[par.name] = par.ty
		}

//--		// check for existing expansion
//--		ex_str := ty.MorphString() + " [ "
//--		ex_str += Gen_MorphString(egroup) + " ]"
//--
//--		println("expand_str:", ex_str)
//--
//--		exist := expansions[ex_str]
//--		if exist != nil {
//--			ty.impl = exist
//--			return OKAY
//--		}

		// ok, perform expansion.
		// Note that this will find and re-use a previous expansion.
		ty.impl = Gen_Instantiate(def.ty, egroup)
		if ty.impl == nil {
			return FAILED
		}
		return OKAY

	case TYP_Custom, TYP_Generic, TYP_Interface:
		// nothing needed
		return OKAY

	case TYP_Void, TYP_Int, TYP_Float, TYP_String, TYP_Enum, TYP_ByteVec:
		// nothing to change
		return OKAY

	default:
		for _, par := range ty.param {
			if Gen_Expand(par.ty, seen) != OKAY {
				return FAILED
			}
		}
		if ty.ret != nil {
			if Gen_Expand(ty.ret, seen) != OKAY {
				return FAILED
			}
		}
		return OKAY
	}
}

// Gen_AssignableTo takes a concrete source type in `src` and checks
// whether it can be assigned to the TYP_Generic type in `dest`.
// The `msg_name` field is the name of the parameter (etc) and is
// shown in error messages.  It should include the word "parameter".
//
// Each pure `@XX` name in the target will match any type (unless it
// is constrained) and its concrete type is stored in the provided map,
// which can be used to instantiate the generic function, e.g. via
// Gen_Monomorphize.
func Gen_AssignableTo(src, dest *Type, msg_name string, tgroup map[string]*Type) error {
	if src == nil || dest == nil {
		panic("Gen_AssignableTo with nil type")
	}
	if src.base == TYP_Generic || src.base == TYP_TypeParam {
		panic("weird src in Gen_AssignableTo")
	}

	// primary logic for generic assignment, allow a type parameter
	// like `@X` to match any type, but ensure that when there are
	// multiple sources their types are consistent.

	if dest.base == TYP_TypeParam {
		exist := tgroup[dest.tp_name]

		if exist != nil && exist != src {
			return fmt.Errorf("mismatch on type parameter %s: got %s and %s",
				dest.tp_name, exist.String(), src.String())
		}

		tgroup[dest.tp_name] = src
		return Ok
	}

	if dest.base != TYP_Generic {
		if !src.AssignableTo(dest) {
			goto mismatch
		}
		return Ok
	}

	// from here on, `dest` is the template
	dest = dest.impl

	if src.base != dest.base {
		goto mismatch
	}

	if len(src.param) != len(dest.param) {
		goto mismatch
	}

	for i := range src.param {
		err := Gen_AssignableTo(src.param[i].ty, dest.param[i].ty, msg_name, tgroup)
		if err != nil {
			return err
		}
	}
	if src.ret != nil {
		err := Gen_AssignableTo(src.ret, dest.ret, msg_name, tgroup)
		if err != nil {
			return err
		}
	}

	return Ok

mismatch:
	return fmt.Errorf("type mismatch on %s: wanted %s, got %s",
		msg_name, dest.String(), src.String())
}

/* TODO probably remove
func Gen_NiceName(new_type, gen_type *Type) {
	// if it is not a user-defined generic type, don't bother
	if gen_type.type_name == "" {
		return
	}

	s := "(" + gen_type.type_name

	// create a simplified (shallow) view of the types
	for _, par := range gen_type.param {
		sub := new_type.tgroup[par.name]
		sub_name := "???"

		if sub == nil {
			// oops
		} else if sub.nice_name != "" {
			sub_name = sub.nice_name
		} else if sub.type_name != "" {
			sub_name = sub.type_name
		} else {
			sub_name = sub.base.String()
		}

		s = s + " " + sub_name
	}

	new_type.nice_name = s + ")"
}
*/

//----------------------------------------------------------------------

type MonoState struct {
	cl      *Closure
	gen_cl  *Closure
	tgroup  map[string]*Type
}

// Gen_Monomorphize takes the closure of a generic function, plus a
// mapping for the type parameters like `@T` to their real (fully
// concrete type), and produces a copy with all generic place-holders
// replaced by their entry in `tgroup`.
func Gen_Monomorphize(gen_cl *Closure, tgroup map[string]*Type) (*Closure, cmError) {
	// sanity check that each generic type got an actual one
	// TODO: is it really possible?
	for _, p := range gen_cl.ty.param {
		if tgroup[p.name] == nil {
			PostError("generic type %s not bound in function call", p.name)
			return nil, FAILED
		}
	}

	// if already done, return it
	morph_str := Gen_MorphString(tgroup)

	exist := gen_cl.monomorphs[morph_str]
	if exist != nil { return exist, OKAY }

	new_type := Gen_Instantiate(gen_cl.ty, tgroup)
	if new_type == nil {
		return nil, FAILED
	}

	new_name := fmt.Sprintf("%s:morph%d", gen_cl.clos_name, len(gen_cl.monomorphs))

	cl := NewClosure(new_name)
	cl.is_method = gen_cl.is_method
	cl.is_monomorph = true
	cl.builtin = gen_cl.builtin

	cl.ty = new_type

	ms := new(MonoState)
	ms.cl = cl
	ms.gen_cl = cl
	ms.tgroup = tgroup

	cl.t_body = ms.MorphNode(gen_cl.t_body)

	// store it now, so that the deduction phase can find it again
	gen_cl.monomorphs[morph_str] = cl

	// apply binding and deduction to it, but compile it later
	if Gen_ProcessMonomorph(cl) != OKAY {
		return nil, FAILED
	}

	return cl, OKAY
}

// Gen_MorphString takes a set of type parameters and produces a
// string that will reflect that set of tgroup.  The tgroup must be
// concrete tgroup (not generic) and custom names are ignored.
func Gen_MorphString(tgroup map[string]*Type) string {
	var sb strings.Builder

	// we need to sort the names into a reproducable order
	sorted_names := make([]string, 0)
	for name, _ := range tgroup {
		sorted_names = append(sorted_names, name)
	}
	sort.Slice(sorted_names, func(i, k int) bool {
		return sorted_names[i] < sorted_names[k]
	})

	for i, name := range sorted_names {
		if i > 0 {
			sb.WriteString(" ")
		}
		ty := tgroup[name]

		// adding `@XX` name as a debugging aid
		sb.WriteString(name)
		sb.WriteString("=")
		sb.WriteString(ty.MorphString())
	}

	return sb.String()
}

func Gen_ProcessMonomorph(cl *Closure) cmError {
	if BindClosure(cl, nil) != OKAY {
		return FAILED
	}
	if DeduceClosure(cl) != OKAY {
		return FAILED
	}

	// compile it later
	AddPendingMethod(cl)

	return OKAY
}

func (ms *MonoState) MorphNode(t *Token) *Token {
	t2 := NewNode(t.Kind, t.Pos)

	t2.Str     = t.Str
	t2.Module  = t.Module
	t2.Cat     = t.Cat

	if t.Info != nil {
		if t.Info.ty != nil {
			t2.Info.ty = ms.MorphType(t.Info.ty)
		}

		t2.Info.mutable   = t.Info.mutable
		t2.Info.tag_id    = t.Info.tag_id
		t2.Info.fmt_spec  = t.Info.fmt_spec
		t2.Info.open      = t.Info.open

		if t.Info.junction != nil {
			t2.Info.junction = new(JunctionInfo)
			t2.Info.junction.name = t.Info.junction.name
		}

		// other fields are created in bind/deduce, no need to copy
	}

	if t.Children != nil {
		for _, child := range t.Children {
			new_child:= ms.MorphNode(child)
			t2.Add(new_child)
		}
	}

	return t2
}

func (ms *MonoState) MorphType(ty *Type) *Type {
	if ty.base == TYP_Generic || ty.base == TYP_TypeParam {
		// this returns NIL on error, but nothing we can do here
		ty = Gen_Instantiate(ty, ms.tgroup)
	}
	return ty
}
