// Copyright 2018 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
import "strconv"
// import "math"
// import "strings"

type GlobalDef struct {
	name    string // its name (for debugging)
	is_fun  bool   // declared via (fun ...) instead of (let ...)
	mutable bool   // can be altered?
	failed  bool   // parsing/deducing/compiling has failed?
	eval    bool   // variable has been evaluated

	ty  *Type  // its type [ loc.ty should always mirror this ]
	fun_cl   *Closure

	// stores full definition tree (before ParseMainElement)
	unparsed *Token

	// stores expression of global var/const
	expr     *Token

	module   string
	out_name string // "" until the global is used (if ever)
}

var cons_cl *Closure
var glob_cl *Closure

//----------------------------------------------------------------------

func Prelude_Begin() {
	doing_prelude = true

	context.AddType("array", array_def)
}

func Prelude_Finish() {
	doing_prelude = false

	if HaveErrors() { return }

	pop := Prelude_LookupGlobal("populate-ARGS-ENVS")
	AddPendingFunc(pop)
}

func Prelude_ResolveTypes() {
	bool_def = N_GetType("bool", "")
	char_def = N_GetType("char", "")

	opt_def  = N_GetType("opt",  "")
	map_def  = N_GetType("map",  "")
	set_def  = N_GetType("map",  "")

	if bool_def == nil || bool_def.failed {
		PostError("bool type is not defined in the prelude")
	}
	if char_def == nil || char_def.failed {
		PostError("char type is not defined in the prelude")
	}
	if opt_def == nil || opt_def.failed {
		PostError("opt type is not defined in the prelude")
	}
	if map_def == nil || map_def.failed {
		PostError("map type is not defined in the prelude")
	}
	if set_def == nil || set_def.failed {
		PostError("set type is not defined in the prelude")
	}
}

func Prelude_LookupGlobal(name string) *GlobalDef {
	def := modules[0].context.GetDef(name)
	if def == nil {
		panic("Global not found in prelude: " + name)
	}
	return def
}

//----------------------------------------------------------------------

func Glob_Setup() {
	// construct a closure for all non-trivial initializers.
	// actually two: one for creation, other for population.
	cons_cl = NewClosure("#Construct#")
	cons_cl.ty = NewType(TYP_Function)
	cons_cl.ty.ret = void_def.ty

	glob_cl = NewClosure("#Globals#")
	glob_cl.ty = NewType(TYP_Function)
	glob_cl.ty.ret = void_def.ty

	// construct a block to contain each initializer
	c_block := NewNode(ND_Block, Position{Line: 0})
	c_block.Info.ty = void_def.ty
	cons_cl.t_body = c_block

	g_block := NewNode(ND_Block, Position{Line: 0})
	g_block.Info.ty = void_def.ty
	glob_cl.t_body = g_block
}

func Glob_Finish() cmError {
	if CompileClosure(cons_cl) != OKAY {
		return FAILED
	}
	if CompileClosure(glob_cl) != OKAY {
		return FAILED
	}
	return OKAY
}

//----------------------------------------------------------------------

func Glob_CreateRaw(t *Token, is_fun bool, is_mut bool) cmError {
	ErrorSetToken(t)

	children := t.Children

	if len(children) < 2 {
		PostError("bad global def: missing name")
		return FAILED
	}

	t_name := children[1]
	if t_name.Kind != TOK_Name {
		PostError("bad global def: expected name got %s",
			t_name.String())
		return FAILED
	}

	// global defs cannot be redefined
	if context.HasDef(t_name.Str) {
		PostError("global '%s' is already defined", t_name.Str)
		return FAILED
	}

	gdef := MakeGlobal(t_name.Str, nil, is_mut)
	gdef.is_fun = is_fun

	gdef.unparsed = t

	return OKAY
}

func Glob_ParseDef(gdef *GlobalDef) {
	t := gdef.unparsed
	gdef.unparsed = nil

	var err2 cmError

	t, err2 = ParseMainElement(t)
	if err2 != OKAY {
		gdef.failed = true
		return
	}

	switch t.Kind {
	case ND_DefVar:
		Glob_ParseVar(gdef, t)

	case ND_DefFunc:
		Glob_ParseFunc(gdef, t)

	default:
		panic("weird node from ParseElement of a global")
	}
}

func Glob_ParseVar(gdef *GlobalDef, t *Token) cmError {
	gdef.expr = t.Children[1]

	// bind identifiers in the value
	if BindVar(gdef) != OKAY {
		return FAILED
	}

	return OKAY
}

func Glob_ParseFunc(gdef *GlobalDef, t *Token) cmError {
	t_name := t.Children[0]
	t_pars := t.Children[1]
	t_body := t.Children[2]

	clos_name := t_name.Module + t_name.Str

	cl := CreateClosureStuff(t, t_pars, t_body, clos_name)

	gdef.fun_cl = cl
	gdef.ty     = cl.ty

	// resolve builtins methods as soon as possible
	if t_body.Kind == ND_Native {
		if Builtin_Resolve(cl) != OKAY {
			return FAILED
		}
	}

	return OKAY
}

func Glob_ParseMethod(orig *Token) cmError {
	t, err2 := ParseMainElement(orig)
	if err2 != OKAY {
		return FAILED
	}

	if t.Kind != ND_DefMethod {
		panic("weird node from ParseElement of a method")
	}

	t_method := t.Children[0]
	t_pars   := t.Children[1]
	t_body   := t.Children[2]

	recv_def  := t_pars.Children[0].Info.recv_def
	recv_type := t_pars.Children[0].Info.ty

	// methods must be unique per type
	if recv_def.MethodExists(t_method.Str) {
		PostError("bad method def, '%s' already defined", t_method.Str)
		return FAILED
	}

	// TODO review this
	var clos_name string
	if recv_type.def != nil {
		clos_name = recv_type.def.name
	} else {
		clos_name = recv_type.MorphString()
	}

	clos_name += t_method.Str

	cl := CreateClosureStuff(t, t_pars, t_body, clos_name)
	cl.is_method = true

	if t_method.Match(".new") && cl.is_generic {
		cl.is_new = true
	}

	// resolve builtin methods as soon as possible
	if t_body.Kind == ND_Native {
		if Builtin_Resolve(cl) != OKAY {
			return FAILED
		}
	}

	recv_def.AddMethod(t_method.Str, cl)

	// hack alert: we do this since 'orig' is from unparsed_methods
	// list, and that same list is re-used for later processing.
	if orig.Info == nil { orig.Info = NewNodeInfo() }
	orig.Info.lambda = cl

	return OKAY
}

func CreateClosureStuff(t, t_pars, t_body *Token, clos_name string) *Closure {
	cl := NewClosure(clos_name)

	cl.t_body = t_body
	cl.ty = t.Info.ty

	if cl.ty.base == TYP_Generic {
		cl.is_generic = true
	}

	return cl
}

//----------------------------------------------------------------------

var globals_vars_seen map[string]bool

func Glob_DeduceVars() {
	globals_vars_seen = make(map[string]bool)

	for _, gdef := range context.defs {
		if gdef.expr != nil && gdef.ty == nil && !gdef.failed {
			// FIXME ensure module name is included!
			nn := gdef.name

			if !globals_vars_seen[nn] {
				globals_vars_seen[nn] = true

				DeduceVar(gdef)
			}
		}
	}
}

//----------------------------------------------------------------------

func Glob_EvaluateVars() {
	for _, gdef := range context.defs {
		if gdef.expr != nil && !gdef.eval && !gdef.failed {
			EvaluateVar(gdef)
		}
	}
}

//----------------------------------------------------------------------

func Glob_CompileFunc(gdef *GlobalDef) cmError {
	return CompileClosure(gdef.fun_cl)
}

func Glob_CompileMethod(m_cl *Closure) cmError {
	return CompileClosure(m_cl)
}

/*
[* FIXME

	// methods //

	for _, ty := range context.types {
		CompileMethodsForType(ty)
	}

	// FIXME ugh, do this better!  [ AND only once ]
	CompileMethodsForType(int_type)
	CompileMethodsForType(str_type)
	CompileMethodsForType(float_type)
	CompileMethodsForType(bytevec_type)

	CompileMethodsForType(array_parent)
	CompileMethodsForType(map_parent)
	CompileMethodsForType(set_parent)
	CompileMethodsForType(class_parent)
	CompileMethodsForType(union_parent)
	CompileMethodsForType(enum_parent)
*]
	return OKAY
}
*/

func CompileMethodsForType(ty *Type) {
/* TODO ?
	for _, cl := range ty.methods {
		if cl.builtin == nil &&
			!cl.IsCompiled() &&
			!cl.failed {

			CompileClosure(cl)
		}
	}
*/
}

//----------------------------------------------------------------------

func Glob_CompileVar(gdef *GlobalDef) cmError {
	// already seen    [ FIXME don't think this check is needed ]
	if gdef.out_name != "" {
		return OKAY
	}

	// convert name for output

	// FIXME proper module for global
	mod := module
	_ = mod
	mod_name := ""

	gdef.out_name = EncodeName("", mod_name, gdef.name)

	// add declaration to header.
	// [ this may add "GV." prefix to the out_name ]
	out_globals.Declare(gdef)

	// a global function? (from the `fun` form)
	if gdef.is_fun {
		return CompileVar_Func(gdef)
	}

	if gdef.expr.Kind == ND_Construct {
		return CompileVar_Alloc(gdef)
	}

	return CompileVar_Basic(gdef)
}

func CompileVar_Func(gdef *GlobalDef) cmError {
	mod_name := ""  // FIXME

	AddPendingFunc(gdef)

	fun_cl := gdef.fun_cl
	name := EncodeName("_fu_", mod_name, fun_cl.clos_name)
	val_str := fmt.Sprintf("_gc_alloc_func(%s, (YRawFunction *) &%s, 0)",
		EncodeStrLiteral(gdef.name), name)

	out_globals.func_asg.Printf("%s = %s;", gdef.out_name, val_str)
	return OKAY
}

func CompileVar_Basic(gdef *GlobalDef) cmError {
	expr := gdef.expr

	// add C cast for custom types
	cast_str := ""

	if gdef.ty.IsCustom() {
		c_type := ConvertType(gdef.ty)
		cast_str = "(" + c_type + ")"

	} else if expr.Kind == ND_Cast {
		c_type := ConvertType(expr.Info.ty)
		cast_str = "(" + c_type + ")"
		expr = expr.Children[0]
	}

	// FIXME do some stuff that SimpleText does  [ factor out? ]

	val_str := ""

	switch expr.Kind {
	case ND_Integer, ND_Float:
		val_str = expr.Str

	case ND_Enum:
		if cast_str == "" {
			cast_str = "(YEnum)"
		}
		val_str = strconv.Itoa(expr.Info.tag_id)

	case ND_String:
		val_str = fmt.Sprintf("_rt_from_cstr(%s)", EncodeStrLiteral(expr.Str))

	default:
		PostError("weird expression for global var: %s", expr.String())
		return FAILED
	}

	out_globals.simp_asg.Printf("%s = %s%s;", gdef.out_name, cast_str, val_str)
	return OKAY
}

func CompileVar_Alloc(gdef *GlobalDef) cmError {
	expr := gdef.expr

	t_var      := expr.Children[0]
	t_allocate := expr.Children[1]
	t_populate := expr.Children[2]

	if t_var.Kind != ND_Global {
		panic("global var with a local in ND_Construct: " + gdef.name)
	}

	// store the allocation code into #Construct#
	t_assign := NewNode(ND_SetVar, Position{Line: 0})
	t_assign.Str = gdef.name
	t_assign.Info.ty = void_def.ty
	t_assign.Add(t_var)
	t_assign.Add(t_allocate)

	cons_cl.t_body.Add(t_assign)

	// store the population code into #Globals#
	glob_cl.t_body.Add(t_populate)

	// compile any globals referenced by the expression
	Glob_FindVarReferences(expr)

	return OKAY
}

func Glob_FindVarReferences(t *Token) {
	if t.Kind == ND_Global {
		AddPendingVar(t.Info.gdef)
	}

	for _, child := range t.Children {
		Glob_FindVarReferences(child)
	}
}

//----------------------------------------------------------------------

func MakeConstant(name string, ty *Type) *GlobalDef {
	return MakeGlobal(name, ty, false)
}

func MakeGlobal(name string, ty *Type, mutable bool) *GlobalDef {
	def := context.GetDef(name)

	if def == nil {
		def = new(GlobalDef)
		context.AddDef(name, def)
	}

	def.ty = ty // can be nil
	def.name = name
	def.mutable = mutable

	return def
}

//----------------------------------------------------------------------

type VN_Flags int

const (
	ALLOW_UNDERSCORE VN_Flags = (1 << iota)
	ALLOW_FIELDS
	ALLOW_SELF
)

func ValidateDefName(t *Token, what string, flags VN_Flags) cmError {
	if t.Module != "" {
		PostError("cannot define a %s in another module", what)
		return FAILED
	}

	name := t.Str

	if t.Str == "_" && (flags & ALLOW_UNDERSCORE) != 0 {
		return OKAY
	}

	if t.Str == "self" && (flags & ALLOW_SELF) != 0 {
		return OKAY
	}

	// disallow language keywords
	if IsLanguageKeyword(name) {
		PostError("bad %s name: '%s' is a reserved keyword", what, name)
		return FAILED
	}

	// disallow built-in types
	if !doing_prelude {
		switch name {
		case "int", "float", "str", "array", "byte-vec", "map", "set",
			"class", "union", "enum", "function":

			PostError("bad %s name: '%s' is a standard type", what, name)
			return FAILED
		}
	}

	// disallow field names
	if len(name) >= 2 && name[0] == '.' {
		if (flags & ALLOW_FIELDS) != 0 {
			return OKAY
		} else {
			PostError("bad %s name: cannot begin with a dot", what)
			return FAILED
		}
	}

	// disallow tag names
	if len(name) >= 2 && name[0] == '`' {
		PostError("bad %s name: cannot begin with a backquote", what)
		return FAILED
	}

	// disallow generic type names
	if len(name) >= 2 && name[0] == '@' {
		PostError("bad %s name: cannot begin with '@'", what)
		return FAILED
	}

	// disallow special keywords
	if len(name) >= 2 && name[0] == '#' {
		PostError("bad %s name: cannot begin with a '#'", what)
		return FAILED
	}

	// disallow names of the operators
	if IsOperator(name) {
		PostError("bad %s name: '%s' is a math operator", what, name)
		return FAILED
	}

	// disallow macro names
	if N_HasMacro(name, "") {
		PostError("bad %s name: '%s' already defined as a macro", what, name)
		return FAILED
	}

	// disallow names of user types
	if N_HasType(name, "") {
		PostError("bad %s name: '%s' already defined as a type", what, name)
		return FAILED
	}

	return OKAY
}

func WhatIsGlobalName(name string) string {
	if IsOperator(name) {
		return "operator"
	}

	if N_HasMacro(name, "") {
		return "macro"
	}

	if N_HasType(name, "") {
		return "type"
	}

	if N_HasDef(name, "") {
		return "global"
	}

	if IsLanguageKeyword(name) {
		return "keyword"
	}

	return "UNSET"
}

func IsLanguageKeyword(name string) bool {
	switch name {
	case "let", "let-mut", "fun", "lam":
		return true

	case "macro", "rule", "type", "interface":
		return true

	case "begin", "set!", "self", "no", "error":
		return true

	case "if", "else", "elif", "bare-loop", "match", "where":
		return true

	case "cast", "unwrap", "default-of", "nothing":
		return true

	// NOTE: the "for" forms are done via macros in the prelude
	case "skip", "junction":
		return true

	case ":", "::", "_", "->", "=>":
		return true
	}

	if IsOperator(name) {
		return true
	}

	return false
}
