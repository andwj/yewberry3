// Copyright 2018 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"

type Macro struct {
	name     string
	name_tok *Token
	unparsed *Token

	locals   map[string]bool
	rules    []*Rule
	analysed bool
	failed   bool
}

type Rule struct {
	pattern *Token
	body    *Token
	params  map[string]bool
	multis  map[string]bool
}

type IdentifierCategory int

const (
	IDC_UNSET IdentifierCategory = iota

	IDC_LanguageWord // includes special symbols and operators
	IDC_GlobalDef    // includes: types, macros, builtin functions

	IDC_MacroLocal
	IDC_MacroKeyword
	IDC_MacroParameter
	IDC_MacroMultiParam
)

type MatchState struct {
	// mapping from macro parameter names to passed-in values.
	// for the "..." parameters (which accept multiple values),
	// the token is a TOK_Data with IDC_MacroMultiParam on it
	// and the children are the passed-in values.
	params map[string]*Token

	// line number for expanded body
	// FIXME explain this better
	new_pos Position
}

// this is used to annotate local bindings in a macro so that
// they never clash with anything supplied in parameters.
// [ i.e. to implement macro hygiene ]
var macro_stamp int

// internal use only [ here in macro.go ]
const NO_MATCH cmError = 9

//----------------------------------------------------------------------

func NewMacro(name string) *Macro {
	mac := new(Macro)

	mac.name = name
	mac.locals = make(map[string]bool)
	mac.rules = make([]*Rule, 0)

	return mac
}

func Macro_CreateRaw(t *Token) cmError {
	ErrorSetToken(t)

	children := t.Children

	if len(children) < 2 {
		PostError("bad macro def: missing name")
		return FAILED
	}

	t_name := children[1]

	// FIXME : make sure we detect duplicate names BUT allow two modules to have same name

	if doing_prelude && (t_name.Str == "int" || t_name.Str == "str" || t_name.Str == "float") {
		// HACK: in prelude allow "int", "str" and "float" macros
	} else if ValidateDefName(t_name, "macro", 0) != OKAY {
		return FAILED
	}

	mac := NewMacro(t_name.Str)
	mac.unparsed = t

	context.AddMacro(t_name.Str, mac)
	return OKAY
}

func Macro_ParseDef(mac *Macro) cmError {
	// preliminary parsing has checked that # children is >= 2
	children := mac.unparsed.Children

	// store token for the name
	mac.name_tok = children[1]
	mac.name_tok.Kind = TOK_String

	if len(children) < 3 {
		PostError("bad macro -- no rules.")
		mac.failed = true
		return FAILED
	}

	for i := 2; i < len(children); i++ {
		tok := children[i]
		ErrorSetToken(tok)

		if tok.Kind != TOK_Expr {
			PostError("bad macro rule -- not an () expr")
			mac.failed = true
			return FAILED
		}
		if len(tok.Children) == 0 {
			PostError("bad macro rule -- empty ()")
			mac.failed = true
			return FAILED
		}

		if tok.Children[0].Match("locals") {
			if mac.ParseLocals(tok) != OKAY {
				mac.failed = true
				return FAILED
			}
		} else if tok.Children[0].Match("rule") {
			if mac.ParseRule(tok) != OKAY {
				mac.failed = true
				return FAILED
			}
		} else {
			PostError("bad macro -- unknown component")
			mac.failed = true
			return FAILED
		}
	}

	return OKAY
}

func (mac *Macro) ParseRule(tok *Token) cmError {
	ErrorSetToken(tok)

	if len(tok.Children) < 3 {
		PostError("bad macro rule -- missing body")
		return FAILED
	}
	if len(tok.Children) > 3 {
		PostError("bad macro rule -- body must be a single element")
		return FAILED
	}

	pattern := tok.Children[1]

	if pattern.Match("_") {
		// ok
	} else {
		if pattern.Kind != TOK_Expr {
			PostError("bad pattern in rule -- must be in ()")
			return FAILED
		}

		// insert macro name into start of pattern
		pattern.Children = append(pattern.Children, nil)
		copy(pattern.Children[1:], pattern.Children[0:])
		pattern.Children[0] = mac.name_tok
	}

	body := tok.Children[2]

	// add the new rule (must be before analysis)
	rule := new(Rule)
	rule.pattern = pattern
	rule.body = body
	rule.params = make(map[string]bool)

	mac.rules = append(mac.rules, rule)

	return OKAY
}

func (mac *Macro) ParseLocals(tok *Token) cmError {
	ErrorSetToken(tok)

	if len(mac.rules) > 0 {
		PostError("bad macro -- locals must occur before rules")
		return FAILED
	}

	for i := 1; i < len(tok.Children); i++ {
		child := tok.Children[i]

		if child.Kind != TOK_Name {
			PostError("bad macro -- local is not a name")
			return FAILED
		}

		name := child.Str

		if IsLanguageKeyword(name) || N_HasMacro(name, "") {
			PostError("bad macro -- invalid local name '%s'", name)
			return FAILED
		}
		if mac.locals[name] {
			PostError("bad macro -- duplicate local '%s'", name)
			return FAILED
		}

		mac.locals[name] = true
	}

	return OKAY
}

/* REPL-only stuff

func PurgeFailedMacros() {
	for name, mac := range context.macros {
		if mac.failed {
			context.RemoveMacro(name)
		}
	}
}
*/

//----------------------------------------------------------------------

func Macro_AnalyseDef(mac *Macro) cmError {
	mac.analysed = true

	for _, rule := range mac.rules {
		if mac.AnalyseRule(rule) != OKAY {
			mac.failed = true
			return FAILED
		}
	}
	return OKAY
}

func (mac *Macro) AnalyseRule(rule *Rule) cmError {
	if mac.AnalysePattern(rule, rule.pattern) != OKAY {
		return FAILED
	}

	if mac.AnalyseBody(rule, rule.body, false) != OKAY {
		return FAILED
	}

	// multi-parameters can only be used in a compound token (TOK_Expr or so)
	// because a macro body must always expand to a single element.
	if rule.body.Kind == TOK_Name && rule.body.Cat == IDC_MacroMultiParam {
		ErrorSetToken(rule.body)
		PostError("bad macro rule -- body cannot be a multi-parameter")
		return FAILED
	}

	return OKAY
}

func (mac *Macro) AnalysePattern(rule *Rule, tok *Token) cmError {
	ErrorSetToken(tok)

	if tok.Kind >= TOK_Expr {
		for _, child := range tok.Children {
			if mac.AnalysePattern(rule, child) != OKAY {
				return FAILED
			}
		}
		return mac.AnalyseEllipses(rule, tok)
	}

	// strings in patterns are for keyword matches
	if tok.Kind == TOK_String {
		tok.Cat = IDC_MacroKeyword
		return OKAY
	}

	// other literals are not usable in patterns
	if tok.Kind != TOK_Name {
		PostError("bad macro: cannot use literals in a pattern")
		return FAILED
	}

	// identifiers in a pattern are macro parameters (for this rule)
	name := tok.Str

	if mac.locals[name] {
		PostError("bad macro: cannot use locals in a pattern")
		return FAILED
	}

	if name == "_" {
		tok.Cat = IDC_MacroParameter
		return OKAY
	}

	// ensure parameter name is unique
	if rule.params[name] {
		PostError("bad macro: duplicate parameter '%s'", name)
		return FAILED
	}

	rule.params[name] = true

	if tok.IsEllipsis() {
		tok.Cat = IDC_MacroMultiParam
		return OKAY
	}

	if IsLanguageKeyword(name) {
		PostError("bad macro: illegal parameter name '%s'", name)
		return FAILED
	}

	tok.Cat = IDC_MacroParameter
	return OKAY
}

func (mac *Macro) AnalyseEllipses(rule *Rule, tok *Token) cmError {
	// more than one multi-parameter in a pattern requires a keyword string
	// (a.k.a. "landmark") somewhere in between them.
	for i := 0; i < len(tok.Children); i++ {
		c1 := tok.Children[i]
		if c1.Cat == IDC_MacroMultiParam {

			for k := i + 1; k < len(tok.Children); k++ {
				c2 := tok.Children[k]
				if c2.Cat == IDC_MacroMultiParam {
					PostError("bad macro: ambiguous use of multi-parameters")
					return FAILED
				}
				if c2.Cat == IDC_MacroKeyword {
					break
				}
			}
		}
	}

	return OKAY
}

func (mac *Macro) AnalyseBody(rule *Rule, tok *Token, in_match bool) cmError {
	ErrorSetToken(tok)

	// check some stuff
	if tok.Kind == TOK_Expr && len(tok.Children) >= 1 {
		head := tok.Children[0]

		if head.Match("match") {
			in_match = true
		}

		// validate the error form
		if head.Match("macro-error") {
			if len(tok.Children) < 2 || tok.Children[1].Kind != TOK_String {
				PostError("bad macro: missing message in macro-error")
				return FAILED
			}
			if len(tok.Children) > 2 {
				PostError("bad macro: extra values in macro-error")
				return FAILED
			}
		}

		// disallow generating certain forms
		if head.Match("macro") || head.Match("type") {
			PostError("macros cannot generate a %s", head.Str)
			return FAILED
		}

		// validate forms which introduce a binding
		if len(tok.Children) >= 2 {
			if mac.DetectBinding(rule, tok) != OKAY {
				return FAILED
			}
		}

		/* TODO REVIEW THIS, it fails with the implicit labels

		// handle the keyword of skip/junction as a macro local.
		if head.Match("skip") || head.Match("junction") {
			if len(tok.Children) >= 2 && tok.Children[1].Kind == TOK_Name {
				junc_name := tok.Children[1].Str
				if rule.params[junc_name] == false {
					tok.Children[1].Cat = IDC_MacroLocal
				}
			}
		}
		*/
	}

	// recursively handle compound forms
	if tok.Kind >= TOK_Expr {
		for _, child := range tok.Children {
			if mac.AnalyseBody(rule, child, in_match) != OKAY {
				return FAILED
			}
		}
		return OKAY
	}

	// already handled?
	if tok.Cat != IDC_UNSET {
		return OKAY
	}

	// we only mark identifiers
	if tok.Kind != TOK_Name || tok.IsTag() {
		return OKAY
	}

	name := tok.Str

	if mac.locals[name] {
		tok.Cat = IDC_MacroLocal
		return OKAY
	}

	if rule.params[name] {
		if tok.IsEllipsis() {
			tok.Cat = IDC_MacroMultiParam
		} else {
			tok.Cat = IDC_MacroParameter
		}
		return OKAY
	}

	if tok.IsEllipsis() && !in_match {
		PostError("bad macro: unknown multi-parameter '%s'", tok.Str)
		return FAILED
	}

	if IsLanguageKeyword(name) {
		tok.Cat = IDC_LanguageWord
		return OKAY
	}

	// everything else must be a global def
	tok.Cat = IDC_GlobalDef
	return OKAY
}

func (mac *Macro) DetectBinding(rule *Rule, tok *Token) cmError {
	// detect language constructs which introduce a binding.
	// the identifier being bound must either be a macro local or a
	// macro parameter.

	ErrorSetToken(tok)

	head := tok.Children[0]

	if head.Match("let") || head.Match("let-mut") ||
		head.Match("fun") || head.Match("for-step") {

		return mac.IsValidBinding(rule, tok.Children[1])

	} else if head.Match("for-each") {
		if len(tok.Children) >= 4 {
			if mac.IsValidBinding(rule, tok.Children[2]) != OKAY {
				return FAILED
			}
			return mac.IsValidBinding(rule, tok.Children[3])
		}

	} else if head.Match("lam") {
		// the parameters of a lambda also create bindings
		if len(tok.Children) >= 3 {
			if tok.Children[1].Kind != TOK_Expr {
				PostError("bad lambda def : params must be in ()")
				return FAILED
			}
			params := tok.Children[1].Children

			i := 0
			for i < len(params) {
				if params[i].Match("->") {
					break
				}
				if mac.IsValidBinding(rule, params[i]) != OKAY {
					return FAILED
				}

				i += 2 // skip type spec
			}
		}
	}

	return OKAY
}

func (mac *Macro) IsValidBinding(rule *Rule, tok *Token) cmError {
	if tok.Kind != TOK_Name {
		PostError("bad macro: binding is not a name")
		return FAILED
	}
	if mac.locals[tok.Str] {
		return OKAY
	}
	if rule.params[tok.Str] {
		return OKAY
	}
	PostError("bad macro: binding '%s' is not a macro local", tok.Str)
	return FAILED
}

//----------------------------------------------------------------------

// MacroExpand checks if the given token or any child in the token
// tree is a macro.  The result is a token tree with all macros
// expanded -- either the input token (possibly modified) or a
// newly created token tree.  Returns NIL if an error occurred.
func MacroExpand(tok *Token) *Token {
	loops := 0

	for {
		// recursively try children.
		// do this FIRST so that macros are expanded in a BOTTOM-UP way.
		if tok.Kind >= TOK_Expr {
			for i := 0; i < len(tok.Children); i++ {
				child := tok.Children[i]

				newchild := MacroExpand(child)
				if newchild == nil {
					return nil
				}

				tok.Children[i] = newchild
			}
		}

		if tok.Kind != TOK_Expr {
			return tok
		}

		if len(tok.Children) == 0 {
			return tok
		}

		head := tok.Children[0]
		if head.Kind != TOK_Name {
			return tok
		}

		mac := N_GetMacro(head.Str, head.Module)
		// treat failed-to-compile macros as non-existent here
		if mac == nil || mac.failed {
			return tok
		}

		loops++
		if loops > 9999 {
			// Todo: ideally show the outermost macro
			PostError("infinite macro recursion (last was %s)", head.Str)
			return nil
		}

		newtok := mac.Apply(tok)
		if newtok == nil {
			return nil
		}

		// attempt to expand again
		tok = newtok
	}
}

func (mac *Macro) Apply(tok *Token) *Token {
	// try all rules until one matches
	for _, r := range mac.rules {
		newtok, err := mac.ApplyRule(r, tok)

		if err != NO_MATCH {
			return newtok
		}
	}

	PostError("no matching rule for %s macro", mac.name)
	return nil
}

func (mac *Macro) ApplyRule(rule *Rule, input *Token) (*Token, cmError) {
	var ms MatchState

	ms.new_pos = input.Pos

	err2 := mac.MatchInput(rule, rule.pattern, input, &ms)
	if err2 != OKAY {
		return nil, err2
	}

	// handle the "macro-error" form
	if rule.body.Kind == TOK_Expr && len(rule.body.Children) >= 1 &&
		rule.body.Children[0].Match("macro-error") {

		PostError("macro-error: %s", rule.body.Children[1].Str)
		return nil, FAILED
	}

	// ensure this expansion will get unique local bindings
	macro_stamp += 1

	newtok, err2 := mac.ExpandBody(rule, rule.body, &ms)

	if err2 != OKAY {
		return nil, err2
	}

	// math expressions in a macro body are stored unexpanded,
	// so we need to handle them here (in the new body).
	newtok = OperatorExpand(newtok)
	if newtok == nil {
		return nil, FAILED
	}

	return newtok, OKAY
}

func (mac *Macro) MatchInput(rule *Rule, pattern, input *Token, ms *MatchState) cmError {
	// a macro parameter will match any token, remembering it
	if pattern.Cat == IDC_MacroParameter {
		if mac.CheckParameter(input) != OKAY {
			return FAILED
		}
		if !pattern.Match("_") {
			ms.SetParam(pattern.Str, input)
		}
		return OKAY
	}

	// a keyword match?
	if pattern.Cat == IDC_MacroKeyword {
		if input.Kind != TOK_Name {
			return NO_MATCH
		}
		if input.Str != pattern.Str {
			return NO_MATCH
		}
		return OKAY
	}

	if pattern.Kind < TOK_Name {
		panic("mac.MatchInput : literal value in pattern")
	}

	// pattern is a compound token, e.g. something in () or [] or {},
	// so require the input to be the same kind.

	if input.Kind != pattern.Kind {
		return NO_MATCH
	}

/* DEBUG MATCHING
print("Pattern:\n")
pattern.Dump(0)
print("Actual:\n")
tok.Dump(0)
*/

	// step through the pattern and input tokens

	pat_pos := pattern.Children
	inp_pos := input.Children

	for {
		// end of pattern or input?
		if len(pat_pos) == 0 && len(inp_pos) == 0 {
			return OKAY
		}
		if len(pat_pos) == 0 {
			return NO_MATCH
		}

		P := pat_pos[0]

		// a multi-parameter?
		// Note that it may match zero input tokens.
		if P.Cat == IDC_MacroMultiParam {
			p_count, t_count, err2 := mac.MatchMulti(rule, pat_pos, inp_pos, ms)
			if err2 != OKAY {
				return err2
			}

			pat_pos = pat_pos[p_count:]
			inp_pos = inp_pos[t_count:]

		} else {
			if len(inp_pos) == 0 {
				return NO_MATCH
			}
			err2 := mac.MatchInput(rule, P, inp_pos[0], ms)
			if err2 != OKAY {
				return err2
			}

			pat_pos = pat_pos[1:]
			inp_pos = inp_pos[1:]
		}
	}

	return OKAY
}

func (mac *Macro) MatchMulti(rule *Rule, pat_pos, inp_pos []*Token,
	ms *MatchState) (p_count, t_count int, err2 cmError) {

	// after_count = number of single-params following the
	//               multi-parameter.  if we reach a keyword,
	//               remember it in "landmark" and stop.
	after_count := 0

	var landmark *Token

	for 1+after_count < len(pat_pos) {
		p := pat_pos[1+after_count]
		if p.Cat == IDC_MacroKeyword {
			landmark = p
			break
		}
		after_count++
	}

	// inp_count = number of input tokens until the landmark / end.
	//             does NOT include the landmark itself.
	inp_count := 0

	if landmark == nil {
		inp_count = len(inp_pos)
	} else {
		// find landmark in the input...
		for {
			if inp_count >= len(inp_pos) {
				// landmark not found
				return -1, -1, NO_MATCH
			}

			t := inp_pos[inp_count]

			if t.Kind == TOK_Name && t.Str == landmark.Str {
				// found the correct landmark
				break
			}

			inp_count++
		}
	}

	// enough input tokens to satisfy the pattern?
	if inp_count < after_count {
		return -1, -1, NO_MATCH
	}

	multi_count := inp_count - after_count

	// process the multi-parameter, create array token for the values
	m_value := new(Token)

	m_value.Kind = TOK_Data
	m_value.Pos = pat_pos[0].Pos
	m_value.Cat = IDC_MacroMultiParam

	for i := 0; i < multi_count; i++ {
		I := inp_pos[i]
		if mac.CheckParameter(I) != OKAY {
			return -1, -1, FAILED
		}
		m_value.Add(I)
	}

	ms.SetParam(pat_pos[0].Str, m_value)

	// process all single-params after it
	for i := 0; i < after_count; i++ {
		P := pat_pos[1+i]
		I := inp_pos[multi_count+i]

		err2 := mac.MatchInput(rule, P, I, ms)
		if err2 != OKAY {
			return -1, -1, err2
		}
	}

	// skip the landmark in pattern and input
	if landmark != nil {
		after_count++
		inp_count++
	}

	return after_count + 1, inp_count, OKAY
}

func (mac *Macro) ExpandBody(rule *Rule, input *Token, ms *MatchState) (*Token, cmError) {
	var output *Token

	// replace parameters with their actual value
	if input.Cat == IDC_MacroParameter {
		temp := ms.params[input.Str]
		if temp == nil {
			panic("mac.ExpandBody : parameter lookup failed")
		}
		output = temp.Clone(true /* deep */)
		return output, OKAY
	}

	if input.Kind < TOK_Expr {
		output = input.Clone(false)
		output.Pos = ms.new_pos

		// annotate a local binding with a stamp
		if input.Cat == IDC_MacroLocal {
			output.Str = mac.AnnotateIdentifier(input.Str)
		}

		return output, OKAY
	}

	output = new(Token)

	output.Kind = input.Kind
	output.Str = input.Str
	output.Pos = ms.new_pos
	output.Cat = input.Cat
	output.Children = make([]*Token, 0)

	// remember operator info
	if input.Info != nil {
		output.Info = NewNodeInfo()
		output.Info.op = input.Info.op
	}

	for _, sub := range input.Children {
		// handle multi-parameters like "..."
		if sub.Cat == IDC_MacroMultiParam {
			temp := ms.params[sub.Str]
			if temp == nil {
				panic("mac.ExpandBody : multi-param lookup failed")
			}
			if temp.Kind != TOK_Data {
				panic("mac.ExpandBody : multi-param has weird value")
			}
			for _, t2 := range temp.Children {
				output.Add(t2.Clone(true /* deep */))
			}

		} else {
			// recursively handle the child
			new_sub, err2 := mac.ExpandBody(rule, sub, ms)
			if err2 != OKAY {
				return nil, err2
			}
			output.Add(new_sub)
		}
	}

	return output, OKAY
}

func (mac *Macro) CheckParameter(input *Token) cmError {
	if input.Match("_") {
		return OKAY
	}
	if input.Kind != TOK_Name {
		return OKAY
	}

	switch WhatIsGlobalName(input.Str) {
	case "macro":
		PostError("bad macro parameter: '%s' is a macro name", input.Str)
		return FAILED

	case "keyword":
		switch {
		case input.Match("self"), input.Match("nothing"):
			// OK
		default:
			PostError("bad macro parameter: '%s' is a language keyword", input.Str)
			return FAILED
		}
	}

	return OKAY
}

func (mac *Macro) AnnotateIdentifier(name string) string {
	return name + fmt.Sprintf("{%d}", macro_stamp)
}

func (ms *MatchState) SetParam(name string, tok *Token) {
	if ms.params == nil {
		ms.params = make(map[string]*Token)
	}
	ms.params[name] = tok
}

func (idc IdentifierCategory) String() string {
	switch idc {
	case IDC_UNSET:
		return ""
	case IDC_LanguageWord:
		return "LANG-WORD"
	case IDC_GlobalDef:
		return "GLOBAL"
	case IDC_MacroLocal:
		return "MACRO-LOCAL"
	case IDC_MacroParameter:
		return "MACRO-PARAM"
	case IDC_MacroMultiParam:
		return "MACRO-MULTI"
	default:
		return "!!INVALID!!"
	}
}
