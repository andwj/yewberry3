// Copyright 2018 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "fmt"
import "strings"
import "path/filepath"

import "gitlab.com/andwj/argv"

var Options struct {
	list_file string
	out_dir   string

	optim_level int

	help bool
}

// certain things are only allowed in the prelude
var doing_prelude bool

//----------------------------------------------------------------------

func main() {
	// this can fatal error
	HandleArgs()

	ClearErrors()

	InitNative()
	InitModules()

	// NOTE: this facility for handling a single code file
	//       is only for testing, it WILL go away eventually.
	if FileHasExtension(Options.list_file, "yb") {
		FakeModuleList(Options.list_file)
	} else {
		ReadModuleList(Options.list_file)
	}

	SetupTypes()
	SetupOperators()
	SetupBuiltins()

	Glob_Setup()

	// read all the tokens from every file
	// [ the only errors will be file errors, lexing and bad/dup names ]
	LoadAllModules()

	if ! HaveErrors() {
		CompileAllModules()
	}

	if ! HaveErrors() {
		SaveAllModules()
	}

	if HaveErrors() {
		ShowAllErrors()
		FatalError("some errors occurred")
	}

	os.Exit(0)
}

func HandleArgs() {
	Options.out_dir = "_out"
	Options.optim_level = 1

	argv.Generic("o", "outdir", &Options.out_dir, "dir", "name of output directory")
	argv.Gap()
	argv.Enabler("h", "help", &Options.help, "display this help text")

	err := argv.Parse()
	if err != nil {
		FatalError("%s", err.Error())
	}

	names := argv.Unparsed()

	if Options.help || len(names) == 0 {
		ShowUsage()
		os.Exit(0)
	}

	if len(names) == 0 {
		FatalError("missing filename of list file")
	}
	if len(names) > 1 {
		FatalError("too many filenames")
	}

	Options.list_file = names[0]
}

func ShowUsage() {
	Print("Usage: yb-compile FILE.list [OPTIONS...]\n")

	Print("\n")
	Print("Available options:\n")

	argv.Display(os.Stdout)
}

func FatalError(format string, a ...interface{}) {
	format = "yb-compile: " + format + "\n"
	fmt.Fprintf(os.Stderr, format, a...)
	os.Exit(2)
}

func Print(format string, a ...interface{}) {
	format = fmt.Sprintf(format, a...)
	fmt.Printf("%s", format)
}

func FileHasExtension(fn, ext string) bool {
		fn = filepath.Ext(fn)
		fn = strings.ToLower(fn)

		if len(fn) > 0 && fn[0] == '.' {
				fn = fn[1:]
		}

		return (fn == ext)
}

//----------------------------------------------------------------------

func LoadAllModules() {
	for _, mod := range modules {
		// setup some frequently used globals
		module = mod
		context = mod.context

		if mod.is_prelude {
			doing_prelude = true
		}

		// visit later files before earlier ones
		for i := len(mod.files)-1; i >= 0; i-- {
			LoadCodeFile(mod.files[i])
		}

		doing_prelude = false
	}
}

func LoadCodeFile(ff *InFile) cmError {
	file, err := os.Open(ff.fullname)
	if err != nil {
		PostError("no such file: %s", ff.fullname)
		return FAILED
	}

	ErrorSetModule("")
	ErrorSetFile(ff.fullname)

	lex := NewLexer(file)
	lex.sweet = true

	err2 := LoadScanner(lex)

	file.Close()

	return err2
}

func LoadScanner(lex *Lexer) cmError {
	for {
		t := lex.Scan()

		ErrorSetToken(t)

		if t.Kind == TOK_ERROR {
			PostError("%s", t.Str)
			return FAILED
		}

		if t.Kind == TOK_EOF {
			return OKAY
		}

		LoadRawDefinition(t)
	}

	if HaveErrors() {
		return FAILED
	} else {
		return OKAY
	}
}

func LoadRawDefinition(t *Token) cmError {
	ErrorSetToken(t)

	if t.Kind == TOK_Expr {
		if len(t.Children) == 0 {
			PostError("empty expr in ()")
			return FAILED
		}

		head := t.Children[0]

		if head.Match("macro") {
			return Macro_CreateRaw(t)
		}

		if head.Match("type") || head.Match("interface") {
			return Type_CreateRaw(t)
		}

		if head.Match("let") {
			return Glob_CreateRaw(t, false, false)
		}
		if head.Match("let-mut") {
			return Glob_CreateRaw(t, false, true)
		}

		if head.Match("fun") {
			// check for a method definition
			if len(t.Children) >= 2 && t.Children[1].IsNamedField() {
				module.AddUnparsedMethod(t)
				return OKAY
			}

			return Glob_CreateRaw(t, true, false)
		}
	}

	// merely remember the top-level expression, handle it later
	if !module.is_main {
		PostError("bare expression found in non-main module")
		return FAILED
	}

	module.AddUnparsedExpr(t)
	return OKAY
}


//----------------------------------------------------------------------

type PendingItem struct {
	p_var  *GlobalDef
	p_func *GlobalDef
	p_meth *Closure
}

var pending_items []*PendingItem

func RawAddPending(it *PendingItem) {
	pending_items = append(pending_items, it)
}

func AddPendingFunc(gdef *GlobalDef) {
	if !gdef.fun_cl.is_used {
		gdef.fun_cl.is_used = true

		it := new(PendingItem)
		it.p_func = gdef

		RawAddPending(it)
	}
}

func AddPendingMethod(cl *Closure) {
	if !cl.is_used {
		cl.is_used = true

		it := new(PendingItem)
		it.p_meth = cl

		RawAddPending(it)
	}
}

func AddPendingVar(gdef *GlobalDef) {
	if gdef.out_name == "" {
		it := new(PendingItem)
		it.p_var = gdef
		RawAddPending(it)
	}
}

//----------------------------------------------------------------------

func CompileAllModules() {
	pending_items = make([]*PendingItem, 0)

	for _, mod := range modules {
		module = mod
		context = mod.context

		if mod.is_prelude {
			Prelude_Begin()
		}

		CompileCurrentModule()

		if mod.is_prelude {
			Prelude_Finish()
		}
	}

	CompileUsedFunctions()

	Glob_Finish()
}

func CompileCurrentModule() {
	// parse all macro definitions
	for _, mac := range context.macros {
		if !mac.failed {
			Macro_ParseDef(mac)
		}
	}

	// analyse all macro rule bodies
	for _, mac := range context.macros {
		if !mac.failed {
			Macro_AnalyseDef(mac)
		}
	}

	// parse all type definitions, generics BEFORE plain ones
	for _, ty := range context.types {
		if ty.generic && !ty.failed {
			Type_ParseGeneric(ty)
		}
	}
	for _, ty := range context.types {
		if !ty.generic && !ty.failed {
			Type_ParseCustom(ty)
		}
	}

	// check for uninstantiable cyclic types
	for _, ty := range context.types {
		if !ty.failed {
			Type_CheckDef(ty)
		}
	}

	// we need to find some commonly used types now, e.g. `bool`
	if module.is_prelude {
		Prelude_ResolveTypes()
	}

	if HaveErrors() {
		return
	}

	// parse all global vars and functions
	for _, gdef := range context.defs {
		if gdef.unparsed != nil {
			Glob_ParseDef(gdef)
		}
	}

	// parse all methods, add them to their types
	for _, t_meth := range module.unparsed_methods {
		Glob_ParseMethod(t_meth)
	}

	if HaveErrors() {
		return
	}

	// perform binding in all global functions and methods
	for _, gdef := range context.defs {
		if gdef.is_fun && !gdef.failed {
			BindClosure(gdef.fun_cl, nil)
		}
	}
	for _, t_meth := range module.unparsed_methods {
		BindClosure(t_meth.Info.lambda, nil)
	}

	// deduce type of all global vars and constants
	Glob_DeduceVars()

	// evaluate const-exprs in all global vars.
	// [ for functions/methods, it is done while deducing them ]
	Glob_EvaluateVars()

	// perform type-checking on all global functions and methods
	for _, gdef := range context.defs {
		if gdef.is_fun && gdef.fun_cl != nil && !gdef.failed {
			DeduceFunction(gdef)
		}
	}
	for _, t_meth := range module.unparsed_methods {
		DeduceMethod(t_meth.Info.lambda)
	}

	if HaveErrors() {
		return
	}

	// Note: global functions and methods only get compiled when
	//       needed (used by something else), and the root of this
	//       dependency tree is the main program expressions.
	//
	//       similarly, global constants and vars only get their
	//       values compiled when they are used by something.

	if module.is_main {
		CompileMainExpressions()
	}
}

func CompileMainExpressions() cmError {
	if len(module.unparsed_exprs) == 0 {
		PostError("no top-level expressions in main program")
		return FAILED
	}

	// construct a single expression to call the other expressions
	main_exp := NewNode(ND_Block, Position{Line: 0})
	main_exp.Info.ty = void_def.ty

	for _, t := range module.unparsed_exprs {
		t2, err2 := ParseMainElement(t)
		if err2 != OKAY {
			return FAILED
		}

		main_exp.Add(t2)
	}

	// add a ND_Void on the end
	t_void := NewNode(ND_Void, Position{Line: 0})
	t_void.Info.ty = void_def.ty

	main_exp.Add(t_void)

	ErrorSetToken(main_exp)

	// construct a closure for it
	cl := NewClosure("#Main#")

	cl.ty = NewType(TYP_Function)
	cl.ty.ret = void_def.ty
//--	cl.ty.instance = function_def

	cl.t_body = main_exp

	if BindClosure(cl, nil) != OKAY {
		return FAILED
	}
	if DeduceClosure(cl) != OKAY {
		return FAILED
	}
	if CompileClosure(cl) != OKAY {
		return FAILED
	}

	return OKAY
}

func CompileUsedFunctions() {
	// compile functions (etc) on an as-needed basis.
	// new things can be added to the pending list as we go.

	for len(pending_items) > 0 {
		it := pending_items[0]
		pending_items = pending_items[1:]

		CompileOneItem(it)
	}
}

func CompileOneItem(it *PendingItem) {
	if it.p_func != nil {
		Glob_CompileFunc(it.p_func)
	}
	if it.p_meth != nil {
		Glob_CompileMethod(it.p_meth)
	}
	if it.p_var != nil {
		Glob_CompileVar(it.p_var)
	}
}

//----------------------------------------------------------------------

func SaveAllModules() {
	for _, mod := range modules {
		module = mod
		context = mod.context

		if module.is_main {
			// tell native code to call this expression in _start or main()
			module.native.AddProgramEntry("#Main#")
		}

		NativeSaveModule(mod)
	}

	// globals.c
	Native_OutGlobals()

	// public.h
	Native_OutPublics()

	Runtimes_Output()
}
