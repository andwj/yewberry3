// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "fmt"
import "path/filepath"

type Module struct {
	// a simple name for this module (without any slash).
	// it may be different from the ".ymod" file, due to several
	// modules having the same name.  main purpose is to be a prefix
	// to distinguish identifiers in compiled code.
	name string

	dir   string
	files []*InFile

	context *Namespace
	native  *NativeOutput

	// true if this is the prelude (very first module)
	is_prelude bool

	// true if this is the main program (very last module)
	is_main bool

	// top-level expressions in the module
	unparsed_exprs []*Token

	// methods depend on their type being parsed already, which has
	// not happened at Load/Lex time, so they are stored here until
	// they can be parsed.
	unparsed_methods []*Token
}

type InFile struct {
	fullname string
}

// modules is the list of modules which are being compiled.
// this list is provided by the driver program.
// the first module is always the prelude, and the last one is
// always the main program.
var modules []*Module

// this is used for all top-level variables
var out_globals *GlobalMapper

// this contains a prototype for all PUBLIC funcs and vars
var public_headers *PublicProtos

// this contains all the runtime libs we will need
var runtime_libs map[string]bool

func InitModules() {
	modules = make([]*Module, 0)

	out_globals = NewGlobalMapper()
	public_headers = NewPublicProtos()

	runtime_libs = make(map[string]bool)

	Runtimes_Add("c/rt_common.c")
	Runtimes_Add("c/rt_gc.c")
	Runtimes_Add("c/rt_string.c")
}

func AddModule(name string) *Module {
	mod := new(Module)
	mod.name = name
	mod.context = NewNamespace()
	mod.native = NewNativeOutput()
	mod.files = make([]*InFile, 0)
	mod.unparsed_exprs = make([]*Token, 0)
	mod.unparsed_methods = make([]*Token, 0)

	modules = append(modules, mod)

	return mod
}

func FakeModuleList(code_name string) {
	// this function is only used for testing, when running the compiler
	// directly from a command line on a single YB code file.  under normal
	// usage (via the yb-build utility) this code is never run.

	yb_root := os.Getenv("YEWBERRY_ROOT")
	if yb_root == "" {
		yb_root = os.Getenv("HOME")
		if yb_root == "" {
			FatalError("YEWBERRY_ROOT is not set")
		}
		yb_root = filepath.Join(yb_root, "yewberry-3")
	}

	pre := AddModule("prelude")
	pre.is_prelude = true
	pre.AddFile(filepath.Join(yb_root, "prelude/pl_macros.yb"))
	pre.AddFile(filepath.Join(yb_root, "prelude/pl_base.yb"))
	pre.AddFile(filepath.Join(yb_root, "prelude/pl_map.yb"))
	pre.AddFile(filepath.Join(yb_root, "prelude/pl_set.yb"))
	pre.AddFile(filepath.Join(yb_root, "prelude/pl_sort.yb"))

	main := AddModule("main")
	main.is_main = true
	main.AddFile(code_name)
}

func ReadModuleList(list_name string) {
	f, err := os.Open(list_name)
	if err != nil {
		FatalError("error opening list file")
	}

	lex := NewLexer(f)

	for {
		tok := lex.Scan()

		if tok.Kind == TOK_EOF {
			break
		}
		if tok.Kind == TOK_ERROR {
			FatalError("bad module list: %s", tok.Str)
		}

		err := ParseModuleDef(tok)
		if err != nil {
			FatalError("bad module list: %s", err.Error())
		}
	}

	if len(modules) < 2 {
		FatalError("module list is too short")
	}
	if modules[0].name != "prelude" {
		FatalError("module list has no prelude")
	}

	modules[0].is_prelude = true
	modules[len(modules)-1].is_main = true
}

func ParseModuleDef(tok *Token) error {
	if tok.Kind != TOK_Expr {
		return fmt.Errorf("expected module in ()")
	}
	if len(tok.Children) < 4 {
		return fmt.Errorf("malformed statement")
	}

	t_head := tok.Children[0]
	if !t_head.Match("module") {
		return fmt.Errorf("unknown keyword '%s'", t_head.String())
	}

	// ignore the module number in Children[1]

	t_name := tok.Children[2]
	t_dir  := tok.Children[3]

	if t_name.Kind != TOK_String {
		return fmt.Errorf("module name is not a string")
	}
	if t_dir.Kind != TOK_String {
		return fmt.Errorf("module dir is not a string")
	}

	// verify name is unique
	for _, vv := range modules {
		if t_name.Str == vv.name {
			return fmt.Errorf("name '%s' used twice", t_name.Str)
		}
	}

	mod := AddModule(t_name.Str)
	mod.dir = t_dir.Str

	// parse the file names
	children := tok.Children[4:]

	for _, t_file := range children {
		if t_file.Kind != TOK_Expr {
			return fmt.Errorf("expected file in ()")
		}
		if len(t_file.Children) < 2 {
			return fmt.Errorf("malformed statement")
		}
		if t_file.Children[0].Match("file") {
			if t_file.Children[1].Kind != TOK_String {
				return fmt.Errorf("file name is not a string")
			}
			name := t_file.Children[1].Str
			fullname := filepath.Join(mod.dir, name)
			mod.AddFile(fullname)
		} else {
			return fmt.Errorf("unknown keyword '%s'",
				t_file.Children[0].String())
		}
	}

	return Ok
}

func (mod *Module) AddFile(fullname string) {
	f := new(InFile)
	f.fullname = fullname

	mod.files = append(mod.files, f)
}

func (mod *Module) AddUnparsedExpr(tok *Token) {
	mod.unparsed_exprs = append(mod.unparsed_exprs, tok)
}

func (mod *Module) AddUnparsedMethod(tok *Token) {
	mod.unparsed_methods = append(mod.unparsed_methods, tok)
}

func Runtimes_Add(name string) {
	runtime_libs[name] = true
}

func Runtimes_AddBuiltin(bu *Builtin) {
	if bu.c_runtime != "" {
		Runtimes_Add(bu.c_runtime)
	}
	if bu.asm_runtime != "" {
		Runtimes_Add(bu.asm_runtime)
	}
}

func Runtimes_Output() {
	// this will be random order [ due to Go ], but it doesn't matter
	for name, _ := range runtime_libs {
		fmt.Fprintf(os.Stdout, "(runtime \"%s\")\n", name)
	}
	for name, _ := range runtime_libs {
		// TODO use 'assemble' statement for .s
		fmt.Fprintf(os.Stdout, "(cc \"%s\")\n", filepath.Base(name))
	}
}

//----------------------------------------------------------------------

type Namespace struct {
	aliases map[string]*Alias
	modules map[string]*Module
	macros  map[string]*Macro
	types   map[string]*TypeDef
	defs    map[string]*GlobalDef
}

type Alias struct {
	mod_name string
	ident    string
}

// context represents the environment of the code currently being
// compiled.  that code will use this to lookup global identifiers.
// nothing outside of this namespace will be accessible without
// module prefixes (except the prelude).
var module  *Module
var context *Namespace

func NewNamespace() *Namespace {
	ns := new(Namespace)

	ns.aliases = make(map[string]*Alias)
	ns.macros = make(map[string]*Macro)
	ns.types = make(map[string]*TypeDef)
	ns.defs = make(map[string]*GlobalDef)
	ns.modules = make(map[string]*Module)

	return ns
}

func (ns *Namespace) HasModule(name string) bool {
	_, ok := ns.modules[name]
	return ok
}

func (ns *Namespace) GetModule(name string) *Module {
	return ns.modules[name]
}

func (ns *Namespace) AddModule(name string, mod *Module) {
	ns.modules[name] = mod
}

func (ns *Namespace) RemoveModule(name string) {
	delete(ns.modules, name)
}

func (ns *Namespace) HasMacro(name string) bool {
	_, ok := ns.macros[name]
	return ok
}

func (ns *Namespace) GetMacro(name string) *Macro {
	return ns.macros[name]
}

func (ns *Namespace) AddMacro(name string, mac *Macro) {
	ns.macros[name] = mac
}

func (ns *Namespace) RemoveMacro(name string) {
	delete(ns.macros, name)
}

func (ns *Namespace) HasType(name string) bool {
	_, ok := ns.types[name]
	return ok
}

func (ns *Namespace) GetType(name string) *TypeDef {
	return ns.types[name]
}

func (ns *Namespace) AddType(name string, tdef *TypeDef) {
	ns.types[name] = tdef
}

func (ns *Namespace) RemoveType(name string) {
	delete(ns.types, name)
}

func (ns *Namespace) HasDef(name string) bool {
	_, ok := ns.defs[name]
	return ok
}

func (ns *Namespace) GetDef(name string) *GlobalDef {
	return ns.defs[name]
}

func (ns *Namespace) AddDef(name string, gdef *GlobalDef) {
	ns.defs[name] = gdef
}

func (ns *Namespace) RemoveDef(name string) {
	delete(ns.defs, name)
}

//----------------------------------------------------------------------

func N_HasMacro(name, mod_name string) bool {
	return N_GetMacro(name, mod_name) != nil
}

func N_GetMacro(name, mod_name string) *Macro {
	if mod_name == "" {
		mac := context.GetMacro(name)
		if mac == nil {
			mac = modules[0].context.GetMacro(name)
		}
		return mac

	} else {
		mod := context.GetModule(mod_name)

		if mod == nil {
			return nil
		} else {
			return mod.context.GetMacro(name)
		}
	}
}

func N_HasType(name, mod_name string) bool {
	return N_GetType(name, mod_name) != nil
}

func N_GetType(name, mod_name string) *TypeDef {
	if mod_name == "" {
		tdef := context.GetType(name)
		if tdef == nil {
			tdef = modules[0].context.GetType(name)
		}
		return tdef

	} else {
		mod := context.GetModule(mod_name)

		if mod == nil {
			return nil
		} else {
			return mod.context.GetType(name)
		}
	}
}

func N_HasDef(name, mod_name string) bool {
	return N_GetDef(name, mod_name) != nil
}

func N_GetDef(name, mod_name string) *GlobalDef {
	if mod_name == "" {
		gdef := context.GetDef(name)
		if gdef == nil {
			gdef = modules[0].context.GetDef(name)
		}
		return gdef

	} else {
		mod := context.GetModule(mod_name)

		if mod == nil {
			return nil
		} else {
			return mod.context.GetDef(name)
		}
	}
}
