// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
import "os"
import "strings"
import "unicode"
import "path/filepath"

// NativeOutput represents a single output file for C or ASM code.
// Usually each module becomes a single file.
type NativeOutput struct {
	file *os.File

	blocks []*CodeBlock

	last_blank bool
}

var func_types map[string]string

func InitNative() {
	func_types = make(map[string]string)
}

func NewNativeOutput() *NativeOutput {
	np := new(NativeOutput)

	np.blocks = make([]*CodeBlock, 0)

	return np
}

func NativeSaveModule(mod *Module) cmError {
	return mod.native.SaveToFile("mod_" + mod.name)
}

func Native_OutGlobals() {
	out_globals.Finish()

	all_globs := NewNativeOutput()

	all_globs.AddBlock(out_globals.normal_c)
	all_globs.AddBlock(out_globals.externs)
	all_globs.AddBlock(out_globals.descripts)

	all_globs.AddBlock(out_globals.simp_asg)
	all_globs.AddBlock(out_globals.func_asg)
	all_globs.AddBlock(out_globals.comp_make)

	all_globs.SaveToFile("globals")
}

func Native_OutPublics() {
	publics := NewNativeOutput()

	publics.AddBlock(public_headers.misc)
	publics.AddBlock(public_headers.types)
	publics.AddBlock(public_headers.vars)
	publics.AddBlock(public_headers.funcs)

	publics.AddBlock(out_globals.normal_h)
	publics.AddBlock(out_globals.alloc_h)

	publics.RawSave("public.h", false)
}

func (np *NativeOutput) SaveToFile(name string) cmError {
	name = name + ".c"

	fmt.Fprintf(os.Stdout, "(cc \"%s\")\n", name)

	return np.RawSave(name, true)
}

func (np *NativeOutput) RawSave(name string, use_public bool) cmError {
	name = filepath.Join(Options.out_dir, name)

	f, err := os.Create(name)
	if err != nil {
		PostError("failed to create ASM code file")
		return FAILED
	}

	np.file = f

	if use_public {
		np.WriteLine("#include \"public.h\"")
		np.WriteLine("")
	}

	np.WriteBlocks()

	np.file.Close()
	np.file = nil

	return OKAY
}

//----------------------------------------------------------------------

func BuildFunctionType(ty *Type) string {
	c_rettype := ConvertType(ty.ret)
	c_pars    := ConvertParamTypes(ty, ty, false)

	key := c_rettype + "(" + c_pars + ")"

	if func_types[key] == "" {
		ident := fmt.Sprintf("_funtype%d", 1 + len(func_types))
		func_types[key] = ident

		public_headers.DeclareType(ident + "(" + c_pars + ")", c_rettype)
	}

	return func_types[key]
}

//----------------------------------------------------------------------

func (np *NativeOutput) AddProgramEntry(clos_name string) {
	c_name := EncodeName("_fu_", "", clos_name)

	cons_name := EncodeName("_fu_", "", "#Construct#")
	glob_name := EncodeName("_fu_", "", "#Globals#")
	pop_name  := EncodeName("_fu_", "", "populate-ARGS-ENVS")

	code := NewCodeBlock()

	public_headers.DeclareFunc(c_name, "void", "YFunction *")

	code.Printf("int main(int argc, char *argv[])")
	code.BeginNest()

	code.Printf("_rt_init(argc, argv);")

	// ensure all globals are in the root set.
	// this also clears the pointers, so it must be done early.
	code.Printf("_gc_push_locals((GC_Locals *) &GV, %d);", out_globals.num_alloc_glob)

	// create (but don't populate) all simple-ish global vars
	code.Printf("_create_globals();")

	// run the top-level expression.
	// it might not return, e.g. when `exit-program` is used.
	code.BeginNest()
	code.Printf("%s(NULL);", cons_name)
	code.Printf("%s(NULL);", glob_name)
	code.Printf("%s(NULL);", pop_name)
	code.Printf("%s(NULL);", c_name)
	code.EndNest()

	code.Printf("_rt_cleanup();")
	code.Printf("return 0;")
	code.EndNest()

	np.AddBlock(code)
}

func (np *NativeOutput) ProcessMethod(m_cl *Closure) {
	// nothing to declare for builtins
	if m_cl.builtin != nil {
		return
	}

	// FIXME
	mod_name := ""

	c_name := EncodeName("_me_", mod_name, m_cl.clos_name)
	c_type := ConvertType(m_cl.ty.ret)
	c_pars := ConvertParamTypes(m_cl.ty, nil, false)

	public_headers.DeclareFunc(c_name, c_type, c_pars)
}

func (np *NativeOutput) AddBlock(grp *CodeBlock) {
	np.blocks = append(np.blocks, grp)
}

//----------------------------------------------------------------------

type GlobalMapper struct {
	// the output C code
	normal_c *CodeBlock
	normal_h *CodeBlock

	alloc_h *CodeBlock
	num_alloc_glob int

	externs   *CodeBlock  // extern decls for functions
	simp_asg  *CodeBlock  // assignment, simple types and strings
	func_asg  *CodeBlock  // function allocators
	comp_make *CodeBlock  // complex types, make the bare object

	// descriptors for classes/maps/etc
	class_desc  map[*Type]*ClassDescriptor
	descripts  *CodeBlock
}

type ClassDescriptor struct {
	out_name   string
	class_name string
	ref_fields string
}

func NewGlobalMapper() *GlobalMapper {
	gm := new(GlobalMapper)

	gm.normal_c = NewCodeBlock()
	gm.normal_h = NewCodeBlock()
	gm.alloc_h  = NewCodeBlock()

	gm.externs   = NewCodeBlock()
	gm.simp_asg  = NewCodeBlock()
	gm.func_asg  = NewCodeBlock()
	gm.comp_make = NewCodeBlock()

	gm.class_desc = make(map[*Type]*ClassDescriptor)
	gm.descripts   = NewCodeBlock()

	gm.alloc_h.Printf("typedef struct")
	gm.alloc_h.Printf("{")
	gm.alloc_h.Printf("  GC_Locals _gc;")
	gm.alloc_h.indent = 1

	gm.externs.Printf("_gc_global_vars_t GV;")
	gm.externs.Printf("")

	gm.simp_asg.Printf("void _create_globals(void)")
	gm.simp_asg.Printf("{")

	gm.simp_asg.indent = 1
	gm.func_asg.indent = 1
	gm.comp_make.indent = 1

	return gm
}

func (gm *GlobalMapper) Finish() {
	gm.alloc_h.indent = 0
	gm.alloc_h.Printf("} _gc_global_vars_t;")
	gm.alloc_h.Printf("")
	gm.alloc_h.Printf("extern _gc_global_vars_t GV;")

	gm.comp_make.indent = 0
	gm.comp_make.Printf("}")
}

func (gm *GlobalMapper) Declare(gdef *GlobalDef) {
	c_type := ConvertType(gdef.ty)

	if gdef.ty.IsAllocated() {
		gm.num_alloc_glob += 1

		gm.alloc_h.Printf("%s %s;", c_type, gdef.out_name)

		gdef.out_name = "GV." + gdef.out_name

	} else {
		gm.normal_c.Printf("%s %s;", c_type, gdef.out_name)
		gm.normal_h.Printf("extern %s %s;", c_type, gdef.out_name)
	}
}

func (gm *GlobalMapper) GetClassDesc(ty *Type) string {
	if gm.class_desc[ty] != nil {
		return gm.class_desc[ty].out_name
	}

	desc := new(ClassDescriptor)
	gm.class_desc[ty] = desc

	if ty.IsCustom() {
		desc.class_name = ty.def.name
	} else {
		desc.class_name = fmt.Sprintf("[class-%d]", len(gm.class_desc))
	}

	mod_name := ""  // TODO

	desc.out_name = EncodeName("_info_", mod_name, desc.class_name)

	// build a C string for ref_fields
	ref_fields := ""
	for _, field := range ty.param {
		if field.ty.IsAllocated() {
			ref_fields += "\\1"
		} else {
			ref_fields += "\\0"
		}
	}
	desc.ref_fields = ref_fields

	// declare the descriptor
	public_headers.DeclareVar(desc.out_name, "const YClassDescriptor")

	// create C code for the descriptor
	gm.descripts.Printf("const YClassDescriptor %s =", desc.out_name)
	gm.descripts.Printf("{")
	gm.descripts.Printf("  %s,", EncodeStrLiteral(desc.class_name))
	gm.descripts.Printf("  \"%s\",", desc.ref_fields)
	gm.descripts.Printf("};")

	return desc.out_name
}

//----------------------------------------------------------------------

type CodeBlock struct {
	lines  []string
	indent int
}

func NewCodeBlock() *CodeBlock {
	group := new(CodeBlock)
	group.lines = make([]string, 0)
	group.indent = 0
	return group
}

func (group *CodeBlock) Printf(format string, a ...interface{}) {
	group.Add(fmt.Sprintf(format, a...))
}

func (group *CodeBlock) Add(line string) {
	if line != "" {
		for i := 0; i < group.indent; i++ {
			line = "  " + line
		}
	}

	group.lines = append(group.lines, line)
}

func (group *CodeBlock) RawAdd(line string) {
	group.lines = append(group.lines, line)
}

func (group *CodeBlock) Blank() {
	group.RawAdd("")
}

func (group *CodeBlock) Comment(cmt string) {
	group.Printf("// %s", cmt)
}

func (group *CodeBlock) BeginNest() {
	group.Printf("{")
	group.indent += 1
}

func (group *CodeBlock) EndNest() {
	group.indent -= 1
	group.Printf("}")
}

func (np *NativeOutput) WriteBlocks() {
	for _, blk := range np.blocks {
		np.WriteGroup(blk)
	}
}

func (np *NativeOutput) WriteGroup(group *CodeBlock) {
	for _, l := range group.lines {
		np.WriteLine(l)
	}
	np.WriteLine("")
}

func (np *NativeOutput) WriteLine(s string) {
	// prevent multiple blank lines in a row
	if s == "" && np.last_blank {
		return
	}
	np.last_blank = (s == "")

	_, err := fmt.Fprintf(np.file, "%s\n", s)
	if err != nil {
		// FIXME mark the error
	}
}

//----------------------------------------------------------------------

type PublicProtos struct {
	// things already done
	seen map[string]bool

	// the output C code
	misc  *CodeBlock
	types *CodeBlock
	vars  *CodeBlock
	funcs *CodeBlock
}

func NewPublicProtos() *PublicProtos {
	pp := new(PublicProtos)
	pp.seen = make(map[string]bool)

	pp.misc  = NewCodeBlock()
	pp.types = NewCodeBlock()
	pp.vars  = NewCodeBlock()
	pp.funcs = NewCodeBlock()

	pp.misc.Printf("#include \"rt_common.h\"")
	pp.misc.Printf("")
	pp.misc.Printf("void _create_globals(void);")

	return pp
}

func (pp *PublicProtos) DeclareType(c_name, c_type string) {
	if !pp.seen[c_name] {
		pp.types.Printf("typedef %s %s;", c_type, c_name)
		pp.seen[c_name] = true
	}
}

func (pp *PublicProtos) DeclareVar(c_name, c_type string) {
	if !pp.seen[c_name] {
		pp.vars.Printf("extern %s %s;", c_type, c_name)
		pp.seen[c_name] = true
	}
}

func (pp *PublicProtos) DeclareFunc(c_name, c_type, c_pars string) {
	if !pp.seen[c_name] {
		pp.funcs.Printf("%s %s(%s);", c_type, c_name, c_pars)
		pp.seen[c_name] = true
	}
}

//----------------------------------------------------------------------

func EncodeName(prefix, mod, name string) string {
	// encode identifiers to be compatible with C/ASM code:
	//   1. letters and digits stay the same
	//   2. convert '-' to "__", except at beginning
	//   3. escape rest as "_XX" or "_uXXXX" or "_vXXXXXX"
	//   4. if solely ASCII letters and digits, append "_"

	has_escape := false

	if mod != "" {
		name = mod + name
	}

	var sb strings.Builder

	sb.WriteString(prefix)

	for pos, ch := range []rune(name) {
		if ch > 0xFFFF {
			fmt.Fprintf(&sb, "_v%06X", ch)
			has_escape = true
		} else if ch > 0xFF {
			fmt.Fprintf(&sb, "_u%04X", ch)
			has_escape = true
		} else if ch == '-' && pos > 0 {
			sb.WriteString("__")
			has_escape = true
		} else if unicode.IsLetter(ch) || unicode.IsDigit(ch) {
			sb.WriteByte(byte(ch))
		} else {
			fmt.Fprintf(&sb, "_%02X", ch)
			if ch == '/' {
				has_escape = false
			} else {
				has_escape = true
			}
		}
	}

	// prevent words like "goto" or "sin" clashing with C keywords
	if !has_escape && prefix == "" {
		sb.WriteByte(byte('_'))
	}

	return sb.String()
}

func EncodeStrLiteral(s string) string {
	res := "\""

	var i int
	var last int

	for i = 0; i < len(s); i++ {
		var b byte = s[i]  // raw byte [ NOT rune! ]

		if b == '\n' {
			res += "\\n"
		} else if b == '\t' {
			res += "\\t"
		} else if b < 32 || b > 127 || b == '"' || b == '\\' {
			// need to use three octal digits.
			// [ one or two could swallow a following digit ]
			res += fmt.Sprintf("\\%03o", b)
		} else {
			res += string(rune(b))
		}

		// split very long strings into multiple pieces
		if len(res) - last > 80 {
			if res[0] != '\n' {
				res = "\n" + res
			}
			res += "\"\n\""
			last = len(res)
		}
	}

	return res + "\""
}

func EncodeCharLiteral(ch rune) string {
	if ch >= 32 && ch < 127 && ch != '\'' {
		return "'" + string(ch) + "'"
	} else {
		return fmt.Sprintf("0x%X", ch)
	}
}

func ConvertType(ty *Type) string {
	if ty.IsChar() {
		return "YChar"
	}
	if ty.IsBool() {
		return "YBool"
	}

	if ty.def != nil && !ty.def.builtin {
		// FIXME mod name
		mod_name := ""

		c_name := EncodeName("_ty_", mod_name, ty.def.name)
		c_type := ConvertType2(ty)

		public_headers.DeclareType(c_name, c_type)
		return c_name
	}

	return ConvertType2(ty)
}

func ConvertType2(ty *Type) string {
	ty = ty.Real()

	switch ty.base {
	case TYP_Void:
		return "void"

	case TYP_Int:
		return "YInt"

	case TYP_Float:
		return "YFloat"

	case TYP_String:
		return "YString *"

	case TYP_Enum:
		return "YEnum"

	case TYP_ByteVec:
		return "YByteVec *"

	case TYP_Class, TYP_Interface:
		return "YClassObj *"

	case TYP_Union:
		return "YUnion *"

	case TYP_Array:
		return "YArray *"

	case TYP_Function:
		return "YFunction *"

	default:
		panic("weird type given to ConvertType: " + ty.String())
	}
}

func ConvertParamTypes(func_type, self_type *Type, do_names bool) string {
	c_pars := ""

	if self_type != nil {
		c_pars = ConvertType(self_type)
		if do_names {
			c_pars += " _self"
		}
	}

	for _, par := range func_type.param {
		if c_pars != "" {
			c_pars += ", "
		}

		c_pars += ConvertType(par.ty)

		if do_names {
			c_pars += " " + EncodeName("", "", par.name)
		}
	}

	if c_pars == "" {
		return "void"
	}
	return c_pars
}
