// Copyright 2018 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

// import "fmt"

type OperatorInfo struct {
	name   string
	method string

	unary       bool
	precedence  int
	right_assoc bool
}

var unary_ops  map[string]*OperatorInfo
var binary_ops map[string]*OperatorInfo

func SetupOperators() {
	unary_ops = make(map[string]*OperatorInfo)
	binary_ops = make(map[string]*OperatorInfo)

	/* unary operators */

	RegisterOperator(0, "not", ".not")
	RegisterOperator(0, "-",   ".neg")
	RegisterOperator(0, "~",   ".flip")

	/* binary infix operators */

	// NOTE: these two are special since they are short-circuiting.
	// they actually invoke a macro instead of becoming a method call.
	RegisterOperator(1, "or",  "m-or")
	RegisterOperator(2, "and", "m-and")

	RegisterOperator(3, "==", ".eq?")
	RegisterOperator(3, "!=", ".ne?")
	RegisterOperator(4, "<",  ".lt?")
	RegisterOperator(4, "<=", ".le?")
	RegisterOperator(4, ">",  ".gt?")
	RegisterOperator(4, ">=", ".ge?")

	RegisterOperator(5, "+",  ".add")
	RegisterOperator(5, "-",  ".sub")
	RegisterOperator(6, "*",  ".mul")
	RegisterOperator(6, "/",  ".div")
	RegisterOperator(6, "%",  ".rem")
	RegisterOperator(7, "**", ".pow")

	RegisterOperator(5, "|", ".or")
	RegisterOperator(5, "^", ".xor")
	RegisterOperator(6, "&", ".and")

	RegisterOperator(6, "<<", ".left")
	RegisterOperator(6, ">>", ".right")
}

func RegisterOperator(prec int, name, method string) {
	info := new(OperatorInfo)
	info.name = name
	info.method = method

	info.unary = (prec == 0)
	info.precedence = prec
	info.right_assoc = (name == "**")

	if prec == 0 {
		unary_ops[name] = info
	} else {
		binary_ops[name] = info
	}
}

func IsOperator(s string) bool {
	return binary_ops[s] != nil || unary_ops[s] != nil
}

func GetOperator(s string, unary bool) *OperatorInfo {
	if unary {
		return unary_ops[s]
	} else {
		return binary_ops[s]
	}
}

func IsOperatorToken(t *Token) bool {
	return t.Kind == TOK_Name && IsOperator(t.Str)
}

func HasOperator(t *Token) bool {
	for _, child := range t.Children {
		if child.Kind == TOK_Name && IsOperator(child.Str) {
			return true
		}
	}
	return false
}

func (op *OperatorInfo) IsComparison() bool {
	return op.precedence == 3 || op.precedence == 4
}


//----------------------------------------------------------------------

// OperatorExpand checks if the given token or any child in the
// tree is a math expression, is if so it parses that expression
// into a new one where the operators are replaced by method calls.
//
// [ essentially this is a kind of macro expansion ]
//
// The result is either the input token (possibly modified), or a
// newly created token tree.  Returns NIL if an error occurred.
func OperatorExpand(tok *Token) *Token {
	// recursively handle children
	if tok.Kind >= TOK_Expr {
		for i := 0; i < len(tok.Children); i++ {
			child := tok.Children[i]

			newchild := OperatorExpand(child)
			if newchild == nil {
				return nil
			}

			tok.Children[i] = newchild
		}
	}

	if tok.Kind == TOK_Expr && HasOperator(tok) {
		return OperatorParseExpr(tok)
	} else {
		return tok
	}
}

func OperatorParseExpr(t *Token) *Token {
	/* first pass : handle multi-token function calls */

	children := make([]*Token, 0)

	for len(t.Children) > 0 {
		elem := t.Children[0]
		t.Children = t.Children[1:]

		// check for two or more terms in a row, convert to an S-expr
		if !IsOperatorToken(elem) &&
			len(t.Children) > 0 &&
			!IsOperatorToken(t.Children[0]) {

			new_exp := NewNode(TOK_Expr, elem.Pos)
			new_exp.Add(elem)

			// for an identifier followed by `()`, drop the `()`.
			// this mimics the sweet syntax for no-arg function calls.
			if elem.Kind == TOK_Name &&
				(len(t.Children) == 1 ||
				 (len(t.Children) > 1 && IsOperatorToken(t.Children[1]))) &&
				t.Children[0].Kind == TOK_Expr &&
				len(t.Children[0].Children) == 0 {

				t.Children = t.Children[1:]
			}

			for len(t.Children) > 0 && !IsOperatorToken(t.Children[0]) {
				new_exp.Add(t.Children[0])
				t.Children = t.Children[1:]
			}

			elem = new_exp
		}

		children = append(children, elem)
	}

	/* this is Dijkstra's shunting-yard algorithm */

	op_stack := make([]*OperatorInfo, 0)
	term_stack := make([]*Token, 0)

	shunt := func() cmError {
		// the op_stack is never empty when this is called
		op := op_stack[len(op_stack)-1]
		op_stack = op_stack[0 : len(op_stack)-1]

		// possible??
		if len(term_stack) < 2 {
			PostError("FAILURE AT THE SHUNTING YARD")
			return FAILED
		}

		L := term_stack[len(term_stack)-2]
		R := term_stack[len(term_stack)-1]
		term_stack = term_stack[0 : len(term_stack)-2]

		t_method := NewNode(TOK_Name, L.Pos)
		t_method.Str = op.method

		node := NewNode(TOK_Expr, L.Pos)
		node.Add(t_method)
		node.Add(L)
		node.Add(R)

		// mark node as a binary operator
		node.Info.op = op

		term_stack = append(term_stack, node)
		return OKAY
	}

	// saved unary ops for the next (unseen) term
	un_ops := make([]*OperatorInfo, 0)

	seen_term := false

	for _, tok := range children {
		if tok.Kind == TOK_Name && IsOperator(tok.Str) {
			// operator is binary after a term, unary otherwise
			unary := !seen_term

			op := GetOperator(tok.Str, unary)

			if op == nil {
				PostError("bad math expression: unexpected %s operator", tok.Str)
				return nil
			}

			if unary {
				un_ops = append(un_ops, op)
				continue
			}

			if len(un_ops) > 0 {
				panic("OOPSIE IN MATH PARSE")
			}

			// shunt existing operators if they have greater precedence
			for len(op_stack) > 0 {
				top := op_stack[len(op_stack)-1]

				if top.precedence > op.precedence ||
					(top.precedence == op.precedence && !top.right_assoc) {

					if shunt() != OKAY {
						return nil
					}
					continue
				}

				break
			}

			op_stack = append(op_stack, op)
			seen_term = false

		} else {
			// if not an operator, it must be a term
			seen_term = true

			term := tok

			// apply any saved unary operators
			for len(un_ops) > 0 {
				op := un_ops[len(un_ops)-1]
				un_ops = un_ops[0 : len(un_ops)-1]

				t_method := NewNode(TOK_Name, term.Pos)
				t_method.Str = op.method

				node := NewNode(TOK_Expr, term.Pos)
				node.Add(t_method)
				node.Add(term)
				node.Info.op = op

				term = node
			}

			term_stack = append(term_stack, term)
		}
	}

	if !seen_term || len(un_ops) > 0 {
		PostError("bad math expression, missing term after last operator")
		return nil
	}

	// handle the remaining operators on stack
	for len(op_stack) > 0 {
		if shunt() != OKAY {
			return nil
		}
	}

	if len(term_stack) != 1 {
		PostError("PARSE MATH FAIlURE")
		return nil
	}

	// replace input token with root of the expression tree
	return term_stack[0]
}
