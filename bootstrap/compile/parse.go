// Copyright 2019 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

// A "node" is a token which has been analysed and given an ND_XXX
// kind and/or extra information in the Info field.  The code in
// this file is primarily concerned with syntax, it is *the* place
// where special syntax is decoded into a simpler node tree.

import "fmt"
import "strconv"

const (
	ND_INVALID TokenKind = 20 + iota

	ND_DefVar    // name | value.  NOTE: ND_Let is used within functions.
	ND_DefFunc   // name | params | body.  Info.ty is function signature.
	ND_DefMethod // name | params | body.  Info.ty is signature.  params[0] has receiver type
	ND_Params    // each child is a TOK_Name, Info.ty has its type (nil if not given)
	ND_Lambda    // params | body.   Info.lambda gets a template closure.
	ND_Native    // represents #native syntax, .Str is the target name.

	ND_Void      // the {void} value, or lack of a value
	ND_Integer   // an integer literal (used for char too)
	ND_Float     // a floating-point literal
	ND_String    // a string literal
	ND_Enum      // an enum literal.  Str is tag name.  Info.ty may be NIL

	ND_Struct    // for stuff in {} when we don't know the overall type
	ND_Array     // children are the array elements
	ND_Map       // children are ND_MapElem
	ND_MapElem   // first child is key, second is value
	ND_Set       // children are the set elements
	ND_Class     // children are field values or ND_Field. Info has type
	ND_Field     // child is the value, Str is field name (or "" and Info.field is set)
	ND_Union     // child is datum value, Str is the tag name
	ND_Opt       // child is the `VAL value
	ND_StrBuild  // children are string values/exprs

	ND_Local     // Str is name, Info.lvar is variable info
	ND_Upvar     // Str is name, Info.upvar is variable info
	ND_Global    // Str is name, Info.gdef is global definition

	ND_FunCall     // func |       zero or more parameters.
	ND_MethodCall  // name | obj | zero or more parameters.

	ND_Block     // sequence of elements, at least one
	ND_Skip      // child is value.  Str is label
	ND_If        // condition | then body | else body
	ND_Loop      // body
	ND_Let       // var | value | body
	ND_Construct // var | alloc | populate

	ND_Cast      // child is value, Info.ty is new type
	ND_BaseCast  // child is value
	ND_Unwrap    // child is value
	ND_Flatten   // child is value
	ND_Fmt       // child is value, Info.fmt_spec is format spec.
	ND_Default   // Info.ty is the type to create a default value for.
	ND_AllocObj  // Info.ty is the type (array etc).
	ND_DummySelf // Info.ty is the type.

	ND_SetVar    // var | value
	ND_SetField  // obj | value.  Str is field name
	ND_GetField  // obj           Str is field name
	ND_SetUnion  // union | value.

	ND_IsTag       // value | tag names...
	ND_RefEqual    // two children, L and R values

	ND_Match       // first is expr, rest are ND_MatchRule nodes
	ND_MatchRule   // pattern | body | where-cond (optional)
	ND_MatchAll    // represents the '_' match-all pattern
	ND_MatchConsts // one or more constant values
	ND_MatchTags   // one or more tag names
	ND_MatchStruct // for stuff in {}
	ND_MatchVar    // a variable binding, Str is name, lvar is info.
)

// NodeInfo provides extra information for each Token (i.e. node)
// in the code tree.
type NodeInfo struct {
	// the type for ND_Array, ND_Map, ND_Union, ND_Cast (etc).
	// for arrays and maps it is generally NIL unless the type was
	// explicitly specified, though it gets decided eventually.
	// for ND_DefFunc, ND_DefMethod it is the function signature.
	ty *Type

	// for ND_Struct, the first element was the name of this generic type.
	data_def *TypeDef

	// for ND_DefMethod, the typedef for the receiver (self)
	recv_def *TypeDef

	// for ND_Let and ND_DefVar, indicates the "let-mut" form was used
	mutable bool

	// for ND_Fmt, the decoded format string like "%3d"
	fmt_spec *Formatter

	// for match against arrays (etc), -1 means "..." at start, +1 means at end.
	open int

	// marks tail-call positions (for function/method calls)
	tail bool

	// for ND_MethodCall, non-nil when it was an operator
	op *OperatorInfo

	// has field index for ND_Field (used by ND_Class).
	// also used by ND_GetField and ND_SetField.
	field int

	// store the tag number of ND_Enum and ND_Union (once known).
	// also used by ND_MatchTags.
	tag_id int

	// for ND_FunCall, this is "" to call a monomorphized function
	monomorph string

	// for ND_Local and ND_Let
	lvar *LocalVar

	// for ND_Upvar
	upvar *Upvar

	// for ND_Global
	gdef *GlobalDef

	// for ND_Lambda
	lambda *Closure

	// for ND_Skip and ND_Block
	junction *JunctionInfo

	// marks a node as deduced, it won't be deduced again
	deduced bool

	// size of array for ND_AllocObj   TODO: REVIEW if needed
	length int

	// for ND_MatchRule, all the bindings created in patterns
	group *LocalGroup

	// global var crud
	is_top bool
}

type ParserState struct {
	// for Type_ParseDef, this is the current def
	def *TypeDef

	// for ParseType, status of handling generic type parameters
	gen_active bool  // may refer to a `@T`
	gen_create bool  // may create a new `@T`
	tgroup     map[string]*Type
}

func NewNode(kind TokenKind, pos Position) *Token {
	nd := new(Token)
	nd.Kind = kind
	nd.Pos  = pos
	nd.Info = NewNodeInfo()
	nd.Children = make([]*Token, 0)
	return nd
}

func NewNodeInfo() *NodeInfo {
	nd := new(NodeInfo)
	nd.field  = -1
	nd.tag_id = -1
	return nd
}

func (nd *Token) InitInfo() {
	if nd.Info == nil {
		nd.Info = NewNodeInfo()
	}

	if nd.Children != nil {
		for _, child := range nd.Children {
			child.InitInfo()
		}
	}
}

func (t *Token) String2() string {
	// this handles the ND_XXX node types defined in the file

	switch t.Kind {
	case ND_DefVar: return "ND_DefVar"
	case ND_DefFunc: return "ND_DefFunc"
	case ND_DefMethod: return "ND_DefMethod"
	case ND_Params: return "ND_Params"
	case ND_Lambda: return "ND_Lambda"
	case ND_Native: return "ND_Native"

	case ND_Void: return "ND_Void"
	case ND_Integer: return "ND_Integer '" + t.Str + "'"
	case ND_Float: return "ND_Float '" + t.Str + "'"
	case ND_String: return "ND_String '" + t.Str + "'"
	case ND_Enum: return "ND_Enum " + t.Str

	case ND_Struct: return "ND_Struct"
	case ND_Array: return "ND_Array"
	case ND_Set: return "ND_Set"
	case ND_Map: return "ND_Map"
	case ND_MapElem: return "ND_MapElem"
	case ND_Class: return "ND_Class"
	case ND_Field: return "ND_Field '" + t.Str + "'"
	case ND_Union: return "ND_Union"
	case ND_Opt: return "ND_Opt"
	case ND_StrBuild: return "ND_StrBuild"

	case ND_Local: return "ND_Local '" + t.Str + "'"
	case ND_Upvar: return "ND_Upvar '" + t.Str + "'"
	case ND_Global: return "ND_Global '" + t.Str + "'"

	case ND_FunCall: return "ND_FunCall"
	case ND_MethodCall: return "ND_MethodCall " + t.Str

	case ND_Block: return "ND_Block"
	case ND_Skip: return "ND_Skip " + t.Str
	case ND_If: return "ND_If"
	case ND_Loop: return "ND_Loop"

	case ND_Let: return "ND_Let"
	case ND_Construct: return "ND_Construct"
	case ND_Cast: return "ND_Cast"
	case ND_BaseCast: return "ND_BaseCast"
	case ND_Unwrap: return "ND_Unwrap"
	case ND_Flatten: return "ND_Flatten"
	case ND_Fmt: return "ND_Fmt"
	case ND_Default: return "ND_Default"
	case ND_AllocObj: return "ND_AllocObj"
	case ND_DummySelf: return "ND_DummySelf"

	case ND_SetVar: return "ND_SetVar"
	case ND_SetField: return "ND_SetField '" + t.Str + "'"
	case ND_GetField: return "ND_GetField '" + t.Str + "'"
	case ND_SetUnion: return "ND_SetUnion"
	case ND_IsTag: return "ND_IsTag"
	case ND_RefEqual: return "ND_RefEqual"

	case ND_Match: return "ND_Match"
	case ND_MatchRule: return "ND_MatchRule"
	case ND_MatchAll: return "ND_MatchAll '_'"
	case ND_MatchConsts: return "ND_MatchConsts"
	case ND_MatchTags: return "ND_MatchTags"
	case ND_MatchStruct: return "ND_MatchStruct"
	case ND_MatchVar: return "ND_MatchVar"

	default:
		return "!!INVALID!!"
	}
}

func (nd *Token) InfoString() string {
	if nd.Info == nil {
		return ""
	}

	s := ""

	switch nd.Kind {
	case ND_DefVar, ND_Let:
		if nd.Info.mutable {
			s = "mutable"
		}
	}

	// show type information, but not too long
	if nd.Info.ty != nil {
		ts := nd.Info.ty.String()
		if len(ts) > 32 {
			ts = ts[0:30] + "..."
		}

		if s != "" {
			s += " "
		}
		s += "ty:" + ts
	}

	return s
}

// DumpTokens is a debugging aid, displays each high-level token.
func DumpTokens(lex *Lexer) {
	for {
		t := lex.Scan()

		t.Dump(0)

		if t.Kind == TOK_EOF {
			break
		}
	}
}

func (t *Token) Dump(level int) {
	Print("line %4d: %*s%s %s %s\n", t.Pos.Line, level, "",
		t.String(), t.InfoString(), t.Cat.String())

	if t.Children != nil {
		for _, child := range t.Children {
			child.Dump(level + 2)
		}
	}
}

//----------------------------------------------------------------------

func ParseMainElement(t *Token) (*Token, cmError) {
	ErrorSetToken(t)

	// expand macros here
	macro_stamp = 0

	// must expand operators first, because:
	// (a) the "&&" and "||" operators use macros
	// (b) allow unbracketed func calls to be a macro
	t = OperatorExpand(t)
	if t == nil {
		return nil, FAILED
	}

	t = MacroExpand(t)
	if t == nil {
		return nil, FAILED
	}

	ps := new(ParserState)

	t = ps.ParseElement(t)
	if t == nil {
		return nil, FAILED
	}

	return t, OKAY
}

func (ps *ParserState) ParseElement(t *Token) *Token {
	ErrorSetToken(t)

	// ensure node and all children have an Info struct
	t.InitInfo()

	switch t.Kind {
	case TOK_Name:
		return ps.ParseName(t)

	case TOK_Int:
		return ps.ParseInt(t)

	case TOK_Float:
		return ps.ParseFloat(t)

	case TOK_Char:
		return ps.ParseChar(t)

	case TOK_String:
		return ps.ParseString(t)

	case TOK_Expr:
		return ps.ParseExpr(t)

	case TOK_Access:
		return ps.ParseAccess(t)

	case TOK_Data:
		return ps.ParseDataStruct(t)

	default:
		PostError("syntax error, got: %s", t.String())
		return nil
	}
}

func (ps *ParserState) ParseAllChildren(t *Token) *Token {
	for i, child := range t.Children {
		new_child := ps.ParseElement(child)
		if new_child == nil {
			return nil
		}
		t.Children[i] = new_child
	}
	return t
}

func (ps *ParserState) ParseInt(t *Token) *Token {
	// Note: the integer is validated by DeduceBasicLiteral

	t.Kind = ND_Integer
	t.Info.ty = int_def.ty
	return t
}

func (ps *ParserState) ParseFloat(t *Token) *Token {
	// Note: the float is validated by DeduceBasicLiteral

	t.Kind = ND_Float
	t.Info.ty = float_def.ty
	return t
}

func (ps *ParserState) ParseChar(t *Token) *Token {
	runes := []rune(t.Str)

	ch := runes[0]

	// since the `char` type is really based on TYP_Int, we need to
	// convert the character to something which will parse as an
	// integer.

	t.Kind = ND_Integer
	t.Info.ty = char_def.ty
	t.Str = strconv.Itoa(int(ch))
	return t
}

func (ps *ParserState) ParseString(t *Token) *Token {
	// the lexer ensures that the string is valid
	t.Kind = ND_String
	t.Info.ty = str_def.ty
	return t
}

func (ps *ParserState) ParseDataStruct(t *Token) *Token {
	children := t.Children

	if len(children) > 0 {
		head := children[0]

		// handle indicator of type, either a vague indicator like
		// "class" and "union" (where full type will be deduced later)
		// or an explicit type.

		switch {
		case head.Match("class"):
			return ps.ParseData_General(t, true, ND_Class)

		case head.Match("enum"):
			return ps.ParseData_Tagged(t, ND_Enum)

		case head.Match("union"):
			return ps.ParseData_Tagged(t, ND_Union)

		case head.Match("str"):
			return ps.ParseStringBuilder(t)

		case head.Match("default-of"):
			return ps.ParseDefaultLiteral(t)

		case head.Match("no"):
			return ps.ParseNoLiteral(t)

		case head.Match("opt"):
			return ps.ParseOptLiteral(t)

//??		case head.Match("res"), head.Match("error"):
//??			return ps.ParseResLiteral(t)
		}

		// is the first element a type spec?
		// when present, we parse the type but still just make a ND_Struct,
		// and let the deduce code sort it out.

		if head.IsGeneric() {
			PostError("type parameter cannot qualify data in {}")
			return nil
		}

		// allow a bare generic name like `array` or `map`
		if head.Kind == TOK_Name {
			def := N_GetType(head.Str, head.Module)
			if def != nil && def.generic {
				res := ps.ParseData_General(t, true, ND_Struct)
				if res != nil { res.Info.data_def = def }
				return res
			}
		}

		if IsTypeSpec(head) {
			data_type := ps.ParseType(head)
			if data_type == nil {
				return nil
			}
			res := ps.ParseData_General(t, true, ND_Struct)
			if res != nil { res.Info.ty = data_type }
			return res
		}
	}

	// without type information, the deduce code will have to
	// figure out what it is....

	return ps.ParseData_General(t, false, ND_Struct)
}

func (ps *ParserState) ParseData_General(t *Token, skip_head bool, kind TokenKind) *Token {
	data := NewNode(kind, t.Pos)

	children := t.Children
	if skip_head {
		children = children[1:]
	}

	// a trailing '...' indicates that not all fields are present, and
	// remaining ones should get their default value.  deduce code
	// checks that it is used appropriately for the final type.
	if len(children) > 0 && children[len(children)-1].Match("...") {
		data.Info.open = 1
		children = children[0 : len(children)-1]
	}

	// check for explicit fields
	has_fields := false
	for _, child := range children {
		if child.IsField() {
			has_fields = true
			break
		}
	}

	if has_fields {
		return ps.ParseData_Fields(data, children)
	}

	for _, child := range children {
		t_elem := ps.ParseElement(child)
		if t_elem == nil {
			return nil
		}
		if t_elem.Match("...") {
			PostError("invalid use of '...' in data struct")
			return nil
		}
		data.Add(t_elem)
	}

	return data
}

func (ps *ParserState) ParseData_Fields(data *Token, children []*Token) *Token {
	seen_fields := make(map[string]bool)

	count := len(children)
	if count%2 != 0 {
		PostError("data struct: missing/malformed fields")
		return nil
	}
	count /= 2

	for i := 0; i < len(children); i += 2 {
		t_field := children[i]
		t_val := children[i+1]

		if !t_field.IsField() {
			PostError("data struct: expected field name, got %s",
				t_field.String())
			return nil
		}
		if seen_fields[t_field.Str] {
			PostError("data struct: duplicate field %s", t_field.Str)
			return nil
		}
		if t_val.IsField() {
			PostError("data struct: missing value for %s", t_field.Str)
			return nil
		}

		seen_fields[t_field.Str] = true

		t_val = ps.ParseElement(t_val)
		if t_val == nil {
			return nil
		}

		field := NewNode(ND_Field, t_field.Pos)
		field.Str = t_field.Str
		field.Add(t_val)

		data.Add(field)
	}

	return data
}

func (ps *ParserState) ParseData_Tagged(t *Token, kind TokenKind) *Token {
	t_enum := NewNode(kind, t.Pos)

	children := t.Children

	if len(children) < 2 || !children[1].IsTag() {
		PostError("data struct: missing tag for enum/union")
		return nil
	}

	t_enum.Str = children[1].Str

	if kind == ND_Enum && len(children) > 2 {
		PostError("data struct: extra rubbish after enum tag")
		return nil
	}
	if kind == ND_Union {
		if len(children) > 3 {
			PostError("data struct: extra rubbish after union datum")
			return nil
		}

		// the datum can be absent, implying a void datum
		var datum *Token

		if len(children) > 2 {
			datum = ps.ParseElement(children[2])
			if datum == nil {
				return nil
			}
		} else {
			datum = NewNode(ND_Void, t.Pos)
			datum.Info.ty = void_def.ty
		}

		t_enum.Add(datum)
	}

	return t_enum
}

func (ps *ParserState) ParseStringBuilder(t *Token) *Token {
	params := t.Children[1:]

	t_build := NewNode(ND_StrBuild, t.Pos)
	t_build.Info.ty = str_def.ty

	append_literal := func(s string) {
		if s == "" {
			return
		}

		// optimize for multiple literals in a row
		if len(t_build.Children) > 0 {
			last := t_build.Children[len(t_build.Children) - 1]
			if last.Kind == ND_String {
				last.Str = last.Str + s
				return
			}
		}

		t_str := NewNode(ND_String, t_build.Pos)
		t_str.Str = s
		t_str.Info.ty = str_def.ty

		t_build.Add(t_str)
	}

	process_child := func(child *Token) {
		// handle string and character literals
		if child.Kind == ND_Integer && child.Info.ty.IsChar() {
			ch, _ := strconv.Atoi(child.Str)
			append_literal(string(rune(ch)))
			return

		} else if child.Kind == ND_String {
			append_literal(child.Str)
			return
		}

		// Note: deduce code will insert a `.to-str` method if needed
		t_build.Add(child)
	}

	for _, child := range params {
		t_elem := ps.ParseElement(child)
		if t_elem == nil {
			return nil
		}

		// handle an embedded string-builder (flatten the tree)
		if t_elem.Kind == ND_StrBuild {
			for _, sub_elem := range t_elem.Children {
				process_child(sub_elem)
			}
		} else {
			process_child(t_elem)
		}
	}

	// an empty builder is allowed, it becomes the empty string
	if len(t_build.Children) == 0 {
		t_empty := NewNode(ND_String, t_build.Pos)
		t_empty.Str = ""
		t_empty.Info.ty = str_def.ty
		return t_empty
	}

	// simplify pure strings to a literal node
	if len(t_build.Children) == 1 &&
		t_build.Children[0].Kind == ND_String &&
		t_build.Children[0].Info.ty.base == TYP_String {

		t_str := NewNode(ND_String, t_build.Pos)
		t_str.Str = t_build.Children[0].Str
		t_str.Info.ty = str_def.ty
		return t_str
	}

	return t_build
}

//----------------------------------------------------------------------

func (ps *ParserState) ParseExpr(t *Token) *Token {
	if len(t.Children) == 0 {
		PostError("empty expression in ()")
		return nil
	}

	// check for special forms....
	if len(t.Children) > 0 {
		head := t.Children[0]

		switch {
		case head.Match("cast"):
			return ps.ParseCast(t)
		case head.Match("unwrap"):
			return ps.ParseUnwrap(t)
		case head.Match("flatten"):
			return ps.ParseFlatten(t)
		case head.Match("is?"):
			return ps.ParseIsTag(t)
		case head.Match("ref-eq?"):
			return ps.ParseRefEqual(t)

		case head.Match("begin"):
			return ps.ParseBegin(t)
		case head.Match("if"):
			return ps.ParseIf(t)
		case head.Match("match"):
			return ps.ParseMatch(t)
		case head.Match("bare-loop"):
			return ps.ParseLoop(t)
		case head.Match("skip"), head.Match("junction"):
			// ParseBlock handles proper usage of skip and junction
			PostError("bad context for %s (must be in a block)", head.Str)
			return nil

		case head.Match("lam"):
			return ps.ParseLambda(t)
		case head.Match("fmt$"):
			return ps.ParseFmt(t)
		case head.Match("set!"):
			return ps.ParseSetVar(t)
		case head.Match("assert"):
			return ps.ParseAssert(t)

		// Note that "let" within a function is handled elsewhere
		case head.Match("let"):
			return ps.ParseLet(t, ND_DefVar, false)
		case head.Match("let-mut"):
			return ps.ParseLet(t, ND_DefVar, true)
		case head.Match("fun"):
			return ps.ParseDefFunc(t)

		case head.Match("macro"), head.Match("type"), head.Match("interface"):
			PostError("cannot define %ss within code", head.Str)
			return nil
		}

		// a method call?
		if head.IsField() {
			return ps.ParseMethodCall(t)
		}
	}

	// everything else is a function call
	t.Kind = ND_FunCall
	return ps.ParseAllChildren(t)
}

func (ps *ParserState) ParseMethodCall(t *Token) *Token {
	if len(t.Children) < 2 {
		PostError("method call too short, missing object")
		return nil
	}

	t.Kind = ND_MethodCall
	return ps.ParseAllChildren(t)
}

func (ps *ParserState) ParseAccess(t *Token) *Token {
	if len(t.Children) < 1 {
		PostError("access in [] is empty")
		return nil
	}
	if len(t.Children) < 2 {
		PostError("access in [] is lacking indexors")
		return nil
	}

	head := t.Children[0]

	if head.IsField() {
		PostError("cannot use field/method name in that context")
		return nil
	}

	t = ps.ParseAllChildren(t)
	if t == nil {
		return nil
	}

	// rejig multiple indexors into a tree of nodes.
	// e.g. [a 1 2 3] --> (.get-elem (.get-elem (.get-elem a 1) 2) 3)
	// also handle the "unwrap" and "flatten" indexors.

	t_object := t.Children[0]
	indexors := t.Children[1:]

	var t_top *Token

	for _, t_index := range indexors {
		t_new := NewNode(ND_MethodCall, t_index.Pos)

		want_idx := false

		if t_index.Match("unwrap") {
			t_new.Kind = ND_Unwrap
		} else if t_index.Match("flatten") {
			t_new.Kind = ND_Flatten
		} else if t_index.IsField() {
			t_new.Kind = ND_GetField
			t_new.Str = t_index.Str
		} else {
			t_method := NewNode(TOK_Name, t_index.Pos)
			t_method.Str = ".get-elem"

			t_new.Add(t_method)

			want_idx = true
		}

		if t_top != nil {
			t_new.Add(t_top)
		} else {
			t_new.Add(t_object)
		}

		if want_idx {
			t_new.Add(t_index)
		}

		t_top = t_new
	}

	return t_top
}

func (ps *ParserState) ParseName(t *Token) *Token {
	if t.IsTag() {
		t.Kind = ND_Enum
		return t
	}

	if t.Match("nothing") {
		t.Kind = ND_Void
		t.Info.ty = void_def.ty
		return t
	}

	// normal names are handled by the bind code...
	return t
}

func (ps *ParserState) ParseNoLiteral(t *Token) *Token {
	children := t.Children

	if len(children) < 2 {
		PostError("the no literal is missing type")
		return nil
	}
	if len(children) > 2 {
		PostError("the no literal has extra rubbish at end")
		return nil
	}

	target_type := ps.ParseType(children[1])
	if target_type == nil {
		return nil
	}

	new_type := NewOptionalType(target_type)
	if Gen_Expand(new_type, nil) != OKAY {
		return nil
	}

	val := NewNode(ND_Void, t.Pos)
	val.Info.ty = void_def.ty

	no_tea := NewNode(ND_Union, t.Pos)
	no_tea.Str = "`NONE"
	no_tea.Info.ty = new_type

	no_tea.Add(val)

	return no_tea
}

func (ps *ParserState) ParseOptLiteral(t *Token) *Token {
	if len(t.Children) < 2 {
		PostError("missing argument to opt")
		return nil
	}
	if len(t.Children) > 2 {
		PostError("too many arguments in opt")
		return nil
	}

	t_exp := ps.ParseElement(t.Children[1])
	if t_exp == nil {
		return nil
	}

	t_opt := NewNode(ND_Opt, t.Pos)
	t_opt.Add(t_exp)

	return t_opt
}

func (ps *ParserState) ParseUnwrap(t *Token) *Token {
	if len(t.Children) < 2 {
		PostError("missing argument to unwrap")
		return nil
	}
	if len(t.Children) > 2 {
		PostError("too many arguments in unwrap")
		return nil
	}

	t_exp := ps.ParseElement(t.Children[1])
	if t_exp == nil {
		return nil
	}

	t_unwrap := NewNode(ND_Unwrap, t.Pos)
	t_unwrap.Add(t_exp)

	return t_unwrap
}

func (ps *ParserState) ParseFlatten(t *Token) *Token {
	if len(t.Children) < 2 {
		PostError("missing argument to flatten")
		return nil
	}
	if len(t.Children) > 2 {
		PostError("too many arguments in flatten")
		return nil
	}

	t_exp := ps.ParseElement(t.Children[1])
	if t_exp == nil {
		return nil
	}

	t_flat := NewNode(ND_Flatten, t.Pos)
	t_flat.Add(t_exp)

	return t_flat
}

func (ps *ParserState) ParseDefaultLiteral(t *Token) *Token {
	children := t.Children

	if len(children) < 2 {
		PostError("default-of is missing the type")
		return nil
	}
	if len(children) > 2 {
		PostError("default-of has extra rubbish at end")
		return nil
	}

	want_type := ps.ParseType(children[1])
	if want_type == nil {
		return nil
	}

	t_dflt := NewNode(ND_Default, t.Pos)
	t_dflt.Info.ty = want_type

	return t_dflt
}

func (ps *ParserState) ParseIsTag(t *Token) *Token {
	children := t.Children

	if len(children) < 2 {
		PostError("missing argument to is?")
		return nil
	}
	if len(children) < 3 {
		PostError("missing tag name(s) to is?")
		return nil
	}

	t_exp := ps.ParseElement(children[1])
	if t_exp == nil {
		return nil
	}

	t_istag := NewNode(ND_IsTag, t.Pos)
	t_istag.Info.ty = bool_def.ty
	t_istag.Add(t_exp)

	for _, t_tag := range children[2:] {
		if t_tag.Kind != TOK_Name {
			PostError("illegal tag keyword for is?, got %s", t_tag.String())
			return nil
		}
		t_istag.Add(t_tag)
	}

	return t_istag
}

func (ps *ParserState) ParseRefEqual(t *Token) *Token {
	children := t.Children

	if len(children) < 3 {
		PostError("missing argument to ref-eq?")
		return nil
	}
	if len(children) > 3 {
		PostError("too many arguments for ref-eq?")
		return nil
	}

	t_left  := ps.ParseElement(children[1])
	t_right := ps.ParseElement(children[2])

	if t_left == nil || t_right == nil {
		return nil
	}

	t_refeq := NewNode(ND_RefEqual, t.Pos)
	t_refeq.Info.ty = bool_def.ty
	t_refeq.Add(t_left)
	t_refeq.Add(t_right)

	return t_refeq
}

func (ps *ParserState) ParseSetVar(t *Token) *Token {
	if len(t.Children) != 3 {
		PostError("bad set! syntax")
		return nil
	}

	t_name := t.Children[1]

	if t_name.Kind == TOK_Access {
		return ps.ParseSetElem(t)
	}

	if t_name.Kind != TOK_Name {
		PostError("expected var name, got: %s", t_name.String())
		return nil
	}

	t_value := ps.ParseElement(t.Children[2])
	if t_value == nil {
		return nil
	}

	t_setvar := NewNode(ND_SetVar, t.Pos)
	t_setvar.Info.ty = void_def.ty
	t_setvar.Add(t_name)
	t_setvar.Add(t_value)

	return t_setvar
}

func (ps *ParserState) ParseSetElem(t *Token) *Token {
	// first element is a TOK_Access, and this produces a node tree
	// with ND_MethodCall, ND_GetField or ND_Unwrap at the top.
	t_access := ps.ParseAccess(t.Children[1])
	if t_access == nil {
		return nil
	}

	t_value := ps.ParseElement(t.Children[2])
	if t_value == nil {
		return nil
	}

	// never a result from setting an element
	t_access.Info.ty = void_def.ty

	switch t_access.Kind {
	case ND_GetField:
		t_access.Kind = ND_SetField
		t_access.Add(t_value)
		return t_access

	case ND_MethodCall:
		// change the method name (get --> set)
		t_access.Children[0].Str = ".set-elem"
		t_access.Add(t_value)
		return t_access

	default:
		PostError("cannot write to unwrap/flatten indexor")
		return nil
	}
}

func (ps *ParserState) ParseBegin(t *Token) *Token {
	block := t.Children[1:]

	// a begin form with no elements is OK
	if len(block) == 0 {
		t_void := NewNode(ND_Void, t.Pos)
		t_void.Info.ty = void_def.ty
		return t_void
	}

	return ps.ParseBlock(block)
}

func (ps *ParserState) ParseIf(t *Token) *Token {
	children := t.Children

	// divide the tokens into clauses, where each clause begins
	// with an "if", "elif" or "else" keyword.

	clauses := make([][]*Token, 0)
	clauses = append(clauses, children[0:1])

	cur_start := 0

	for i := 1; i < len(children); i++ {
		tok := children[i]

		if tok.Match("elif") || tok.Match("else") {
			// start a new clause
			cur_start = i
			clauses = append(clauses, children[i:i+1])

		} else {
			// extend the current clause
			L := len(clauses) - 1
			clauses[L] = children[cur_start : i+1]
		}
	}

/* DEBUG
	Print("NUM CLAUSES: %d\n", len(clauses))
	for i, cl := range clauses {
		Print("   %d: len %d keyword %q\n", i, len(cl), cl[0].Str)
	}
*/

	var first *Token
	var  prev *Token

	for idx, grp := range clauses {
		// check stuff
		is_last := (idx == len(clauses)-1)
		is_else := (grp[0].Str == "else")

		ErrorSetToken(grp[0])

		if len(grp) < 2 || (!is_else && len(grp) < 3) {
			PostError("%s clause is lacking block", grp[0].Str)
			return nil
		}

		if is_else {
			if !is_last {
				PostError("else clause must be last in if")
				return nil
			}

			else_tok := ps.ParseBlock(grp[1:])
			if else_tok == nil {
				return nil
			}

			// complete the previous ND_If
			prev.Add(else_tok)
			continue
		}

		// get the condition
		cond_tok := ps.ParseElement(grp[1])
		if cond_tok == nil {
			return nil
		}

		// get the code block
		then_tok := ps.ParseBlock(grp[2:])
		if then_tok == nil {
			return nil
		}

		t_if := NewNode(ND_If, cond_tok.Pos)
		t_if.Add(cond_tok)
		t_if.Add(then_tok)

		if first == nil {
			first = t_if
		}

		// `elif` clauses become the else part of the previous ND_If
		if prev != nil {
			prev.Add(t_if)
		}
		prev = t_if
	}

	// when there is no `else`, complete the last ND_If
	if len(prev.Children) == 2 {
		else_body := NewNode(ND_Void, prev.Pos)
		else_body.Info.ty = void_def.ty

		prev.Add(else_body)
	}

	return first
}

func (ps *ParserState) ParseLoop(t *Token) *Token {
	children := t.Children

	t_body := ps.ParseBlock(children[1:])
	if t_body == nil {
		return nil
	}

	t_loop := NewNode(ND_Loop, t.Pos)
	t_loop.Info.ty = void_def.ty
	t_loop.Add(t_body)

	return t_loop
}

func (ps *ParserState) ParseBlock(children []*Token) *Token {
	total := len(children)

	if total == 0 {
		PostError("missing statement (block is empty)")
		return nil
	}

	var seq  *Token
	var junc *JunctionInfo

	for i := total - 1; i >= 0; i-- {
		sub := children[i]

		// the "let" forms are restricted to use inside a block,
		// so we must handle it here....

		if sub.Kind == TOK_Expr {
			is_let := sub.Children[0].Match("let")
			is_var := sub.Children[0].Match("let-mut")

			if is_let || is_var {
				// if nothing after the let, add a `nothing`
				if seq == nil {
					seq = NewNode(ND_Void, sub.Pos)
					seq.Info.ty = void_def.ty
				}

				t_let := ps.ParseLet(sub, ND_Let, is_var)
				if t_let == nil {
					return nil
				}

				// current stuff becomes body of the let
				t_let.Add(seq)

				seq = t_let
				continue
			}

			// the "junction" forms is restricted to use as the last
			// element of a block, and "skip" must also be in a block,
			// so handle them here....
			is_skip := sub.Children[0].Match("skip")
			is_junc := sub.Children[0].Match("junction")

			if is_skip {
				sub = ps.ParseSkipOrJunction(sub, nil)
				if sub == nil {
					return nil
				}
				goto add_it
			}

			if is_junc {
				if i != (total - 1) {
					PostError("junction must be last thing in a block")
					return nil
				}

				junc = new(JunctionInfo)
				junc_expr := ps.ParseSkipOrJunction(sub, junc)
				if junc_expr == nil {
					return nil
				}

				sub = junc_expr
				goto add_it
			}
		}

		sub = ps.ParseElement(sub)
		if sub == nil {
			return nil
		}

	add_it:
		if seq == nil {
			seq = NewNode(ND_Block, sub.Pos)

		} else if seq.Kind != ND_Block {
			other := seq

			seq = NewNode(ND_Block, other.Pos)
			seq.Add(other)
		}

		seq.Prepend(sub)
	}

	// ensure result is an ND_Block
	if seq.Kind != ND_Block {
		other := seq

		seq = NewNode(ND_Block, other.Pos)
		seq.Add(other)
	}

	if junc != nil {
		seq.Info.junction = junc
	}

	return seq
}

func (ps *ParserState) ParseSkipOrJunction(t *Token, junc *JunctionInfo) *Token {
	// NOTE: for junctions, we just return the value portion

	what := t.Children[0].Str

	children := t.Children

	if len(children) < 2 {
		PostError("%s is missing label name", what)
		return nil
	}
	if len(children) > 3 {
		PostError("bad syntax for %s (too many elements)", what)
		return nil
	}

	t_name := children[1]
	if !t_name.IsTag() {
		PostError("skip label must be an identifier beginning with `")
		return nil
	}

	var t_exp *Token

	if len(children) < 3 {
		t_exp = NewNode(ND_Void, t_name.Pos)
		t_exp.Info.ty = void_def.ty

	} else {
		t_exp = ps.ParseElement(children[2])
		if t_exp == nil {
			return nil
		}
	}

	if junc != nil {
		junc.name = t_name.Str
		return t_exp
	}

	t_skip := NewNode(ND_Skip, t_name.Pos)
	t_skip.Info.ty = void_def.ty  // TODO noreturn_type
	t_skip.Str = t_name.Str
	t_skip.Add(t_exp)

	return t_skip
}

func (ps *ParserState) ParseLet(t *Token, kind TokenKind, mutable bool) *Token {
	children := t.Children

	if len(children) < 2 {
		PostError("missing var name in let")
		return nil
	}
	if len(children) < 3 {
		PostError("missing value in let")
		return nil
	}

	t_var := children[1]
	t_exp := children[2]

	// validate binding name
	if t_var.Kind != TOK_Name {
		PostError("bad var name: wanted identifier, got: %s", t_var.String())
		return nil
	}
	if ValidateDefName(t_var, "var", 0) != OKAY {
		return nil
	}

	if len(children) > 3 {
		PostError("bad let statement, too many values")
		return nil
	}

	// check for defining a global variable to itself
	if kind == ND_DefVar && t_exp.Kind == TOK_Name && t_exp.Str == t_var.Str {
		PostError("cannot define '%s' as itself", t_var.Str)
		return nil
	}

	// process expression
	t_exp = ps.ParseElement(t_exp)
	if t_exp == nil {
		return nil
	}

	t_let := NewNode(kind, t.Pos)
	t_let.Info.mutable = mutable
	t_let.Add(t_var)
	t_let.Add(t_exp)

	return t_let
}

func (ps *ParserState) ParseCast(t *Token) *Token {
	children := t.Children

	// the (cast v) form is for going from custom type to base.
	// the (cast T v) form is the more general operation.
	var user_type *Type

	if len(children) >= 3 {
		t_type := children[1]

		user_type = ps.ParseType(t_type)
		if user_type == nil {
			return nil
		}

		children = children[1:]
	}

	if len(children) < 2 {
		PostError("missing argument to type cast")
		return nil
	}
	if len(children) > 2 {
		PostError("too many arguments in type cast")
		return nil
	}

	t_exp := ps.ParseElement(children[1])
	if t_exp == nil {
		return nil
	}

	t_cast := NewNode(ND_Cast, t.Pos)
	t_cast.Info.ty = user_type
	t_cast.Add(t_exp)

	if user_type == nil {
		t_cast.Kind = ND_BaseCast
	}

	return t_cast
}

func (ps *ParserState) ParseDefFunc(t *Token) *Token {
	children := t.Children

	if len(children) < 2 {
		PostError("bad function def: missing name")
		return nil
	}
	if len(children) < 4 {
		PostError("bad function def: missing params or body")
		return nil
	}

	t_name := children[1]

	if t_name.Kind != TOK_Name {
		PostError("bad function def: name is not an identifier")
		return nil
	}

	// check for reserved keywords
	if ValidateDefName(t_name, "function", ALLOW_FIELDS) != OKAY {
		return nil
	}

	t_func := NewNode(ND_DefFunc, t.Pos)

	// Note: we allow method names that are the same as field names.
	//       it is not an issue since method call and field access
	//       have different syntax, former in (), latter in [].

	// a method?
	if t_name.IsField() {
		t_func.Kind = ND_DefMethod
	}

	// notify type system to collect generic types like "@T".
	// [ lambdas do not support that, only global funcs and methods ]
	ps.BeginGenerics()
	defer ps.FinishGenerics()

	t_parlist := ps.ParseParameters(children[2], (t_func.Kind == ND_DefMethod), false)
	if t_parlist == nil {
		return nil
	}
	t_func.Info.ty = t_parlist.Info.ty

	fun_type := t_parlist.Info.ty
	if fun_type.base == TYP_Generic {
		fun_type = fun_type.impl
	}
	Void := fun_type.ret.IsVoid()

	t_body := ps.ParseFunctionBody(children[3:], false, Void)
	if t_body == nil {
		return nil
	}

	t_func.Add(t_name)
	t_func.Add(t_parlist)
	t_func.Add(t_body)

	return t_func
}

func (ps *ParserState) ParseLambda(t *Token) *Token {
	children := t.Children

	if len(children) < 3 {
		PostError("bad lambda: missing params or body")
		return nil
	}

	t_lam := NewNode(ND_Lambda, t.Pos)

	t_parlist := ps.ParseParameters(children[1], false, false)
	if t_parlist == nil {
		return nil
	}
	t_lam.Info.ty = t_parlist.Info.ty

	fun_type := t_parlist.Info.ty
	if fun_type.base == TYP_Generic {
		fun_type = fun_type.impl
	}
	Void := fun_type.ret.IsVoid()

	t_body := ps.ParseFunctionBody(children[2:], true, Void)
	if t_body == nil {
		return nil
	}

	t_lam.Add(t_parlist)
	t_lam.Add(t_body)

	return t_lam
}

func (ps *ParserState) ParseParameters(pars *Token, Method, IntFace bool) *Token {
	// this parses the parameters and result type for the three
	// function forms ("fun", "method" and "lam"), as well as
	// interface methods.  pars must be a TOK_Expr.
	//
	// returns a ND_Params token, where each TOK_Name parameter node
	// gets the parsed type.  the ND_Params token has the full
	// function/method signature in its Info.ty field.

	if pars.Kind != TOK_Expr {
		PostError("bad parameter list, names must be in ()")
		return nil
	}

	plist := pars.Children

	/* find the result */

	// Note: must handle it AFTER parameters due to generics

	var result *Token
	var result_idx = -1

	for i := 0; i < len(plist); i++ {
		if plist[i].Match("->") {
			if result != nil {
				PostError("bad usage of '->' in signature")
				return nil
			}
			if i+1 >= len(plist) {
				PostError("missing type after '->' in signature")
				return nil
			}
			if i+2 != len(plist) {
				PostError("too much stuff after '->' in signature")
				return nil
			}
			result = plist[i+1]
			result_idx = i
		}
	}

	if result != nil {
		// remove from plist
		plist = plist[0:result_idx]
	}

	/* handle 'self' parameter of methods */

	var recv_type *Type
	var recv_def  *TypeDef

	if Method {
		if len(plist) == 0 || !plist[0].Match("self") {
			PostError("bad method def: missing self parameter")
			return nil
		}

		if len(plist) < 2 {
			PostError("bad method def: missing type for self")
			return nil
		}

		t_type := plist[1]

		// for interfaces, require the type to be "_"
		if IntFace {
			if !t_type.Match("_") {
				PostError("bad interface method, self type should be '_'")
				return nil
			}

			recv_def  = void_def
			recv_type = void_def.ty

		} else if t_type.Kind == TOK_Name {
			// a previously defined type name

			recv_def = N_GetType(t_type.Str, t_type.Module)

			// the prelude is allowed to define methods on builtin types
			if recv_def == nil && doing_prelude {
				switch t_type.Str {
				case "int":      recv_def = int_def
				case "float":    recv_def = float_def
				case "str":      recv_def = str_def
				case "byte-vec": recv_def = bytevec_def
				}
			}
			if recv_def == nil {
				PostError("bad method def: unknown plain type '%s'", t_type.Str)
				return nil
			}

			recv_type = recv_def.ty

			if recv_type.base == TYP_Interface {
				PostError("bad method def: receiver '%s' is an interface",
					t_type.Str)
				return nil
			}
			if recv_def.generic {
				PostError("missing parameters for generic type: %s", t_type.Str)
				return nil
			}

		} else if t_type.Kind == TOK_Expr {
			// a method on a generic type

			if len(t_type.Children) == 0 || t_type.Children[0].Kind != TOK_Name {
				PostError("bad method def: malformed generic type for self")
				return nil
			}
			head := t_type.Children[0]
			recv_def = N_GetType(head.Str, head.Module)

			// the prelude is allowed to define methods on builtin types
			if recv_def == nil && doing_prelude {
				switch head.Str {
				case "array": recv_def = array_def
				}
			}
			if recv_def == nil {
				PostError("bad method def: unknown generic type: %s",
					head.Str)
				return nil
			}

			recv_type = ps.ParseGenUsage(recv_def, t_type.Children)
			if recv_type == nil {
				return nil
			}

			// disallow *extra* type parameters now.
			// technically such a thing does make sense, however it
			// makes everything more complex, so keep it simple for now.
			ps.gen_create = false

		} else {
			PostError("bad method def: expected type for self, got: %s",
				t_type.String())
			return nil
		}
	}

	/* handle parameters */

	t_parlist := NewNode(ND_Params, pars.Pos)
	need_self := (recv_type != nil)

	for len(plist) > 0 {
		t_par := plist[0]
		plist = plist[1:]

		if t_par.Kind != TOK_Name {
			PostError("bad parameter name: %s", t_par.String())
			return nil
		}

		validate := ALLOW_UNDERSCORE

		if recv_type != nil {
			validate |= ALLOW_SELF
		}

		if ValidateDefName(t_par, "parameter", validate) != OKAY {
			return nil
		}

		// check if duplicate
		for _, old := range t_parlist.Children {
			if old.Str == t_par.Str && !t_par.Match("_") {
				PostError("duplicate parameter name '%s'", t_par.Str)
				return nil
			}
		}

		// grab the type
		if len(plist) == 0 {
			PostError("missing type spec for parameter '%s'", t_par.Str)
			return nil
		}

		t_type := plist[0]
		plist = plist[1:]

		// we already know the type of 'self' parameter
		if need_self {
			t_par.Info.ty = recv_type
			t_par.Info.recv_def = recv_def
			need_self = false

		} else {
			par_type := ps.ParseType(t_type)
			if par_type == nil {
				return nil
			}
			if par_type.IsVoid() {
				PostError("parameters cannot be void")
				return nil
			}

			t_par.Info.ty = par_type
		}

		t_parlist.Add(t_par)
	}

	/* handle the result */

	// only parameters can be generic, disallow *new* ones now
	ps.gen_create = false

	ret_type := void_def.ty

	if result != nil {
		ret_type = ps.ParseType(result)
		if ret_type == nil {
			return nil
		}
	}

	/* build the function signature */

	ty := NewType(TYP_Function)
	ty.ret = ret_type
//--	ty.instance = function_def

	for _, t_par := range t_parlist.Children {
		if t_par.Info.ty == nil {
			panic("parameter lacks a type")
		}
		ty.AddParam(t_par.Str, t_par.Info.ty)
	}

	ty = ty.WrapGeneric()

	t_parlist.Info.ty = ty

	return t_parlist
}

func (ps *ParserState) ParseFunctionBody(input []*Token, Lambda, Void bool) *Token {
	if len(input) == 0 { panic("more input!") }

	// handle the "#native" form
	if len(input) > 0 && input[0].Kind == TOK_Expr &&
		len(input[0].Children) > 0 &&
		input[0].Children[0].Match("#native") {

		t_native := input[0]

		if Lambda {
			PostError("lambda functions cannot be #native")
			return nil
		}
		if len(t_native.Children) != 2 || t_native.Children[1].Kind != TOK_String {
			PostError("malformed #native syntax")
			return nil
		}

		t_body := NewNode(ND_Native, t_native.Pos)
		t_body.Str = t_native.Children[1].Str

		return t_body
	}

	block := ps.ParseBlock(input)
	if block == nil {
		return nil
	}

	// add implicit `end junction, unless block has explicit one
	if Void && block.Info.junction == nil {
		block.Info.junction = new(JunctionInfo)
		block.Info.junction.name = "`end"
	}

	return block
}

func (ps *ParserState) ParseFmt(t *Token) *Token {
	children := t.Children[1:]

	// no arguments --> empty string
	if len(children) == 0 {
		t_empty := NewNode(ND_String, t.Pos)
		t_empty.Str = ""
		t_empty.Info.ty = str_def.ty
		return t_empty
	}

	// we construct a string-builder node to concat the pieces
	t_build := NewNode(ND_StrBuild, t.Pos)
	t_build.Info.ty = str_def.ty

	add_string := func(s string, pos Position) {
		t_str := NewNode(ND_String, pos)
		t_str.Str = s
		t_str.Info.ty = str_def.ty

		t_build.Add(t_str)
	}

	add_format := func(f *Formatter, t_expr *Token) {
		if f.before != "" {
			add_string(f.before, t_expr.Pos)
		}

		t_fmt := NewNode(ND_Fmt, t_expr.Pos)
		t_fmt.Info.ty = str_def.ty
		t_fmt.Info.fmt_spec = f
		t_fmt.Add(t_expr)

		t_build.Add(t_fmt)

		if f.after != "" {
			add_string(f.after, t_expr.Pos)
		}
	}

	// process each pair of format + value
	for len(children) >= 2 {
		t_fmt := children[0]
		t_exp := children[1]
		children = children[2:]

		if t_fmt.Kind != TOK_String {
			PostError("format string must be string literal, got %s",
				t_fmt.String())
			return nil
		}

		f, err := FormatDecode(t_fmt.Str)
		if err != nil {
			PostError("%s", err.Error())
			return nil
		}

		if f.verb == 0 {
			PostError("format string lacks a %% directive")
			return nil
		}

		t_exp = ps.ParseElement(t_exp)
		if t_exp == nil {
			return nil
		}

		add_format(f, t_exp)
	}

	// handle a trailing string literal
	if len(children) > 0 {
		t_lone := children[0]

		if t_lone.Kind != TOK_String {
			PostError("format string must be string literal, got %s",
				t_lone.String())
			return nil
		}

		// we only decode it to ensure it has NO verb at all
		f, err := FormatDecode(t_lone.Str)
		if err != nil {
			PostError("%s", err.Error())
			return nil
		}

		if f.verb != 0 {
			PostError("missing value after %%%c format directive", f.verb)
			return nil
		}

		// convert TOK_String to ND_String
		t_lone = ps.ParseElement(t_lone)
		if t_lone == nil {
			return nil
		}

		t_build.Add(t_lone)
	}

	return t_build
}

func (ps *ParserState) ParseAssert(t *Token) *Token {
	children := t.Children

	if len(children) != 2 {
		PostError("bad syntax for assert form")
		return nil
	}

	t_cond := ps.ParseElement(children[1])
	if t_cond == nil {
		return nil
	}

	// FIXME: get the proper filename
	filename := "xxx"
	msg := fmt.Sprintf("assertion failed at %s:%d\n", filename, t.Pos.Line)

	// build a tree representing this:
	//    if (not COND)
	//       abort-program MSG

	t_msg := NewNode(ND_String, t.Pos)
	t_msg.Str = msg
	t_msg.Info.ty = str_def.ty

	t_funcname := NewNode(TOK_Name, t.Pos)
	t_funcname.Str = "abort-program"

	t_func := NewNode(ND_FunCall, t.Pos)
	t_func.Add(t_funcname)
	t_func.Add(t_msg)

	t_void := NewNode(ND_Void, t.Pos)
	t_void.Info.ty = void_def.ty

	t_assert := NewNode(ND_If, t.Pos)
	t_assert.Add(t_cond)
	t_assert.Add(t_void)
	t_assert.Add(t_func)

	return t_assert
}

//----------------------------------------------------------------------

func (ps *ParserState) ParseMatch(t *Token) *Token {
	children := t.Children
	if len(children) < 2 {
		PostError("match statement without expression")
		return nil
	}
	if len(children) < 3 {
		PostError("match statement is lacking rules")
		return nil
	}

	t_exp := ps.ParseElement(children[1])
	if t_exp == nil {
		return nil
	}

	t_match := NewNode(ND_Match, t.Pos)

	t_match.Add(t_exp)

	for i := 2; i < len(children); i++ {
		t_rule := ps.ParseMatchRule(children[i])
		if t_rule == nil {
			return nil
		}
		t_match.Add(t_rule)
	}

	return t_match
}

func (ps *ParserState) ParseMatchRule(t *Token) *Token {
	ErrorSetToken(t)

	if t.Kind != TOK_Expr {
		PostError("malformed match rule, must be in ()")
		return nil
	}

	// find the '=>' symbol
	mid_pos := -1
	for i, child := range t.Children {
		if child.Match("=>") {
			if mid_pos >= 0 {
				PostError("bad match rule, multiple => found")
				return nil
			}
			mid_pos = i
		}
	}
	if mid_pos < 0 {
		PostError("bad match rule, no => found")
		return nil
	}

	terms := t.Children[0:mid_pos]

	// find the 'where' clause, if any
	where_pos := -1
	for i := 0; i < mid_pos; i++ {
		if t.Children[i].Match("where") {
			if where_pos >= 0 {
				PostError("bad match rule, multiple wheres")
				return nil
			}
			where_pos = i
			terms = terms[0:i]
		}
	}

	/* handle pattern */

	t_pattern := ps.ParseMatchPattern(terms, true)
	if t_pattern == nil {
		return nil
	}

	/* handle where clause */

	var t_where *Token

	if where_pos >= 0 {
		size := mid_pos - where_pos - 1
		if size < 1 {
			PostError("bad match rule, no expression after where")
			return nil
		} else if size > 1 {
			PostError("bad match rule, where clause is too long")
			return nil
		}

		t_where = ps.ParseElement(t.Children[where_pos+1])
		if t_where == nil {
			return nil
		}
	}

	/* handle body */

	t_body := ps.ParseBlock(t.Children[mid_pos+1:])
	if t_body == nil {
		return nil
	}

	t_rule := NewNode(ND_MatchRule, t_pattern.Pos)
	t_rule.Add(t_pattern)
	t_rule.Add(t_body)

	if t_where != nil {
		t_rule.Add(t_where)
	}
	return t_rule
}

func (ps *ParserState) ParseMatchPattern(input []*Token, is_top bool) *Token {
	// looks at the input token(s) and produces one of the following
	// node types to represent the pattern:
	//    TOK_Name        -- term is a non-special identifier
	//    ND_MatchConsts  -- terms are literals or global constants
	//    ND_MatchTags    -- terms are tag names
	//    ND_MatchAll     -- term is "_"
	//    ND_MatchStruct  -- stuff in {} brackets
	//    ND_Field        -- used when field-names appear in ND_MatchStruct

	if len(input) == 0 {
		PostError("bad match rule, missing pattern")
		return nil
	}

	head := input[0]

	// a group of tags?
	if head.IsTag() {
		return ps.ParsePattern_Tags(input)
	}

	// a single identifier?  handles '_' too.
	if len(input) == 1 && head.Kind == TOK_Name {
		return ps.ParsePattern_Name(head)
	}

	// a group of literals or global constants?
	if len(input) > 1 || head.IsMatchConstant() {
		return ps.ParsePattern_Consts(input)
	}

	// handle the `(one-of ...)` form
	if head.Kind == TOK_Expr && len(head.Children) > 0 &&
		head.Children[0].Match("one-of") {
		return ps.ParsePattern_OneOf(head.Children)
	}

	switch head.Kind {
	case TOK_Data:
		return ps.ParsePattern_Struct(head)

	case TOK_Expr, TOK_Access:
		PostError("bad match pattern, cannot be an expression")
		return nil

	default:
		PostError("cannot parse match pattern: %s", head.String())
		return nil
	}
}

func (ps *ParserState) ParsePattern_Tags(input []*Token) *Token {
	pat := NewNode(ND_MatchTags, input[0].Pos)

	for _, t_tag := range input {
		if !t_tag.IsTag() {
			PostError("expected tag name, got: %s", t_tag.String())
			return nil
		}
		pat.Add(t_tag)
	}

	return pat
}

func (ps *ParserState) ParsePattern_Name(t *Token) *Token {
	if t.Match("_") {
		pat := NewNode(ND_MatchAll, t.Pos)
		return pat
	}

	// Note: global vars are checked in bind code

	if IsLanguageKeyword(t.Str) {
		PostError("bad binding name: '%s' is a reserved word", t.Str)
		return nil
	}

	return t
}

func (ps *ParserState) ParsePattern_Struct(t *Token) *Token {
	// this handles anything in between `{}` brackets.

	// check for '...' at beginning or end
	input := t.Children

	open := 0

	if len(input) > 0 {
		if input[0].Match("...") {
			open = -1
			input = input[1:]
		} else if input[len(input)-1].Match("...") {
			open = 1
			input = input[0 : len(input)-1]
		}
	}

	// check for erroneous type specs
	if len(input) > 0 {
		head := input[0]

		if IsTypeSpec(head) ||
			head.IsGeneric() ||
			head.Match("array") || head.Match("map") ||
			head.Match("set")   || head.Match("function") ||
			head.Match("enum")  || head.Match("union") ||
			head.Match("class") {

			PostError("bad pattern: cannot use type specs")
			return nil
		}
	}

	for _, elem := range input {
		if elem.Match("...") {
			PostError("bad pattern: bad/extra '...' found")
			return nil
		}
	}

	// check for explicit fields
	has_fields := false
	for _, child := range input {
		if child.IsField() {
			has_fields = true
			break
		}
	}

	pat := NewNode(ND_MatchStruct, t.Pos)
	pat.Info.open = open
	if len(input) > 0 {
		pat.Pos = input[0].Pos
	}

	if has_fields {
		return ps.ParsePattern_Fields(pat, input)
	}

	for i := 0; i < len(input); i++ {
		t_pat2 := ps.ParseMatchPattern(input[i:i+1], false)
		if t_pat2 == nil {
			return nil
		}
		pat.Add(t_pat2)
	}

	return pat
}

func (ps *ParserState) ParsePattern_Fields(pat *Token, input []*Token) *Token {
	count := len(input)
	if count%2 != 0 {
		PostError("bad pattern: missing/malformed fields")
		return nil
	}

	seen_fields := make(map[string]bool)

	for i := 0; i < len(input); i += 2 {
		t_field := input[i]
		t_val   := input[i+1]

		if !t_field.IsField() {
			PostError("bad pattern: expected field name, got %s", t_field.String())
			return nil
		}
		if t_val.IsField() {
			PostError("bad pattern: missing value for %s", t_field.String())
			return nil
		}

		// ensure field names are unique
		if seen_fields[t_field.Str] {
			PostError("bad pattern: duplicate field %s", t_field.Str)
			return nil
		}
		seen_fields[t_field.Str] = true

		t_sub := ps.ParseMatchPattern(input[i+1:i+2], false)
		if t_sub == nil {
			return nil
		}

		pair := NewNode(ND_Field, t_field.Pos)
		pair.Str = t_field.Str
		pair.Add(t_sub)

		pat.Add(pair)
	}

	return pat
}

func (ps *ParserState) ParsePattern_OneOf(input []*Token) *Token {
	input = input[1:]

	// it is debatable whether an empty `one-of` form makes sense,
	// hence I think it is reasonable to simply disallow it.

	if len(input) == 0 {
		PostError("missing elements in the 'one-of' form")
		return nil
	}

	// a list of enum tags?
	if input[0].IsTag() {
		pat := NewNode(ND_MatchTags, input[0].Pos)

		for _, t_tag := range input {
			if !t_tag.IsTag() {
				PostError("multiple tag pattern with non-tag: %s", t_tag.String())
				return nil
			}
			pat.Add(t_tag)
		}
		return pat
	}

	return ps.ParsePattern_Consts(input)
}

func (ps *ParserState) ParsePattern_Consts(input []*Token) *Token {
	pat := NewNode(ND_MatchConsts, input[0].Pos)

	for _, child := range input {
		if child.IsTag() {
			PostError("unexpected tag %s in match pattern", child.Str)
			return nil
		}
		if child.Match("_") {
			PostError("bad use of %s in match pattern", child.Str)
			return nil
		}
		if !child.IsMatchConstant() {
			PostError("multiple match terms must all be constants")
			return nil
		}

		t_const := ps.ParseElement(child)
		if t_const == nil {
			return nil
		}
		pat.Add(t_const)
	}

	return pat
}

func (nd *Token) IsMatchConstant() bool {
	switch nd.Kind {
	// TOK_Name should be a global constant with a simple type
	// [ this is checked later on ]
	case TOK_Int, TOK_Float, TOK_Char, TOK_String, TOK_Name:
		return true
	}

	return false
}
