// Copyright 2018 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
import "strconv"
// import "strings"

type BaseType int

const (
	// TYP_Void signifies the absent of a value.  Hence it does
	// not make sense to use `void` as the type of a parameter,
	// element type of an array, field of a class, etc...
	TYP_Void BaseType = iota

	TYP_Int
	TYP_Float
	TYP_String
	TYP_Union
	TYP_Enum

	TYP_Array
	TYP_ByteVec
	TYP_Class
	TYP_Function

	// Note that values never have this type, a compatible value will
	// always be a TYP_Class type.
	TYP_Interface

	// TYP_Generic is used for a type where at least one component in
	// the this type tree is a place-holder like `@T` (a TYP_TypeParam).
	// For a generic function, a TYP_Generic (in cl.ty) represents the
	// full signature.
	TYP_Generic

	// this represents a type parameter, with a name which begins
	// with the at sign like `@T` or `@K`.
	TYP_TypeParam

	// this represents a user-defined non-generic type.
	TYP_Custom

	// this represents an invocation of a known generic type, such as
	// `(array int)` or `(MyList str)`.  it consists of two parts: the
	// generic type (as a TypeDef) and a group of type parameters
	// (including their target values).
	//?? the result of an invocation
	//?? is not necessarily concrete, take `(map str @T)` for example,
	//?? though such a TYP_Usage must be wrapped by a TYP_Generic.
	TYP_Usage
)

type Type struct {
	base BaseType

	// for all user-defined types and some builtin ones, this refers
	// back to the TypeDef.  if this Type is T, then we always have
	// that T.def.ty == T.
	def *TypeDef

	// parameters of a function, fields of a class, tags of an enum.
	// for arrays, first one provides the element type.
	// for maps, first one is key type, second is element type.
	// for sets, first one is key type.
	// for interfaces, this has the method signatures, but the "self"
	// parameter in the type is not used (just void).
	// for TYP_Generic, these are the type parameters (TYP_TypeParam).
	// for TYP_Usage, these are the actual type parameters.
	param []ParamInfo

	// return type for functions
	ret *Type

	// for TYP_TypeParam, this is the `@XX` name.
	// never used for anything else.
	tp_name string

	// for TYP_Usage, this is the generic type being used.
	using *TypeDef

	// for TYP_Custom, this is the real type.
	// for TYP_Usage, this is the instantiated type.
	// for TYP_Generic this a template which gets "instantiated" to
	// produce the concrete type.
	impl *Type

	// private field for MapAccessType() method.
	_map_access *Type

	// private field for MorphString() method.
	_sequence int
}

type ParamInfo struct {
	// name of parameter or custom class field.
	// in type specifiers like `(function ...)`, the name is not used.
	name string

	// the type of this parameter
	ty   *Type
}

type MethodSet struct {
	// all the methods in this set
	group map[string]*Closure
}

type TypeDef struct {
	name     string
	ty       *Type
	methods  *MethodSet // all methods defined for this type
	builtin  bool
	generic  bool
	param    []string  // generic parameter names
	unparsed *Token
	embeds   []*TypeDef
	failed   bool
}

var (
	void_def     *TypeDef
	int_def      *TypeDef
	float_def    *TypeDef
	str_def      *TypeDef
	bytevec_def  *TypeDef

	array_def    *TypeDef

	union_def    *TypeDef
	enum_def     *TypeDef
	class_def    *TypeDef
	function_def *TypeDef

	// these five are defined in the prelude
	bool_def *TypeDef
	char_def *TypeDef

	opt_def  *TypeDef
	map_def  *TypeDef
	set_def  *TypeDef
)

var total_types int

//----------------------------------------------------------------------

func SetupTypes() {
	instantiations = make(map[string]*Type)
	expansions     = make(map[string]*Type)

	void_def    = NewTypeDef("void", true, false)
	void_def.ty = NewType(TYP_Void)
	void_def.ty.def = void_def

	int_def    = NewTypeDef("int", true, false)
	int_def.ty = NewType(TYP_Int)
	int_def.ty.def = int_def

	float_def    = NewTypeDef("float", true, false)
	float_def.ty = NewType(TYP_Float)
	float_def.ty.def = float_def

	str_def    = NewTypeDef("str", true, false)
	str_def.ty = NewType(TYP_String)
	str_def.ty.def = str_def

	bytevec_def    = NewTypeDef("byte-vec", true, false)
	bytevec_def.ty = NewType(TYP_ByteVec)
	bytevec_def.ty.def = bytevec_def

	// Note: these four never have a Type struct
	union_def       = NewTypeDef("union", true, false)
	enum_def        = NewTypeDef("enum", true, false)
	class_def       = NewTypeDef("class", true, false)
	function_def    = NewTypeDef("function", true, false)

	SetupArrayType()
}

func SetupArrayType() {
	// we create a genuine-ish generic type.
	// this cannot be defined in the prelude since it would look
	// like a self-recursive thing (the template would be wrong).

	elem := NewType(TYP_TypeParam)
	elem.tp_name = "@T"

	template := NewType(TYP_Array)
	template.AddParam("", elem)

	generic := NewType(TYP_Generic)
	generic.impl = template
	generic.AddTypeParam(elem.tp_name, elem)

	array_def        = NewTypeDef("array", true, true)
	array_def.ty     = generic
	array_def.ty.def = array_def
	array_def.param  = []string{elem.tp_name}
}

func NewType(base BaseType) *Type {
	total_types += 1

	ty := new(Type)
	ty.base = base
	ty.param = make([]ParamInfo, 0)
	ty._sequence = total_types
	return ty
}

func NewTypeDef(name string, builtin bool, generic bool) *TypeDef {
	def := new(TypeDef)
	def.name = name
	def.builtin = builtin
	def.generic = generic
	def.methods = NewMethodSet()
	return def
}

func NewMethodSet() *MethodSet {
	ms := new(MethodSet)
	ms.group = make(map[string]*Closure)
	return ms
}

func (base BaseType) String() string {
	switch base {
	case TYP_Void:
		return "<Void>"
	case TYP_Int:
		return "<Int>"
	case TYP_Float:
		return "<Float>"
	case TYP_String:
		return "<Str>"
	case TYP_Union:
		return "<Union>"
	case TYP_Enum:
		return "<Enum>"
	case TYP_Array:
		return "<Array>"
	case TYP_ByteVec:
		return "<ByteVec>"
	case TYP_Class:
		return "<Class>"
	case TYP_Function:
		return "<Function>"
	case TYP_Interface:
		return "<Interface>"
	case TYP_Generic:
		return "<Generic>"
	case TYP_TypeParam:
		return "<TypeParam>"
	case TYP_Custom:
		return "<Custom>"
	case TYP_Usage:
		return "<Usage>"
	default:
		return "!!INVALID!!"
	}
}

//----------------------------------------------------------------------

func Type_CreateRaw(t *Token) cmError {
	ErrorSetToken(t)

	children := t.Children

	if len(children) < 2 {
		PostError("bad type def: missing name")
		return FAILED
	}

	t_head := children[0]
	t_name := children[1]

	generic := false
	iface   := false

	if t_head.Match("interface") {
		iface = true
	} else if t_name.Kind != TOK_Name {
		if t_name.Kind == TOK_Expr && len(t_name.Children) > 0 {
			t_name = t_name.Children[0]
			generic = true
		}
	}

	if t_name.Kind != TOK_Name {
		PostError("bad type def: name is not an identifier")
		return FAILED
	}
	if ValidateDefName(t_name, "type", 0) != OKAY {
		return FAILED
	}

	def := NewTypeDef(t_name.Str, false, generic)
	def.param = make([]string, 0)
	def.unparsed = t

	if generic {
		// parse the names of the type parameters now
		params := children[1].Children[1:]

		for _, t_par := range params {
			if !t_par.IsGeneric() {
				PostError("expected parameter like @T, got %s", t_par.String())
				return FAILED
			}

			is_dup := false
			for _, s := range def.param {
				if s == t_par.Str {
					is_dup = true
				}
			}
			if is_dup {
				PostError("duplicate generic parameter '%s'", t_par.Str)
				return FAILED
			}

			def.param = append(def.param, t_par.Str)
		}

	} else if iface {
		def.ty = NewType(TYP_Interface)
		def.ty.def = def
		def.embeds = make([]*TypeDef, 0)

	} else {
		// create a unusable Type now, so that other types have
		// something they can refer to.  it is fleshed out later.
		// Note that nothing must use it until the whole type
		// handling phase of the compiler is finished.
		def.ty = NewType(TYP_Custom)
		def.ty.def = def
	}

	context.AddType(t_name.Str, def)
	return OKAY
}

func Type_ParseCustom(def *TypeDef) cmError {
	// ignore builtin type
	if def.builtin { return OKAY }

	t := def.unparsed
	def.unparsed = nil

	ErrorSetToken(t)

	ps := new(ParserState)
	ps.def = def

	if t.Children[0].Match("interface") {
		if ps.ParseInterface(t) != OKAY {
			def.failed = true
			return FAILED
		}
		return OKAY
	}

	children := t.Children

	if len(children) < 3 {
		PostError("bad type def: missing type spec")
		def.failed = true
		return FAILED
	}
	if len(children) > 3 {
		PostError("bad type def: extra rubbish at end")
		def.failed = true
		return FAILED
	}

	t_name := children[1]
	t_spec := children[2]

	impl := ps.ParseType(t_spec)
	if impl == nil {
		def.failed = true
		return FAILED
	}

	if impl.IsVoid() {
		PostError("custom type '%s' cannot be void", t_name.Str)
		def.failed = true
		return FAILED
	}
	if impl.base == TYP_Interface {
		PostError("custom type '%s' cannot be an interface", t_name.Str)
		def.failed = true
		return FAILED
	}
	if impl.IsCustom() {
		PostError("custom type '%s' cannot be another custom type", t_name.Str)
		def.failed = true
		return FAILED
	}

	// this should not be possible
	switch impl.base {
	case  TYP_Generic, TYP_TypeParam:
		panic("generic roaming in the wild")
	}

	// update the existing TYP_Custom Type
	def.ty.impl = impl

	// NOTE: Gen_Expand will be done on `impl` later (in Type_CheckDef)

	return OKAY
}

func Type_ParseGeneric(def *TypeDef) cmError {
	// ignore builtin type
	if def.builtin { return OKAY }

	t := def.unparsed
	def.unparsed = nil

	ErrorSetToken(t)

	children := t.Children

	if len(children) < 2 || children[1].Kind != TOK_Expr {
		PostError("bad generic type syntax")
		return FAILED
	}

	t_param := children[1]

	if len(t_param.Children) < 1 {
		PostError("bad generic type: missing type name")
		return FAILED
	}
	if len(t_param.Children) < 2 {
		PostError("bad generic type: missing parameters")
		return FAILED
	}
	if len(children) < 3 {
		PostError("bad generic type: missing type spec")
		return FAILED
	}
	if len(children) > 3 {
		PostError("bad generic type: extra rubbish at end")
		return FAILED
	}

//	t_name := t_param.Children[0]
	t_spec := children[2]

	ps := new(ParserState)
	ps.def = def

	// add the parameters (they were checked in Type_CreateRaw)

	ps.BeginGenerics()
	defer ps.FinishGenerics()

	for _, par_name := range def.param {
		par := NewType(TYP_TypeParam)
		par.tp_name = par_name

		// allow it to be found by ParseType() below
		ps.AddTypeParam(par_name, par)
	}

	// disallow further names beginning with `@`
	ps.gen_create = false

	/* parse the implementation */

	impl := ps.ParseType(t_spec)
	if impl == nil {
		def.failed = true
		return FAILED
	}

	// disallow stuff like: type (Foo @A) @A
	if impl.base == TYP_TypeParam {
		PostError("custom generic type '%s' is way too generic", def.name)
		return FAILED
	}

	if impl.base != TYP_Generic {
		PostError("custom generic type '%s' is not generic enough", def.name)
		return FAILED
	}

	impl.def = def

	def.ty = impl

	return OKAY
}

func (ps *ParserState) ParseInterface(t *Token) cmError {
	t.InitInfo()

	children := t.Children[2:]

	// Note: an empty interface is allowed, but not very useful

	for _, sub := range children {
		if sub.Kind != TOK_Expr {
			PostError("bad interface: expected method in (), got %s",
				sub.String())
			return FAILED
		}
		if len(sub.Children) == 0 {
			PostError("bad interface: empty () found")
			return FAILED
		}

		head := sub.Children[0]
		err2 := FAILED

		if head.Match("fun") {
			err2 = ps.ParseInterfaceMethod(sub)
		} else if head.Match("embed") {
			err2 = ps.ParseInterfaceEmbedding(sub)
		} else {
			PostError("bad interface: unknown element (%s ...)", head.Str)
		}

		if err2 != OKAY {
			return FAILED
		}
	}

	return OKAY
}

func (ps *ParserState) ParseInterfaceMethod(t *Token) cmError {
	def := ps.def

	iface := def.ty

	children := t.Children

	if len(children) < 2 {
		PostError("bad interface method: missing name")
		return FAILED
	}
	if len(children) < 3 {
		PostError("bad interface method: missing parameters")
		return FAILED
	}
	if len(children) > 3 {
		PostError("bad interface method: extra rubbish at end")
		return FAILED
	}

	t_method := children[1]
	t_pars   := children[2]

	if t_method.Kind != TOK_Name {
		PostError("bad interface method: wanted method name, got %s", t_method.String())
		return FAILED
	}
	if !t_method.IsField() {
		PostError("bad interface method: name must begin with a dot")
		return FAILED
	}

	t_parlist := ps.ParseParameters(t_pars, true, true)
	if t_parlist == nil {
		return FAILED
	}
	meth_sig := t_parlist.Info.ty

	return iface.AddInterfaceMethod(t_method.Str, meth_sig)
}

func (ps *ParserState) ParseInterfaceEmbedding(t *Token) cmError {
	def := ps.def

	for _, sub := range t.Children[1:] {
		if sub.Kind != TOK_Name {
			PostError("bad interface embed: expected name, got %s",
				sub.String())
			return FAILED
		}

		name  := sub.Str
		other := N_GetType(name, sub.Module)

		if other != nil && other.failed {
			return FAILED
		}
		if other == nil {
			PostError("bad interface embed: no such type %s", name)
			return FAILED
		}
		if other.generic || other.ty.base != TYP_Interface {
			PostError("bad interface embed: %s is not an interface", name)
			return FAILED
		}

		// only remember it now, we must perform the embedding after all
		// the interface types have been parsed.

		def.embeds = append(def.embeds, other)
	}

	return OKAY
}

func (def *TypeDef) EmbedInterfaces(depth int) cmError {
	if depth > 999 {
		PostError("cyclic embedding of interfaces (last was %s)", def.name)
		return FAILED
	}

	for _, other := range def.embeds {
		if !other.failed {
			// if target interface has unresolved embeds, do them now
			if len(other.embeds) > 0 {
				if other.EmbedInterfaces(depth + 1) != OKAY {
					continue
				}
			}

			for _, par := range other.ty.param {
				if def.ty.AddInterfaceMethod(par.name, par.ty) != OKAY {
					return FAILED
				}
			}
		}
	}

	// mark us as finished
	def.embeds = nil

	return OKAY
}

func Type_CheckDef(def *TypeDef) cmError {
	if def.ty == nil || def.builtin || def.failed {
		return OKAY
	}

	if def.generic {
		//??

	} else if def.ty.base == TYP_Interface {
		if def.EmbedInterfaces(0) != OKAY {
			def.failed = true
			return FAILED
		}

	} else if def.ty.base == TYP_Custom {
		if Gen_Expand(def.ty.impl, nil) != OKAY {
			def.failed = true
			return FAILED
		}
		if def.ty.Unconstructable() {
			PostError("type %s cannot be constructed (cyclic refs)", def.name)
			def.failed = true
			return FAILED
		}

	} else {
		panic("weird non-generic type")
	}

	return OKAY
}

//----------------------------------------------------------------------

func (ps *ParserState) ParseType(t *Token) *Type {
	switch {
	case t.Match("void"):
		return void_def.ty

	case t.Match("int"):
		return int_def.ty

	case t.Match("str"):
		return str_def.ty

	case t.Match("float"):
		return float_def.ty

	case t.Match("byte-vec"):
		return bytevec_def.ty
	}

	// check for a generic name
	if t.IsGeneric() {
		if !ps.gen_active {
			PostError("cannot use type parameter %s in that context", t.Str)
			return nil
		}

		ty := ps.tgroup[t.Str]
		if ty != nil {
			return ty
		}
		if !ps.gen_create {
			PostError("type parameter %s was not previously defined", t.Str)
			return nil
		}

		ty = NewType(TYP_TypeParam)
		ty.tp_name = t.Str

		ps.AddTypeParam(t.Str, ty)
		return ty
	}

	// check for user defined types
	if t.Kind == TOK_Name {
		def := N_GetType(t.Str, t.Module)
		if def == nil {
			PostError("unknown type: %s", t.Str)
			return nil
		}
		if def.failed {
			return nil
		}

		// can never use a generic type name by itself.
		// it needs to be in `()` with some type parameters.
		if def.generic {
			PostError("missing parameters for generic type: %s", t.Str)
			return nil
		}

		return def.ty
	}

	if t.Kind != TOK_Expr || len(t.Children) < 2 {
		PostError("malformed type, got: %s", t.String())
		return nil
	}

	res := ps.ParseCompound(t.Children)
	if res != nil {
		res = res.WrapGeneric()
	}
	if res != nil && ps.def == nil {
		if Gen_Expand(res, nil) != OKAY {
			return nil
		}
	}
	return res
}

func (ps *ParserState) ParseCompound(children []*Token) *Type {
	head := children[0]

	if head.Kind != TOK_Name {
		PostError("malformed type, got: %s", head.String())
		return nil
	}

	// handle user-created generic types
	def := N_GetType(head.Str, head.Module)
	if def != nil {
		return ps.ParseGenUsage(def, children)
	}

	switch {
	case head.Match("class"):
		return ps.ParseClass(children[1:])

	case head.Match("enum"):
		return ps.ParseEnum(children[1:])

	case head.Match("union"):
		return ps.ParseUnion(children[1:])

	case head.Match("function"):
		return ps.ParseFunction(children)

	default:
		PostError("unknown compound type: %s", head.Str)
		return nil
	}
}

func (ps *ParserState) ParseGenUsage(def *TypeDef, children []*Token) *Type {
	if def.failed {
		return nil
	}
	if !def.generic {
		PostError("cannot use non-generic type '%s' in ()", def.name)
		return nil
	}

	want_num := len(def.param)

	if len(children)-1 != want_num {
		PostError("wrong number of type parameters for %s, wanted %d, got %d",
			def.name, want_num, len(children)-1)
		return nil
	}

	invoke := NewType(TYP_Usage)
	invoke.using = def

	// get the actual types
	for i := 0; i < want_num; i++ {
		name := def.param[i]

		sub := ps.ParseType(children[1+i])
		if sub == nil {
			return nil
		}
		invoke.AddParam(name, sub)
	}

	return invoke.WrapGeneric()
}

func (ps *ParserState) ParseClass(children []*Token) *Type {
	count := len(children)

	if count < 1 {
		PostError("malformed class type")
		return nil
	}

	named_fields := children[0].IsField()

	// when field names are present, there must be a field name
	// before *every* element.
	res := NewType(TYP_Class)
//--	res.instance = class_def

	if named_fields {
		if count%2 != 0 {
			PostError("malformed class type")
			return nil
		}

		count /= 2
	}

	// get types for each field
	for i := 0; i < count; i++ {
		var t_type *Token
		field_name := ""

		if named_fields {
			t_field := children[i*2]
			t_type = children[i*2+1]

			if !t_field.IsNamedField() {
				PostError("bad field name in class type: %s", t_field.Str)
				return nil
			}

			field_name = t_field.Str

		} else {
			t_type = children[i]

			if t_type.IsField() {
				PostError("malformed class type (unexpected field name)")
				return nil
			}

			field_name = fmt.Sprintf(".%d", i)
		}

		sub := ps.ParseType(t_type)
		if sub == nil {
			return nil
		}
		if sub.IsVoid() {
			PostError("class elements cannot be void")
			return nil
		}

		res.AddParam(field_name, sub)
	}

	return res
}

func (ps *ParserState) ParseEnum(children []*Token) *Type {
	count := len(children)

	if count == 0 {
		PostError("enum type is missing tags")
		return nil
	}

	res := NewType(TYP_Enum)
//--	res.instance = enum_def

	// get each tag keyword
	for i := 0; i < count; i++ {
		t_tag := children[i]

		if !t_tag.IsTag() {
			PostError("enum tag must be identifier beginning with `")
			return nil
		}

		res.AddParam(t_tag.Str, void_def.ty)
	}

	return res
}

func (ps *ParserState) ParseUnion(children []*Token) *Type {
	count := len(children)

	if count == 0 || count%2 != 0 {
		PostError("malformed union type")
		return nil
	}

	count /= 2

	res := NewType(TYP_Union)
//--	res.instance = union_def

	// get each tag keyword and associated data type
	for i := 0; i < count; i++ {
		t_tag := children[i*2]
		t_datum := children[i*2+1]

		if !t_tag.IsTag() {
			PostError("union tag must be identifier beginning with `")
			return nil
		}

		sub := ps.ParseType(t_datum)
		if sub == nil {
			return nil
		}

		res.AddParam(t_tag.Str, sub)
	}

	// if we have a void datum, make sure it is the first.
	// [ making a default value relies on this! ]
	for i := range res.param {
		if res.param[i].ty.IsVoid() {
			if i > 0 {
				tmp := res.param[0]
				res.param[0] = res.param[i]
				res.param[i] = tmp
			}
			break
		}
	}

	return res
}

func (ps *ParserState) ParseFunction(children []*Token) *Type {
	num := len(children)

	if num < 2 {
		PostError("malformed function type")
		return nil
	}

	// the return type is last
	ret_type := ps.ParseType(children[num-1])
	if ret_type == nil {
		return nil
	}

	res := NewType(TYP_Function)
	res.ret = ret_type
//--	res.instance = function_def

	// get types for each parameter
	for i := 1; i < num-1; i++ {
		par_type := ps.ParseType(children[i])
		if par_type == nil {
			return nil
		}
		if par_type.IsVoid() {
			PostError("parameters cannot be void")
			return nil
		}

		res.AddParam("", par_type)
	}

	return res
}

// WrapGeneric tests that any sub-types of this one are generic,
// and if so it wraps it in a TYP_Generic with the used parameters
// and returns the new type.  otherwise returns the existing type.
// except builtins, this is the only place a TYP_Generic is created.
func (ty *Type) WrapGeneric() *Type {
	seen_gen := false

	switch ty.base {
	case TYP_Generic, TYP_TypeParam, TYP_Interface:
		// no wrap needed
		return ty

	case TYP_Usage, TYP_Union, TYP_Array, TYP_Class, TYP_Function:
		for _, par := range ty.param {
			if par.ty.base == TYP_Generic || par.ty.base == TYP_TypeParam {
				seen_gen = true
			}
		}
		if ty.ret != nil && (ty.ret.base == TYP_Generic || ty.ret.base == TYP_TypeParam) {
			seen_gen = true
		}

		if !seen_gen {
			return ty
		}

		gen := NewType(TYP_Generic)
		gen.impl = ty

		for _, par := range ty.param {
			gen.InstallGenParam(par.ty)
		}
		if ty.ret != nil {
			gen.InstallGenParam(ty.ret)
		}
		return gen

	default:
		// no wrap needed
		return ty
	}
}

func (ty *Type) InstallGenParam(child *Type) {
	switch child.base {
	case TYP_TypeParam:
		ty.AddTypeParam(child.tp_name, child)

	case TYP_Generic:
		for _, par := range child.param {
			ty.AddTypeParam(par.name, par.ty)
		}
	}
}

func IsTypeSpec(t *Token) bool {
	if t.Module == "" {
		if t.Match("void") || t.Match("int") ||
			t.Match("float") || t.Match("str") ||
			t.Match("byte-vec") {
			return true
		}
	}

	if t.Kind == TOK_Name {
		return N_HasType(t.Str, t.Module)
	}

	if t.Kind == TOK_Expr && len(t.Children) >= 1 {
		head := t.Children[0]

		if head.Module == "" {
			if  head.Match("array") || head.Match("map") || head.Match("set") ||
				head.Match("union") || head.Match("enum") ||
				head.Match("class") || head.Match("function") {

				return true
			}
		}

		if head.Kind == TOK_Name && N_HasType(head.Str, head.Module) {
			return true
		}
	}

	return false
}

//----------------------------------------------------------------------

// TODO RENAME THIS TO Impl
func (ty *Type) Real() *Type {
	if ty.base == TYP_Custom {
		ty = ty.impl
		if ty == nil { panic("Real() on unfinished TYP_Custom") }
	}
	if ty.base == TYP_Usage {
		oty := ty
		ty = ty.impl
		if ty == nil { panic("Real() on uninstantiated TYP_Usage: " + oty.using.name) }
	}
	return ty
}

func (ty *Type) IsBasic() bool {
	ty = ty.Real()

	switch ty.base {
	case TYP_Int, TYP_Float, TYP_Enum, TYP_String:
		return true
	default:
		return false
	}
}

func (ty *Type) IsCustom() bool {
	return ty.base == TYP_Custom
}

func (ty *Type) String() string {
	if ty == nil {
		return "(nil)"
		// panic("nil given to Type.String()")
	}

/* TODO probably remove
	if ty.nice_name != "" {
		return ty.nice_name
	}
*/
	if ty.def != nil {
		return ty.def.name
	}

	switch ty.base {
	case TYP_Void:
		return "void"

	case TYP_Int:
		return "int"

	case TYP_String:
		return "str"

	case TYP_Float:
		return "float"

	case TYP_Array:
		return "array"

	case TYP_ByteVec:
		return "byte-vec"

	case TYP_Class:
		s := "(class"
		for _, par := range ty.param {
			s = s + " " + par.ty.String()
		}
		return s + ")"

	case TYP_Union:
		s := "(union"
		for _, par := range ty.param {
			s = s + " " + par.name
			s = s + " " + par.ty.String()
		}
		return s + ")"

	case TYP_Enum:
		s := "(enum"
		for _, par := range ty.param {
			s = s + " " + par.name
		}
		return s + ")"

	case TYP_Function:
		s := "(function "

		for _, par := range ty.param {
			s = s + par.ty.String() + " "
		}
		if ty.ret == nil {
			return s + "!!UNKNOWN-RETURN!! )"
		}

		return s + ty.ret.String() + ")"

	case TYP_Generic:
		{
			s := "(Gen ("
			for i, par := range ty.param {
				if i > 0 { s = s + " " }
				s = s + par.ty.String()
			}
			return s + ") " + ty.impl.String() + ")"
		}
		// return ty.impl.String()

	case TYP_TypeParam:
		if ty.tp_name == "" {
			return "@INVALID@"
		} else {
			if false { // DEBUG MODE
				return ty.tp_name + ":" + strconv.Itoa(ty._sequence)
			}
			return ty.tp_name
		}

	case TYP_Usage:
		s := "(" + ty.using.name
		for _, par := range ty.param {
			if false { // DEBUG
				s += " " + par.name + "=" + par.ty.String()
			} else {
				s += " " + par.ty.String()
			}
		}
		return s + ")"

	default:
		return "!!BAD-TYPE!!"
	}
}

func (ty *Type) MorphString() string {
	if ty == nil {
		panic("MorphString with NIL ty")
	}

	base := ""

	if ty.def != nil {
		base = ty.def.name
	} else {
		base = ty.base.String()
	}

	return fmt.Sprintf("%s:%d", base, ty._sequence)
}

func (ty *Type) AddParam(name string, par_type *Type) {
	ty.param = append(ty.param, ParamInfo{name: name, ty: par_type})
}

func (ty *Type) AddTypeParam(name string, par_type *Type) {
	if ty.FindParam(name) < 0 {
		ty.AddParam(name, par_type)
	}
}

func (ty *Type) CopyParams(other *Type) {
	for _, par := range other.param {
		ty.AddParam(par.name, par.ty)
	}
}

func (ty *Type) FindParam(name string) int {
	for i := range ty.param {
		if ty.param[i].name == name {
			return i
		}
	}

	// not found
	return -1
}

func (ty *Type) IsVoid() bool {
	return ty.base == TYP_Void
}

func (ty *Type) IsBool() bool {
	return ty.def != nil && ty.def.name == "bool"
}

func (ty *Type) IsChar() bool {
	return ty.def != nil && ty.def.name == "char"
}

func (ty *Type) IsMap() bool {
	if ty.base == TYP_Custom {
		return ty.impl.IsMap()
	}
	return ty.base == TYP_Usage && ty.using.name == "map"
}

func (ty *Type) IsSet() bool {
	if ty.base == TYP_Custom {
		return ty.impl.IsSet()
	}
	return ty.base == TYP_Usage && ty.using.name == "set"
}

func (ty *Type) KeyType() *Type {
	return ty.param[0].ty
}

func (ty *Type) ElemType() *Type {
	if ty.base == TYP_ByteVec {
		return int_def.ty
	}
	/* FIXME !!!!
	if ty.base == TYP_Map {
		return ty.param[1].ty
	} */
	return ty.param[0].ty
}

func (ty *Type) LookupMethod(name string) *Closure {
	// methods in a known type take priority
	if ty.def != nil {
		meth := ty.def.GetMethod(name)
		if meth != nil {
			return meth
		}
	}

	// handle methods on a usage of a generic type, e.g. `(array int)`
	if ty.using != nil {
		meth := ty.using.GetMethod(name)
		if meth != nil {
			return meth
		}
	}

	// for a custom type, try the implementation type next.
	// this handles arithmetic on a `Duration` type (for example).
	if ty.base == TYP_Custom {
		return ty.impl.LookupMethod(name)
	}

	// not found
	return nil
}

type ConstructorSet struct {
	normal *Closure
	arrays *Closure
	maps   *Closure
}

func (cs *ConstructorSet) Count() int {
	count := 0
	if cs.normal != nil { count += 1 }
	if cs.arrays != nil { count += 1 }
	if cs.maps   != nil { count += 1 }
	return count
}

func (def *TypeDef) GetConstructors() ConstructorSet {
	var cs ConstructorSet

	cs.normal = def.GetMethod(".new")
	cs.arrays = def.GetMethod(".new-array")
	cs.maps   = def.GetMethod(".new-map")

	return cs
}

func (ty *Type) UsableAsKey() bool {
/* FIXME: FIX THIS ONCE Deduce.LookupMethod is done

	if ty.MethodExists(".cmp") && ty.MethodExists(".hash") {
		return true
	}
*/
	return false
}

func (ty *Type) UnusableMapKey(deep int) *Type {
/* FIXME
	ty = ty.Real()

	switch ty.base {
	case TYP_Map, TYP_Set:
		key_type := ty.KeyType()
		if !key_type.UsableAsKey() {
			return key_type
		}
	}

	// to avoid infinite loops, we only recurse so far.
	// so there are cases we may not catch.
	if deep > 0 {
		deep--

		for _, par := range ty.param {
			bad_type := par.ty.UnusableMapKey(deep)
			if bad_type != nil {
				return bad_type
			}
		}
	}
*/

	// none found
	return nil
}

func (ty *Type) MapAccessType() *Type {
	if ty._map_access == nil {
		ty._map_access = NewOptionalType(ty.Real().ElemType())
		if Gen_Expand(ty._map_access, nil) != OKAY {
			// FIXME this don't belong here!
			panic("Gen_Expand failed in MapAccessType")
		}
	}

	return ty._map_access
}

func (ty *Type) UnwrappedType() *Type {
	ty = ty.Real()

	if ty.base != TYP_Union {
		return nil
	}
	if len(ty.param) != 2 {
		return nil
	}
	if ty.param[1].ty.IsVoid() {
		return nil
	}

	// check for Opt and Result
	switch ty.param[1].name {
	case "`VAL", "`OK":
		return ty.param[1].ty

	default:
		return nil
	}
}

//----------------------------------------------------------------------

func (ps *ParserState) BeginGenerics() {
	ps.gen_active = true
	ps.gen_create = true
	ps.tgroup = make(map[string]*Type)
}

func (ps *ParserState) AddTypeParam(name string, ty *Type) {
	ps.tgroup[name] = ty
}

func (ps *ParserState) FinishGenerics() {
	ps.gen_active = false
	ps.gen_create = false
	ps.tgroup = nil
}

//----------------------------------------------------------------------

func (def *TypeDef) MethodExists(name string) bool {
	_, ok := def.methods.group[name]
	return ok
}

func (def *TypeDef) GetMethod(name string) *Closure {
	cl, exist := def.methods.group[name]
	if !exist { return nil }
	return cl
}

func (def *TypeDef) AddMethod(name string, cl *Closure) {
	def.methods.group[name] = cl
}

func (def *TypeDef) DeleteMethod(name string) {
	delete(def.methods.group, name)
}

func (ty *Type) AddInterfaceMethod(name string, sig *Type) cmError {
/* !!!!
	// allow a duplicate name ONLY if signature is the same
	idx := ty.FindParam(name)
	if idx < 0 {
		ty.AddParam(name, sig)
		return OKAY
	}
	if !sig.Equal(ty.param[idx].ty) {
		PostError("bad interface method: '%s' exists with different type", name)
		return FAILED
	}
*/
	return OKAY
}

func (ty *Type) HasInterfaceMethod(name string, sig *Type) bool {
// !!!!
	return false
/*
	if ty.base == TYP_Class {
		if ty.methods == nil { return false }
		_, exist := ty.methods.group[name]
		if !exist { return false }

		cl := ty.methods.group[name]

		if cl.ty == nil { panic("nil method type") }
		if cl.ty.base != TYP_Function { panic("weird method type") }

		// a true method signature has a real type for "self", but
		// interface methods lack it, so we must do our own check.

		if !sig.ret.Equal(cl.ty.ret) {
			return false
		}

		if len(sig.param) != len(cl.ty.param) {
			return false
		}

		for i := 1; i < len(sig.param); i++ {
			if !sig.param[i].ty.Equal(cl.ty.param[i].ty) {
				return false
			}
		}

		return true

	} else if ty.base == TYP_Interface {
		idx := ty.FindParam(name)
		if idx < 0 {
			return false
		}
		return sig.Equal(ty.param[idx].ty)

	} else {
		return false
	}
*/
}

func (ty *Type) ImplementsInterface(ifc *Type) bool {
	// Note: HasInterfaceMethod will check that ty is a class or interface

	for _, par := range ifc.param {
		if !ty.HasInterfaceMethod(par.name, par.ty) {
			return false
		}
	}

	return true
}

func NewOptionalType(sub_type *Type) *Type {
	ty := NewType(TYP_Usage)
	ty.using = opt_def
	ty.AddParam("@T", sub_type)

	return ty.WrapGeneric()
}

func NewArrayType(elem_type *Type) *Type {
	ty := NewType(TYP_Usage)
	ty.using = array_def
	ty.AddParam("@T", elem_type)

	return ty.WrapGeneric()
}

func NewMapType(key_type, val_type *Type) *Type {
	ty := NewType(TYP_Usage)
	ty.using = map_def
	ty.AddParam("@K", key_type)
	ty.AddParam("@V", val_type)

	return ty.WrapGeneric()
}

func NewSetType(key_type *Type) *Type {
	ty := NewType(TYP_Usage)
	ty.using = set_def
	ty.AddParam("@K", key_type)

	return ty.WrapGeneric()
}

//----------------------------------------------------------------------

func (ty *Type) Equal(other *Type) bool {
	// TYPE_RULES.md calls this "mutually assignable".

	return ty.AssignableTo(other) && other.AssignableTo(ty)
}

func (src *Type) AssignableTo(dest *Type) bool {
	if src == nil || dest == nil {
		panic("AssignableTo with nil type")
	}

	if src == dest {
		return true
	}

	// interface handling
	if dest.base == TYP_Interface {
		return src.ImplementsInterface(dest)
	}

	// if both types are custom types, they must be the exact same one.
	// otherwise allow interchange between a custom type and its base.
	if src.IsCustom() && dest.IsCustom() {
		return src.def == dest.def
	}

	src  = src.Real()
	dest = dest.Real()

	if src.base != dest.base {
		return false
	}

	// functions are deliberately strict
	if src.base == TYP_Function {
		if len(src.param) != len(dest.param) {
			return false
		}

		for i := range src.param {
			if !src.param[i].ty.Equal(dest.param[i].ty) {
				return false
			}
		}

		// check return type
		return src.ret.Equal(dest.ret)
	}

	// for classes, check all fields are assignable.
	// for the container types, check if key/element types are assignable.
	if src.base == TYP_Class ||
		src.base == TYP_Array {

		if len(src.param) != len(dest.param) {
			return false
		}

		for i := range src.param {
			if !src.param[i].ty.AssignableTo(dest.param[i].ty) {
				return false
			}
		}
	}

	if src.base == TYP_Union {
		if len(src.param) != len(dest.param) {
			return false
		}

		for i := range src.param {
			if src.param[i].name != dest.param[i].name {
				return false
			}
			if !src.param[i].ty.AssignableTo(dest.param[i].ty) {
				return false
			}
		}
	}

	if src.base == TYP_Enum {
		if len(src.param) != len(dest.param) {
			return false
		}

		for i := range src.param {
			if src.param[i].name != dest.param[i].name {
				return false
			}
		}
	}

	return true
}

func (src *Type) CastableTo(dest *Type) bool {
	if src == nil || dest == nil {
		panic("CastableTo with nil type")
	}

	// allow casting anything to void
	if dest.IsVoid() {
		return true
	}

	return src.WeaklyCastableTo(dest)
}

func (src *Type) WeaklyCastableTo(dest *Type) bool {
	if src == dest {
		return true
	}

	// interface handling
	if dest.base == TYP_Interface {
		return src.ImplementsInterface(dest)
	}

	src  = src.Real()
	dest = dest.Real()

	if src.base != dest.base {
		return false
	}

	// functions are deliberately more restrictive than other casts
	if src.base == TYP_Function {
		return src.AssignableTo(dest)
	}

	// for classes, check all fields are castable.
	// also used for the container types.
	if src.base == TYP_Class ||
		src.base == TYP_Array {

		if len(src.param) != len(dest.param) {
			return false
		}
		for i := range src.param {
			if !src.param[i].ty.WeaklyCastableTo(dest.param[i].ty) {
				return false
			}
		}
	}

	if src.base == TYP_Union {
		if len(src.param) != len(dest.param) {
			return false
		}
		for i := range src.param {
			if !src.param[i].ty.WeaklyCastableTo(dest.param[i].ty) {
				return false
			}
		}
	}

	if src.base == TYP_Enum {
		if len(src.param) != len(dest.param) {
			return false
		}
	}

	return true
}

//----------------------------------------------------------------------

// Defaultable tests whether the given type has a default value.
// The simple types (like int and str) always do, unions only if
// they have a void datum, classes only if all fields have a default
// and no fields are class type.  Functions never have a default.
// Arrays, maps sets, the default is an new empty container.
//
func (ty *Type) Defaultable() bool {
	ty = ty.Real()

	// FIXME for custom generic types (even `map`)

	switch ty.base {
	case TYP_Void, TYP_Int, TYP_Float, TYP_String, TYP_Enum:
		return true

	case TYP_Array, TYP_ByteVec:
		return true

	case TYP_Union:
		// no need to check them all, a void datum is always first
		return ty.param[0].ty.IsVoid()

	case TYP_Class:
		for _, par := range ty.param {
			if par.ty.Real().base == TYP_Class {
				return false
			}
			if !par.ty.Defaultable() {
				return false
			}
		}
		return true

	default:
		return false
	}
}

// Unconstructable detects whether this type cannot ever be
// be constructed (due to an unbreakable cyclic reference).
// Only unions can form "unusable" cyclic refs.
// For example, new arrays and maps can be empty.
// Class objects are allowed to have cycles.
func (ty *Type) Unconstructable() bool {
	seen := make(map[*Type]bool)

	return !ty.canConstruct(seen)
}

func (ty *Type) canConstruct(seen map[*Type]bool) bool {
	ty = ty.Real()

	if ty.base != TYP_Union {
		return true
	}

	// if we reach an already visited type, it means the "path"
	// from original type to current one is cyclic.
	if seen[ty] {
		return false
	}

	// recreate the seen map with new type in it
	new_seen := make(map[*Type]bool)
	for S, _ := range seen {
		new_seen[S] = true
	}
	new_seen[ty] = true

	// for unions we need ONE of its tags to be constructable.
	if ty.base == TYP_Union {
		for _, par := range ty.param {
			if par.ty.canConstruct(new_seen) {
				return true
			}
		}
		return false
	}

	return true
}

// IsAllocated returns true for types which require a memory allocation
// to construct a value.  All reference types have this property, but
// some value types also do (strings, unions).
func (ty *Type) IsAllocated() bool {
	switch ty.Real().base {
	case TYP_Int, TYP_Float, TYP_Enum, TYP_Void:
		return false
	default:
		return true
	}
}

func (ty *Type) IsAllocated_int() int {
	if ty.IsAllocated() {
		return 1
	} else {
		return 0
	}
}
