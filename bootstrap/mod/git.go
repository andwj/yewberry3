// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

// import "fmt"
// import "strings"

/*
Repository Handling
-------------------

The import path gets escaped to create a directory name DIR.
We create this directory in the cache dir:

   $YEWBERRY_CACHE/mod/$DIR

All copies of that module appear under there.

A dir called "current" appears under there, and contains the latest
repository.  If it does not exists, the repository is fetched via
the following command:

   git clone --bare GITURL $FULLPATH/current

If it needs to be updated, then "git fetch" is used instead.
[ TODO: figure out when to do that.... ]

To get the available versions, we "cd" into that repository
and use "git tag --list".

If the version number (in TAG) exists, then we perform a full
checkout into a directory matching the version number:

   git clone --branch TAG current v1.2.3
*/


/* TODO
func DownloadRepository() {
	cmd := exec.Command("git", "clone", "--bare", git_url, in_name, out_arg, out_name)

	// drop normal output on stdout, prevent clobbering our messages
	cmd.Stdout = nil

	// collect any error messages from the tool
	err_box := new(strings.Builder)
	cmd.Stderr = err_box
}
*/

