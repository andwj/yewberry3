// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "fmt"

import "gitlab.com/andwj/argv"

var Options struct {
	help    bool
}

var YewberryRoot  string
var YewberryCache string

func main() {
	argv.Enabler("h", "help", &Options.help, "display this help text")

	err := argv.Parse()
	if err != nil {
		FatalError("%s", err.Error())
	}

	unparsed := argv.Unparsed()

	cmd := ""
	if len(unparsed) > 0 {
		cmd = unparsed[0]
		unparsed = unparsed[1:]
	}

	if Options.help || cmd == "help" || cmd == "" {
		ShowUsage()
		os.Exit(0)
	}

	// these calls can fatal error
	YewberryRoot = FindRootDir()
	YewberryCache = FindCacheDir()

	switch cmd {
	case "analyse", "analyze":
		err = CmdAnalyse(unparsed)
	default:
		err = fmt.Errorf("unknown command '%s'", cmd)
	}

	if err != nil {
		FatalError("%s", err.Error())
	}

	os.Exit(0)
}

func ShowUsage() {
	Message("Usage: yb-mod <command> [FILE] [OPTIONS...]")
	Message("")
	Message("Available commands:")
	Message("   analyse  FILE.ymod")

	Message("")
	Message("Available options:")
	argv.Display(os.Stdout)
}

func FatalError(format string, a ...interface{}) {
	format = "yb-mod: " + format + "\n"
	fmt.Fprintf(os.Stderr, format, a...)
	os.Exit(2)
}

func Message(format string, a ...interface{}) {
	fmt.Printf(format, a...)
	fmt.Printf("\n")
}

//------------------------------------------------------------------------

func CmdAnalyse(names []string) error {
	if len(names) == 0 {
		return fmt.Errorf("missing filename for analyse command")
	} else if len(names) > 1 {
		return fmt.Errorf("too many filenames")
	} else {
		filename := names[0]

		// simple programs are just a single code file
		if FileHasExtension(filename, "yb") {
			return AnalyseSimpleProgram(filename)

		} else if FileHasExtension(filename, "ymod") {
			return AnalyseModuleProgram(filename)

		} else {
			return fmt.Errorf("unknown file type (wanted .yb or .ymod)")
		}
	}
}

func AnalyseSimpleProgram(filename string) error {
	if !FileExists(filename) {
		return fmt.Errorf("no such file: %s", filename)
	}

	AnalyseShowPrelude()

	path := "."

	Message("(module 1 \"main\" \"%s\"", path)
	Message("    (file \"%s\")", filename)
	Message(")")

	return Ok
}

func AnalyseModuleProgram(filename string) error {
	// TODO !!
	return fmt.Errorf("modules not implemented yet")
}

func AnalyseShowPrelude() {
	Message("(module 0 \"prelude\" \"%s\"", YewberryRoot + "/prelude")
	Message("   (file \"pl_macros.yb\")")
	Message("   (file \"pl_base.yb\")")
	Message("   (file \"pl_map.yb\")")
	Message("   (file \"pl_set.yb\")")
	Message("   (file \"pl_sort.yb\")")
	Message(")")
}
