// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "fmt"

// Module represents the contents of a ".ymod" file.
type Module struct {
	files   []*FileRef
	imports []*Import

	// path of the ".ymod" file in the local file system.
	full_path string
}

type FileRef struct {
	base_name string // e.g. "mycode.yb"
	full_path string
}

type Import struct {
	import_path string
	version     Version
	short_name  string // it has a trailing '/'

	renamed bool // true if short_name was given explicitly
	local   bool // true for a local import (like "./bar")
	bump    bool // true for bump statements

	// the corresponding module info (once loaded)
	mod *Module
}

//----------------------------------------------------------------------

func NewModule(full_path string) *Module {
	mod := new(Module)

	mod.files = make([]*FileRef, 0)
	mod.imports = make([]*Import, 0)
	mod.full_path = full_path

	return mod
}

// current line number, can be zero if unknown
var parse_line_num int

// Load opens the file for the module and parses the contents.
// It fills out the 'files' and 'imports' lists.  The FileRef
// The FileRef structs will not determine the full_path here,
// that must be done later.
//
// This method should only be called once.
// Returns Ok on success, or an error on failure.
// Error messages typically mention the line number where
// parsing failed.
func (mod *Module) Load() error {
	f, err := os.Open(mod.full_path)
	if err != nil {
		return err
	}
	defer f.Close()

	parse_line_num = 0

	lex := NewLexer(f)

	for {
		t := lex.Scan()

		if t.Kind == TOK_EOF {
			break
		}

		if t.LineNum > 0 {
			parse_line_num = t.LineNum
		}

		if t.Kind == TOK_ERROR {
			return ParsingError("%s", t.Str)
		}

		err = mod.ParseToken(t)
		if err != nil {
			return err
		}
	}

	// a module file is technically invalid when there are no
	// file specs.  however for "yb-mod setup" we allow it here,
	// and other commands will validate it.

	return Ok
}

func (mod *Module) ParseToken(t *Token) error {
	if t.Kind != TOK_Expr {
		return ParsingError("expected item in (), got: %s", t.String())
	}

	if len(t.Children) == 0 {
		return ParsingError("empty definition found: ()")
	}

	head := t.Children[0]

	if head.Kind != TOK_Name {
		return ParsingError("expected a keyword, got: %s", head.String())
	}

	switch {
	case head.Match("file"):
		return mod.ParseCodeFile(t)

	case head.Match("import"):
		return mod.ParseImport(t, false /* is_bump */)

	case head.Match("bump"):
		return mod.ParseImport(t, true)
	}

	return ParsingError("unknown keyword: %s", head.Str)
}

func (mod *Module) ParseCodeFile(t *Token) error {
	if len(t.Children) < 2 {
		return ParsingError("bad file spec: missing filename")
	}
	if len(t.Children) > 2 {
		return ParsingError("bad file spec: extra stuff after filename")
	}

	t_file := t.Children[1]

	if t_file.Kind != TOK_String {
		return ParsingError("bad filename in spec: wanted string, got: %s", t_file.String())
	}
	if t_file.Str == "" {
		return ParsingError("filename is malformed: %q", t_file.Str)
	}

	ref := new(FileRef)
	ref.base_name = t_file.Str

	mod.files = append(mod.files, ref)
	return Ok
}

func (mod *Module) ParseImport(t *Token, is_bump bool) error {
	var err error

	im := new(Import)
	im.bump = is_bump

	/* import path */

	input := t.Children[1:]
	if len(input) < 1 {
		return ParsingError("bad import spec: missing import path")
	}
	if input[0].Kind != TOK_String {
		return ParsingError("bad import path: wanted string, got: %s",
			input[0].String())
	}
	im.import_path = input[0].Str // FIXME validate path
	input = input[1:]

	/* explicit short name */

	if len(input) >= 1 && input[len(input)-1].Match("->") {
		return ParsingError("bad import spec: missing module name after ->")
	}
	for p := 0; p < len(input)-2; p++ {
		if input[p].Match("->") {
			return ParsingError("bad import spec: extra stuff at end")
		}
	}
	if len(input) >= 2 && input[len(input)-2].Match("->") {
		t_rename := input[len(input)-1]

		if t_rename.Kind != TOK_Module {
			return ParsingError("bad import spec: wanted module name, got: %s",
				t_rename.String())
		}

		im.short_name = t_rename.Str
		im.renamed = true

		input = input[0:len(input)-2]
	}

	/* version string */

	if len(input) > 1 {
		return ParsingError("bad import spec: too many elements")
	}
	if len(input) == 1 {
		t_vers := input[0]

		if t_vers.Kind != TOK_String {
			return ParsingError("bad version spec: wanted string, got: %s",
				t_vers.String())
		}

		if t_vers.Str == "" {
			// empty version is ok here, will be validated later
		} else {
			im.version.str = t_vers.Str
			err = im.version.Decode()
			if err != nil {
				return err
			}
		}
	}

	if !is_bump {
		im.CheckIsLocal()
	}
	if !is_bump && !im.renamed {
		im.DetermineShortName()
	}

	mod.imports = append(mod.imports, im)
	return Ok
}

func ParsingError(format string, a ...interface{}) error {
	format = fmt.Sprintf(format, a...)

	if parse_line_num > 0 {
		format = fmt.Sprintf("line %d: %s", parse_line_num, format)
	}

	return fmt.Errorf("%s", format)
}

//----------------------------------------------------------------------

// Save overwrites the module file with new text generated
// from the info in the Module struct.  Any user comments
// in the original file will be lost by this operation.
//
// Returns Ok on success, or an error on failure.
//
// A failure almost certainly means that the module file is
// now corrupt.  It is the caller's responsibility to prepare
// for potential failures (like backing up the original file)
// and to clean things up when they occur.
func (mod *Module) Save() error {
	f, err := os.Create(mod.full_path)

	if err == nil {
		err = mod.WriteCodeFiles(f)
	}
	if err == nil {
		err = mod.WriteLocals(f)
	}
	if err == nil {
		err = mod.WriteImports(f)
	}
	if err == nil {
		err = mod.WriteBumps(f)
	}

	err2 := f.Close()

	if err == nil {
		err = err2
	}
	return err
}

func (mod *Module) WriteCodeFiles(f *os.File) error {
	var err error

	for _, ref := range mod.files {
		_, err = fmt.Fprintf(f, "(file %q)\n", ref.base_name)
		if err != nil {
			return err
		}
	}

	_, err = fmt.Fprintf(f, "\n")
	return err
}

func (mod *Module) WriteImports(f *os.File) error {
	var err error
	var count int

	for _, im := range mod.imports {
		if !im.bump && !im.local {
			if im.renamed {
				_, err = fmt.Fprintf(f, "(import %q %q -> %s)\n",
					im.import_path, im.version.str, im.short_name)
			} else {
				_, err = fmt.Fprintf(f, "(import %q %q)\n",
					im.import_path, im.version.str)
			}
			if err != nil {
				return err
			}
			count++
		}
	}

	if count > 0 {
		_, err = fmt.Fprintf(f, "\n")
		return err
	}
	return Ok
}

func (mod *Module) WriteLocals(f *os.File) error {
	var err error
	var count int

	for _, im := range mod.imports {
		if !im.bump && im.local {
			if im.renamed {
				_, err = fmt.Fprintf(f, "(import %q -> %s)\n",
					im.import_path, im.short_name)
			} else {
				_, err = fmt.Fprintf(f, "(import %q)\n", im.import_path)
			}
			if err != nil {
				return err
			}
			count++
		}
	}

	if count > 0 {
		_, err = fmt.Fprintf(f, "\n")
		return err
	}
	return Ok
}

func (mod *Module) WriteBumps(f *os.File) error {
	var err error
	var count int

	for _, im := range mod.imports {
		if im.bump {
			_, err = fmt.Fprintf(f, "(bump %q %q)\n", im.import_path,
				im.version.str)

			if err != nil {
				return err
			}
			count++
		}
	}

	if count > 0 {
		_, err = fmt.Fprintf(f, "\n")
		return err
	}
	return Ok
}

//----------------------------------------------------------------------

type ImportPath struct {
	repo string // the repository GIT URL
	file string // the file within the repository (no extension)
}

// Decode pulls apart an import path string and fills out the
// ImportPath struct.  If the file part of the import path is
// absent, the 'file' field will have the guessed name.
//
// Syntax is an optional protocol (a word followed by "://")
// followed by a host/port part (possibly empty) then '/'
// to start a pathname.  After that pathname can be a ':'
// which separates the FILE part of the import path.
//
// TODO: check how file:// handles Windows drive letters.
//
func (imp *ImportPath) Decode(s string) error {
	/* step 1 : see if we have a protocol */
	proto := ""

	r := []rune(s)

	pos := 0

	for pos < len(r) && 'a' <= r[pos] && r[pos] <= 'z' {
		pos++
	}

	if pos > 0 && pos < len(r) && r[pos] == ':' {
		proto = string(r[0:pos])
		pos++

		if pos+1 < len(r) || r[pos] != '/' || r[pos+1] != '/' {
			return fmt.Errorf("bad git url: expected // after %s",
				string(r[0:pos]))
		}

		pos += 2
		r = r[pos:]

	} else {
		pos = 0
	}

	/* step 2 : skip the host/port part */

	for pos < len(r) && r[pos] != '/' {
		pos++
	}

	if pos >= len(r) {
		return fmt.Errorf("bad git url: expected / after hostname")
	}

	/* step 3 : skip the remote path */

	for pos < len(r) && r[pos] != ':' {
		pos++
	}

	imp.repo = string(r[0:pos])

	if pos < len(r) {
		imp.file = string(r[pos+1:])
	}

	// add the default proto
	if proto == "" {
		imp.repo = "https://" + imp.repo
	}

	return Ok
}

// Escaped takes the 'repo' name and produces a valid filename.
// All colons and slashes are escaped, plus other potentially
// problematic characters.
func (imp *ImportPath) Escaped() string {
	s := ""

	for _, r := range imp.repo {
		if r == '/' {
			s += "__"
		} else if r == '\\' || r == '\'' || r == '"' ||
			r == ' ' || r == ':' || r == '~' || r == '*' {
			if r > 0xFFFF {
				s += fmt.Sprintf("_v%08x", r)
			} else if r > 0xFF {
				s += fmt.Sprintf("_u%04x", r)
			} else {
				s += fmt.Sprintf("_%02x", r)
			}
		} else {
			s += string(r)
		}
	}

	// sanity check, prevent an empty string
	if s == "" {
		return "_z"
	} else {
		return s
	}
}

//----------------------------------------------------------------------

func (im *Import) CheckIsLocal() {
	// FIXME : CheckIsLocal
}

func (im *Import) DetermineShortName() error {
	// FIXME DetermineShortName
	im.short_name = "foo"
	return Ok
}
