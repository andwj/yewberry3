// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
//import "fmt"
import "strings"
import "path/filepath"

var Ok error

func FindRootDir() string {
	// environment var overrides other methods
	yb_root := os.Getenv("YEWBERRY_ROOT")

	if yb_root != "" {
		if !FileExists(yb_root + "/YB_VERSION") {
			FatalError("YEWBERRY_ROOT is set to invalid dir")
		}
		return yb_root
	}

	// a decent fallback: the dir above one containing this executable
	exe_name, err := os.Executable()
	if err == nil {
		yb_root = filepath.Dir(exe_name)
		if len(yb_root) > 6 && yb_root[len(yb_root)-4:] == "/bin" {
			yb_root = yb_root[0:len(yb_root)-4]
			if FileExists(yb_root + "/YB_VERSION") {
				return yb_root
			}
		}
	}

	// emergency fallback : a hard-coded path
	// TODO: better path for Windows
	yb_root = "/usr/local/yewberry"

	if !FileExists(yb_root + "/YB_VERSION") {
		FatalError("unknown root dir (set YEWBERRY_ROOT in env)")
	}
	return yb_root
}

func FindCacheDir() string {
	yb_cache := os.Getenv("YEWBERRY_CACHE")

	if yb_cache == "" {
		cache_dir, err := os.UserCacheDir()
		if err != nil {
			FatalError("unknown cache dir (set YEWBERRY_CACHE in env)")
		}

		yb_cache = filepath.Join(cache_dir, "yewberry")
	}

	// create the directory (and any parents too)
	if os.MkdirAll(yb_cache, 0777) != nil {
		FatalError("error creating yewberry cache dir")
	}

	return yb_cache
}

func FileExists(fn string) bool {
	f, err := os.Open(fn)
	if err == nil {
		f.Close()
	}
	return (err == nil)
}

func FileHasExtension(fn, ext string) bool {
		fn = filepath.Ext(fn)
		fn = strings.ToLower(fn)

		if len(fn) > 0 && fn[0] == '.' {
				fn = fn[1:]
		}

		return (fn == ext)
}

func FileRemoveExtension(fn string) string {
	old_ext := filepath.Ext(fn)
	return strings.TrimSuffix(fn, old_ext)
}

//------------------------------------------------------------------------

/*
func Output(format string, a ...interface{}) {
	Stage.output += fmt.Sprintf(format, a...)
}

func OutputToken(t *Token) error {
	switch t.Kind {
	case TOK_Expr:
		return OutputTokenList(t, "(", ")")
	case TOK_Access:
		return OutputTokenList(t, "[", "]")
	case TOK_Data:
		return OutputTokenList(t, "{", "}")
	default:
		Output("%s", t.Str)
		return Ok
	}
}

func OutputTokenList(list *Token, opener, closer string) error {
	Output("%s", opener)

	for i, child := range list.Children {
		if i > 0 {
			Output(" ")
		}
		OutputToken(child)
	}

	Output("%s", closer)
	return Ok
}
*/
