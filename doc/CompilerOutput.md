
Compiler Output
===============

Introduction
------------

When compiling a program, the Yewberry compiler prints some
text to STDOUT which describes how the compiled code should be
assembled and linked.  This text has the form of S-expressions.

Any errors from the Yewberry compiler are output on STDERR, and
the exit code will be non-zero.  On error, anything printed to
stdout will be useless and should be ignored.


Output Statements
-----------------

The "assemble" statement specifies a file which the assembler
must be invoked on to produce an object file.  These files are
always in the temporary build directory (created by yb-build)
and the object filename is implied (replace ".s" with ".o").

Example:
```
(assemble "main.s")
(assemble "prelude.s")
```

The "cc" statement is similar but specifies a C language file,
and a C compiler must be invoked to produce the object file
(whose name is determined by replacing ".c" with ".o").

Example:
```
(cc "main.c")
(cc "prelude.c")
```

The "runtime" statement specifies a file from the runtime to
transfer to the temporary directory.  A subsequent statement
is needed to actually compile/assemble it.
The source filename will be found within the $YEWBERRY_ROOT
tree, under the "runtime" directory.

Example:
```
(runtime "c/string.c")
(runtime "amd64/syscall.s")
```
