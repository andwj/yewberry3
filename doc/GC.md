
GC DESIGN DOCUMENT
==================

by Andrew Apted, 2020.


Introduction
------------

The garbage collector used in the current Yewberry compiler and
runtime aims to be as simple as possible.  It assumes we can
keep enough information in allocated "objects" to be able to
know exactly which parts are references to other objects.


GC Algorithm
------------

The algorithm is the well-known incremental tri-color method.
Every object which has been allocated will be in one of three
states:

1. WHITE (unknown)
2. GRAY  (referenced)
3. BLACK (scanned)

Just before a GC cycle begins, all objects are considered to be
WHITE, which means we don't know if they are reachable or not,
but are candidates for collection.

Objects which have a reference from somewhere will, if reached,
get turned from WHITE to GRAY.  The term "GRAY SET" refers to
all the objects which are currently gray.

Each GRAY object gets scanned for references to other objects,
which in turn may become gray.  Just before we begin scanning a
gray object, it is marked as BLACK and removed from the gray set.
BLACK objects are what remains after a GC cycle is complete,

A cycle begins by marking all "root" objects as GRAY (see below),
and it continues as long as the GRAY SET is non-empty.
After a cycle finishes, all the WHITE objects are freed.

A big advantage of a tri-color garbage collector is that a GC cycle
does not need to "stop the world" and scan every single object,
instead it can be an on-going process where we do some amount of
scanning work each time the program allocates some memory.


Roots
-----

The "roots" consist of the following:

- all global variables of the program
- all local variables on the stack
- possibly other state of the language runtime


Write Barrier
-------------

A potential danger of allowing the program and garbage collector
to run concurrently is that the program could store a reference to
a WHITE object into an object which the GC has already scanned
(a BLACK object).  If that happens, the GC could easily miss that
WHITE object and erroneously free it.

To prevent that, the compiler ensures that WHENEVER a reference
to a WHITE object is stored into a BLACK object, either the WHITE
object is darkened to GRAY or the BLACK object is lightened to GRAY
(forcing it to be re-scanned).  It is not clear which strategy will
be the best one yet....

The set of all global and local variables is also considered to be
a BLACK object, and any assignment to those variables MUST follow
the above rule.

Newly allocated objects will begin life as BLACK, and hence they
avoid being scanned until the next GC cycle.  This guarantees that
the GC will not consider them as garbage and prematurely free them.


Collection Phases
-----------------

Before the first GC cycle begins, all objects allocated by the
program will be BLACK -- there will not be any GRAY or WHITE
objects.  Once the amount of allocated memory reaches a certain
threshhold, a cycle begins by marking all objects as WHITE.

Once a cycle is complete (after all reachable objects have been
scanned), the list of all objects is partitioned into two lists,
one for WHITE (to be freed) and one for BLACK (used objects).
Freeing those WHITE objects can be done later, in an incremental
way, and they could even be recycled to satisfy allocations.

The GC is in an inactive phase immediately after a cycle has
finished, and will wait for the amount of memory allocated to
reach the same threshhold (or an increased one) to begin a new
cycle.  As an optimisation, the partitioning step marked all
used objects as WHITE and hence we don't need to repeat it.

The program can enable and disable the GC at any time, and may
request that a "full collection" is performed, which causes any
current cycle to run until finished and all unused memory to be
freed.


The Gray Set
------------

The GRAY SET is implemented as an explicit list of objects.
Each time an object is darkened (becomes GRAY), it is appended
to this list.  Since an object only turns GRAY once during a GC
cycle, it can never already exist in the list.

Visiting an object in the GRAY SET causes that object to be
scanned for references.  A reference to an object which is known
to not contain any pointers can be darkened straight to BLACK.

The most recently added object in the GRAY SET is the next to
be scanned.  This simplifies the storage requirements, as the
GRAY SET can be implemented as a stack.


Pacing
------

During program executing, the GC can perform some work every time
the program allocates some memory.  It can also perform work due
to an explicit call from the program (e.g. a game engine may want
to perform a full collection after loading a new map).

But how much work should the GC do?

If the GC does not collect fast enough, the program could "run away"
and allocate memory faster than can be collected, even though it
isn't really using that much.  On a low memory machine, this could
cause a program which would otherwise run fine to be killed by the
operating system.

If the GC does too much work at each step, then performance will
suffer.  The worst case scenario would be to perform a complete
cycle before each allocation.

To keep things simple, the GC will have a configurable setting on
how much "work" to perform before each allocation.  The value 1000
will be the default, and tuned to work decently for many programs.
Lower values cause fewer objects and less memory to be scanned,
whereas higher values cause more objects and memory to be scanned.
Freeing no-longer-used objects is also part of that work.


Small Allocator
---------------

The small allocator packs a group of same-sized objects into a
single page.  The following table shows the supported object
sizes:

```
      4     8    12    16
     24    32    48    64
     80    96   112   128
    160   192   224   256
    320   384   448   512
    640   768   896  1024
   1280  1536  1792
```

Each size has a group of page sets.  The first page set in the
group is a single page (once it is created).  The next page set
uses two pages, and each additional page set is twice as large
as the previous one.

Page sets themselves are allocated and freed via the large
allocator.


Large Allocator
---------------

This allocates and frees memory directly via the operating system.
For Linux and other Unices, it uses the mmap(2) and munmap(2)
syscalls.  On Windows it uses VirtualAlloc() and VirtualFree().

