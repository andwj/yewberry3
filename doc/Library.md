
Standard Library
================

by Andrew Apted, 2020.


Introduction
------------

The standard library is a set of modules which are included in
a distribution of the Yewberry compiler.  These modules do not
have an explicit version, as they are considered part of the
language itself, and there is no mechanism to use older or newer
versions or specify a required version.

The standard library does not include the facilities which are
provided by the core language, for example the `print` function.
Such facilities can be used without needing to add statements
to a `.ymod` file, whereas the standard libraries require a
statement in the `.ymod` file to be used.


