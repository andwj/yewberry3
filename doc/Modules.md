
Module System
=============

By Andrew Apted.


What is a module?
-----------------

A module is primarily a text file with the ".ymod" extension.
This file will record the code dependencies quite precisely.
Module files can be created and edited by both humans and
by various tools.

A module file specifies two main things: which code files are
a part of the module, and which other modules are required by
this one.  It also supports a few other features like "vendoring".

Module imports must form an *acyclic* directed graph.  Hence
it is not permitted for any two modules to import each other.
But two modules, A and B say, can both import a third one.


Programs
--------

A program is either a single ".yb" code file (for tiny programs)
or a ".ymod" module file (for everything else).  Only module files
can use import statements, hence most normal programs will require
a module file.  This restriction is deliberate, since it means the
module system mostly concerns itself with ".ymod" files (a layer
above the language itself).

The full text of a program is the initial ".yb" file(s) combined
with the ".yb" files from all imported modules in the dependency
chain.  Tools will require all these files to exist somewhere
on the filesystem.

The compiler will create a native executable which can be run
stand-alone.  The name of the executable will be the same as the
".yb" or ".ymod" program file, but replacing the extension to
what is appropriate for the OS, such as ".exe" on Windows and
nothing on Linux and BSD.


Module Names
------------

In code, each module has a short name which is a standard
identifier ending with a '/' (forward slash) character.
Replacing '/' with ".ymod" often produces the filename of
the module file.

These short names are used to scope identifiers in code.
For example, if the module "foo/" has a function "compute",
then other modules must use the qualified name "foo/compute"
to refer to that function.


Standard Library
----------------

The standard library is a set of modules which are included in
a distribution of the Yewberry compiler.  These modules do not
have an explicit version, as they are considered part of the
language itself, and there is no mechanism to use older or
newer versions or specify a required version.

These modules are imported via a "std" statement.  The import
path is generally a single word, or two words separated by a '/'
slash.  The last word will be the in-code module name unless a
different one is specified.


Local Modules
-------------

A local module is a module whose ".ymod" file exists in the same
directory as the other module which uses it, or a sub-directory
of it.  They are basically just a way of splitting the code into
separate pieces and using the benefits of module scoping.

Local modules are imported using a "local" statement, and uses
a normal path name for the module location.  This path cannot be
absolute or begin with "..", but can include directory components,
such as "nice/utils" for a module "utils" in the "nice" directory.

Local modules do not have a version, and are never downloaded
separately from the program or module containing them.  They
cannot be accessed in any way by modules not using them.

The scope (in code) of the short name of a local module is
limited to the module using it.  Distinct external modules
can each have a local module with the same name.


External Modules
----------------

Modules may exist on some other computer, typically a code
hosting service on the internet.  These are called "external"
modules, and their location is specified by an "import path".

Since external modules have version numbers, they must exist
in some kind of version control which supports tagging, so they
are currently limited to GIT repositories.

An import path consists of two parts: a compulsory URL of the
GIT repository, and an optional location of the ".ymod" file in
that repository.  The GIT repository may begin with a protocol
name followed by a colon, like "ssh:" or "git:" or "file:", but
when absent then "https:" is assumed.

The second part of an import path is often absent, and the
location of the ".ymod" file is inferred from the repo name.
When present, it must be separated from the repository name
with a colon (':'), and must be a relative path name.
The ".ymod" extension is implied.

The short name of a module is usually inferred from the import
path.  For example, if the import path is "mysite.com/blah:foo"
then the remote repository has a directory "blah" containing a
file called "foo.ymod", and that implies a module name of "foo/".
An import statement can, however, specify a different short name.
This only affects the code using that module -- it has no effect
on the filename of the ".ymod" file.


Vendoring
---------

The term "vendoring" means that the code for a module will be
found in a directory belonging to the main program, instead of
downloading it from a third-party site.  This allows a module
to be customized for a particular application.

To enable vendoring of a module, firstly the ".ymod" file of
the program must use a "vendor" statement instead of "import".
Secondly, the short name of the module (whether given explictly
or derived) provides the directory name under "vendor/" which
houses the code of the module.  The "vendor" directory must
exist in the same directory as the program being compiled.

The vendor statement also specifies the version of the module
which exists locally, and this will override all other version
numbers in the dependency analysis.  It is still an error when
another module imports the vendored one and the major version
number is different (see next section).

Vendoring only applies to the program being compiled, it cannot
be used by library modules.  This rule exists to prevent the
problem of two different modules vendoring the same third module.
Any "vendor" statement in a library module causes an error,
and any "vendor" directory in a library module will be ignored.


Version Scheme
--------------

All external modules have a semantic version number, which is
three integers separated by dots, representing the major, minor
and patch level.  There may also be additional components after
the patch level, following a dash ('-').  The version number may
be prefixed with the letter 'v' in some contexts, but the usual
convention is to omit it.

Import statements for external modules must specify the minimum
version required.  The downloaded GIT repository must have a tag
which matches the given version, it is an error if no such tag
exists.

At compile time, the chosen version of a module will be the
maximum version specified in any import in the dependency chain
(unless the module is vendored, see above).  If any two modules
require a different major version of a module, then the dependency
chain is considered broken and an error occurs.


Visibility of Identifiers
-------------------------

Identifiers in a module are either public or private.
Only public identifiers may be used by another module.
If an identifier is a type and is private, then any tags,
field names and method names associated with that type
are also private.

By default, all definitions in a ".yb" code file are private.
The symbols `#public` and `#private` can appear anywhere a
top-level definition can, and cause all subsequent top-level
definitions to acquire that visibility.

For macros, the visibility of identifiers in the macro body
is determined where the macro is defined (NOT where it is
invoked).  Hence a macro defined in a module foo/ can refer
to private identifiers in foo/ and be successfully invoked
in other modules.

The symbol `#restricted` is another qualifier which mainly
applies to types.  For non-types, it is the same as `#public`,
but for types it means that outside code may refer to the type
name and invoke methods on values of the type, but may not
construct new values or access any of the fields.


Aliases
-------

Aliases allow using a bare (unqualified) name for something
in another module.  They are declared somewhere in the code
of a module, and are only visible within that module.  They
are similar to macros, but they are processed very early.

Example:
```
(alias cracker Foo/cracker)
```


Syntax
------

Specify a code file belonging to the module:
```
(file "blah.yb")
```

Import a module in the standard library:
```
(std      "math/complex")
(std cmp/ "math/complex")
```

Import a local module:
```
(local      "xyz-stuff")
(local xyz/ "xyz-stuff")
```

Import an external (third-party) module:
```
(external      "some.site/path/name" "1.2.3")
(external foo/ "some.site/path/name" "1.2.3")
```

Import a vendored module:
```
(vendor      "some.site/path/name" "1.2.3")
(vendor foo/ "some.site/path/name" "1.2.3")
```

Bump the version of an indirectly imported module:
```
(bump "some.site/path/name" "1.2.3")
```

Specify the license of the module:
```
(license "MIT")
```

Specify required minimum version of the language itself:
```
(yewberry "1.2.3")
```


Tooling
-------

A program called "yb-mod" will be created to assist in
setting up module files, downloading dependencies from the
internet, and upgrading and downgrading module versions.

There will be a directory where downloaded modules are stored.
For example, in Linux it might be `$HOME/.yewberry/modules/`.
The directory name for a module is derived from the long name
and can consist of multiple directory levels.  The last directory
component of a downloaded module is the version string.

The following command determines the current version for
imports which don't yet specify a version, or specify a
partial version (such as "v1").  The given module file gets
rewritten with the new version information.
Modules may be downloaded if needed.
```
yb-mod setup FILE.ymod
```

The following command upgrades the module imports in a file
to the latest version available (keeping the major version
number intact).  No new imports are ever added.
Modules may be downloaded if needed.
```
yb-mod update FILE.ymod
```

This command is similar to `update` but it looks at the
imports of imported modules, and so forth down the dependency
chain, and finds which of those modules can be updated to a
more recent version.  This can cause "bump" statements to be
added to the given module -- these statements only exist to
cause a version bump of something further down the chain.
Modules may be downloaded if needed.
```
yb-mod deep-update FILE.ymod
```


Analysis for Compiler
---------------------

The `analyse` command from yb-mod produces a file which the
compiler uses.  It contains a simplified view of the required
modules of a program.  It consists of "module" statements which
specify the absolute path where the files are, and a short name
used for error messages.

Each module gets an index number, starting at zero, and later
modules may refer to earlier ones via their index number (the
converse is not allowed).  The very last entry is always the
main program.

Each module specifies the code files via "file" statements,
and the used modules via "use" statements.  The prelude module
is used implicitly by all other modules.

Example:
```
(module 0 "prelude" "/the/yewberry/root/prelude"
   (file "xxx.yb")
)
(module 1 "foo" "/absolute/path/foo"
   (file "xxx.yb)
   (file "zzz.yb)
)
(module 2 "myprog" "/another/path/myprog"
   (file "myprog.yb")
   (use 1 foo/)
)
```
