
Sweet Syntax
============

By Andrew Apted.


Introduction
------------

This describes the "sweet" syntax of Yewberry code, which is
inspired by (but not compatible with) SRFI 110 "Sweet Expressions"
for Scheme, by David A. Wheeler and Alan Manuel K. Gloria, plus
the earlier SRFI 49 by Egil Möller.


File Reading
------------

All lines MUST be indented by N tabs, where N >= 0.
It is an error if any lines contain a space character before the
first non-whitespace character (except in a multi-line comment).

Lines may also be indented using "visible tabs".  These are a `|`
pipe character followed by one or more spaces, which are converted
to a tab while reading.  The purpose of this syntax is for sample
code, and is not recommended for everyday use.  It is an error if
a tab appears after a `|` and before the first real character on a
line.  This construct cannot be used elsewhere in a line, only for
indentation.

Assume all comments are completely removed, including multi-line
comments between `;[` and `;]` and all whitespace after `;]`.
Following that step, assume all lines which are blank or contain
nothing but whitespace are removed.  Naturally line numbers will
be preserved for proper error messages.

Inside `()`, `[]` or `{}` brackets, any newline character MUST
be treated as if it were a single space, and not finish a line or
begin a new line.  These forms will form a single token for the
purposes of this document, and a line which began before the
opening bracket continues after the closing bracket.


Function Calls
--------------

An identifier followed by the empty S-expression `()` will be
considered to be a function call with no arguments, and gets
rewritten as an S-expression containing just that identifier.
The identifier and `()` parentheses MUST be on the same line.

This conversion occurs before the next phase (indentation stuff)
is done, and will form a single token for that phase.  It happens
in all contexts with a couple of exceptions....

Since formal parameter lists are often empty, this conversion is
inhibited when the identifier is `lam` or `rule`, or when the
previous token is the keyword `fun`.

Example:
```
fun foo ()        -->    (fun foo ()
|  bar()                   (bar)
|  print name()            (print (name))
|                        )
```

Additionally, inside a math expression a sequence of two or more
tokens which are not operators will be considered to be a function
or method call, i.e. grouped as an S-expression.  This conversion
is not strictly part of this specification, since it is usable in
plain ".yb" files too, but it was inspired by the bracket-less
function calls allowed by this spec.


Expression Parsing
------------------

Any line which is indented more than the previous line is a *child*
of that line, and the previous line is called the *parent*.  It is
an error if there is no such previous line.  Subsequent lines with
the same indentation as the first child are also children of that
parent.

A line with only a single token and NO child lines represents itself,
i.e. does not form a new S-expression.

Otherwise the line begins a new S-expression within `(` and `)`
parentheses.  This can be used for any language form which uses
parentheses, not only code but also definitions of macros, types,
functions, methods and global vars.  The expression ends when a line
at the same indentation level or lower as the start line is reached,
or the end of file.  Each child of a parent is appended to its
S-expression, usually as a single token.

Example:
```
print              -->    (print
|  $                         ($
|  |  "Hello"                   "Hello"
|  |  "world!"                  "world!"
|                            )
|                         )
print "Goobye!"    -->    (print "Goodbye!")
123                       123
```


Multi-pronged Forms
-------------------

The term "multi-pronged" refers to an expression which not only
begins with a known language keyword, but has sections which are
delimited by other language keywords.  Currently there is only one
such form in Yewberry: the `if` form.  It has optional sections
marked by the `elif` and `else` keywords (these are the "prongs").

For these kind of expressions, an additional rule applies: when
a line begins with a prong keyword, then find the previous line
which is at the exact same indentation level and begins with an
identifier which is not a prong.  The line containing the prong
becomes a child of that previous line (instead of terminating it).
There must not be any lines at a lower indentation level in
between, and it is an error if no such line can be found.

Example:
```
|  if (some expr)       -->    (if (some expr)
|  |  print "hello"               (print "hello")
|  else                         else
|  |  print "goodbye"             (print "goodbye")
|                              )
```

User macros can be used to create custom multi-pronged expressions.
Any identifier beginning with a `|` pipe character followed by a
letter will behave as a prong (as described above).


Inhibiting S-Expressions
------------------------

It is sometimes useful to force a line to inhibit construction
of an S-expression.  This can be done by placing the `\` symbol
(backslash) as the first element of a line.  It must be followed
by at least one whitespace character, and it is an error if no
normal tokens appear after it, or if the `\` symbol is used in
any other context.

The inhibited line may also have children, and these will still
be processed normally, e.g. they can form S-expressions.  However
the parent line will stay inhibited and not form an S-expression.

Example:
```
blah            -->    (blah
|  \ 123 456              123 456
|  \ 987 543              987 543
|  |  sqrt 9.0            (sqrt 9.0)
|                      )
```

Note that certain language keywords also inhibit S-expressions.
Firstly, any of the prongs of a multi-prong expression will
inhibit it -- they would fail miserably otherwise.  In addition,
the keywords `where` and `=>` which are used in match expressions
also inhibit S-expressions when at the start of a line.


Pseudo Lines
------------

Lines containing the `=` (equal sign) or `=>` symbol and at least
one additional token are handled in a special way.  It is as though
the line is split, and everything after the `=` or `=>` is placed
on a new line at the same indentation level as the old line.  The
new line also becomes the sole child of the old line, and children
of the old line become children of the new line.

This splitting can occur multiple times on a single line, starting
with the left-most occurrence of `=` or `=>` symbols.  These new
"pseudo lines" may undergo construction of S-expressions using the
normal rules, though the original (shortened) line always does.
Both the old line and new line(s) will be terminated by a future
line at a lower indentation level, or the end of file.

If a line begins with a `\` to inhibit S-expressions, then the
splitting described above is also inhibited.

Example:
```
fun foo ()                    -->    (fun foo ()
|  let x = rand-int 0 10                (let x (rand-int 0 10))
|  match x                              (match x
|  |  3 4 5 => print "good"                (3 4 5 => (print "good"))
|  |  _     => print "bad"                 (_     => (print "bad"))
|                                       )
                                     )
```


Assignment Forms
----------------

When a line containing `=` is split (see above), it is expected
to be part of an assignment or `let` form.  If there is only a
single token before the `=` symbol, the line is rewritten to
begin with the `set!` keyword.  Otherwise the line should begin
with a `let` form and `=` should be the third token.  In both
cases the `=` symbol is removed.  Using the `=` symbol in any
other context is an error.

Example:
```
d = hypot x y     -->    (set! d (hypot x y))

let foo = sin x          (let foo (sin x))
```

