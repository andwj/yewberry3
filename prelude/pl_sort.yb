;;
;; Yewberry prelude : sorting
;;
;; Copyright 2020 Andrew Apted.
;; Use of this code is governed by an MIT-style license.
;; See the top-level "LICENSE.md" file for the full text.
;;

fun .sort (self (array @T) cmp (function @T @T Compare))
	let L = .len self
	if (L > 1)
		.sort-section self 0 (L - 1) cmp

fun .sort-section (self (array @T) start int end int cmp (function @T @T Compare))
	; this is a Quick-sort algorithm, but crafted so that the
	; comparison function could return complete garbage and this
	; code will not die (e.g. access out-of-bound elements).

	if (start > end)
		abort-program "sort-section: start > end"

	let-mut s = start
	let-mut e = end

	while (s < e)
		; handle the two element case (trivially)
		if (s == e - 1)
			if (is? (cmp [self e] [self s]) `LESS)
				swap! [self e] [self s]
			skip `end

		; choosing a pivot in the middle should avoid the worst-case
		; behavior that otherwise occurs when pivot is first or last
		; and the array is already sorted.
		let pivot-idx = ((s + e) >> 1)

		let mid = .sort-partition self s e pivot-idx cmp

		; handle degenerate cases
		if (mid <= s)
			s = (s + 1)
			skip `next

		if (mid + 1 >= e)
			e = (e - 1)
			skip `next

		; only use recursion on the smallest group
		; [ it helps to limit stack usage ]
		if (mid - s < e - mid)
			.sort-section self s mid cmp
			s = (mid + 1)
		else
			.sort-section self (mid + 1) e cmp
			e = mid

fun .sort-partition (self (array @T) lo int hi int pivot-idx int
                     cmp (function @T @T Compare) -> int)
	; this is Hoare's algorithm.
	; result is the index of pivot element after the partition.

	let A = self

	let-mut s = lo
	let-mut e = hi
	let-mut pivot-idx = pivot-idx

	loop
		while (s <= e && (is? (cmp [A s] [A pivot-idx]) `LESS))
			s = (s + 1)

		if (s > hi)
			; all values were < pivot, including the pivot itself!
			if (pivot-idx != hi)
				swap! [A pivot-idx] [A hi]
			skip `end (hi - 1)

		while (e >= s && (is? (cmp [A e] [A pivot-idx]) `EQUAL `GREATER))
			e = (e - 1)

		if (e < lo)
			; all values were >= pivot
			if (pivot-idx != lo)
				swap! [A pivot-idx] [A lo]
			skip `end lo

		if (s >= e)
			skip `end (s - 1)

		swap! [A s] [A e]

		if (pivot-idx == s)
			pivot-idx = e
		elif (pivot-idx == e)
			pivot-idx = s

		s = (s + 1)
		e = (e - 1)

	junction `end 0
