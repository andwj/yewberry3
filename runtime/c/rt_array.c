// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

/* RunTime : array operations */

#include "rt_common.h"

#include <string.h>  // memcpy

static void array_grow(YArray * arr, uint32_t new_total) {
	size_t new_size = (size_t)     new_total * sizeof(YValue);
	size_t old_size = (size_t)arr->mem.total * sizeof(YValue);

	YValue *new_elems = _gc_raw_alloc(new_size);

	if (arr->elems != NULL) {
		memcpy(new_elems, arr->elems, old_size);
		_gc_raw_free(arr->elems /*, old_size */);
	}

	arr->mem.total = new_total;
	arr->elems = new_elems;
}

static inline uint32_t array_bump_capacity(YArray *arr) {
	if (arr->mem.total < 250) {
		return (arr->mem.total + 1) * 2;

	} else {
		uint32_t new_cap = arr->mem.total + arr->mem.total / 2;

		// overflow?
		if (new_cap <= arr->mem.total) {
			_rt_fatal_error("array overflow");
		}
		return new_cap;
	}
}

void _rt_array_clear(YArray * arr) {
	size_t old_size = (size_t)arr->mem.total * sizeof(YValue);

	if (old_size > 0) {
		_gc_raw_free(arr->elems /*, old_size */);
	}

	arr->mem.total = 0;
	arr->len = 0;
	arr->elems = NULL;
}

void _rt_array_append(YArray * arr, YValue val) {
	// need to grow it?
	if (arr->len >= arr->mem.total) {
		array_grow(arr, array_bump_capacity(arr));
	}

	arr->elems[arr->len] = val;
	arr->len += 1;
}

void _rt_array_insert(YArray * arr, YInt pos, YValue val) {
	// need to grow it?
	if (arr->len >= arr->mem.total) {
		array_grow(arr, array_bump_capacity(arr));
	}

	if (pos < 0) {
		pos = 0;
	}

	if (pos >= (YInt) arr->len) {
		// inserting onto the end is easy
		arr->elems[arr->len] = val;

	} else {
		// shift elements up
		uint32_t tail = arr->len - (uint32_t)pos;
		YValue * src = &arr->elems[pos];

		memmove(src + 1, src, tail * sizeof(YValue));

		arr->elems[pos] = val;
	}

	arr->len += 1;
}

void _rt_array_delete(YArray * arr, YInt start, YInt end) {
	// NOTE that we don't need to clear elements.
	// plus we currently never resize (shrink) the memory block.

	// clamp the start to 0, end to the length
	if (start < 0) {
		start = 0;
	}
	if (end > (YInt) arr->len) {
		end = (YInt) arr->len;
	}

	if (start >= end) {
		// nothing to delete
		return;
	}

	uint32_t count = (uint32_t)(end - start);

	if (end == (YInt) arr->len) {
		// deleting elements off the end is easy
	} else {
		// shift elements down
		uint32_t tail = arr->len - (uint32_t)end;

		YValue * src  = &arr->elems[end];
		YValue * dest = &arr->elems[start];

		memmove(dest, src, tail * sizeof(YValue));
	}

	arr->len -= count;
}

YArray * _rt_array_subseq(const YArray *a, YInt start, YInt end) {
	// clamp the start to 0, end to the length
	if (start < 0) {
		start = 0;
	}
	if (end > (YInt)a->len) {
		end = (YInt)a->len;
	}

	YArray *b;
	YBool  isref = (a->mem.kind & YM_REF_VALUE) ? Y_TRUE : Y_FALSE;

	// result will be empty?
	// [ this handles all weird cases, e.g. start >= length ]
	if (start >= end) {
		b = _gc_alloc_array(0, 0, isref);
	} else {
		uint32_t i;
		uint32_t new_len = (uint32_t)(end - start);

		b = _gc_alloc_array(new_len, new_len, isref);

		// TODO use a memcpy
		for (i = 0 ; i < new_len ; i++) {
			b->elems[i] = a->elems[start + i];
		}
	}

	return b;
}
