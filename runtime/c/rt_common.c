// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

/* RunTime : common funcs and vars */

#include "rt_common.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>    // nan() and INFINITY
#include <string.h>  // memcpy


// our argument list (passed from the generated main)
static int     _argc;
static char ** _argv;

// our environment strings (will be created by libc, not us)
extern char **environ;

// current position in args and envs
static int     arg_pos;
static char ** env_pos;


YFloat Y_PosInf;  // +INF
YFloat Y_NegInf;  // -INF
YFloat Y_NaN;     //  NAN


void _rt_init(int argc, char *argv[]) {
	_argc = argc;
	_argv = argv;

	arg_pos = 0;
	env_pos = environ;

	Y_PosInf =  INFINITY;
	Y_NegInf = -Y_PosInf;
	Y_NaN    =  nan("");
}

void _rt_cleanup(void) {
	// nothing needed yet
}

void _rt_fatal_error(const char *msg) {
	fprintf(stderr, "runtime error: %s\n", msg);
	_rt_cleanup();
	exit(2);
}

YUnion * _fu_rt__read__next__arg(const YFunction *_self) {
	if (_argv == NULL || arg_pos >= _argc) {
		// create a {no str}
		return _gc_alloc_union(YOPT_NONE, 0);
	}

	const char *arg = _argv[arg_pos++];

	YString *y_str = _rt_from_cstr(arg);

	// create {opt XX} wrapper
	YUnion * un = _gc_alloc_union(YOPT_VAL, 1);
	un->datum.p = (YMemory *) y_str;
	return un;
}

YUnion * _fu_rt__read__next__env(const YFunction *_self) {
	if (env_pos == NULL || *env_pos == NULL) {
		// create a {no str}
		return _gc_alloc_union(YOPT_NONE, 0);
	}

	const char *env = *env_pos++;

	YString *y_str = _rt_from_cstr(env);

	// create {opt XX} wrapper
	YUnion * un = _gc_alloc_union(YOPT_VAL, 1);
	un->datum.p = (YMemory *) y_str;
	return un;
}
