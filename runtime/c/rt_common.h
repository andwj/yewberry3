// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

/* RunTime : common definitions */

#include <stddef.h>
#include <stdint.h>


/* ---- memory management ------- */

typedef enum
{
	YM_INVALID = 0,

	YM_String,
	YM_Union,
	YM_Array,
	YM_ByteVec,
	YM_ClassObj,
	YM_Function,
	YM_ExternalVar,

	// used to mask off the top bits.
	YM_MASK = 0x3F,

	// indicates at least one value is a reference.
	// always set for YM_Function.
	// never set for YM_String, YM_ByteVec.
	YM_REF_VALUE = 0x40,

} YMemoryKind;

typedef enum
{
	YC_BLACK   = 0,  // must be zero (for new allocs)
	YC_GRAY    = 1,
	YC_WHITE   = 2,
	YC_ETERNAL = 3,  // can never be freed

} YMemoryColor;

typedef struct YMemory_s
{
	// link in global list
	struct YMemory_s * next;

	// what kind of data is held here : YM_XXX
	uint8_t  kind;

	// color of this node : YC_XXX
	uint8_t  color;

	// used by some types of data (e.g. as a union tag)
	uint16_t  tag;

	// used by some types of data (e.g. the string length)
	uint32_t  total;

	// NOTE: type-specific data follows this structure....

} YMemory;


/* ---- types -------- */

typedef  int64_t YInt;
typedef double   YFloat;

typedef uint16_t YEnum;
typedef YEnum    YBool;

typedef uint32_t YChar;

typedef enum {
	YCMP_LESS    = 0,
	YCMP_EQUAL   = 1,
	YCMP_GREATER = 2,
} YCompare;

typedef void YRawFunction();

// YValue represents *any* yewberry value.
// Simple values are in the `i` or `f` fields, everything else
// is a pointer in the `p` field.
typedef union
{
	int64_t   i;
	double    f;
	YMemory * p;
} YValue;

typedef YCompare YCompareFunc(YValue, YValue);

typedef struct
{
	YMemory  mem;
	YChar    chars[1];  // real size in mem.total
} YString;

typedef struct
{
	const char * class_name;
	const char * ref_fields;
	// FIXME method lookup table for interfaces
} YClassDescriptor;

// YClassObj can contain multiple fields, but always at least one.
typedef struct
{
	YMemory  mem;
	const YClassDescriptor * desc;
	YValue   fields[1];
} YClassObj;

// this is used for external variables too.
typedef struct
{
	YMemory  mem;  // has tag value
	YValue   datum;
} YUnion;

typedef YUnion  YExternalVar;

typedef struct
{
	YMemory   mem;  // total = the capacity
	uint32_t  len;  // the current length
	YValue  * elems;
} YArray;

typedef struct
{
	YMemory    mem;  // total = the capacity
	uint32_t   len;  // the current length
	uint8_t  * bytes;
} YByteVec;

typedef struct
{
	YMemory   mem;
	YRawFunction * code;
	const char   * name;
	YExternalVar * upvars[1];  // total = actual length
} YFunction;


/* ---- allocation functions -------- */

typedef struct GC_Locals_s
{
	struct GC_Locals_s * next;
	size_t count;

	// following this structure is: YMemory * vars[count]
} GC_Locals;

void * _gc_raw_alloc(size_t size);
void   _gc_raw_free(const void * p);

void _gc_push_locals(GC_Locals * block, size_t count);
void _gc_pop_locals(void);

void      * _gc_alloc_ymemory(size_t size, YMemoryKind kind);
YString   * _gc_alloc_string(uint32_t len, YChar ** chars);
YUnion    * _gc_alloc_union(YEnum tag, YBool isref);
YClassObj * _gc_alloc_class(uint32_t len, const YClassDescriptor * desc);
YArray    * _gc_alloc_array(uint32_t len, uint32_t cap, YBool isref);
YFunction * _gc_alloc_func(const char *, YRawFunction *, uint32_t num_upvar);

void _fu_gc__enable(const YFunction *, YBool v);
void _fu_gc__step(const YFunction *);
void _fu_gc__collect__all(const YFunction *);
YInt _fu_gc__used__memory(const YFunction *);


/* ---- common functions -------- */

extern YFloat Y_PosInf;  // +INF
extern YFloat Y_NegInf;  // -INF
extern YFloat Y_NaN;     //  NAN

void _rt_init(int argc, char *argv[]);
void _rt_cleanup(void);

YUnion * _fu_rt__read__next__arg(const YFunction *);
YUnion * _fu_rt__read__next__env(const YFunction *);

void _rt_fatal_error(const char *msg);


/* ---- rt_os.c -------- */

void _fu_exit__program(const YFunction *, YInt code);
void _fu_abort__program(const YFunction *, const YString *ys);

void _fu_print(const YFunction *, const YString *ys);
void _fu_print__err(const YFunction *, const YString *ys);

YUnion * _fu_input__line(const YFunction *, const YString *prompt);


/* ---- rt_int.c -------- */

#define Y_INAN  ((YInt) (-9223372036854775807LL-1))

YInt _rt_int_mod(YInt a, YInt b);
YInt _rt_int_pow(YInt a, YInt b);
YInt _rt_int_abs(YInt a);

YCompare _rt_int_cmp(YInt, YInt);
YInt     _rt_int_hash(YInt);

YString * _rt_int_to_str(YInt);

// TODO parse-int


/* ---- rt_bool.c -------- */

#define Y_FALSE  (YBool)0
#define Y_TRUE   (YBool)1

#define YOPT_NONE  (YEnum)0
#define YOPT_VAL   (YEnum)1

#define YRES_ERR   (YEnum)0
#define YRES_OK    (YEnum)1


/* ---- rt_float.c -------- */

YFloat _rt_float_rem(YFloat a, YFloat b);
YFloat _rt_float_mod(YFloat a, YFloat b);

YFloat _rt_float_pow(YFloat a, YFloat b);
YFloat _rt_float_abs(YFloat a);
YFloat _rt_float_sqrt(YFloat a);

YCompare _rt_float_cmp(YFloat, YFloat);
YInt     _rt_float_hash(YFloat);

YFloat _rt_float_from_int(YInt i);
YInt   _rt_float_to_int(YFloat f);
YString * _rt_float_to_str(YFloat);

// TODO parse-float

YBool  _rt_float_nan_3F(YFloat x);
YBool  _rt_float_inf_3F(YFloat x);

YFloat _rt_float_floor(YFloat x);
YFloat _rt_float_ceil(YFloat x);
YFloat _rt_float_round(YFloat x);
YFloat _rt_float_trunc(YFloat x);

YFloat _rt_float_log(YFloat x);
YFloat _rt_float_log10(YFloat x);
YFloat _rt_float_sin(YFloat x);
YFloat _rt_float_cos(YFloat x);
YFloat _rt_float_tan(YFloat x);
YFloat _rt_float_asin(YFloat x);
YFloat _rt_float_acos(YFloat x);
YFloat _rt_float_atan(YFloat x);

YFloat _fu_hypot(YFunction *_self, YFloat x, YFloat y);
YFloat _fu_atan2(YFunction *_self, YFloat y, YFloat x);


/* ---- rt_char.c -------- */

YChar _rt_char_lower(YChar ch);
YChar _rt_char_upper(YChar ch);

YString * _rt_string_lower(const YString *);
YString * _rt_string_upper(const YString *);

YBool _rt_char_is_letter(YChar ch);
YBool _rt_char_is_digit(YChar ch);
YBool _rt_char_is_symbol(YChar ch);
YBool _rt_char_is_mark(YChar ch);
YBool _rt_char_is_space(YChar ch);
YBool _rt_char_is_control(YChar ch);


/* ---- rt_string.c -------- */

YString * _rt_from_cstr(const char *);
YString * _rt_char_to_str(YChar);
YString * _rt_string_add(const YString *, const YString *);

size_t _rt_utf8_length(const char *);

YCompare _rt_string_cmp(const YString *, const YString *);
YInt     _rt_string_hash(const YString *);

YString * _rt_string_subseq(const YString *, YInt, YInt);


/* ---- rt_array.c -------- */

void _rt_array_clear(YArray * arr);
void _rt_array_append(YArray *, YValue);
void _rt_array_insert(YArray *, YInt, YValue);
void _rt_array_delete(YArray *, YInt, YInt);

YArray * _rt_array_subseq(const YArray *, YInt, YInt);


/* ---- rt_random.c -------- */

void   _fu_rand__seed(const YFunction *yf, YInt seed);
YInt   _fu_rand__int(const YFunction *yf, YInt low, YInt high);
YFloat _fu_rand__float(const YFunction *yf);
