// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

/* RunTime : floating point operations */

#include "rt_common.h"

#include <math.h>

// `%` operator
YFloat _rt_float_rem(YFloat a, YFloat b) {
	// this implements a remainder: if N is the numerator and
	// D is the divisor, then compute (N - (N / D) * D) where
	// (N / D) produces an integer by truncating the result.
	//
	// examples:  5.0 %  3.0 =  2.0
	//           -5.0 %  3.0 = -2.0
	//            5.0 % -3.0 =  2.0
	//           -5.0 % -3.0 = -2.0
	//
	// Note: languages vary wildly about how they implement the
	//       floating-point remainder.  The Java and Javascript
	//       '%' operator is the same as here.  The C#, Python and
	//       Lua '%' operators use a floor()ed division.  Scheme R7
	//       has "truncate-remainder" which acts the same as here.

	return fmod(a, b);
}

// `.mod` method
YFloat _rt_float_mod(YFloat a, YFloat b) {
	// this implements a modulo which uses the *floor* of the
	// division i.e. rounded down toward -infinity.
	//
	// examples:  5.0 mod  3.0 =  2.0
	//           -5.0 mod  3.0 =  1.0
	//            5.0 mod -3.0 = -1.0
	//           -5.0 mod -3.0 = -2.0
	//
	// Note: some languages use a different way to compute a
	//       floating point modulo.  The C "fmod" function and
	//       Go's math.Mod() both use truncated division, and
	//       a few other languages follow suit.

	if (b == 0.0) {
		return nan("");
	}

	return (a - floor(a / b) * b);
}

// `**` operator
YFloat _rt_float_pow(YFloat a, YFloat b) {
	return pow(a, b);
}

// the `.abs` method
YFloat _rt_float_abs(YFloat a) {
	return fabs(a);
}

// the `.sqrt` method
YFloat _rt_float_sqrt(YFloat a) {
	return sqrt(a);
}

// the `.cmp` method
YCompare _rt_float_cmp(YFloat a, YFloat b) {
	if (a < b) return YCMP_LESS;
	if (a > b) return YCMP_GREATER;
	return            YCMP_EQUAL;
}

// the `.hash` method
YInt _rt_float_hash(YFloat a) {
	YValue val = { .f = a };

	uint64_t x = (uint64_t) (val . i);

	x = (x ^ (x >> 30)) * 0xbf58476d1ce4e5b9ULL;
	x = (x ^ (x >> 27)) * 0x94d049bb133111ebULL;
	x = (x ^ (x >> 31));

	// only produce values >= 0
	return (YInt)(x & 0x7fffffffffffffffULL);
}

//----------------------------------------------------------------------

YFloat _rt_float_from_int(YInt i) {
	if (i == Y_INAN) {
		return nan("");
	}

	// convert integer to floating point
	return (YFloat) i;
}

YInt _rt_float_to_int(YFloat f) {
	if (isnan(f) || isinf(f) ||
		f >  9.223372036854775e+18 ||
		f < -9.223372036854775e+18) {

		return Y_INAN;
	}

	// this is truncation toward zero
	return (YInt) f;
}

YString * _rt_float_to_str(YFloat num) {
	char buffer[256];

	snprintf(buffer, sizeof(buffer), "%1.7f", num);
	buffer[sizeof(buffer)-1] = 0;

	return _rt_from_cstr(buffer);
}

// TODO parse-float

//----------------------------------------------------------------------

YBool _rt_float_nan_3F(YFloat x) {
	return (fpclassify(x) == FP_NAN);
}

YBool _rt_float_inf_3F(YFloat x) {
	return (fpclassify(x) == FP_INFINITE);
}

YFloat _rt_float_floor(YFloat x) {
	return floor(x);
}

YFloat _rt_float_ceil(YFloat x) {
	return ceil(x);
}

YFloat _rt_float_round(YFloat x) {
	// we round halfway cases *away* from zero.
	return round(x);
}

YFloat _rt_float_trunc(YFloat x) {
	return trunc(x);
}

YFloat _rt_float_log(YFloat x) {
	return log(x);
}

YFloat _rt_float_log10(YFloat x) {
	return log10(x);
}

YFloat _rt_float_sin(YFloat x) {
	return sin(x);
}

YFloat _rt_float_cos(YFloat x) {
	return cos(x);
}

YFloat _rt_float_tan(YFloat x) {
	return tan(x);
}

YFloat _rt_float_asin(YFloat x) {
	return asin(x);
}

YFloat _rt_float_acos(YFloat x) {
	return acos(x);
}

YFloat _rt_float_atan(YFloat x) {
	return atan(x);
}

YFloat _fu_atan2(YFunction *_self, YFloat y, YFloat x) {
	return atan2(y, x);
}

YFloat _fu_hypot(YFunction *_self, YFloat x, YFloat y) {
	return hypot(x, y);
}
