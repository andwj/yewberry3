// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

/* RunTime : memory management, garbage collection */

#include "rt_common.h"

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>  // memset


// every YMemory structure that is allocated goes here.
static YMemory * gc_used_list;
static size_t    gc_used_count;
static size_t    gc_used_quantity;

// after a cycle is finished, this list gets all the garbage.
static YMemory * gc_free_list;
static size_t    gc_free_count;

// each stack frame with GC allocated locals is here.
// the very first entry (bottom of stack) are the global vars.
static GC_Locals * gc_root_set;

// state of the GC
static _Bool gc_is_enabled   = false;
static _Bool gc_cycle_active = false;

static size_t gc_threshhold = 32 * 1024;


// the GRAY SET, a stack of nodes
#define GRAY_CHUNK_LEN  120

typedef struct GC_GrayChunk_s
{
	struct GC_GrayChunk_s * newer;
	struct GC_GrayChunk_s * older;

	size_t    len;
	YMemory * nodes[GRAY_CHUNK_LEN];

} GC_GrayChunk;

// the chunk containing the top of the stack.
// [ note there may be empty chunks AFTER that one ]
static GC_GrayChunk * gc_gray_set;
static int            gc_gray_count;
static int            gc_gray_num_chunks;


//----------------------------------------------------------------------

void * _gc_raw_alloc(size_t size) {
	if (size == 0) {
		return NULL;
	}
	void *mem = malloc(size);
	if (mem == NULL) {
		_rt_fatal_error("out of memory");
	}
	return mem;
}

void _gc_raw_free(const void * p) {
	if (p != NULL) {
		free((void *) p);
	}
}

void _gc_push_locals(GC_Locals * block, size_t count) {
	// clear all the local variables to zero.
	if (count > 0) {
		YMemory ** var_p = (YMemory **) block;
		var_p += 2;  // skip the `GC_Locals` field
		memset(var_p, 0, count * sizeof(YMemory *));
	}

	block->count = count;
	block->next  = gc_root_set;

	gc_root_set = block;
}

void _gc_pop_locals(void) {
	gc_root_set = gc_root_set->next;
}

//----------------------------------------------------------------------

static void gc_work(void);

void * _gc_alloc_ymemory(size_t size, YMemoryKind kind) {
	if (gc_is_enabled)
		gc_work();

	YMemory * mem = _gc_raw_alloc(size);
	memset(mem, 0, sizeof(YMemory));
	mem->kind = kind;

	// link into the list of all nodes
	mem->next = gc_used_list;
	gc_used_list = mem;
	gc_used_count += 1;
	gc_used_quantity += size;

	return (void *) mem;
}

YFunction * _gc_alloc_func(const char *name, YRawFunction *raw_code, uint32_t num_upvar) {
	size_t size = sizeof(YFunction);
	if (num_upvar >= 2) {
		size += (num_upvar - 1) * sizeof(YExternalVar *);
	}

	YFunction * fu = _gc_alloc_ymemory(size, YM_Function | YM_REF_VALUE);
	fu->mem.total = num_upvar;
	fu->code = raw_code;
	fu->name = name;

	if (num_upvar > 0) {
		memset(&fu->upvars[0], 0, num_upvar * sizeof(YExternalVar *));
	}
	return fu;
}

YString * _gc_alloc_string(uint32_t len, YChar ** chars) {
	// the characters follow the YMemory header

	size_t size = sizeof(YString);
	if (len >= 2) {
		size += (len - 1) * sizeof(YChar);
	}

	YString * str = _gc_alloc_ymemory(size, YM_String);
	str->mem.total = len;

	(*chars) = (YChar *) &str->chars[0];
	return str;
}

YUnion * _gc_alloc_union(YEnum tag, YBool isref) {
	YMemoryKind kind = (tag & 0x8000) ? YM_ExternalVar : YM_Union;
	if (isref)
		kind |= YM_REF_VALUE;

	YUnion * un = _gc_alloc_ymemory(sizeof(YUnion), kind);

	un->mem.tag = tag;
	un->datum.p = 0;

	return un;
}

YClassObj * _gc_alloc_class(uint32_t len, const YClassDescriptor * desc) {
	uint32_t i;
	YMemoryKind kind = YM_ClassObj;
	for (i = 0 ; i < len ; i++) {
		if (desc->ref_fields[i] != 0) {
			kind |= YM_REF_VALUE;
			break;
		}
	}

	size_t size = sizeof(YClassObj);
	if (len >= 2) {
		size += (len - 1) * sizeof(YValue);
	}

	YClassObj * obj = _gc_alloc_ymemory(size, kind);
	obj->mem.total = len;
	obj->desc = desc;

	memset(&obj->fields[0], 0, len * sizeof(YValue));

	return obj;
}

YArray * _gc_alloc_array(uint32_t len, uint32_t cap, YBool isref) {
	YMemoryKind kind = YM_Array;
	if (isref)
		kind |= YM_REF_VALUE;

	YArray * arr = _gc_alloc_ymemory(sizeof(YArray), kind);

	arr->mem.total = cap;
	arr->len = len;

	size_t elem_size = cap * sizeof(YValue);

	arr->elems = _gc_raw_alloc(elem_size);

	// zero out the elements to ensure GC does not visit a
	// bogus pointer.
	if (elem_size != 0) {
		memset(&arr->elems[0], 0, elem_size);
	}
	return arr;
}

//----------------------------------------------------------------------

static GC_GrayChunk * gc_new_chunk(void) {
	GC_GrayChunk * k = (GC_GrayChunk *) _gc_raw_alloc(sizeof(GC_GrayChunk));
	k->older = k->newer = NULL;
	k->len = 0;
	gc_gray_num_chunks += 1;
	return k;
}

void _gc_mark_as_gray(YMemory * mem) {
	// ASSERT(mem->color == YC_WHITE)

	// if node has no references, go straight to BLACK
	if ((mem->kind & YM_REF_VALUE) == 0) {
		mem->color = YC_BLACK;
		return;
	}

	mem->color = YC_GRAY;

	if (gc_gray_set == NULL) {
		gc_gray_set = gc_new_chunk();
	}

	// current chunk is full?
	if (gc_gray_set->len >= GRAY_CHUNK_LEN) {
		if (gc_gray_set->newer == NULL) {
			GC_GrayChunk * k = gc_new_chunk();
			k->older = gc_gray_set;
			gc_gray_set->newer = k;
		}
		gc_gray_set = gc_gray_set->newer;
	}

	gc_gray_set->nodes[gc_gray_set->len] = mem;
	gc_gray_set->len += 1;

	gc_gray_count += 1;
}

static YMemory * gc_pop_gray(void) {
	if (gc_gray_set && gc_gray_set->len == 0) {
		gc_gray_set = gc_gray_set->older;
	}

	// this should never ever happen
	if (gc_gray_set == NULL) {
		_rt_fatal_error("gc gray set underflow");
	}

	gc_gray_set->len -= 1;
	YMemory *mem = gc_gray_set->nodes[gc_gray_set->len];

	mem->color = YC_BLACK;
	gc_gray_count -= 1;

	return mem;
}

static size_t gc_estimate_size(const YMemory * mem) {
	size_t base = sizeof(YMemory);
	size_t elem = sizeof(YValue);

	switch ((YMemoryKind) mem->kind & YM_MASK) {
	case YM_Union:
	case YM_ExternalVar:
		return base + elem;

	case YM_ClassObj:
	case YM_Array:
		return base + (mem->total + 1) * elem;

	case YM_String:
		return base + mem->total * sizeof(YChar);

	case YM_ByteVec:
		return base + mem->total;

	case YM_Function:
		return base + (mem->total + 3) * sizeof(YExternalVar *);

	default:
		return base * 2;
	}
}

static void gc_partition(void) {
	// this scans all nodes and moves WHITE ones into the free_list
	// and keeps BLACK ones in the main used_list.

	YMemory * cur  = gc_used_list;
	YMemory * prev = NULL;
	YMemory * next;

	// reset the used memory statistic, we recompute it
	gc_used_count    = 0;
	gc_used_quantity = 0;

	while (cur != NULL) {
		next = cur->next;

		if (cur->color == YC_WHITE) {
			// unlink from used list
			if (prev != NULL) {
				prev->next = next;
			} else {
				gc_used_list = next;
			}

			// link into free list
			cur->next = gc_free_list;
			gc_free_list = cur;
			gc_free_count += 1;

		} else {
			gc_used_count += 1;
			gc_used_quantity += gc_estimate_size(cur);

			// mark as WHITE for next cycle
			if (cur->color != YC_ETERNAL) {
				cur->color = YC_WHITE;
			}

			prev = cur;
		}

		cur = next;
	}
}

//----------------------------------------------------------------------

static inline void gc_free_array(YArray * arr) {
	_gc_raw_free(arr->elems);
}

static inline void gc_free_bytevec(YByteVec * bv) {
	_gc_raw_free(bv->bytes);
}

static void gc_free_a_node(YMemory * mem) {
	switch ((YMemoryKind) mem->kind & YM_MASK) {
	case YM_Array:
		gc_free_array((YArray *) mem);
		break;

	case YM_ByteVec:
		gc_free_bytevec((YByteVec *) mem);
		break;

	default:
		// nothing additional needed
		// [ e.g. for YM_String the chars are in the same block ]
		break;
	}

	// a debugging aid [ disable this eventually ]
	memset(mem, 0xDD, sizeof(YMemory));

	_gc_raw_free(mem);
}

static void gc_free_work(void) {
	size_t count = 4;

	while (gc_free_list != NULL && count > 0) {
		// unlink the head
		YMemory * mem = gc_free_list;
		gc_free_list = mem->next;
		gc_free_count -= 1;

		gc_free_a_node(mem);

		count--;
	}
}

//----------------------------------------------------------------------

static inline void gc_scan_class(YClassObj * obj) {
	const uint8_t * info = obj->desc->ref_fields;
	uint32_t fields = obj->mem.total;
	uint32_t i;

	for (i = 0 ; i < fields ; i++) {
		if (info[i]) {
			YMemory * other = obj->fields[i].p;

			// fields can be NULL while the object is constructed
			if (other != NULL && other->color == YC_WHITE) {
				_gc_mark_as_gray(other);
			}
		}
	}
}

static inline void gc_scan_union(YUnion * un) {
	YMemory * other = un->datum.p;

	if (other != NULL && other->color == YC_WHITE) {
		_gc_mark_as_gray(other);
	}
}

static inline void gc_scan_array(YArray * arr) {
	YValue * val_p = &arr->elems[0];
	uint32_t count = arr->len;

	for ( ; count > 0 ; count--, val_p++) {
		YMemory * other = val_p->p;

		if (other != NULL && other->color == YC_WHITE) {
			_gc_mark_as_gray(other);
		}
	}
}

static inline void gc_scan_function(YFunction * fu) {
	uint32_t i;
	uint32_t count = fu->mem.total;

	for (i = 0 ; i < count ; i++) {
		YMemory * other = (YMemory *) fu->upvars[i];

		if (other != NULL && other->color == YC_WHITE) {
			_gc_mark_as_gray(other);
		}
	}
}

static void gc_scan_a_node(YMemory * mem) {
	switch ((YMemoryKind) mem->kind & YM_MASK) {
	case YM_ClassObj:
		gc_scan_class((YClassObj *) mem);
		break;

	case YM_Union:
	case YM_ExternalVar:
		gc_scan_union((YUnion *) mem);
		break;

	case YM_Array:
		gc_scan_array((YArray *) mem);
		break;

	case YM_Function:
		gc_scan_function((YFunction *) mem);
		break;

	default:
		// nothing needed for YM_String, YM_ByteVec
		break;
	}
}

static void gc_begin_cycle(_Bool whiten) {
	gc_cycle_active = true;

	if (whiten) {
		YMemory * mem;

		for (mem = gc_used_list ; mem != NULL ; mem = mem->next) {
			if (mem->color != YC_ETERNAL) {
				mem->color = YC_WHITE;
			}
		}
	}

	// mark the root set....

	GC_Locals * block;

	for (block = gc_root_set ; block != NULL ; block = block->next) {
		size_t count = block->count;
		YMemory ** var_p = (YMemory **) block;
		var_p += 2;  // skip the `next` and `count` in GC_Locals

		for ( ; count > 0 ; count--) {
			YMemory * mem = *var_p++;

			// variables can be NULL before they are initialized
			if (mem != NULL && mem->color == YC_WHITE) {
				_gc_mark_as_gray(mem);
			}
		}
	}
}

static void gc_finish_cycle(void) {
	gc_cycle_active = false;
	gc_partition();
}

static void gc_work(void) {
	gc_free_work();

	if (! gc_cycle_active) {
		// should we begin a new cycle?
		if (gc_used_quantity < gc_threshhold) {
			return;
		}

		// we assume the used_list have been marked WHITE from a previous
		// gc_partition() call.  On the very first cycle this won't be
		// true (everything will be BLACK), but that is OK.

		gc_begin_cycle(false);
		return;
	}

	// has a cycle finished?
	if (gc_gray_count == 0) {
		gc_finish_cycle();
		return;
	}

	// the more objects there are, the more scanning work we do.
	// this logic is based on the square root of # objects.
	size_t count;

	     if (gc_used_count > 1000000000) count = 768;
	else if (gc_used_count >  100000000) count = 256;
	else if (gc_used_count >   10000000) count = 72;
	else if (gc_used_count >    1000000) count = 24;
	else if (gc_used_count >      60000) count = 12;
	else if (gc_used_count >       4000) count = 6;
	else                                 count = 3;

	if (count > gc_gray_count) {
		count = gc_gray_count;
	}

	for ( ; count > 0 ; count--) {
		gc_scan_a_node(gc_pop_gray());
	}
}

//----------------------------------------------------------------------

// the `gc-enable` builtin
void _fu_gc__enable(const YFunction * yf, YBool v) {
	gc_is_enabled = v ? true : false;
}

// the `gc-step` builtin
void _fu_gc__step(const YFunction * yf) {
	gc_work();
}

// the `gc-collect-all` builtin
void _fu_gc__collect__all(const YFunction * yf) {
	gc_begin_cycle(true);

	while (gc_cycle_active) {
		gc_work();
	}
	while (gc_free_list != NULL) {
		gc_free_work();
	}
}

// the `gc-used-memory` builtin
YInt _fu_gc__used__memory(const YFunction * yf) {
	return (YInt) gc_used_quantity;
}
