// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

/* RunTime : integer operations */

#include "rt_common.h"

// the `.mod` method (no corresponding operator).
//
// this implements a modulo which uses the *floor* of the
// integer division i.e. as if the division was done with
// infinite precision, then rounded down toward -infinity.
YInt _rt_int_mod(YInt a, YInt b) {
	if (b == 0) {
		return Y_INAN;
	} else if (a >= 0 && b > 0) {
		return (a % b);
	} else if (a < 0 && b > 0) {
		return ((a % b) + b) % b;
	} else if (a >= 0 && b < 0) {
		YInt div = (a-b-1) / (-b);
		return a + div * b;
	} else {
		YInt div = (-a) / (-b);
		return a - div * b;
	}
}

// the `**` operator
YInt _rt_int_pow(YInt a, YInt b) {
	// handle some special cases...
	if (b < 0)
		return Y_INAN;

	if (b == 0)
		return 1;

	if (b == 1 || a == 0 || a == 1)
		return a;

	if (a == -1) {
		if ((b & 1) == 0) {
			return 1;
		} else {
			return -1;
		}
	}

	// here we have abs(a) >= 2 && b >= 2.
	// hence any b >= 64 will definitely overflow an int64_t.

	if (b >= 64)
		return Y_INAN;

	YInt res = a;
	YInt i;

	for (i = b ; i > 1 ; i--) {
		YInt newval = res * a;

		// check for overflow
		if ((newval / a) != res)
			return Y_INAN;

		res = newval;
	}

	return res;
}

// the `.abs` method
YInt _rt_int_abs(YInt a) {
	return a < 0 ? -a : a;
}

// the `.cmp` method
YCompare _rt_int_cmp(YInt a, YInt b) {
	if (a < b) return YCMP_LESS;
	if (a > b) return YCMP_GREATER;
	return            YCMP_EQUAL;
}

// the `.hash` method
YInt _rt_int_hash(YInt a) {
	uint64_t x = (uint64_t) a;

	x = (x ^ (x >> 30)) * 0xbf58476d1ce4e5b9ULL;
	x = (x ^ (x >> 27)) * 0x94d049bb133111ebULL;
	x = (x ^ (x >> 31));

	// only produce values >= 0
	return (YInt)(x & 0x7fffffffffffffffULL);
}

// the `.str` method
YString * _rt_int_to_str(YInt num) {
	char buffer[256];

	snprintf(buffer, sizeof(buffer), "%lld", num);
	buffer[sizeof(buffer)-1] = 0;

	return _rt_from_cstr(buffer);
}
