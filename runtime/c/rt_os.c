// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

/* RunTime : os functions */

#include "rt_common.h"

#include <stdlib.h>
#include <stdio.h>

static void put_unicode_char(YChar ch, FILE *f) {
	if (ch <= 0x7f) {
		fputc((int) ch, f);

	} else if (ch <= 0x7ff) {
		fputc((int) (0xC0 | (ch >> 6)), f);
		fputc((int) (0x80 | (ch & 63)), f);

	} else if (ch <= 0xffff) {
		fputc((int) (0xE0 | (ch >> 12)), f);
		fputc((int) (0x80 | (ch >> 6 & 63)), f);
		fputc((int) (0x80 | (ch & 63)), f);

	} else if (ch <= 0x10ffff) {
		fputc((int) (0xF0 | (ch >> 18)), f);
		fputc((int) (0x80 | (ch >> 12 & 63)), f);
		fputc((int) (0x80 | (ch >> 6  & 63)), f);
		fputc((int) (0x80 | (ch & 63)), f);

	} else {
		// invalid unicode char
		fputc('?', f);
	}
}

void _fu_exit__program(const YFunction *_self, YInt code) {
	_rt_cleanup();
	exit((int) code);
}

void _fu_abort__program(const YFunction *_self, const YString *ys) {
	uint32_t i;
	for (i = 0 ; i < ys->mem.total ; i++) {
		put_unicode_char(ys->chars[i], stderr);
	}
	fputc('\n', stderr);
	_rt_cleanup();
	exit(1);
}

void _fu_print(const YFunction *_self, const YString *ys) {
	uint32_t i;
	for (i = 0 ; i < ys->mem.total ; i++) {
		put_unicode_char(ys->chars[i], stdout);
	}
	putchar('\n');
	fflush(stdout);
}

void _fu_print__err(const YFunction *_self, const YString *ys) {
	uint32_t i;
	for (i = 0 ; i < ys->mem.total ; i++) {
		put_unicode_char(ys->chars[i], stderr);
	}
	fputc('\n', stderr);
	fflush(stderr);
}

YUnion * _fu_input__line(const YFunction *_self, const YString *prompt) {
	static char buffer[10000];

	// display the prompt
	uint32_t i;
	for (i = 0 ; i < prompt->mem.total ; i++) {
		put_unicode_char(prompt->chars[i], stdout);
	}
	fflush(stdout);

	char *s = fgets(buffer, sizeof(buffer), stdin);

	if (s == NULL) {
		// we could probably distinguish between a true EOF and some
		// other error by checking feof(stdin), however there is not
		// much we could do in the non-EOF case....

		// create a {no str}
		return _gc_alloc_union(YOPT_NONE, 0);
	}

	// remove any trailing LF char (or CR LF pair).
	// we assume CR or LF will only occur at the end.

	for (i = 0 ; buffer[i] != 0 ; i++) {
		if (buffer[i] == '\n' || buffer[i] == '\r') {
			buffer[i] = 0;
			break;
		}
	}

	// create {opt XX} wrapper
	YUnion * un = _gc_alloc_union(YOPT_VAL, 1);
	un->datum.p = (YMemory *) _rt_from_cstr(buffer);
}
