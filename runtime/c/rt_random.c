// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

/* RunTime : random numbers */

#include "rt_common.h"
#include <limits.h>

// This random number generator is 'xorshift*' by George Marsaglia.
// The multiplication makes it slower than other generators, but I
// prefer the small amount of state and simpler code.  Like other
// xorshift algorithms, the initial state must never be fully zero.

static uint64_t random_state = 1;

static uint64_t xorshift64s(void) {
	uint64_t x = random_state;
	x ^= x >> 12;
	x ^= x << 25;
	x ^= x >> 27;
	random_state = x;
	return x * UINT64_C(0x2545F4914F6CDD1D);
}

void _fu_rand__seed(const YFunction *yf, YInt seed) {
	random_state = (uint64_t) seed;

	if (random_state == 0) {
		random_state = 1;
	}

	// warm up the generator (discard some initial values)
	int loop;
	for (loop = 0 ; loop < 500 ; loop++) {
		xorshift64s();
	}
}

YInt _fu_rand__int(const YFunction *yf, YInt low, YInt high) {
	/* result will satisfy: low <= x <= high */

	if (high <= low) {
		return low;
	}

	int64_t approx_diff = (high >> 2) - (low >> 2) + 4;

	if (approx_diff > (INT64_MAX >> 2)) {
		// the real difference could overflow an int64_t,
		// so use an alternate method to generate a value.
		// this logic is crude, but it is 100% correct.
		for (;;) {
			YInt r = (YInt) xorshift64s();

			if (low <= r && r <= high) {
				return r;
			}
		}
	}

	uint64_t mod = (uint64_t) (high - low + 1);

	uint64_t r = xorshift64s();

	if ((mod & (mod - 1)) == 0) {
		// powers of two are easy, just apply a mask
		r = r & (mod - 1);

	} else {
		// there is a threshold value 't' near the top of the range
		// where random values >= t would produce skewed results
		// (some results occurring slightly more often).
		// so we try again when the raw random value is >= t.

		// this is equivalent to (UINT64_MAX / mod) * mod
		uint64_t t = UINT64_MAX - (UINT64_MAX % (uint64_t)mod);

		while (r >= t) {
			r = xorshift64s();
		}

		r = r % mod;
	}

	return low + (YInt) r;
}

YFloat _fu_rand__float(const YFunction *yf) {
	/* result will satisfy: 0.0 <= x < 1.0 */

	for (;;) {
		uint64_t r = xorshift64s();

		// only need 53 bits for an IEEE 754 double
		YFloat f = ldexp((double)(r >> 11), -53);

		// this check is probably not needed, but I prefer being safe
		if (f < 1.0) {
			return f;
		}
	}
}
