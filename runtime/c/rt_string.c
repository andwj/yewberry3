// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

/* RunTime : string operations */

#include "rt_common.h"

#include <string.h>  // strlen


size_t _rt_utf8_length(const char *orig) {
	size_t len = strlen(orig);
	size_t count = 0;
	size_t i;

	for (i = 0 ; i < len ; i++) {
		YChar ch = (uint8_t) orig[i];

		if ((ch & 0xE0) == 0xC0 && i+1 < len) {
			i += 1;
		} else if ((ch & 0xF0) == 0xE0 && i+2 < len) {
			i += 2;
		} else if ((ch & 0xF8) == 0xF0 && i+3 < len) {
			i += 3;
		}

		count += 1;
	}

	return count;
}

YString * _rt_from_cstr(const char *orig) {
	uint32_t len = strlen(orig);
	uint32_t new_len = _rt_utf8_length(orig);
	uint32_t i, p;

	YChar * chars;
	YString * str = _gc_alloc_string(new_len, &chars);

	for (i = p = 0 ; i < len ; i++) {
		YChar ch = (uint8_t) orig[i];

		// two byte UTF-8 code?
		if ((ch & 0xE0) == 0xC0 && i+1 < len) {
			ch  = (ch & 0x1F) << 6;
			ch |= (YChar) ((uint8_t) orig[++i] & 0x3F);

		// three byte UTF-8 code?
		} else if ((ch & 0xF0) == 0xE0 && i+2 < len) {
			ch  = (ch & 0x1F) << 12;
			ch |= (YChar) ((uint8_t) orig[++i] & 0x3F) << 6;
			ch |= (YChar) ((uint8_t) orig[++i] & 0x3F);

		// four byte UTF-8 code?
		} else if ((ch & 0xF8) == 0xF0 && i+3 < len) {
			ch  = (ch & 0x1F) << 18;
			ch |= (YChar) ((uint8_t) orig[++i] & 0x3F) << 12;
			ch |= (YChar) ((uint8_t) orig[++i] & 0x3F) << 6;
			ch |= (YChar) ((uint8_t) orig[++i] & 0x3F);
		}

		chars[p++] = ch;
	}

	return str;
}

YString * _rt_char_to_str(YChar ch) {
	YChar * chars;
	YString * str = _gc_alloc_string(1, &chars);
	chars[0] = ch;
	return str;
}

YString * _rt_string_add(const YString *a, const YString *b) {
	uint32_t a_len = a->mem.total;
	uint32_t b_len = b->mem.total;

	if (b_len == 0) return (YString *) a;
	if (a_len == 0) return (YString *) b;

	uint32_t new_len = (a_len + b_len);
	if (new_len < a_len || new_len < b_len) {
		// too large for a 32-bit int
		_rt_fatal_error("string overflow");
	}

	YChar   * chars;
	YString * str = _gc_alloc_string(a_len + b_len, &chars);

	memcpy(&chars[0],     &a->chars[0], a_len * sizeof(YChar));
	memcpy(&chars[a_len], &b->chars[0], b_len * sizeof(YChar));

	return str;
}

YCompare _rt_string_cmp(const YString *a, const YString *b) {
	uint32_t a_len = a->mem.total;
	uint32_t b_len = b->mem.total;

	const YChar *a_ptr = &a->chars[0];
	const YChar *b_ptr = &b->chars[0];

	while (a_len > 0 && b_len > 0) {
		YChar ac = *a_ptr++;
		YChar bc = *b_ptr++;

		if (ac < bc) return YCMP_LESS;
		if (ac > bc) return YCMP_GREATER;

		a_len--;
		b_len--;
	}

	if (a_len < b_len) return YCMP_LESS;
	if (a_len > b_len) return YCMP_GREATER;

	return YCMP_EQUAL;
}

YString * _rt_string_subseq(const YString *a, YInt start, YInt end) {
	// clamp the start to 0, end to the length
	if (start < 0) {
		start = 0;
	}
	if (end > (YInt)a->mem.total) {
		end = (YInt)a->mem.total;
	}

	YChar *chars;
	YString *b;

	// result will be empty?
	// [ this handles all weird cases, e.g. start >= length ]
	if (start >= end) {
		b = _gc_alloc_string(0, &chars);
	} else {
		uint32_t i;
		uint32_t new_len = (uint32_t)(end - start);

		b = _gc_alloc_string(new_len, &chars);

		// TODO use a memcpy
		for (i = 0 ; i < new_len ; i++) {
			b->chars[i] = a->chars[start + i];
		}
	}

	return b;
}

YInt _rt_string_hash(const YString *a) {
	// this follows the FNV (Fowler/Noll/Vo) algorithm,
	// except this handles a 32-bit unicode char at each step.

	const YChar *pos = &a->chars[0];
	const YChar *end = &a->chars[a->mem.total];

	// the empty string hashes to zero
	if (end == pos) {
		return 0;
	}

	uint64_t x     = 14695981039346656037ULL;
	uint64_t prime = 1099511628211ULL;

	for (; pos < end ; pos++) {
		x = x ^ (uint64_t) *pos;
		x = x * prime;
	}

	// use xorshift64 a few times to mix the bits
	x ^= x >> 12; x ^= x << 25; x ^= x >> 27;
	x ^= x >> 12; x ^= x << 25; x ^= x >> 27;
	x ^= x >> 12; x ^= x << 25; x ^= x >> 27;

	// only produce values >= 0
	return (YInt)(x & 0x7fffffffffffffffULL);
}
